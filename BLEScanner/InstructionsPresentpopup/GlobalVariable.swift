//
//  GlobalVariable.swift
//  GoogleMapDemo
//
//  Created by vamsi on 4/6/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class GlobalVariable: NSObject {
    
    class func popupAlert() -> PopAlertView? {
        
        let popup = Bundle.main.loadNibNamed("PopupAlert", owner: self, options: nil)?[0] as? PopAlertView
        popup?.frame = UIScreen.main.applicationFrame
        popup?.popupAlert.layer.cornerRadius = 10.0
        popup?.popupAlert.clipsToBounds = true
        return popup

    }

}
