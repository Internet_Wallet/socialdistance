//
//  PopAlertView.swift
//  GoogleMapDemo
//
//  Created by vamsi on 4/6/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class PopAlertView: UIView {

    @IBOutlet var popupAlert: UIView!
   
    @IBOutlet var firstLabel: UILabel!{
        didSet {
            firstLabel.text = "Please keep app always open".localized
        }
    }
    @IBOutlet var secondLabel: UILabel!{
        didSet {
            secondLabel.text = "Please keep Bluetooth always on".localized
        }
    }
    @IBOutlet var thirdLabel: UILabel!{
        didSet {
            thirdLabel.text = "Please keep mobile sound in highest".localized
        }
    }
    @IBOutlet var fourthLabel: UILabel!{
        didSet {
            fourthLabel.text = "Please don't keep mobile in silence".localized
        }
    }
    @IBOutlet var fifthLabel: UILabel!{
        didSet {
            fifthLabel.text = "Please tell other people around you to download the app".localized
        }
    }
    @IBOutlet var okbutton: UIButton!{
        didSet {
            okbutton.setTitle("OK".localized, for: .normal)
        }
    }
    
    @IBAction func OkbuttonClick(_ sender: Any) {
        
        self.removeFromSuperview()
        
    }
    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        // NSLog(@"touches began");
        let cv: PopAlertView? = nil
        let touch = touches.first
        if touch?.view != cv {
            removeFromSuperview()
        }
    }
    
}
