//
//  AddFamilyMemberViewController.swift
//  BLEScanner
//
//  Created by Vinod Kumar on 06/04/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class AddFamilyMemberViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var backbutton: UIButton!
    
    @IBOutlet var detailLabel: UILabel!{
        didSet {
            detailLabel.text = "Add Devices To Ignore Alert".localized
        }
    }
    @IBOutlet var lblTenTimes: UILabel!{
        didSet {
            lblTenTimes.text = " These id's you have met more than 10 times".localized
        }
    }
    
    @IBOutlet var blockuserSegment: UISegmentedControl!
    @IBOutlet var familyMembersTableView: UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    var alert: UIAlertController? = nil
    var yesButton: UIAlertAction? = nil
    var noButton: UIAlertAction? = nil
    
    var apiName: String? = ""
    let apiObj = ApiRequestClass()
    var localModelObj = [BluetoothDataModel]()
    var selectedsegment = ""
    var selectedIndex: Int?
    var blockUser = false
    var rowIndex = 0
    var selectedCategory = ""
    let validObj = PayToValidations()
    var contactNumber = ""
    var contactName = ""
    var bluetoothName = ""
    var bluetoothID = ""
    var chennalName = ""
    var dateTimeStr = ""
    var model = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoRecord.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 16)
        lblNoRecord.text = "No records".localized
        //        blockuserSegment.setTitleTextAttributes([NSAttributedString.Key.font: appFont], for: .normal)
        familyMembersTableView.tableFooterView = UIView()
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        blockuserSegment.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        let font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 13)
        blockuserSegment.setTitleTextAttributes([NSAttributedString.Key.font: font ?? "Zawgyi-One"],
                                                for: .normal)
        blockuserSegment.setTitle("IGNORED DEVICES".localized, forSegmentAt: 0)
        blockuserSegment.setTitle("DEVICES TO ADD FOR IGNORE ALERT".localized, forSegmentAt: 1)
        self.navigationController?.navigationBar.isHidden = true
        selectedsegment = "UnBlock"
        blockuserSegment.selectedSegmentIndex = 1
        //Here only data will populate of the people who met more than 10 times
        let countOfPeopleMeetMoreThanTen = ApplicationState.sharedAppState.blueToothModelObj//.filter({$0.peopleMeet >= 30})
        localModelObj = countOfPeopleMeetMoreThanTen.filter({$0.userCategory == CustomNotification.DefaultValue.description})
        
        if localModelObj.count>0{
            familyMembersTableView.delegate = self
            familyMembersTableView.dataSource = self
            familyMembersTableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ApplicationState.sharedAppState.blueToothMainClassObj.stop()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            ApplicationState.sharedAppState.blueToothMainClassObj.isSartedCalled = false
            ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let identifier = "HeaderTitleTableViewCell"
        var cellView: HeaderTitleTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeaderTitleTableViewCell
        if cellView == nil {
            tableView.register (UINib(nibName: "HeaderTitleTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeaderTitleTableViewCell
        }
        return cellView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedsegment == "Block" {
            if model.count > 0 {
                self.lblNoRecord.isHidden = true
                return model.count
            }else {
               self.lblNoRecord.isHidden = false
            }
        }else {
            if localModelObj.count > 0 {
                self.lblNoRecord.isHidden = true
                return localModelObj.count
            }else {
                self.lblNoRecord.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyMemberTableViewCell") as! FamilyMemberTableViewCell
        
        if selectedsegment == "Block" {
            let dic = model[safe : indexPath.row]
            if dic?["BName"] as? String ?? "" == "No Name" {
                if dic?["Name"] as? String ?? "" == "" {
                    cell.nameLabel.text = dic?["BName"] as? String ?? ""
                }else {
                    cell.nameLabel.text = dic?["Name"] as? String ?? ""
                }
            }else {
                if dic?["Name"] as? String ?? "" == "" {
                  cell.nameLabel.text = dic?["BName"] as? String ?? ""
                }else {
                 cell.nameLabel.text = (dic?["BName"] as? String ?? "") + "(" + (dic?["Name"] as? String ?? "") + ")"
                }
            }
            cell.bluetoothidLabel.text = dic?["BId"] as? String ?? ""
            cell.blockUnBlockButton.setImage(UIImage(named: "unblock"), for: .normal)
        }else {
            if localModelObj[indexPath.row].userCategory == CustomNotification.DefaultValue.description {
                cell.nameLabel.text = localModelObj[indexPath.row].bluetoothName
                cell.bluetoothidLabel.text = localModelObj[indexPath.row].bluetoothId
                cell.blockUnBlockButton.setImage(UIImage(named: "block"), for: .normal)
            }else{
                cell.blockUnBlockButton.setImage(UIImage(named: "unblock"), for: .normal)
            }
        }
        cell.blockUnBlockButton.tag = indexPath.row
        cell.blockUnBlockButton.addTarget(self, action: #selector(switchTriggered), for: .touchUpInside)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    @IBAction func blockusersegmentClick(_ sender: Any) {
        
        if blockuserSegment.selectedSegmentIndex == 0 {
            selectedsegment = "Block"
            self.callBlockedBluethootList()
        }else{
            selectedsegment = "UnBlock"
            localModelObj.removeAll()
            let countOfPeopleMeetMoreThanTen = ApplicationState.sharedAppState.blueToothModelObj//.filter({$0.peopleMeet >= 30})
            localModelObj = countOfPeopleMeetMoreThanTen.filter({$0.userCategory == CustomNotification.DefaultValue.description})
            familyMembersTableView.reloadData()
        }
    }
    
    @objc func switchTriggered(sender: AnyObject) {
        selectedIndex = sender.tag
        if selectedsegment == "UnBlock" {
            blockUser = true
            self.showSAertForFive(message: "Please Select the below option to ignore notification".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "Home".localized, actionTitleSecond: "Office".localized, actionTitleThird: "Device".localized, actionTitleFourth: "Other".localized, actionTitlefifth: "Cancel".localized, firstActoin:  #selector(homeAction), secondActoin: #selector(officeAction), thirdActoin: #selector(deviceAction), fourthAction: #selector(otherAction), fifthAction: #selector(Cancel), controller: self)
        }else{
            blockUser = false
            let dic = model[safe : selectedIndex ?? 0]
         
            if dic?["BName"] as? String ?? "" == "No Name" {
                if dic?["Name"] as? String ?? "" == "" {
                    self.bluetoothName = dic?["BName"] as? String ?? ""
                   }else {
                    self.bluetoothName = dic?["Name"] as? String ?? ""
                   }
               }else {
                if dic?["Name"] as? String ?? "" == "" {
                    self.bluetoothName = dic?["BName"] as? String ?? ""
                   }else {
                    self.bluetoothName = (dic?["BName"] as? String ?? "") + "(" + (dic?["Name"] as? String ?? "") + ")"
                   }
               }
            
            //self.bluetoothName = dic["BName"] as? String ?? ""
            markAsFav(index: selectedIndex ?? 0, category: CustomNotification.DefaultValue.description, block: blockUser, bluetoothID: dic?["BId"] as? String ?? "")
        }
    }
    
    @objc func homeAction () {
        markAsFav(index: selectedIndex ?? 0, category: CustomNotification.Home.description, block: blockUser, bluetoothID: localModelObj[selectedIndex ?? 0].bluetoothId)
    }
    
    @objc func officeAction () {
        markAsFav(index: selectedIndex ?? 0, category: CustomNotification.Office.description, block: blockUser, bluetoothID: localModelObj[selectedIndex ?? 0].bluetoothId)
    }
    
    
    @objc func deviceAction () {
        markAsFav(index: selectedIndex ?? 0, category: CustomNotification.Devices.description, block: blockUser, bluetoothID: localModelObj[selectedIndex ?? 0].bluetoothId)
    }
    
    @objc func otherAction () {
        markAsFav(index: selectedIndex ?? 0, category: CustomNotification.Devices.description, block: blockUser, bluetoothID: localModelObj[selectedIndex ?? 0].bluetoothId)
    }
    
    @objc func Cancel () {
        
    }
    
    func markAsFav(index: Int,category: String,block: Bool,bluetoothID: String) {
        rowIndex = index
        //this block will work to block the user
        if block {
            dateTimeStr = Utility.makeDateFormatForAddFamily(date: localModelObj[safe : index]?.bluetoothDate ?? "")
            let name = localModelObj[safe : index]?.bluetoothName ?? ""
            let id = localModelObj[safe : index]?.bluetoothId ?? ""
            self.bluetoothID = id
            self.bluetoothName = name
            self.selectedCategory = category
            if bluetoothID.count < 10 {
                chennalName = "0"
            }else {
                chennalName = "1"
            }
            blockuserSegment.selectedSegmentIndex = 1
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "", message: "Do you want to save this device with Name and Contact Number?".localized, preferredStyle: .alert)
                let yes = UIAlertAction(title: "YES".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                    self.blockUserWithContact()
                }
                let no = UIAlertAction(title: "NO".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                    self.blockUserWithoutContact(name: name, id: id, chennale: self.chennalName, block: "1")
                }
                alertController.addAction(no)
                alertController.addAction(yes)
                self.present(alertController, animated: true, completion:nil)
            }
            
        }else {
            // funtion to unblock User
            self.bluetoothID = bluetoothID
            self.selectedCategory = category
            if bluetoothID.count < 10 {
                chennalName = "0"//Audio QR
            }else {
                chennalName = "1"//Bluetooth ID
            }
            
            blockuserSegment.selectedSegmentIndex = 0
            let dic = model[safe : index]
            let number = dic?["MobileNumber"] as? String ?? ""
            let name =  dic?["Name"] as? String ?? ""
            let date = dic?["EndDateTime"] as? String ?? ""
            
            dateTimeStr = date.replacingOccurrences(of: "T", with: " ")
            self.callBlockBluetoothDevice(b_name: self.bluetoothName, b_id:  bluetoothID, b_chennal: self.chennalName, block: "0", c_name: name, c_nubmer: number)
        }
    }
    
    func blockUserWithContact() {
        // opencontact
        self.showContactPickerView()
    }
    
    func showContactPickerView() {
        let nav = Utility.openContact(multiSelection: false, self)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    func blockUserWithoutContact(name:String,id:String, chennale: String, block: String) {
        self.callBlockBluetoothDevice(b_name: name, b_id: id, b_chennal: chennale, block: block, c_name: "", c_nubmer: "")
    }
    
    
    
    @objc func buttonSelected(sender: UIButton){
        alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
        
        let messagelabel = UILabel(frame: CGRect(x: 20, y: 70, width: 210, height: 80))
        messagelabel.text = "Are you Sure? You want to add this id as a family member."
        messagelabel.textAlignment = .center
        messagelabel.numberOfLines = 4
        messagelabel.lineBreakMode = .byTruncatingTail
        messagelabel.textColor = UIColor.black
        
        //  customAlerttwobuttons()
        // namelabel.text = "vamsi"
        let namelabel = UILabel(frame: CGRect(x: 40, y: 160, width: 190, height: 30))
        //  namelabel.text = "vamsi"
        namelabel.textAlignment = .center
        namelabel.numberOfLines = 4
        namelabel.lineBreakMode = .byTruncatingTail
        namelabel.textColor = UIColor.black
        alert?.view.addSubview(messagelabel)
        alert?.view.addSubview(namelabel)
        imageView.image = UIImage(named: "AlertIcon") // Your image here...
        alert?.view.addSubview(imageView)
        let height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        let width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        alert!.view.addConstraint(height)
        alert!.view.addConstraint(width)
        
        yesButton = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
        })
        
        noButton = UIAlertAction(title: "No", style: .default, handler: { action in
            
        })
        yesButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        noButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        
        alert?.addAction(yesButton!)
        alert?.addAction(noButton!)
        self.present(alert!, animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension AddFamilyMemberViewController : ApiRequestProtocol {
    
    func callBlockedBluethootList() {
        let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
        let dic = [
              "MobileNumber": mobilNum,"simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
              ,"IsBlocked":1] as [String : Any]
          showLoader()
          print(dic)
          apiObj.customDelegate = self
          self.apiName = "BlockedBluetoothList"
          apiObj.sendHttpRequest(requestUrl: URL(string:Constant.getBlockedBlutooth())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
    }
    
    func callBlockBluetoothDevice(b_name: String,b_id : String, b_chennal:String, block: String, c_name: String, c_nubmer: String) {
        if Utility.isNetworkAvailable(controller: self) {
            self.contactName = c_name
            self.contactNumber = c_nubmer
            let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
            var bluetooth = Dictionary<String,Any>()
            var ultrasound = Dictionary<String,Any>()
            var arrB = [Any]()
            var arrU = [Any]()
            if b_chennal == "0" {
                //UltraSound
                ultrasound = ["UltraSoundName":b_name,"UltraSoundid":b_id,"CategoryId": "54ad5797-189f-418c-9c7f-15a0e89c9e2c","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
                arrU.append(ultrasound)
            }else {
                // Bluetotth
                bluetooth = ["BluetoothName":b_name,"Bluetoothid":b_id,"CategoryId": "f1b171b8-b481-4b29-a5e1-d8ee55109613","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
                arrB.append(bluetooth)
            }
            
            let dic = [ "UserDetails":
                ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
                ],
                        "DeviceDetails":[
                            "BlueTooth": arrB,
                            "UltraSound": arrU
                ]
                ] as [String : Any]
            showLoader()
            print(dic)
            apiObj.customDelegate = self
            self.apiName = "BlockBluetooth"
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.getBlutoothBlock())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
        }else {
            DispatchQueue.main.async {
                Utility.alert(message: "", title: "No Network Available".localized, controller: self)
            }
        }
        
    }
    
    func httpResponse(responseObj:Any,hasError: Bool) {
        hideLoader()
        if !hasError {
            if self.apiName == "BlockedBluetoothList"  {
                let dataResponse = responseObj as! NSDictionary
                if let messageCode = dataResponse["Code"] as? Int , messageCode == 200 {
                    do {
                        if let arr = dataResponse["Data"] as? Array<Dictionary<String,Any>> {
                            print("Blocked User: \n \(arr)")
                            self.model.removeAll()
                            for item in arr {
                                let date = item["EndDateTime"] as? String ?? ""
                                let convertedDate = date.components(separatedBy: "T")[0]
                                if convertedDate == Utility.getDateYYYYMMDD().components(separatedBy: " ")[0] {
                                    self.model.append(item)
                                }
                            }
                        }
                        if self.selectedsegment == "Block" {
                            DispatchQueue.main.async {
                                self.familyMembersTableView.reloadData()
                            }
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }else if  (self.apiName == "BlockBluetooth"){
                let dataResponse = responseObj as! NSDictionary
                if dataResponse.count > 0 {
                    DispatchQueue.main.async {
                        if let code = dataResponse["Code"] as? Int {
                            if code == 200 {
                                
                                let copyObj = ApplicationState.sharedAppState.blueToothModelObj
                                let index = copyObj.indices.filter({ copyObj[$0].bluetoothId ==  self.bluetoothID})
                                print(index.count)
                                
                                if index.count == 0{
                                    //append new data when we unblock
                                    ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: self.bluetoothName, id: self.bluetoothID, bluetoothDate: Utility.getDateForCoreData(), bluetoothDateLessThanSixFeet: 1, peopleMeet: 0, userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "0",rssiID: "-50",mobileNumber: self.contactNumber))
                                }else{
                                    //remove and append new elements
                                    if ApplicationState.sharedAppState.blueToothModelObj.indices.contains(index[0]){
                                        let copyObj = ApplicationState.sharedAppState.blueToothModelObj[index[0]]
                                        ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
                                        if self.contactName == "" {
                                            ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: copyObj.bluetoothName, id: copyObj.bluetoothId, bluetoothDate: Utility.getDateForCoreData(), bluetoothDateLessThanSixFeet: copyObj.bluetoothDateLessThanSixFeet, peopleMeet: copyObj.peopleMeet, userCategory: self.selectedCategory,numberOfTimePeopleMeet: copyObj.numberOfTimePeopleMeet,rssiID: copyObj.rssiID,mobileNumber: self.contactNumber))
                                        }else {
                                            ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: self.contactName, id: copyObj.bluetoothId, bluetoothDate: Utility.getDateForCoreData(), bluetoothDateLessThanSixFeet: copyObj.bluetoothDateLessThanSixFeet, peopleMeet: copyObj.peopleMeet, userCategory: self.selectedCategory,numberOfTimePeopleMeet: copyObj.numberOfTimePeopleMeet,rssiID: copyObj.rssiID,mobileNumber: self.contactNumber))
                                        }
                                        
                                    }
                                }
                                
                                self.contactName = ""
                                self.contactNumber = ""
                                self.bluetoothID = ""
                                
                                self.localModelObj.removeAll()
                                self.localModelObj = ApplicationState.sharedAppState.blueToothModelObj
                                if self.blockUser {
                                    self.selectedsegment = "UnBlock"
                                    self.blockuserSegment.selectedSegmentIndex = 1
                                    self.localModelObj.removeAll()
                                    let countOfPeopleMeetMoreThanTen = ApplicationState.sharedAppState.blueToothModelObj
                                    //.filter({$0.peopleMeet >= 30})
                                    self.localModelObj = countOfPeopleMeetMoreThanTen.filter({$0.userCategory == CustomNotification.DefaultValue.description})
                                    //  if localModelObj.count>0{
                                    self.familyMembersTableView.delegate = self
                                    self.familyMembersTableView.dataSource = self
                                    self.familyMembersTableView.reloadData()
                                    //  }
                                }else{
                                    self.selectedsegment = "Block"
                                    self.blockuserSegment.selectedSegmentIndex = 0
                                    self.model.remove(at: self.selectedIndex ?? 0)
                                    self.localModelObj.removeAll()
                                    let countOfPeopleMeetMoreThanTen = ApplicationState.sharedAppState.blueToothModelObj
                                    // .filter({$0.peopleMeet >= 30})
                                    self.localModelObj = countOfPeopleMeetMoreThanTen.filter({$0.userCategory != CustomNotification.DefaultValue.description})
                                    //  if localModelObj.count>0{
                                    
                                    self.familyMembersTableView.delegate = self
                                    self.familyMembersTableView.dataSource = self
                                    self.familyMembersTableView.reloadData()
                                }
                                
                                
                                var messageStr = ""
                                if self.selectedsegment == "UnBlock" {
                                    messageStr = "Notification is Ignored".localized
                                }else {
                                    messageStr = "Notification is Activated".localized
                                }
                                DispatchQueue.main.async {
                                    let alertController = UIAlertController(title: "", message: messageStr, preferredStyle: .alert)
                                    let yes = UIAlertAction(title: "OK".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                        self.familyMembersTableView.reloadData()
                                    }
                                    alertController.addAction(yes)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }else {
                                DispatchQueue.main.async {
                                    Utility.alert(message: "", title: "API Failed".localized, controller: self)
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                Utility.alert(message: "", title: "API Failed".localized, controller: self)
                            }
                        }
                    }
                }
            }
        }else {
            DispatchQueue.main.async {
                Utility.alert(message: "", title: "API Failed".localized, controller: self)
            }
        }
    }
    
}
