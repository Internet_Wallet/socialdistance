//
//  HeaderTitleTableViewCell.swift
//  BLEScanner
//
//  Created by Vinod Kumar on 06/04/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class HeaderTitleTableViewCell: UITableViewCell {

    @IBOutlet var lblID: UILabel!{
        didSet{
            lblID.text = "Bluetooth ID".localized
        }
    }
    @IBOutlet var lblName: UILabel!{
        didSet{
            lblName.text = "Bluetooth Name".localized
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
