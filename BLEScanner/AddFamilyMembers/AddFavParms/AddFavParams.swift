//
//  AddFavParams.swift
//  BLEScanner
//
//  Created by Avaneesh Kumar on 07/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct  AddFavParms {
    
    static func getTheAddFavParams () -> Dictionary< String , Any> {

        let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
           var wifiArray = [NSMutableDictionary]()
          if NetWorkCode.getNetworkType() == "Wifi" {
              wifiArray.append(RegisterParam.getWifiInfo(wifiName: NetWorkCode.getNetworkInfos()[0], wifiID: NetWorkCode.getNetworkInfos()[1]))
          }else{
              wifiArray.append(RegisterParam.getWifiInfo(wifiName: "", wifiID: ""))
          }
          let dic = [
              "UserInfo":
                  ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID(),"Wifi": wifiArray,"Bluetooth":[RegisterParam.getBluetoothInfo(blueToothName: "iPhone", bluetoothId: "iPhone", newSelf: "1")],"GPS":RegisterParam.getGpsInfo(lat: "16.76611328125", long: "96.17500019032727", cellid: "", mcc: "adads", mnc: "asdasd", hspambps: "", nearLocation: "Vintage Luxury Yatch", Township: "BotaTown", divisionName: "Yangon")],
              "FavBlootoothList":
                  [["BName": "Realme 3 pro","BId": "9887421134646","BCategoryId":"F1B171B8-B481-4B29-A5E1-D8EE55109613"] ,["BName": "Realme 2 pro","BId": "123456987","BCategoryId":"54AD5797-189F-418C-9C7F-15A0E89C9E2C"]]
            ] as [String : Any]
        return dic
    }
    
    static func getTheBlutoothData () -> Dictionary< String , Any> {

         let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
          let dic = [
              "UserInfo":
                  ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
              ] ,"DeviceDetails":["BlueTooth":[[ "BluetoothName":"OKERPLAP4","Bluetoothid":"12121212","Channel":"","Self":1]]]] as [String : Any]
        return dic
    }
    
    static func getBlockBlutoothParams(bName: String,bID: String,bChannel: String,isBlock: Int,name:String,bNumber: String) -> Dictionary< String , Any> {
        let dic = [
            "UserDetails":
                ["MobileNumber":  ApplicationState.sharedAppState.currentUser.MobileNumber,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
            ] ,
            "DeviceDetails":
                ["BlueTooth":
                    [[ "BluetoothName":bName,"Bluetoothid": bID,"Channel":bChannel,"Self":1,"IscBlocked": isBlock,"Name": name,"MobileNumber": bNumber]]
            ]
            ] as [String : Any]
        return dic
    }

}


struct AddQuarintineMember {
    
    static func getTheParmsForQuarintine( name : String , fatherName : String, nrcDetails : String , townShip : String , devision : String , googleAddres : String)-> Dictionary <String,Any> {
        
        let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
        let dic = [
            "UserDetails":
                ["MobileNumber": mobilNum,"Name":name,"OcupationId":"784FB3D1-6C89-4CCA-B2E8-095E94CF5420","FbId":"NA","Gmailid":"NA","ViberNumber":mobilNum,"NRCNumber":nrcDetails,"RegDetails":"0",
                 "SendSms":"0","OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID(),"TownshipName":townShip, "DivisionName":devision,"GoogleAddress":googleAddres]] as [String : Any]
        return dic
    }
}
