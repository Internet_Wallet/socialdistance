//
//  FamilyMemberTableViewCell.swift
//  BLEScanner
//
//  Created by Vinod Kumar on 06/04/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class FamilyMemberTableViewCell: UITableViewCell {
    
    @IBOutlet var bluetoothidLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addbuttton: UIButton!{
        didSet{
            addbuttton.setTitle("+ ADD".localized, for: .normal)
        }
    }
    
    @IBOutlet var familyswitch: UISwitch!
    
    @IBOutlet var blockUnBlockButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
