//
//  BaseTabController.swift
//  BLEScanner
//
//  Created by Sam on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class BaseTabController: UITabBarController {
    
    var fromlogin = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateTabBarTitle), name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
        
        self.tabBar.unselectedItemTintColor = UIColor(red: 43/255, green: 125/255, blue: 203/255, alpha: 1.0)
        
        UITabBarItem.appearance()
        .setTitleTextAttributes(
            [NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 12) ?? "Zawgyi-One"],
        for: .normal)
        
        if fromlogin == "LOGIN"
        {
            
                    var cv: PopAlertView?
                    cv = GlobalVariable.popupAlert()
                    self.view.addSubview(cv!)
        }
        else{
            // nothing
        }
        
        if #available(iOS 13.0, *) {
            let sb = UIStoryboard(name: "Geofence", bundle: nil)
            let bashboard = sb.instantiateViewController(identifier: "CategoryViewControllerIdentifier") as! CategoryViewController
            bashboard.tabBarItem = UITabBarItem(title: "Dashboard".localized, image: UIImage(named: "home"), tag: 1)
            
            
            let newreports =  UIStoryboard(name: "QRCode", bundle: nil)
            let newbashboard = newreports.instantiateViewController(identifier: "NewReportsViewController") as! NewReportsViewController
            newbashboard.tabBarItem = UITabBarItem(title: "Reports".localized, image: UIImage(named: "menureport"), tag: 2)

            let more = self.storyboard?.instantiateViewController(identifier: "More") as! More
            more.tabBarItem = UITabBarItem(title: "More".localized, image: UIImage(named: "more"), tag: 3)
            
            self.viewControllers = [bashboard,newbashboard,more]
            
        } else {
            // Fallback on earlier versions
            let  homeVC = UIStoryboard(name: "Geofence", bundle: nil)
            let dashbord = homeVC.instantiateViewController(withIdentifier: "CategoryViewControllerIdentifier") as? CategoryViewController

            var objNav: UINavigationController? = nil
                  if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)

                    }
            if let objNav = objNav {

              objNav.tabBarItem = UITabBarItem(title: "Dashboard".localized, image: UIImage(named: "home"), tag: 1)

            }
            
            let  reportVC = UIStoryboard(name: "QRCode", bundle: nil)
                let reportdashbord = reportVC.instantiateViewController(withIdentifier: "NewReportsViewController") as? NewReportsViewController

                var reportobjNav: UINavigationController? = nil
                      if let reportdashbord = reportdashbord {
                        reportobjNav = UINavigationController(rootViewController: reportdashbord)

                        }
                if let reportobjNav = reportobjNav {

                  reportobjNav.tabBarItem = UITabBarItem(title: "Reports".localized, image: UIImage(named: "menureport"), tag: 2)

                }
            
            let  moreVC = UIStoryboard(name: "QRCode", bundle: nil)
            let dashbord2 = moreVC.instantiateViewController(withIdentifier: "More") as? More
            var objNav2: UINavigationController? = nil
            if let dashbord2 = dashbord2 {
                    objNav2 = UINavigationController(rootViewController: dashbord2)
                   }
            if let objNav2 = objNav2 {
               //   dashbord2?.modalPresentationStyle = .fullScreen
                 objNav2.tabBarItem = UITabBarItem(title: "More".localized, image: UIImage(named: "more"), tag: 3)
                            
                //   self.present(objNav2, animated: true)
                }
            
            self.viewControllers = [objNav!,reportobjNav!,objNav2!]
        }
  
        print("Presented Base Controller")
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if item.title == "News".localized{
//              Utility.alert(message: "", title: "Coming Soon", controller: self)
//        }
    }
  
    @objc func updateTabBarTitle() {

        UITabBarItem.appearance()
              .setTitleTextAttributes(
                  [NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 12) ?? "Zawgyi-One"],
              for: .normal)
        
        self.tabBar.items?[0].title = "Dashboard".localized
        self.tabBar.items?[1].title = "Reports".localized
        self.tabBar.items?[2].title = "More".localized

    }
}
