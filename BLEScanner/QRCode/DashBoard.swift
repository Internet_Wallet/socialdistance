//
//  DashBoard.swift
//  BLEScanner
//
//  Created by Sam on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class DashBoard: UIViewController {

    @IBOutlet weak var tvDashboard: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvDashboard.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

}

extension DashBoard : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardFirstCell", for: indexPath) as! dashboardFirstCell
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardSecondCell", for: indexPath) as! dashboardSecondCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 250
        }else {
            return 310
        }
    }
}



class dashboardFirstCell : UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var bvView: UIView!
    
    override func awakeFromNib() {
    }
}

class dashboardSecondCell : UITableViewCell {
    @IBOutlet weak var lblTotalWeMeet: UILabel!
    @IBOutlet weak var lblTotalWeMeetNumber: UILabel!
    @IBOutlet weak var lblPeopleMeet: UILabel!
    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var bvView: UIView!
    
    override func awakeFromNib() {
        bvView.layer.cornerRadius = 5
        bvView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        bvView.layer.borderWidth = 1.0
        bvView.layer.masksToBounds = true
     }
}

