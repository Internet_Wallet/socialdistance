//
//  GeneratedQR.swift
//  BLEScanner
//
//  Created by Sam on 04/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit


class GeneratedQR: UIViewController {
    @IBOutlet weak var imgQR: UIImageView!
    var qrString: String?
    let qrOjb = QRCodeGenericClass()
    @IBOutlet weak var btnGenerateQR: UIButton!
    @IBOutlet weak var btnScanQR: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnGenerateQR.isHidden = true
        self.setMarqueLabelInNavigation()
        btnGenerateQR.setTitle("Generate QR Code".localized, for: .normal)
        btnScanQR.setTitle("Share QR".localized, for: .normal)
       
        if ApplicationState.sharedAppState.currentUser.qrID != "" {
            imgQR.image = qrOjb.generteQRCodeImage(qrStr: "covid19@" + ApplicationState.sharedAppState.currentUser.qrID)
        }else {
            imgQR.image = qrOjb.generteQRCodeImage(qrStr: "covid19@" + ApplicationState.sharedAppState.currentUser.qrID)
        }
    }
    
    @IBAction func onClickBackAction(_ : UIButton) {
        if ApplicationState.sharedAppState.currentUser.generateButtonShow == true {
            self.dismiss(animated: true, completion: nil)
        }else {
            ApplicationState.sharedAppState.currentUser.generateButtonShow = true
            if #available(iOS 13.0, *) {
                self.navigationController?.popViewController(animated: false)
            }else {
              self.dismiss(animated: false, completion: nil)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "QRNatigateToRoot"), object: nil)
        }
    }
    
    @IBAction func onClickQRCodeGeneratedAction(_ : UIButton) {
        if ApplicationState.sharedAppState.currentUser.generateButtonShow == true {
            self.dismiss(animated: true, completion: nil)
        }else {
            if #available(iOS 13.0, *) {
                self.navigationController?.popViewController(animated: true)
            }else {
                self.dismiss(animated: false, completion: nil)
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "QRNatigateToRoot"), object: nil)
            }
        }
    }
    
    @IBAction func shareAction(_ : UIButton) {
        let imageToShare = [captureScreen(view: imgQR)]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.modalPresentationStyle = .fullScreen
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func captureScreen(view:UIView) -> UIImage {
        let bounds = view.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        view.drawHierarchy(in: bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

 
    private func setMarqueLabelInNavigation() {
            let lblMarque = MarqueeLabel()
            lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            lblMarque.textColor = UIColor.white
            lblMarque.textAlignment = .center
            lblMarque.font =  UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 18)
            lblMarque.text = "My QR Code".localized
            self.navigationItem.titleView = nil
            self.navigationItem.titleView = lblMarque
        }
}
