//
//  More.swift
//  BLEScanner
//
//  Created by Sam on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import AVFoundation
//import engine
import CoreData

class More: UIViewController {
    var localBundle : Bundle?
    var session: AVAudioSession = AVAudioSession.sharedInstance()
    var scanUserList = [String]()
    
    var isAudioRecordingOn = false
    var listMenu = [
        ["title": "Choose Language","icon": "language"],
        ["title": "Myanmar","icon": "myanmar"],
        ["title": "Nearby Person","icon": "navigation"],
        ["title": "Add Devices To Ignore Alert","icon": "family_members"],
        ["title": "Add My Home Location","icon": "home_location"],
        ["title": "QR Code","icon": "qr_code_blue"],
        ["title": "Scan QR Code","icon": ""],
        ["title": "Quarantine","icon": "quarantine"],
        ["title": "Log Out","icon": "logout"]
    ]
    
    let listLanguage = [
        ["title": "EnglishMenu","icon": "uk"],
        ["title": "MyanmarMenu","icon": "myanmar"],
        ["title": "ChineseMenu","icon": "chinaF"],
        ["title": "ThaiMenu","icon": "thaiF"],
        ["title": "UnicodeMenu","icon": "myanmar"]
    ]
    
    
    
    var listQR = [["title": "Scan QR Code","icon": "scan_qr_code"]]
    
    var isLanguageOpen = false
    var isQRCodeOpen  = false
    var titleHome = "Add My Home Location".localized
    var titleWork = "Add My Work Location".localized
    @IBOutlet weak var tvMore: UITableView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    var alert: UIAlertController? = nil
    var yesButton: UIAlertAction? = nil
    var noButton: UIAlertAction? = nil
    var intLanguageSelection = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ApplicationState.sharedAppState.currentUser.qrID == "" {
            self.listQR.append(["title": "Generate QR Code","icon": "qr_code_blue"])
        }else {
            self.listQR.append(["title": "My QR Code","icon": "qr_code_blue"])
        }
        //Engine setup
        self.navigationController?.navigationBar.isHidden = true
        //GUI Setup
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.borderColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        imgUser.layer.borderWidth = 1.0
        imgUser.layer.masksToBounds = true
        imgUser.image = UIImage(named: "user_logo")
        //    ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
        
        tvMore.tableFooterView = UIView()
        nameLabel.text = ApplicationState.sharedAppState.currentUser.mobileNumberToDisplay
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeNameQR), name: NSNotification.Name(rawValue: "QRChangeName"), object: nil)
    }
    
    @objc func changeNameQR() {
        listQR.remove(at: 1)
        listQR.append(["title": "My QR Code","icon": "qr_code_blue"])
        let index = IndexPath(row: 1, section: 6)
        self.tvMore.reloadRows(at: [index], with: .automatic)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "QRChangeName") , object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Update cell text based on add location
        updateMoreCellGUI()
        
        let lang = ApplicationState.sharedAppState.currentUser.currentLanguage
        appDel.currentLanguage = lang
        if lang == "en" {
            intLanguageSelection = 0
        } else if lang == "my" {
            intLanguageSelection = 1
        } else if lang == "zh-Hans" {
            intLanguageSelection = 2
        } else if lang == "th" {
            intLanguageSelection = 3
        } else if lang == "uni" {
            intLanguageSelection = 4
        }
        
        isLanguageOpen = false
        isQRCodeOpen  = false
        tvMore.reloadData()
    }
    
    func updateMoreCellGUI() {
        
        if let isHomeLocation = checkForGeoFense(typeX: Constant.typeHomeLocation) as? Bool , isHomeLocation == true {
            
            self.titleHome = "View Your Home Location".localized
        }
        else {
            self.titleHome = "Add My Home Location".localized
        }
        if let isworkLocation = checkForGeoFense(typeX: Constant.typeWorkLocation) as? Bool , isworkLocation == true {
            
            self.titleWork = "View My Work Location".localized
        }
        else {
            self.titleWork = "Add My Work Location".localized
        }
    }
    
    private func checkForGeoFense(typeX : String) -> Bool {
        var isFound : Bool = false
        for obj in ApplicationState.sharedAppState.geofenceModelObj {
            if let type = obj.txtType as? String , type == typeX {
                isFound = true
                break
            }
        }
        return isFound
    }
    
    private func navigateToScanToQRCode() {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "QRCode", bundle: nil)
                if let scanQR = sb.instantiateViewController(identifier: "ScanQR") as? ScanQR {
                    scanQR.modalPresentationStyle = .fullScreen
                    self.present(scanQR, animated: true, completion: nil)
                }
            }  else {
                let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "ScanQR") as? ScanQR
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if let objNav = objNav {
                    dashbord?.modalPresentationStyle = .fullScreen
                    self.present(objNav, animated: true)
                }
            }
        }
    }
    
    private func navigateToGenerateQRCode() {
        
        if ApplicationState.sharedAppState.currentUser.qrID != "" {
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let sb = UIStoryboard(name: "QRCode", bundle: nil)
                    let baseObj = sb.instantiateViewController(identifier: "QR_Code_Generated")
                    baseObj.modalPresentationStyle = .fullScreen
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        topController.present(baseObj, animated: true, completion: nil)
                    }
                } else {
                    let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                    let dashbord = homeVC.instantiateViewController(withIdentifier: "QR_Code_Generated") as? UINavigationController
                    var objNav: UINavigationController? = nil
                    if let dashbord = dashbord {
                        objNav = dashbord
                    }
                    if let objNav = objNav {
                        dashbord?.modalPresentationStyle = .fullScreen
                        self.present(objNav, animated: true)
                    }
                }
            }
        }else {
            DispatchQueue.main.async {
                if #available(iOS 13.0, *) {
                    let sb = UIStoryboard(name: "QRCode", bundle: nil)
                    let baseObj = sb.instantiateViewController(identifier: "verifyUserNav")
                    baseObj.modalPresentationStyle = .fullScreen
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        topController.present(baseObj, animated: true, completion: nil)
                    }
                } else {
                    let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                    let dashbord = homeVC.instantiateViewController(withIdentifier: "verifyUserNav") as? UINavigationController
                    var objNav: UINavigationController? = nil
                    if let dashbordX = dashbord {
                        objNav = dashbordX
                    }
                    if let objNavX = objNav {
                        dashbord?.modalPresentationStyle = .fullScreen
                        self.present(objNavX , animated: true)
                    }
                }
            }
        }
    }
    
}

extension More : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listMenu.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if isLanguageOpen {
                return 5
            }else {
                return 0
            }
        }else if section == 6 {
            if isQRCodeOpen {
                return 2
            }else {
                return 0
            }
        }else if section == 4 {
            return 2
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! moreCell
            let dic = listMenu[indexPath.section]
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.lblTitle.text = dic["title"]?.localized
            cell.imgDropDown.image = UIImage(named: "")
            cell.selectionStyle = .none
            if isLanguageOpen {
                cell.imgDropDown.image = UIImage(named: "up_arrow")
            }else {
                cell.imgDropDown.image = UIImage(named: "down_arrow")
            }
            
            if appLang == "English" {
                cell.lblTitle.text = "ဘာသာစကား ေရြးပါ" + " (" + "အဂၤလိပ္" + ")" + "\n" + "Choose Language" + "  " + "🇲🇲"
            } else if appLang == "Chinese" {
                cell.lblTitle.text = "ဘာသာစကား ေရြးပါ" + " (" + "အဂၤလိပ္" + ")" + "\n" + "Choose Language" + "  " + "🇲🇲"
            } else if appLang == "Thai" {
                cell.lblTitle.text = "ဘာသာစကား ေရြးပါ" + " (" + "အဂၤလိပ္" + ")" + "\n" + "Choose Language" + "  " + "🇲🇲"
            }
            else {
                cell.lblTitle.text = "Choose Language".localized + " (" + appLang.localized + ")" + "\n" + "Choose Language" + "  " + "🇬🇧"
            }
            
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as! languageCell
            let dic = listLanguage[indexPath.row]
            cell.lblTitle.text = dic["title"]?.localized
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            if intLanguageSelection == indexPath.row {
                cell.bulletIcon.image = UIImage(named: "radio_selected")
            } else {
                cell.bulletIcon.image = UIImage(named: "radio_unselected")
            }
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! moreCell
            //let dic = listMenu[indexPath.section]
            if indexPath.row == 0 {
                cell.imgIcon.image = UIImage(named:"home_location")
                cell.lblTitle.text = titleHome
            }else {
                cell.imgIcon.image = UIImage(named:"work_location")
                cell.lblTitle.text = titleWork
            }
            cell.imgDropDown.image = UIImage(named: "")
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! moreCell
            let dic = listMenu[indexPath.section]
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.lblTitle.text = dic["title"]?.localized
            cell.imgDropDown.image = UIImage(named: "")
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            cell.selectionStyle = .none
            if isQRCodeOpen {
                cell.imgDropDown.image = UIImage(named: "up_arrow")
            }else {
                cell.imgDropDown.image = UIImage(named: "down_arrow")
            }
            return cell
        }else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as! languageCell
            let dic = listQR[indexPath.row]
            cell.lblTitle.text = dic["title"]?.localized
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.bulletIcon.image = UIImage(named:"")
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 7 || indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! moreCell
            let dic = listMenu[indexPath.section]
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.lblTitle.text = dic["title"]?.localized
            cell.imgDropDown.image = UIImage(named: "")
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell", for: indexPath) as! moreCell
            let dic = listMenu[indexPath.section]
            cell.imgIcon.image = UIImage(named: dic["icon"] ?? "")
            cell.lblTitle.text = dic["title"]?.localized
            cell.lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
            cell.imgDropDown.image = UIImage(named: "")
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            isLanguageOpen = !isLanguageOpen
            if let cell = tvMore.cellForRow(at: indexPath) as? moreCell {
                if isLanguageOpen {
                    cell.imgDropDown.image = UIImage(named: "up_arrow")
                }else {
                    cell.imgDropDown.image = UIImage(named: "down_arrow")
                }
            }
            self.tvMore.reloadSections([1], with: .automatic)
        }else if indexPath.section == 1 {
            if indexPath.row == 0 {
                //English
                print("English")
                appLang = "English"
                appFont = "Zawgyi-One"
                appDel.currentLanguage = "en"
                ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
                languageUpdated(key:"en")
                intLanguageSelection = 0
            }else if indexPath.row == 1{
                // Zawgyi
                print("Zawgyi")
                appLang = "Myanmar"
                appFont = "Zawgyi-One"
                appDel.currentLanguage = "my"
                ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
                languageUpdated(key:"my")
                intLanguageSelection = 1
            }else if indexPath.row == 2 {
                //Chinese
                print("Chinese")
                appLang = "Chinese"
                appFont = "Zawgyi-One"
                appDel.currentLanguage = "zh-Hans"
                ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
                languageUpdated(key:"zh-Hans")
                intLanguageSelection = 2
            }else if indexPath.row == 3 {
                //Thai
                print("Thai")
                appLang = "Thai"
                appFont = "Zawgyi-One"
                appDel.currentLanguage = "th"
                ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
                languageUpdated(key:"th")
                intLanguageSelection = 3
            }else if indexPath.row == 4 {
                // Unicode
                print("Unicode")
                appLang = "Unicode"
                appFont = "Myanmar3"
                appDel.currentLanguage = "uni"
                ApplicationState.sharedAppState.currentUser.currentFont = "Myanmar3"
                languageUpdated(key:"uni")
                intLanguageSelection = 4
            }
            isLanguageOpen = !isLanguageOpen
            self.tvMore.reloadSections([1], with: .automatic)
        }else if indexPath.section == 5 {
            isQRCodeOpen = !isQRCodeOpen
            if let cell = tvMore.cellForRow(at: indexPath) as? moreCell {
                if isQRCodeOpen {
                    cell.imgDropDown.image = UIImage(named: "up_arrow")
                }else {
                    cell.imgDropDown.image = UIImage(named: "down_arrow")
                }
            }
            self.tvMore.reloadSections([6], with: .automatic)
        }else if indexPath.section == 6 {
            if indexPath.row == 0 {
                //Scan QR Code
                self.navigateToScanToQRCode()
            }else {
                //Generate QR Code
                self.navigateToGenerateQRCode()
            }
        }else if indexPath.section == 2 {
            
            //   ApplicationState.sharedAppState.blueToothMainClassObj.stop()
            
            // At Home add family members
            
            //  Utility.alert(message: "", title: "Coming Soon", controller: self)
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "Reports", bundle: nil)
                let baseObj = sb.instantiateViewController(identifier: "ReportListViewController")
                baseObj.modalPresentationStyle = .fullScreen
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(baseObj, animated: true, completion: nil)
                }
            } else{
                let  homeVC = UIStoryboard(name: "Reports", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "ReportListViewController") as? ReportListViewController
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if let objNav = objNav {
                    dashbord?.modalPresentationStyle = .fullScreen
                    self.present(objNav, animated: true)
                }
            }
        }
        else if indexPath.section == 3 {
            // At Home add family members
            //  Utility.alert(message: "", title: "Coming Soon", controller: self)
            
            if #available(iOS 13.0, *) {
                
                let sb = UIStoryboard(name: "AddFamilynumber", bundle: nil)
                let baseObj = sb.instantiateViewController(identifier: "AddFamilyMemberViewController")
                baseObj.modalPresentationStyle = .fullScreen
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(baseObj, animated: true, completion: nil)
                }
                
                
            }  else{
                let  homeVC = UIStoryboard(name: "AddFamilynumber", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "AddFamilyMemberViewController") as? AddFamilyMemberViewController
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if let objNav = objNav {
                    dashbord?.modalPresentationStyle = .fullScreen
                    self.present(objNav, animated: true)
                }
                
                
            }
            
        }else if indexPath.section == 7 {
            // Quarantine
            let isAlreadyQuarintine = ApplicationState.sharedAppState.currentUser.isQuarintine
            if (isAlreadyQuarintine) {
                
                if #available(iOS 13.0, *) {
                    if let viewController = UIStoryboard(name: "NewGeofence", bundle: nil).instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController{
                        viewController.modalPresentationStyle = .fullScreen
                        if let VC = viewController.topViewController as? NewGeofenceController{
                            VC.destinationString = Constant.quarintine
                        }
                        self.present(viewController, animated: true, completion: nil)
                    }
                }
                else{
                    let  homeVC = UIStoryboard(name: "NewGeofence", bundle: nil)
                    let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController
                    var objNav: UINavigationController? = nil
                    
                    if let dashbord = dashbord {
                        objNav = dashbord
                    }
                    
                    if let VC = dashbord?.topViewController as? NewGeofenceController{
                        VC.destinationString = Constant.quarintine
                    }
                    
                    
                    if let objNav = objNav {
                        dashbord?.modalPresentationStyle = .fullScreen
                        self.present(objNav, animated: true)
                    }
                }
            }
            else {
                self.showSAert(message: "Are you sure you want to \n make yourself quarantine for 14 days?".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "YES".localized, actionTitleSecond: "NO".localized, actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(OkAction), secondActoin: #selector(cancelAction), thirdActoin: nil , fourthAction: nil, controller: self)
            }
            
        }else if indexPath.section == 4 {
            // Geo Location
            if indexPath.row == 0 {
                if #available(iOS 13.0, *) {
                    if let viewController = UIStoryboard(name: "NewGeofence", bundle: nil).instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController{
                        if let VC = viewController.topViewController as? NewGeofenceController{
                            VC.destinationString = Constant.geofenceHome
                            VC.titleStr = titleHome
                        }
                        viewController.modalPresentationStyle = .fullScreen
                        self.present(viewController, animated: true, completion: nil)
                    }
                }else {
                    let  homeVC = UIStoryboard(name: "NewGeofence", bundle: nil)
                    let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController
                    var objNav: UINavigationController? = nil
                    
                    if let dashbord = dashbord {
                        objNav = dashbord
                    }
                    if let VC = dashbord?.topViewController as? NewGeofenceController{
                        VC.destinationString = Constant.geofenceHome
                        VC.titleStr = titleHome
                    }
                    if let objNav = objNav {
                        dashbord?.modalPresentationStyle = .fullScreen
                        self.present(objNav, animated: true)
                    }
                }
            }else {
                print("Add work location")
                
                
                
                // Geo Location
                
                if #available(iOS 13.0, *) {
                    if let viewController = UIStoryboard(name: "NewGeofence", bundle: nil).instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController{
                        if let VC = viewController.topViewController as? NewGeofenceController{
                            VC.destinationString = Constant.geofenceWork
                            VC.titleStr = titleWork
                        }
                        viewController.modalPresentationStyle = .fullScreen
                        self.present(viewController, animated: true, completion: nil)
                    }
                }else {
                    let  homeVC = UIStoryboard(name: "NewGeofence", bundle: nil)
                    let dashbord = homeVC.instantiateViewController(withIdentifier: "NewGeofence") as? UINavigationController
                    var objNav: UINavigationController? = nil
                    
                    if let dashbord = dashbord {
                        objNav = dashbord
                    }
                    if let VC = dashbord?.topViewController as? NewGeofenceController{
                        VC.destinationString = Constant.geofenceWork
                        VC.titleStr = titleWork
                    }
                    if let objNav = objNav {
                        dashbord?.modalPresentationStyle = .fullScreen
                        self.present(objNav, animated: true)
                    }
                }
                
                
                
                
                
            }
        }else if indexPath.section == 8 {
            // Log Out
            
            
            alert = UIAlertController(title: appName, message: "Are you sure you want to Logout?".localized, preferredStyle: .alert)
            
            yesButton = UIAlertAction(title: "Yes".localized, style: .default, handler: { action in
                
                //Stop Audio QR
                appDel.stopListening()
                
                ApplicationState.sharedAppState.blueToothMainClassObj.stop()
                ApplicationState.sharedAppState.currentUser.logout()
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                
                var navigationController: UINavigationController? = nil
                if let homeViewController = homeViewController {
                    navigationController = UINavigationController(rootViewController: homeViewController)
                }
                
                
                let window = UIApplication.shared.delegate?.window
                
                window?!.rootViewController = navigationController
                
                
                
            })
            
            noButton = UIAlertAction(title: "Cancel".localized, style: .default, handler: { action in
                
                
            })
            
            
            
            alert?.addAction(yesButton!)
            alert?.addAction(noButton!)
            present(alert!, animated: true)
            
            
        }else {
            //ccEngine.queueLive("")
            //ccEngine.queueTrigger(asNumber: 12345)
            //ccEngine.queueTrigger(asNumber: 4200)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            if isLanguageOpen {
                return 70
            }else {
                return 0
            }
        }else if indexPath.section == 6 {
            if isQRCodeOpen {
                return 70
            }else {
                return 0
            }
        }else {
            return 70
        }
    }
    
    @objc func OkAction() {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "Qurantine", bundle: nil)
                let baseObj = sb.instantiateViewController(identifier: "QuarantineQR")
                baseObj.modalPresentationStyle = .fullScreen
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(baseObj, animated: true, completion: nil)
                }
            } else {
                
                let  homeVC = UIStoryboard(name: "Qurantine", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "QuarantineQR") as? UINavigationController
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    //                  objNav = UINavigationController(rootViewController: dashbord)
                    objNav = dashbord
                }
                if let objNav = objNav {
                    dashbord?.modalPresentationStyle = .fullScreen
                    self.present(objNav, animated: true)
                }
            }
        }
    }
    
    @objc func cancelAction () {}
    
    func languageUpdated(key:String) {
        //Localization
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            ApplicationState.sharedAppState.currentUser.currentLanguage = "en"
            appDel.currentLanguage = "en"
            if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else{
            if let path =  Bundle.main.path(forResource: ApplicationState.sharedAppState.currentUser.currentLanguage, ofType: "lproj") {
                appDel.currentLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
                localBundle = Bundle(path: path)
            }
        }
        
        ApplicationState.sharedAppState.currentUser.currentLanguage = key
        
        appDel.setSeletedlocaLizationLanguage(language: key)
        updateMoreCellGUI()
        
        tvMore.reloadData()
    }
}

class moreCell : UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}

class languageCell : UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bulletIcon: UIImageView!
}


class qrNavigation : UINavigationController {
    
}
