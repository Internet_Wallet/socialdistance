//
//  GenerateQR.swift
//  BLEScanner
//
//  Created by Sam on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class GenerateQR: UIViewController {
    
    @IBOutlet weak var lblShopCategory: UILabel!{
        didSet {
            lblShopCategory.text = "Building Type".localized
        }
    }
    @IBOutlet weak var lblShopName: UILabel!{
        didSet {
            lblShopName.text = "Name".localized
        }
    }
    @IBOutlet weak var lblShopAddress: UILabel!{
        didSet {
            lblShopAddress.text = "Place".localized
        }
    }
    
    @IBOutlet weak var tfShopCategory: UITextField!{
        didSet {
            tfShopCategory.placeholder = "Eg: Home, Restaurants, Hotels etc.".localized
        }
    }
    @IBOutlet weak var tfShopName: UITextField!{
        didSet {
            tfShopName.placeholder = "Eg: John, Ka won etc.".localized
        }
    }
    @IBOutlet weak var tfShopAddress: UITextField!{
        didSet {
            tfShopAddress.placeholder = "Eg: Home, Restaurants, Hotels etc.".localized
        }
    }
    @IBOutlet weak var btnGenerateQR: UIButton!{
        didSet {
            btnGenerateQR.setTitle("Generate QR".localized, for: .normal)
        }
    }
    let apiObj = ApiRequestClass()
    let qrOjb = QRCodeGenericClass()
    var categoryList : OccupationModel?
    let categoryPicker = UIPickerView()
    var categoryID = ""
    var apiName = ""
    var myLanguage = ""
    var CHARSET = ""
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    var township : String?
    var devision : String?
    var googleAddress : String?
    var locationLat : String?
    var locationLong : String?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        
        if  ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            myLanguage = "my"
        }else{
            myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
        }
        
        
        self.loadInitialize()
        self.setMarqueLabelInNavigation()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20)!]
        tfShopCategory.delegate = self
        tfShopName.delegate = self
        tfShopAddress.delegate = self
        tfShopCategory.addPadding(padding: .left(5))
        tfShopName.addPadding(padding: .left(5))
        tfShopAddress.addPadding(padding: .left(5))
        tfShopAddress.addTarget(self, action: #selector(GenerateQR.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToRoot), name: NSNotification.Name(rawValue: "QRNatigateToRoot"), object: nil)
        DispatchQueue.main.async {
            self.callCategoryAPI()
        }
    }
    
    @objc func navigateToRoot() {
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "QRNatigateToRoot") , object: nil)
    }
    
    private func setMarqueLabelInNavigation() {
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.textColor = UIColor.white
        lblMarque.textAlignment = .center
        lblMarque.font =  UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 18)
        lblMarque.text = "Generate QR Code".localized
        self.navigationItem.titleView = nil
        self.navigationItem.titleView = lblMarque
    }
    
    func loadInitialize() {
        if myLanguage == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ "
        }else if myLanguage == "uni"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ "
        }else {
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
        }
        self.location()
    }
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        if let street = addressDic["street"]  {
                            self.township = street
                        }
                        if let region = addressDic["region"]  {
                            self.devision = region
                        }
                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                }
            }
        }
        else{
            showLocationDisabledpopUp()
        }
    }
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled".localized, withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                self.navigationController?.dismiss(animated: true , completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    
    
    @IBAction func onClickBackAction(_ : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickOnGenerateQRActionn(_ sender: UIButton) {
        self.view.endEditing(true)
        if tfShopCategory.text ?? "" == "" {
            Utility.alert(message: "", title: "Please enter Building Type".localized, controller: self)
            return
        }
        if tfShopName.text ?? "" == "" {
            Utility.alert(message: "", title: "Please enter Name".localized, controller: self)
            return
        }
        
        if tfShopAddress.text ?? "" == "" {
            Utility.alert(message: "", title: "Please enter Place".localized, controller: self)
            return
        }
        if self.view.contains(tvStreetList) {
            self.removeStreetList()
        }
        
        self.callGenerateQRAPI()
        
    }
    
}

extension GenerateQR: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfShopCategory   {
            textField.tintColor = .clear
            if self.categoryList?.detailObj.count ?? 0 == 0 {
                DispatchQueue.main.async {
                    self.callCategoryAPI()
                }
                return false
            }else {
                return true
            }
        }else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfShopCategory   {
            if textField.text == "" {
                categoryID = self.categoryList?.detailObj[safe : 0]?.id ?? ""
                textField.text = self.categoryList?.detailObj[safe : 0]?.name ?? ""
            }
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.setUpStreetList()
        self.showStreetView(text: textField.text ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if textField == tfShopName {
//            if textField.text?.count ?? 0 > 41 {
//               return false
//            }
        }else if textField == tfShopAddress {
//            if textField.text?.count ?? 0 > 41 {
//                return false
//            }
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
         if str == " " && textField.text?.last == " "{
             return false
         } else {
             if characterBeforeCursor(tf: textField) == " " && str == " " {
                 return false
             } else {
                 if characterAfterCursor(tf: textField) == " " && str == " " {
                     return false
                 } else {
                     return true
                 }
             }
         }
     }
     
     func characterBeforeCursor(tf : UITextField) -> String? {
         if let cursorRange = tf.selectedTextRange {
             if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                 let range = tf.textRange(from: newPosition, to: cursorRange.start)
                 return tf.text(in: range!)
             }
         }
         return nil
     }
     
     func characterAfterCursor(tf : UITextField) -> String? {
         if let cursorRange = tf.selectedTextRange {
             if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                 let range = tf.textRange(from: newPosition, to: cursorRange.start)
                 return tf.text(in: range!)
             }
         }
         return nil
     }
    
}

extension GenerateQR : StreetListViewDelegate {
    
    func setUpStreetList() {
        if !self.view.contains(tvStreetList)  {
            tvStreetList.frame = CGRect(x: 5, y: 64, width: self.view.frame.width - 10, height: 50)
            tvStreetList.backgroundColor = UIColor.clear
            tvStreetList.delegate = self
            tvStreetList.tvList.isHidden = true
            tvStreetList.isUserInteractionEnabled = true
            self.view.addSubview(tvStreetList)
            self.view.bringSubviewToFront(tvStreetList)
        }
    }
    
    func showStreetView(text: String) {
        if text.count > 0 {
            if self.view.contains(tvStreetList) {
            }else {
                setUpStreetList()
                tvStreetList.tvList.isHidden = false
            }
            tvStreetList.getAllAddressDetails(searchTextStr: text, yAsix: 0)
            self.view.bringSubviewToFront(tvStreetList)
        }else {
            if self.view.contains(tvStreetList) {
                self.removeStreetList()
            }
        }
    }
    
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        tfShopAddress.text = street_Name.address
        tfShopAddress.resignFirstResponder()
        self.removeStreetList()
    }
    
}

extension GenerateQR : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.categoryList?.detailObj.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        categoryID = self.categoryList?.detailObj[row].id ?? ""
        return self.categoryList?.detailObj[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tfShopCategory.text = self.categoryList?.detailObj[row].name ?? ""
        categoryID = self.categoryList?.detailObj[row].id ?? ""
    }
    
}

extension GenerateQR : ApiRequestProtocol {
    
    func callCategoryAPI() {
        if Utility.isNetworkAvailable(controller: self) {
            let dic = ["Mobilenumber": ApplicationState.sharedAppState.currentUser.MobileNumber,"simid": Constant.uuid,"Msid": NetWorkCode.getMSIDForSimCard().0[1],"AppId": NetWorkCode.getTheAppID()]
            print(dic)
            showLoader()
            apiObj.customDelegate = self
            self.apiName = "CategoryID"
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.getCategory())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
        }else {
            Utility.alert(message: "", title: "No Network Available".localized, controller: self)
        }
    }
    
    func callGenerateQRAPI() {
        if Utility.isNetworkAvailable(controller: self) {
            
            let currentLocation = GeofencingRegion.shared.currentLocation
            let lat  = currentLocation?.coordinate.latitude
            let long = currentLocation?.coordinate.longitude
            
            let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
            
            let dic = [
                "UserInfo":
                    ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID(),"Wifi": [["ID": "", "Name": "", "Self": "1"]],"Bluetooth":[["ID": "iPhone", "Name": "iPhone", "Self": "1"]],"GPS":RegisterParam.getGpsInfo(lat: self.locationLat ?? "", long: self.locationLong ?? "", cellid: "", mcc: "adads", mnc: "asdasd", hspambps: "", nearLocation: self.township ?? "", Township: self.township ?? "", divisionName: self.devision ?? "")],
                "QRInfo":
                    ["QRID": "","CategoryId": categoryID,"Latitude":lat ?? "","Longitude":long ?? "","Name":self.tfShopName.text ?? ""]
            ]
            print(dic)
            showLoader()
            apiObj.customDelegate = self
            self.apiName = "GenerateQR"
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.qrPost())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
        }else {
            Utility.alert(message: "", title: "No Network Available".localized, controller: self)
        }
    }
    
    
    
    
    func httpResponse(responseObj:Any,hasError: Bool) {
        hideLoader()
        
        if !hasError {
            if self.apiName == "CategoryID"  {
                let dataResponse = responseObj as! NSDictionary
                if dataResponse.count > 0 {
                    DispatchQueue.main.async {
                        self.categoryList = OccupationModel(dicData: dataResponse)
                        print(self.categoryList?.detailObj ?? [])
                        self.categoryPicker.delegate = self
                        self.categoryPicker.dataSource = self
                        self.categoryPicker.reloadAllComponents()
                        self.tfShopCategory.inputView = self.categoryPicker
                    }
                }
            }else {
                let dataResponse = responseObj as! NSDictionary
                if dataResponse.count > 0 {
                    DispatchQueue.main.async {
                        print("dataResponse: \(dataResponse)")
                        if let code = dataResponse["Code"] as? Int {
                            if code == 200 {
                                if let data = dataResponse["Data"] as? [Dictionary<String,String>] {
                                    let dic = data[0]
                                    if let qrID = dic["QRId"] {
                                        let strQRCode =  qrID
                                        print(strQRCode)
                                        ApplicationState.sharedAppState.currentUser.qrID = qrID
                                        ApplicationState.sharedAppState.currentUser.categoryID = self.categoryID
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "QRChangeName"), object: nil)
                                        DispatchQueue.main.async {
                                            if #available(iOS 13.0, *) {
                                                let sb = UIStoryboard(name: "QRCode", bundle: nil)
                                                let baseObj = sb.instantiateViewController(identifier: "GeneratedQR") as! GeneratedQR
                                                baseObj.modalPresentationStyle = .fullScreen
                                                self.navigationController?.pushViewController(baseObj, animated: true)
                                            } else {
                                                let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                                                let dashbord = homeVC.instantiateViewController(withIdentifier: "QR_Code_Generated") as? UINavigationController
                                                var objNav: UINavigationController? = nil
                                                if let dashbord = dashbord {
                                                    objNav = dashbord
                                                }
                                                if let objNav = objNav {
                                                    dashbord?.modalPresentationStyle = .fullScreen
                                                    self.present(objNav, animated: true)
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }else if code == 300 {
                                Utility.alert(message: "", title: "QR Already Exists".localized, controller: self)
                            }else {
                                Utility.alert(message: "", title: "API Failed".localized, controller: self)
                            }
                            
                        }else {
                            
                        }
                    }
                }
            }
        }else {
            Utility.alert(message: "", title: "API Failed".localized, controller: self)
        }
    }
    
}
