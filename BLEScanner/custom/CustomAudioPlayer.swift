//
//  CustomAudioPlayer.swift
//  BLEScanner
//
//  Created by Avaneesh Kumar on 08/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import AVKit

   var player: AVAudioPlayer?
   var isPlayingSound = false
   var session: AVAudioSession = AVAudioSession.sharedInstance()


extension UIViewController {
    
    func PlayAudioSoundForFIle( fileName : String , type : String , controller : UIViewController) {
        
        guard let url = Bundle.main.url(forResource: fileName , withExtension: type) else { return }
        do {
            if AVAudioSession.sharedInstance().isOtherAudioPlaying {
                
            }
            else {
                
                try session.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
                         try AVAudioSession.sharedInstance().setActive(true)
                         player = try AVAudioPlayer(contentsOf: url)
                         guard let player = player else { return }
                         player.prepareToPlay()
                         isPlayingSound = true
                         player.play()
                         player.delegate = controller as? AVAudioPlayerDelegate
                   }

                  } catch let error {
                      print(error.localizedDescription)
                }
           }
    
      }

extension NSObject {
   
func PlayAudioSoundForFIle( fileName : String , type : String , controller : NSObject) {
    
    guard let url = Bundle.main.url(forResource: fileName , withExtension: type) else { return }
    do {
        if AVAudioSession.sharedInstance().isOtherAudioPlaying {
            
        }
        else {
            
            try session.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
                     try AVAudioSession.sharedInstance().setActive(true)
                     player = try AVAudioPlayer(contentsOf: url)
                     guard let player = player else { return }
                     player.prepareToPlay()
                     isPlayingSound = true
                     player.play()
                     player.delegate = controller as? AVAudioPlayerDelegate
               }

              } catch let error {
                  print(error.localizedDescription)
            }
       }

  }

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}
