//
//  DataSynParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 04/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

struct DataSynParam {
    static func getDataSynParam(simOne: String,simTwo: String,bluetoothId: String,blueToothName: String,sourceBName: String,sourceName: String,sourceBId: String,sourceMobileNumber: String,gps: String,cellId: String,destinationName: String,destinationMobileNumber: String,wifiName: String,startingDate: String,endingDate: String,feet: String,nooftime: String) -> NSMutableDictionary {
              
        let dataSynParam = NSMutableDictionary()
        dataSynParam.setValue(simOne, forKey: "SIMID1")
        dataSynParam.setValue(simTwo, forKey: "SIMID2")
        dataSynParam.setValue(bluetoothId, forKey: "Bluetooth_ID")
        dataSynParam.setValue(blueToothName, forKey: "Bluetooth_Name")
        dataSynParam.setValue(sourceBName, forKey: "SOURCE_B_NAME")
        dataSynParam.setValue(sourceBId, forKey: "SOURCE_B_ID")
        dataSynParam.setValue(sourceName, forKey: "SOURCE_NAME")
        dataSynParam.setValue(sourceMobileNumber, forKey: "SOURCE_MOBILENO")
        dataSynParam.setValue(gps, forKey: "GPS")
        dataSynParam.setValue(cellId, forKey: "CELLID")
        dataSynParam.setValue(destinationName, forKey: "DESTINATION_NAME")
        dataSynParam.setValue(destinationMobileNumber, forKey: "DESTINATION_MOBILENumber")
        dataSynParam.setValue(wifiName, forKey: "WIFIName")
        dataSynParam.setValue(startingDate, forKey: "StartingDate")
        dataSynParam.setValue(endingDate, forKey: "EndingDate")
        dataSynParam.setValue(Int(feet), forKey: "Feet")
        dataSynParam.setValue(nooftime, forKey: "NoOfTime")
        dataSynParam.setValue(bluetoothId, forKey: "UltraSoundId")
        
        return dataSynParam
        
    }
    
    static func getSynUserParam(mobile: String, simID: String, msID: String, appID: String,sendsms:String) -> NSMutableDictionary{
        let synParam = NSMutableDictionary()
        synParam.setValue(mobile, forKey: "Mobilenumber")
        synParam.setValue(simID, forKey: "simid")
        synParam.setValue(msID, forKey: "Msid")
        synParam.setValue("1", forKey: "OSType")
        synParam.setValue(appID, forKey: "AppId")
        synParam.setValue(sendsms, forKey: "SendSms")
        return synParam
    }
    
    
  static  func makeParamForDataSync() -> NSMutableDictionary{
    var copyObj = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
    if ApplicationState.sharedAppState.currentUser.lastDateSynced == ""{
        ApplicationState.sharedAppState.currentUser.lastDateSynced = copyObj[copyObj.count - 1].bluetoothDate
    }else{
        copyObj.removeAll()
        let date = ApplicationState.sharedAppState.currentUser.lastDateSynced
        let totalSyncData = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
        copyObj = totalSyncData.filter({$0.bluetoothDate > date})
        ApplicationState.sharedAppState.currentUser.lastDateSynced = copyObj[copyObj.count - 1].bluetoothDate
    }
        
        var syncDataArray = [NSMutableDictionary]()
        var blueToothModelObj = [BluetoothDataModel]()
        let finalPostDic = NSMutableDictionary()
    
        if copyObj.count>0{
            for i in 0..<copyObj.count{
                let dateNew = copyObj[i].bluetoothDate.components(separatedBy: " ")[0]
                if dateNew == Utility.getDateForCoreData().components(separatedBy: " ")[0]{
                    blueToothModelObj.append(copyObj[i])
                }
            }
           
            let number = ApplicationState.sharedAppState.currentUser.MobileNumber
            if number != ""{
                for j in 0..<blueToothModelObj.count{
                    syncDataArray.append(DataSynParam.getDataSynParam(simOne: Constant.uuid, simTwo: Constant.uuid, bluetoothId: blueToothModelObj[j].bluetoothId, blueToothName:blueToothModelObj[j].bluetoothName, sourceBName: "", sourceName: "", sourceBId: "Not Available in iphone", sourceMobileNumber: number, gps: "", cellId: "", destinationName: "", destinationMobileNumber:"", wifiName: "WIFI", startingDate: Utility.makeDateFormatForSync(date: blueToothModelObj[j].bluetoothDate) , endingDate: Utility.getDateForDataSyn(), feet:String(blueToothModelObj[j].bluetoothDateLessThanSixFeet) , nooftime: blueToothModelObj[j].numberOfTimePeopleMeet ))
                }
                
                finalPostDic.setValue(DataSynParam.getSynUserParam(mobile: number, simID: Constant.uuid, msID: NetWorkCode.getMSIDForSimCard().0[1], appID: NetWorkCode.getTheAppID(),sendsms:"1"), forKey: "UserInfo")
                finalPostDic.setValue(syncDataArray, forKey: "SyncInfo")
            }
        }
        return finalPostDic
    }
   
}





