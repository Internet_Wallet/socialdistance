//
//  ApplicationSettingParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 12/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

struct ApplicationSettingParam {
    
    static func getParamForApplicationSetting(mobileNumber: String) -> NSMutableDictionary{
        let appSettingParam = NSMutableDictionary()
        appSettingParam.setValue(mobileNumber, forKey: "Mobilenumber")
        appSettingParam.setValue("1", forKey: "OSType")
        appSettingParam.setValue(Constant.uuid, forKey: "simid")
        appSettingParam.setValue(NetWorkCode.getMSIDForSimCard().0[1], forKey: "Msid")
        appSettingParam.setValue(NetWorkCode.getTheAppID(), forKey: "AppId")
        return appSettingParam
    }
}

