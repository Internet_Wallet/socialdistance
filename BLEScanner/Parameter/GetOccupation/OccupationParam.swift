//
//  OccupationParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
struct GetOccupationParam {
    
    static func getOccupationParam(mobile: String, simID: String, msID: String, appID: String) -> NSMutableDictionary{
        let occupationParam = NSMutableDictionary()
        occupationParam.setValue(mobile, forKey: "Mobilenumber")
        occupationParam.setValue(simID, forKey: "simID")
        occupationParam.setValue(msID, forKey: "msID")
        occupationParam.setValue(appID, forKey: "appID")
        return occupationParam
    }
}
