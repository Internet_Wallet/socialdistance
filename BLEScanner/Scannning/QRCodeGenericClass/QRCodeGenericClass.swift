//
//  QRCodeGenericClass.swift
//  BLEScanner
//
//  Created by Sam on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import CoreGraphics

class QRCodeGenericClass: NSObject {
    
    var qrImage : CIImage?
    public func qrStringGenerationForPrint(name: String, userID: String, categoryID: String,address: String) -> String {
        let currentLocation = GeofencingRegion.shared.currentLocation
       // let lat  = currentLocation?.coordinate.latitude
        //let long = currentLocation?.coordinate.longitude
        //let currentDateTimeStr = getCurrentDateForQRCode()
  
        let firstPart  = "00#" + "\(name)" + "%" + "\(userID)" + "@" + "\(categoryID)" + "&"
        let secondPart =  address + "β" //+ "\(String(describing: lat))" + "γ" + "\(String(describing: long))" + "α" + currentDateTimeStr
        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
        print("-------> \(finalPart)")
        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return "" }
        return hashedQRKey
    }
      
      public func generteQRCodeImage(qrStr : String) -> UIImage? {
          print("QRCode STR: \(qrStr)")
          guard let image =  getQRImage(stringQR: qrStr, withSize: 10) else {
              print("QRCode Image not Generated")
              return UIImage(named: "qr-codeside")
          }
          print("QRCode Image Generated")
          return image
      }
      
    private func getCurrentDateForQRCode() -> String {
          let dateFormatter : DateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
          let date = Date()
          let dateString = dateFormatter.string(from: date)
          return dateString
      }
      
      public func currentDateAndTime() -> (String,String){
          let date = Date()
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
          let currentDateString: String = dateFormatter.string(from: date)
          dateFormatter.dateFormat = "HH:mm:ss"
          let someDateTime: String = dateFormatter.string(from: date)
          return (currentDateString,someDateTime)
      }

      func getQRImage(stringQR: String, withSize rate: CGFloat) -> UIImage? {
          if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
              filter.setDefaults()
              if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                  filter.setValue(data, forKey: "inputMessage")
                  if let resultImage : CIImage = filter.outputImage {
                      let transform = CGAffineTransform(scaleX: 12, y: 12)
                      let translatedImage = resultImage.transformed(by: transform)
//                      guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
//                          return nil
//                      }
//                      guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
//                          return nil
//                      }
                      let context = CIContext.init(options: nil)
                      guard let qrWithLogoCG = context.createCGImage(translatedImage, from: translatedImage.extent) else {
                          return nil
                      }
                      var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                      let width  =  image.size.width * rate
                      let height =  image.size.height * rate
                      UIGraphicsBeginImageContext(.init(width: width, height: height))
                      let cgContext = UIGraphicsGetCurrentContext()
                      cgContext?.interpolationQuality = .none
                      image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                      guard let imageFromCurrentImageContext = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
                      image = imageFromCurrentImageContext
                      UIGraphicsEndImageContext()
                      return image
                  }
              }
          }
          return nil
      }
      
      
}
