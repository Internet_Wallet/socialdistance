//
//  StreetListView.swift
//  OK
//
//  Created by Imac on 4/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol StreetListViewDelegate {
  func streetNameSelected(street_Name: addressModel)
}

class StreetListView: CardView {
    
    @IBOutlet weak var tvList: UITableView!
    var delegate: StreetListViewDelegate?
    var arrFilterVillageData : [Dictionary<String,String>] = []
    var isSearch = true
    var addressArray = [addressModel]()
    var ok_default_language = ""
    var isComingFromWhereTo = false
    
    //prabu
    
    override func awakeFromNib() {

        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            ok_default_language = "my"
        }else{
            ok_default_language = ApplicationState.sharedAppState.currentUser.currentLanguage
        }
//        tvList.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height)
        tvList.backgroundColor = UIColor.white
        tvList.tableFooterView = UIView(frame: .zero)
        tvList.register(UINib(nibName: "StreetCell", bundle: nil), forCellReuseIdentifier: "StreetCell")
    }
    
    
    func getAllAddressDetails(searchTextStr : String, yAsix: CGFloat) {
        var urlString = ""
        if ok_default_language == "my"{
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=my&key=\(Google_API_KEY)"
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchTextStr)&components=country:mm&language=en&key=\(Google_API_KEY)"
        }
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let session = URLSession.shared
        if url != nil {
            let task = session.dataTask(with:url!) { (data, response, error) -> Void in
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,AnyObject> {
                            print("address dic---------\(dic)");
                            if let resultArray = dic["predictions"] as? [Dictionary<String,Any>] {
                                self.addressArray.removeAll()
                                DispatchQueue.main.async {
                                    for dic in resultArray {
                                        print("address dic11---------\(dic)"); self.addressArray.append(addressModel.init(dict: dic))
                                    }
                                    
                                        if self.addressArray.count > 5 {
                                            self.frame = CGRect(x: 10, y: yAsix, width: UIScreen.main.bounds.width - 20, height: 200)
                                            self.tvList.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 200)
                                        }else {
                                            self.frame = CGRect(x: 10, y: Int(yAsix), width: Int(UIScreen.main.bounds.width - 20), height: Int(self.addressArray.count * 50))
                                            self.tvList.frame = CGRect(x: 0, y: 0, width: Int(self.frame.width), height: Int(self.addressArray.count * 50))
                                        }
                                    
                                    if self.addressArray.count > 0 {
                                        DispatchQueue.main.async {
                                            self.tvList.isHidden = false
                                            self.tvList.delegate = self
                                            self.tvList.dataSource = self
                                            self.tvList.reloadData()
                                        }
                                    }else {
                                        self.tvList.isHidden = true
                                    }
                                }
                            }
                        }
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }else {
                    print("API NOT CALLING")
                }
            }
            task.resume()
        }
    }
}

extension StreetListView: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : StreetCell = tableView.dequeueReusableCell(withIdentifier: "StreetCell") as! StreetCell
        cell.lblFullAdress.isHidden = false
        cell.lblStreetName.isHidden = false
        if let obj = addressArray[safe: indexPath.row] {
            if ok_default_language == "my"{
                cell.lblStreetName.text = obj.shortName ?? "" //Rabbit.uni2zg(obj.shortName ?? "")
                cell.lblFullAdress.text = obj.address ?? "" //Rabbit.uni2zg(obj.address ?? "")
            }else {
                cell.lblStreetName.text =  obj.shortName ?? ""
                cell.lblFullAdress.text = obj.address ?? ""
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = addressArray[safe: indexPath.row]
        let streetName = dic?.shortName ?? "" //Rabbit.uni2zg(dic?.shortName ?? "")
        let address = dic?.address ?? ""//Rabbit.uni2zg(dic?.address ?? "")
        let id = dic?.placeID ?? ""
        if let del = delegate {
            var tempModel = addressModel(dict: Dictionary<String, String>())
            tempModel.shortName = streetName
            tempModel.address = address
            tempModel.placeID = id
            locationArr.append(tempModel)
            //del.streetNameSelected(street_Name: streetName)
            del.streetNameSelected(street_Name: tempModel)
        }
    }
}



struct addressModel {
    var shortName : String?
    var address : String?
    var placeID : String?
    init(dict : Dictionary<String, Any>)  {
        if let addressComponents = dict["structured_formatting"] as? Dictionary<String, Any>
        {
            if addressComponents.count > 0
            {
                self.shortName = addressComponents["main_text"] as? String ?? ""
            } else {
                self.shortName = ""
            }
        }
        self.address = dict["description"] as? String ?? ""
        self.placeID = dict["place_id"] as? String ?? ""
    }
}



class StreetCell : UITableViewCell {
    @IBOutlet weak var lblFullAdress: MarqueeLabel!
    @IBOutlet weak var lblStreetName: MarqueeLabel!
}
