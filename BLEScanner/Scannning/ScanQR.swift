//
//  ScanQR.swift
//  BLEScanner
//
//  Created by Sam on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import PhotosUI
import AVFoundation
import AudioToolbox


class ScanQR: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    let apiObj = ApiRequestClass()
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblHeader: UILabel!{
        didSet {
            lblHeader.text = "Scan QR Code".localized
        }
    }
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var containerViews: UIView!
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    let codeFrame:UIView = {
        // ffkljfklffdskljaf
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(named: "F3C632")?.cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Scan QR".localized
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
        }else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if !granted {
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: appName, message: "Need camera access to scan".localized, preferredStyle: .alert)
                        let back = UIAlertAction(title: "Setting".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                            if let url = URL(string: UIApplication.openSettingsURLString) {
                                if UIApplication.shared.canOpenURL(url) {
                                    _ =  UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                }
                            }
                        }
                        let scan = UIAlertAction(title: "Cancel", style: .default) { (alert: UIAlertAction!) -> Void in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(back)
                        alertController.addAction(scan)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }else {
                    self.loadCamera()
                }
            })
        }
        btnClose.layer.cornerRadius = btnClose.frame.width / 2
        btnClose.layer.masksToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(changeFlashLisghtIcon), name: NSNotification.Name(rawValue: "ChangFlashLightIcon"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnClose.isHidden = false
        lblHeader.isHidden = false
        btnFlash.isHidden = false
        btnFlash.setImage(UIImage(named: "flash_off"), for: .normal)
    }
    
    
    @objc func changeFlashLisghtIcon() {
        btnFlash.setImage(UIImage(named: "flash_off"), for: .normal)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ChangFlashLightIcon") , object: nil)
    }
    private func loadCamera() {
        
        
        DispatchQueue.global(qos: .background).async {
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    self.captureSession = AVCaptureSession()
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    self.captureSession?.startRunning()
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    if let session = self.captureSession {
                        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    }
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.containerViews.frame
                        if let viewVideo = self.videoPreviewLayer {
                            self.view.layer.addSublayer(viewVideo)
                        }
                        self.view.bringSubviewToFront(self.containerViews)
                        //self.view.bringSubviewToFront(self.buttonContainerView)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        //   self.removeActivityIndicator(view: self.view)
                    }
                }
            }
            DispatchQueue.main.async {
                //  self.removeActivityIndicator(view: self.view)
            }
        }
    }
    
    //MARK:- Delegate QR
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            print("no objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)
        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        codeFrame.frame = metaDataCoordinates.bounds
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        feedbackGenerator.impactOccurred()
        //                if self.captureSession.isRunning {
        //                    self.captureSession.stopRunning()
        //                }
        print(StringCodeValue)
        scannedString(str: StringCodeValue)
    }
    
    
    func scannedString(str: String) {
        
        if(str.count>0) {
            print("QR Code String = \(str)")
            let qrString = str.components(separatedBy: "@")
            
            if !str.contains("covid19@") {
                DispatchQueue.main.async {
                    if self.captureSession?.isRunning ?? false {
                        self.captureSession?.stopRunning()
                    }
                    let myalert = UIAlertController(title: "", message: "Invalid QR Code".localized, preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK".localized, style: .default) { (action:UIAlertAction!) in
                        self.captureSession?.startRunning()
                    })
                    self.present(myalert, animated: true)
                }
                return
            }else if ApplicationState.sharedAppState.currentUser.qrID == qrString[safe: 1] ?? "" {
                DispatchQueue.main.async {
                    if self.captureSession?.isRunning ?? false {
                        self.captureSession?.stopRunning()
                    }
                    let myalert = UIAlertController(title: "", message: "You scanned you own QR code. Please try another QR code".localized, preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK".localized, style: .default) { (action:UIAlertAction!) in
                        self.captureSession?.startRunning()
                    })
                    self.present(myalert, animated: true)
                }
                return
            }
            
            if qrString[safe: 1] ?? "" == "" {
                DispatchQueue.main.async {
                    if self.captureSession?.isRunning ?? false {
                        self.captureSession?.stopRunning()
                    }
                    let myalert = UIAlertController(title: "", message: "Invalid QR Code".localized, preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK".localized, style: .default) { (action:UIAlertAction!) in
                        self.captureSession?.startRunning()
                    })
                    self.present(myalert, animated: true)
                }
                return
            }else {
                self.callCategoryAPI(qrID: qrString[safe: 1] ?? "", catID: ApplicationState.sharedAppState.currentUser.categoryID)
            }
        } else {
            self.captureSession?.stopRunning()
            DispatchQueue.main.async {
                if self.captureSession?.isRunning ?? false {
                    self.captureSession?.stopRunning()
                }
                let myalert = UIAlertController(title: "", message: "Invalid QR Code".localized, preferredStyle: UIAlertController.Style.alert)
                myalert.addAction(UIAlertAction(title: "OK".localized, style: .default) { (action:UIAlertAction!) in
                    self.captureSession?.startRunning()
                })
                self.present(myalert, animated: true)
            }
            return
            
        }
        codeFrame.removeFromSuperview()
    }
    
    @IBAction func onClickCloseAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickFashAction() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
                self.btnFlash.setImage(UIImage(named: "flash_off"), for: .normal)
            }else {
                self.btnFlash.setImage(UIImage(named: "flash_on"), for: .normal)
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
}


extension ScanQR : ApiRequestProtocol {
    
    func callCategoryAPI(qrID : String, catID: String) {
        if Utility.isNetworkAvailable(controller: self) {
            
            if self.captureSession?.isRunning ?? false {
                self.captureSession?.stopRunning()
            }
            
            let currentLocation = GeofencingRegion.shared.currentLocation
            let lat  = currentLocation?.coordinate.latitude
            let long = currentLocation?.coordinate.longitude
         
            let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
            
            let dic = [
                "UserInfo":
                    ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID(),"Wifi": [["ID": "", "Name": "", "Self": "1"]],"Bluetooth":[["ID": "iPhone", "Name": "iPhone", "Self": "1"]],"GPS":RegisterParam.getGpsInfo(lat: "16.76611328125", long: "96.17500019032727", cellid: "", mcc: "adads", mnc: "asdasd", hspambps: "", nearLocation: "Vintage Luxury Yatch", Township: "BotaTown", divisionName: "Yangon")],
                "QRInfo":
                    ["QRID": qrID,"CategoryId":catID,"Latitude":lat ?? "","Longitude":long ?? ""]
            ]
            print(dic)
            showLoader()
            apiObj.customDelegate = self
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.qrScan())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
        }else {
            
        }
    }
    
    func httpResponse(responseObj:Any,hasError: Bool) {
        hideLoader()
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count > 0 {
                if let code = dataResponse["Code"] as? Int {
                    if code == 200 {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "", message: "User scanned successfully".localized, preferredStyle: .alert)
                            let back = UIAlertAction(title: "OK".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(back)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        
                    }else if code == 300 {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "", message: "User not found. Do you want to scan again".localized, preferredStyle: .alert)
                            let back = UIAlertAction(title: "Back".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                            }
                            let scan = UIAlertAction(title: "Scan".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                self.captureSession?.startRunning()
                            }
                            alertController.addAction(back)
                            alertController.addAction(scan)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }else {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "", message: "API Failed. Do you want to scan again".localized, preferredStyle: .alert)
                            let back = UIAlertAction(title: "Back".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                self.dismiss(animated: true, completion: nil)
                            }
                            let scan = UIAlertAction(title: "Scan".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                                self.captureSession?.startRunning()
                            }
                            alertController.addAction(back)
                            alertController.addAction(scan)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: "", message: "API Failed. Do you want to scan again".localized, preferredStyle: .alert)
                        let back = UIAlertAction(title: "Back".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                            self.dismiss(animated: true, completion: nil)
                        }
                        let scan = UIAlertAction(title: "Scan".localized, style: .default) { (alert: UIAlertAction!) -> Void in
                            self.captureSession?.startRunning()
                        }
                        alertController.addAction(back)
                        alertController.addAction(scan)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
        }else{
            DispatchQueue.main.async {
                Utility.alert(message: "", title: "API Failed".localized, controller: self)
            }
        }
    }
    
}
