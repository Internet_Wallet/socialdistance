//
//  MobileNumberViewController.swift
//  BLEScanner
//
//  Created by vamsi on 4/15/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class MobileNumberViewController: UIViewController,CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions {
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var navheadingLabel: UILabel!{
        didSet {
            navheadingLabel.text = "Reports".localized
        }
    }
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mobileNumTF: NoPasteTextField!{
        didSet {
            mobileNumTF.placeholder = "Enter Mobile Number".localized
        }
    }
    
    @IBOutlet var nameLabel: UILabel!
        {
        didSet {
            nameLabel.text = "Name".localized
        }
    }
    @IBOutlet var nameTF: UITextField! {
        didSet {
            nameTF.placeholder = "Enter Name".localized
        }
    }
    @IBOutlet var subheadingLabel: UILabel!{
        didSet {
            subheadingLabel.text = "Do you want ignore alert from this device?".localized
        }
    }
    @IBOutlet var checkmarkButton: UIButton!
    @IBOutlet var checkmarkLabel: UILabel!
    @IBOutlet var saveButton: UIButton!{
        didSet {
            self.saveButton.layer.cornerRadius = 4.0
            self.saveButton.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
            self.saveButton.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.saveButton.layer.shadowOpacity = 1.0
            self.saveButton.layer.shadowRadius = 4.0
            self.saveButton.layer.masksToBounds = false
            self.saveButton.setTitle("Save".localized, for: .normal)
        }
    }
    
    @IBOutlet var clearButton: UIButton!
    
    @IBOutlet var homeButton: UIButton!{
        didSet {
            self.homeButton.setTitle("Home".localized, for: .normal)
        }
    }
    
    @IBOutlet var workButton: UIButton!{
        didSet {
            self.workButton.setTitle("Work".localized, for: .normal)
        }
    }
    
    @IBOutlet var deviceButton: UIButton!{
        didSet {
            self.deviceButton.setTitle("Device".localized, for: .normal)
        }
    }
    @IBOutlet var othersButton: UIButton!{
        didSet {
            self.othersButton.setTitle("Other".localized, for: .normal)
        }
    }
    
    @IBOutlet var buttonsView: UIView!{
        didSet{
            self.buttonsView.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
            self.buttonsView.layer.shadowOffset = CGSize(width: 2, height: 2)
            self.buttonsView.layer.shadowOpacity = 1.0
            self.buttonsView.layer.shadowRadius = 2.0
            self.buttonsView.layer.masksToBounds = false
        }
    }
    
    var loginViewModel =  LoginViewModel()
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    var countryCodeStr = ""
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    var nameAcceptedChars  = ""
    var myLanguage = "en"
    var buttonSelect = ""
    var blueToothId = ""
    var blueToothDate = ""
    var category = ""
    
    
    let apiObj = ApiRequestClass()
    var mobileNumber = ""
    var dateTimeStr = ""
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navheadingLabel.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        titleLabel.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        mobileNumTF.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        nameLabel.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        nameTF.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        subheadingLabel.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        
        saveButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        homeButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        workButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        deviceButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        othersButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reports".localized
        self.titleLabel.text = "Enter Mobile Number".localized
        self.navigationController?.navigationBar.isHidden = true
        subheadingLabel.isHidden = true
        checkmarkButton.isHidden = true
        checkmarkLabel.isHidden = true
        if UserDefaults.standard.bool(forKey: "FromContact") {
            mobileNumTF.isUserInteractionEnabled = false
            nameTF.isUserInteractionEnabled = false
            if let dic = UserDefaults.standard.dictionary(forKey: "ContactDetails") as? Dictionary<String,String> {
                dismissKey()
                mobileNumTF.setPropertiesForTextField()
                clearButton.isHidden = true
                phNumValidationsFile = getDataFromJSONFile() ?? []
                self.country = Country.init(name: dic["country"] ?? "Myanmar", code: dic["flag"] ?? "myanmar", dialCode: dic["code"] ?? "+95")
                countryView = PaytoViews.updateView()
                let countryCode = String(format: "(%@)", dic["code"] ?? "+95")
                self.countryCodeStr = dic["code"] ?? "+95"
                countryView?.wrapCountryViewData(img: dic["flag"] ?? "myanmar", str: countryCode)
                countryView?.delegate = self
                self.mobileNumTF.leftViewMode = .always
                self.mobileNumTF.leftView = countryView
                self.mobileNumTF.text = dic["number"] ?? ""
                self.nameTF.text = dic["name"] ?? ""
            }
        }else {
            mobileNumTF.text = ""
            nameTF.text = ""
            mobileNumTF.isUserInteractionEnabled = true
            nameTF.isUserInteractionEnabled = true
            setUI()
        }
        setCheckMark()
        
        
        
        //        if let b_id = UserDefaults.standard.value(forKey: "Bluetooth_ID_Contact") as? String {
        //            if b_id != "" {
        //                let items = ApplicationState.sharedAppState.blueToothModelObj
        //                let index = items.indices.filter({ items[$0].bluetoothId ==  b_id})
        //                if index.count>0{
        //                    if items.indices.contains(index[0]) {
        //                        if ApplicationState.sharedAppState.blueToothModelObj[index[0]].userCategory == CustomNotification.DefaultValue.description{
        //                            checkmarkButton.setImage(UIImage(named: "uncheckMark"), for: .normal)
        //                            checkmarkLabel.text = "NO".localized
        //                        }else{
        //                            checkmarkButton.setImage(UIImage(named: "CheckMark"), for: .normal)
        //                            checkmarkLabel.text = "YES".localized
        //                        }
        //
        //                    }
        //                }
        //
        //            }
        //        }
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            myLanguage = "my"
        }else{
            myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
        }
        if myLanguage == "my"{
            nameAcceptedChars = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if myLanguage == "en"{
            nameAcceptedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else if myLanguage == "uni"{
            nameAcceptedChars = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else {
            nameAcceptedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }
        
    }
    
    func setCheckMark(){
        if blueToothDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]{
            let items = ApplicationState.sharedAppState.blueToothModelObj
            let index = items.indices.filter({ items[$0].bluetoothId ==  blueToothId})
            if index.count>0{
                if items.indices.contains(index[0]) {
                    if ApplicationState.sharedAppState.blueToothModelObj[index[0]].userCategory == CustomNotification.DefaultValue.description{
                        checkmarkButton.setImage(UIImage(named: "uncheckMark"), for: .normal)
                        checkmarkLabel.text = "NO".localized
                    }else{
                        checkmarkButton.setImage(UIImage(named: "CheckMark"), for: .normal)
                        checkmarkLabel.text = "YES".localized
                    }
                    
                }
            }
        }else{ // change in previous date dp
            let items = ApplicationState.sharedAppState.previousReportsArray
            let index = items.indices.filter({ items[$0].bluetoothId ==  blueToothId && items[$0].bluetoothDate.components(separatedBy: " ")[0] == blueToothDate.components(separatedBy: " ")[0]})
            if index.count>0{
                if items.indices.contains(index[0]) {
                    if ApplicationState.sharedAppState.previousReportsArray[index[0]].userCategory == CustomNotification.DefaultValue.description{
                        checkmarkButton.setImage(UIImage(named: "uncheckMark"), for: .normal)
                        checkmarkLabel.text = "NO".localized
                    }else{
                        checkmarkButton.setImage(UIImage(named: "CheckMark"), for: .normal)
                        checkmarkLabel.text = "YES".localized
                    }
                }
            }
            
        }
    }
    
    func clearButtonShowHide(count: Int) {
        if count > 0 {
            self.clearButton.isHidden = false
        } else {
            self.clearButton.isHidden = true
        }
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearButtonClick(_ sender: Any) {
        self.clearButton.isHidden = true
        mobileNumTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        self.mobileNumTF.becomeFirstResponder()
    }
    
    
    @IBAction func checkMarkClick(_ sender: Any) {
        if checkmarkButton.image(for: .normal)?.isEqual(UIImage(named: "uncheckMark")) ?? false {
            checkmarkButton.setImage(UIImage(named: "CheckMark"), for: .normal)
            checkmarkLabel.text = "YES".localized
            category = CustomNotification.Others.description
           // saveDataToDataBase(blueToothID: blueToothId, blueToothDate: blueToothDate, category: CustomNotification.Others.description)
        }else {
            checkmarkLabel.text = "NO".localized
            checkmarkButton.setImage(UIImage(named: "uncheckMark"), for: .normal)
        }
    }
    
    @IBAction func homecheckClick(_ sender: Any) {
        
        homeButton.backgroundColor = UIColor.systemBlue
        homeButton.setTitleColor(.white, for: .normal)
        workButton.backgroundColor = UIColor.white
        workButton.setTitleColor(.black, for: .normal)
        deviceButton.backgroundColor = UIColor.white
        deviceButton.setTitleColor(.black, for: .normal)
        othersButton.backgroundColor = UIColor.white
        othersButton.setTitleColor(.black, for: .normal)
        subheadingLabel.isHidden = true
        checkmarkButton.isHidden = true
        checkmarkLabel.isHidden = true
        buttonSelect = "1"
        category = CustomNotification.Home.description
      //  saveDataToDataBase(blueToothID: blueToothId, blueToothDate: blueToothDate, category: CustomNotification.Home.description)
    }
    
    @IBAction func workcheckClick(_ sender: Any) {
        
        workButton.backgroundColor = UIColor.systemBlue
        workButton.setTitleColor(.white, for: .normal)
        homeButton.backgroundColor = UIColor.white
        homeButton.setTitleColor(.black, for: .normal)
        deviceButton.backgroundColor = UIColor.white
        deviceButton.setTitleColor(.black, for: .normal)
        othersButton.backgroundColor = UIColor.white
        othersButton.setTitleColor(.black, for: .normal)
        subheadingLabel.isHidden = true
        checkmarkButton.isHidden = true
        checkmarkLabel.isHidden = true
        buttonSelect = "1"
        category = CustomNotification.Office.description
       // saveDataToDataBase(blueToothID: blueToothId, blueToothDate: blueToothDate, category: CustomNotification.Office.description)
        
        
    }
    @IBAction func devicecheckClick(_ sender: Any) {
        deviceButton.backgroundColor = UIColor.systemBlue
        deviceButton.setTitleColor(.white, for: .normal)
        homeButton.backgroundColor = UIColor.white
        homeButton.setTitleColor(.black, for: .normal)
        workButton.backgroundColor = UIColor.white
        workButton.setTitleColor(.black, for: .normal)
        othersButton.backgroundColor = UIColor.white
        othersButton.setTitleColor(.black, for: .normal)
        subheadingLabel.isHidden = true
        checkmarkButton.isHidden = true
        checkmarkLabel.isHidden = true
        buttonSelect = "1"
        category = CustomNotification.Devices.description
       // saveDataToDataBase(blueToothID: blueToothId, blueToothDate: blueToothDate, category: CustomNotification.Devices.description)
        
    }
    
    @IBAction func otherscheckClick(_ sender: Any) {
        othersButton.backgroundColor = UIColor.systemBlue
        othersButton.setTitleColor(.white, for: .normal)
        homeButton.backgroundColor = UIColor.white
        homeButton.setTitleColor(.black, for: .normal)
        deviceButton.backgroundColor = UIColor.white
        deviceButton.setTitleColor(.black, for: .normal)
        workButton.backgroundColor = UIColor.white
        workButton.setTitleColor(.black, for: .normal)
        
        subheadingLabel.isHidden = false
        checkmarkButton.isHidden = false
        checkmarkLabel.isHidden = false
        buttonSelect = "1"
        category = CustomNotification.Others.description
       // saveDataToDataBase(blueToothID: blueToothId, blueToothDate: blueToothDate, category: CustomNotification.Others.description)
    }
    
    func saveDataToDataBase(blueToothID: String,blueToothDate: String,category: String){
        
        //we will make changes in current Model
        if blueToothDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]{
            let items = ApplicationState.sharedAppState.blueToothModelObj
            let index = items.indices.filter({ items[$0].bluetoothId ==  blueToothID})
            if index.count>0{
                if items.indices.contains(index[0]) {
                    ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
                    ApplicationState.sharedAppState.blueToothModelObj.insert(BluetoothDataModel(name: self.nameTF.text ?? "", id: items[index[0]].bluetoothId, bluetoothDate: items[index[0]].bluetoothDate, bluetoothDateLessThanSixFeet: items[index[0]].bluetoothDateLessThanSixFeet, peopleMeet: items[index[0]].peopleMeet, userCategory: category, numberOfTimePeopleMeet: items[index[0]].numberOfTimePeopleMeet, rssiID: items[index[0]].rssiID, mobileNumber: self.mobileNumTF.text ?? ""), at: index[0])
                }
            }
        }else{ // change in previous date dp
            let items = ApplicationState.sharedAppState.previousReportsArray
            let index = items.indices.filter({ items[$0].bluetoothId ==  blueToothID && items[$0].bluetoothDate.components(separatedBy: " ")[0] == blueToothDate.components(separatedBy: " ")[0]})
            if index.count>0{
                if items.indices.contains(index[0]) {
                    ApplicationState.sharedAppState.previousReportsArray.remove(at: index[0])
                    ApplicationState.sharedAppState.previousReportsArray.insert(BluetoothDataModel(name: self.nameTF.text ?? "", id: items[index[0]].bluetoothId, bluetoothDate: items[index[0]].bluetoothDate, bluetoothDateLessThanSixFeet: items[index[0]].bluetoothDateLessThanSixFeet, peopleMeet: items[index[0]].peopleMeet, userCategory: category, numberOfTimePeopleMeet: items[index[0]].numberOfTimePeopleMeet, rssiID: items[index[0]].rssiID, mobileNumber: self.mobileNumTF.text ?? ""), at: index[0])
                }
            }
        }
    }
    
    @IBAction func saveClick(_ sender: Any) {
        if mobileNumTF.text?.count ?? 0 < 3 {
            if countryCodeStr != "+95" {
                if mobileNumTF.text?.count ?? 0 > 0 {
                    Utility.alert(message: "", title: "Please enter valid number".localized, controller: self)
                    return
                }
            }
        }else {
            if countryCodeStr == "+95" {
                let object = validObj.getNumberRangeValidation(mobileNumTF.text ?? "")
                if !(mobileNumTF.text?.count ?? 0 >= object.min && mobileNumTF.text?.count ?? 0 <= object.max) {
                    Utility.alert(message: "", title: "Please enter valid number".localized, controller: self)
                    return
                }
            }else {
                if mobileNumTF.text?.count ?? 0 < 4  {
                    Utility.alert(message: "", title: "Please enter valid number".localized, controller: self)
                    return
                }
            }
        }
        
        if nameTF.text == "" {
            Utility.alert(message: "", title: "Please enter contact name".localized, controller: self)
            return
        }
        if buttonSelect != "1"
        {
            Utility.alert(message: "", title: "Please select the below option to ignore notification".localized, controller: self)
            return
        }
        
        var mobileNum = ""
        if countryCodeStr == "+95" && (mobileNumTF.text?.count ?? 0 <= 2) {
            mobileNum = ""
        }else {
            if countryCodeStr != "+95" && mobileNumTF.text?.count ?? 0 == 0 {
                mobileNum = ""
            }else {
                mobileNum = "(" + self.countryCodeStr + ")" + (self.mobileNumTF.text ?? "")
            }
        }
        
        
        
        var chennalName = "0"
        if blueToothId.count > 9 {
            chennalName = "1"
        }
        
        let ind = ApplicationState.sharedAppState.blueToothModelObj.indices.filter({ ApplicationState.sharedAppState.blueToothModelObj[$0].bluetoothId ==  blueToothId})
        let name = ApplicationState.sharedAppState.blueToothModelObj[ind[0]].bluetoothName
        self.dateTimeStr = Utility.makeDateFormatForAddFamily(date: blueToothDate )
        self.mobileNumber = mobileNum
        self.callBlockBluetoothDevice(b_name: name, b_id: blueToothId, b_chennal: chennalName, block: "1", c_name: self.nameTF.text ?? "", c_nubmer: mobileNum)
    }
    
    func callBlockBluetoothDevice(b_name: String,b_id : String, b_chennal:String, block: String, c_name: String, c_nubmer: String) {
        let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
        var bluetooth = Dictionary<String,Any>()
        var ultrasound = Dictionary<String,Any>()
        var arrB = [Any]()
        var arrU = [Any]()
        if b_chennal == "0" {
            //UltraSound
            ultrasound = ["UltraSoundName":b_name,"UltraSoundid":b_id,"CategoryId": "54ad5797-189f-418c-9c7f-15a0e89c9e2c","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
            arrU.append(ultrasound)
        }else {
            // Bluetotth
            bluetooth = ["BluetoothName":b_name,"Bluetoothid":b_id,"CategoryId": "f1b171b8-b481-4b29-a5e1-d8ee55109613","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
            arrB.append(bluetooth)
        }
        
        let dic = [ "UserDetails":
            ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
            ],
                    "DeviceDetails":[
                        "BlueTooth": arrB,
                        "UltraSound": arrU
            ]
            ] as [String : Any]
        showLoader()
        print(dic)
        apiObj.customDelegate = self
        apiObj.sendHttpRequest(requestUrl: URL(string:Constant.getBlutoothBlock())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}


extension MobileNumberViewController :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool) {
        hideLoader()
        //success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count > 0 {
                DispatchQueue.main.async {
                    if let code = dataResponse["Code"] as? Int {
                        if code == 200 {
                            
                            
                            
                            
                            let items = ApplicationState.sharedAppState.blueToothModelObj
                            let index = items.indices.filter({ items[$0].bluetoothId ==  self.blueToothId})
                            if index.count > 0 {
                                if items.indices.contains(index[0]) {
                                    
                                    self.saveDataToDataBase(blueToothID: self.blueToothId, blueToothDate: self.blueToothDate, category: self.category)
                                    self.category = ""
                                    
                                    
//                                    ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
//                                    ApplicationState.sharedAppState.blueToothModelObj.insert(BluetoothDataModel(name: self.nameTF.text ?? "", id: items[index[0]].bluetoothId, bluetoothDate: items[index[0]].bluetoothDate, bluetoothDateLessThanSixFeet: items[index[0]].bluetoothDateLessThanSixFeet, peopleMeet: items[index[0]].peopleMeet, userCategory: items[index[0]].userCategory, numberOfTimePeopleMeet: items[index[0]].numberOfTimePeopleMeet, rssiID: items[index[0]].rssiID, mobileNumber: self.mobileNumber), at: index[0])
                                }
                            }
                            self.dismiss(animated: true, completion: nil)
                            self.dateTimeStr = ""
                            // self.showMyAlert(title: "Notification is Ignored".localized)
                        }else {
                            DispatchQueue.main.async {
                                Utility.alert(message: "", title: "API Failed".localized, controller: self)
                            }
                        }
                    }else {
                        DispatchQueue.main.async {
                            Utility.alert(message: "", title: "API Failed".localized, controller: self)
                        }
                    }
                }
            }
        }else {
            //self.showMyAlert(title: "API Failed".localized)
        }
    }
}
extension MobileNumberViewController{
    
    func setUI() {
        dismissKey()
        mobileNumTF.setPropertiesForTextField()
        clearButton.isHidden = true
        phNumValidationsFile = getDataFromJSONFile() ?? []
        self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
        countryView = PaytoViews.updateView()
        let countryCode = String(format: "(%@)", "+95")
        self.countryCodeStr = "+95"
        countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
        countryView?.delegate = self
        self.mobileNumTF.leftViewMode = .always
        self.mobileNumTF.leftView = countryView
        
    }
    
}

extension MobileNumberViewController: CountryViewControllerDelegate {
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.country = country
        self.mobileNumTF.leftView = nil
        self.clearButton.isHidden = true
        self.countryCodeStr = country.dialCode
        let countryCode = String.init(format: "(%@)", country.dialCode)
        countryView?.wrapCountryViewData(img: country.code, str: countryCode)
        self.mobileNumTF.leftView = countryView
        list.dismiss(animated: true, completion: nil)
        
        switch self.country?.dialCode {
        case "+91":
            currentSelectedCountry = .indiaCountry
        case "+95":
            currentSelectedCountry = .myanmarCountry
        case "+66":
            currentSelectedCountry = .thaiCountry
        case "+86":
            currentSelectedCountry = .chinaCountry
        default:
            currentSelectedCountry  = .other
        }
        
        self.mobileNumTF.text = (self.country?.dialCode == "+95") ? "09" : ""
        self.mobileNumTF.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func publicCountryView() {
        guard let countryVC = countryViewController(delegate: self) else { return }
        countryVC.modalPresentationStyle = .fullScreen
        self.present(countryVC, animated: true, completion: nil)
    }
    
}

//MARK:- TextField Delegate & Functions
extension MobileNumberViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //        textField.text =  (self.country?.dialCode == "+95") ? "09" : ""
        //         self.clearButton.isHidden = true
        //         self.loginButton.backgroundColor = .lightGray
        if textField == mobileNumTF{
            if  currentSelectedCountry == .myanmarCountry  {
                
                if let text = textField.text, text.count < 3 {
                    textField.text = "09"
                    self.clearButton.isHidden = true
                    
                }
            }
        }
        else {
            // textField.text = ""
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if self.country?.dialCode == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobileNumTF {
            
            if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
                let endOfDocument = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
                return false
            }
            
            if currentSelectedCountry == .myanmarCountry {
                if range.location <= 1 {
                    let newPosition = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                    return false
                }
            }
            
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            switch currentSelectedCountry {
            case .indiaCountry:
                
                let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount  = text.count
                clearButtonShowHide(count: textCount)
                if textField.text!.count >= 10 && range.length == 0 {
                    return false
                }
                if textCount >= 10 {
                    textField.text = text
                    self.mobileNumTF.resignFirstResponder()
                    return false
                }else {
                    return true
                }
            case .chinaCountry:
                
                let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount = text.count
                clearButtonShowHide(count: textCount)
                if textField.text!.count >= 11 && range.length == 0 {
                    return false
                }
                
                if textCount >= 11  {
                    textField.text = text
                    self.mobileNumTF.resignFirstResponder()
                    return false
                }else {
                    return true
                }
            case .myanmarCountry:
                if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0) {
                    return false
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    self.clearButton.isHidden = true
                    return false
                }
                
                let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount = text.count
                if textCount > 2 {
                    self.clearButton.isHidden = false
                }else {
                    self.clearButton.isHidden = true
                }
                
                let object = validObj.getNumberRangeValidation(text)
                
                let char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92) {
                    if textField.text == "09" {
                        textField.text = "09"
                        self.clearButton.isHidden = true
                        return false
                    }
                }
                
                if object.isRejected == true {
                    textField.text = "09"
                    self.clearButton.isHidden = true
                    textField.resignFirstResponder()
                    
                    let text = "Invalid Mobile Number".localized
                    Utility.alert(message: text, title: "", controller: self)
                    return false
                }
                if textCount < object.min {
                } else if (textCount >= object.min && textCount < object.max) {
                } else if (textCount == object.max) {
                    textField.text = text
                    self.mobileNumTF.resignFirstResponder()
                    return false
                } else if textCount > object.max {
                    return false
                }
            case .thaiCountry:
                
                let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount = text.count
                clearButtonShowHide(count: textCount)
                if textField.text!.count >= 9 && range.length == 0 {
                    return false
                }
                
                if textCount >= 9  {
                    textField.text = text
                    self.mobileNumTF.resignFirstResponder()
                    return false
                }else {
                    return true
                }
                
            case .other:
                
                let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let textCount = text.count
                clearButtonShowHide(count: textCount)
                
                if textField.text!.count >= 13 && range.length == 0 {
                    return false
                }
                
                if textCount >= 4 {
                    textField.text = text
                    if textCount == 13 {
                        textField.text = text
                        self.mobileNumTF.resignFirstResponder()
                    }
                    return false
                }
                
                if textCount > 13 {
                    textField.text = text
                    self.mobileNumTF.resignFirstResponder()
                    return false
                }else {
                    return true
                }
            }
        }else {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: nameAcceptedChars).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            if string != filteredSet { return false }
            
            if text.count > 40 {
                return false
            }
        }
        return true
    }
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
        if str == " " && textField.text?.last == " "{
            return false
        } else {
            if characterBeforeCursor(tf: textField) == " " && str == " " {
                return false
            } else {
                if characterAfterCursor(tf: textField) == " " && str == " " {
                    return false
                } else {
                    return true
                }
            }
        }
    }
    
    func characterBeforeCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
    func characterAfterCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
}
