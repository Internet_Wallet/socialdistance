//
//  NetworkCode.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony

struct NetWorkCode {
    static let serviceTypePost = "POST"
      static func getTheAppID() -> String {

//            _ = UIDevice.current.name as String
//            let deviceModel = UIDevice.current.model
//            let systemVersion = UIDevice.current.systemVersion
//
//        let appID = "0," + "1" + "," + Constant.uuid + "," + deviceModel + "," + deviceModel + "," + systemVersion +  "," + "0" + "," + "0" + "," + Constant.uuid
            
            return "1"

        }
    
    //Get sim network Type
      static func getNetworkType() -> String{
          var networkType = ""
          let networkInfo = CTTelephonyNetworkInfo()
          let networkString = networkInfo.currentRadioAccessTechnology
          
          if isConnectedToWifi(){
              return "Wifi"
          }else{
              if networkString == CTRadioAccessTechnologyLTE{
                  networkType = "4G"
                  // LTE (4G)
              }else if networkString == CTRadioAccessTechnologyWCDMA || networkString == CTRadioAccessTechnologyHSDPA{
                  networkType = "3G"
                  // 3G
              }else if networkString == CTRadioAccessTechnologyEdge{
                  networkType = "2G"
                  // EDGE (2G)
              }
              return networkType
          }
         
      }
      
      
      //Get the sim card operator name
      static func getSimName() -> String {
          var carrierName: String?
          let typeName: (Any) -> String = { String(describing: type(of: $0)) }
          let statusBar = UIApplication.shared.value(forKey: "_statusBar") as! UIView
          for statusBarForegroundView in statusBar.subviews {
              if typeName(statusBarForegroundView) == "UIStatusBarForegroundView" {
                  for statusBarItem in statusBarForegroundView.subviews {
                      if typeName(statusBarItem) == "UIStatusBarServiceItemView" {
                          carrierName = (statusBarItem.value(forKey: "_serviceString") as! String)
                      }
                  }
              }
          }
          return carrierName ?? ""
      }
      
      
      //Get Wifi Details
      static func getNetworkInfos() -> [String] {
          guard let interfaceNames = CNCopySupportedInterfaces() as? [String] else {
              return []
          }
          return interfaceNames.compactMap{ name in
              guard let info = CNCopyCurrentNetworkInfo(name as CFString) as? [String:AnyObject] else {
                  return nil
              }
              guard let ssid = info[kCNNetworkInfoKeySSID as String] as? String else {
                  return nil
              }
              guard let bccid = info[kCNNetworkInfoKeyBSSID as String] as? String else {
                  return nil
              }
              return "\(ssid),\(bccid)"
          }
      }
      
      
    static func isConnectedToWifi() -> Bool{
          if getNetworkInfos().count == 0{
              return false
          }
          return true
      }
      
     static func getSignalStrength() -> String {
          let url = URL(string: "https://www.google.com/")
          let request = URLRequest(url: url!)
          let session = URLSession.shared
          let startTime = Date()
          var finalValue = ""
          let task =  session.dataTask(with: request) { (data, resp, error) in
              
              guard error == nil && data != nil else{
                  print("connection error or data is nill")
                  return
              }
              
              guard resp != nil else{
                  print("respons is nill")
                  return
              }
              
              let length  = CGFloat( (resp?.expectedContentLength)!) / 1000000.0
              let elapsed = CGFloat( Date().timeIntervalSince(startTime))
              //  print("elapsed: \(elapsed)")
              //  print("Speed: \(length/elapsed) Mb/sec")
              
              finalValue = String(format: "%.2f", (length/elapsed))
              
              
          }
          
          
          task.resume()
          
          return finalValue
      }
    
    static func checkifSimAvailableInPhone() -> Bool{
            var isSuccess = false
            var primaryNetworkCode = ""
            
            
            if #available(iOS 13.0, *) {
             
                if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                   
                    if data.count == 1{
                        if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" {
                            isSuccess = false
                        }else {
                            primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                            
                            isSuccess = true
                        }
                    }else{
                        if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                            isSuccess = false
                        }else {
                            primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                            isSuccess = true
                        }
                    }
                }
            } else {
                if #available(iOS 12.0, *) {
                  
                    if  CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders?.count ?? 0 > 0{
                        if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                            print(data)
                            if data.count == 1{
                                if getMsid() == ""{
                                    isSuccess = false
                                }else{
                                    primaryNetworkCode = getMsid()
                                    isSuccess = true
                                }
                            }else if data.count == 2{
                                if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                                    isSuccess = false
                                }else {
                                    primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                                    isSuccess = true
                                }
                            }
                        }
                    }
                }
                else {
                    let telephony = CTTelephonyNetworkInfo()
                                  if let data = telephony.subscriberCellularProvider{
                                      print(data)
                                      if data != nil {
                                               if getMsid() == ""{
                                                         isSuccess = false
                                                     }else{
                                                         primaryNetworkCode = getMsid()
                                                         isSuccess = true
                                                     }
                                           }
                                      }
                    
                      }
            }
           print(primaryNetworkCode)
            return isSuccess
        }
    
    
   static func getMsid() -> String {
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        if let mnc = carrier?.mobileNetworkCode {
            return mnc
        }
        return ""
    }
    
    
    static func getMSIDForSimCard() -> ([String],[String]){
        var primaryNetworkCode = [String]()
        var operatorName = [String]()
        primaryNetworkCode.removeAll()
        operatorName.removeAll()
        
        if #available(iOS 13.0, *) {
            if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                if data.count == 1{
                    primaryNetworkCode.append("1")
                        primaryNetworkCode.append(data["0000000100000001"]?.mobileNetworkCode ?? "")
                       operatorName.append(data["0000000100000001"]?.carrierName ?? "")
                        
                }else{
                    primaryNetworkCode.append("2")
                    primaryNetworkCode.append(data["0000000100000001"]?.mobileNetworkCode ?? "")
                    operatorName.append(data["0000000100000001"]?.carrierName ?? "")
                    primaryNetworkCode.append(data["0000000100000002"]?.mobileNetworkCode ?? "")
                    operatorName.append(data["0000000100000002"]?.carrierName ?? "")
                }
            }
        } else {
            if #available(iOS 12.0, *) {
                if  CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders?.count ?? 0 > 0{
                    if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                        print(data)
                        if data.count == 1{
                            primaryNetworkCode.append("1")
                            primaryNetworkCode.append(getMsid())
                            operatorName.append(data["0000000100000001"]?.carrierName ?? "")
                        }else if data.count == 2{
                            primaryNetworkCode.append("2")
                            primaryNetworkCode.append(data["0000000100000001"]?.mobileNetworkCode ?? "")
                             operatorName.append(data["0000000100000001"]?.carrierName ?? "")
                            primaryNetworkCode.append(data["0000000100000002"]?.mobileNetworkCode ?? "")
                            operatorName.append(data["0000000100000002"]?.carrierName ?? "")
                        }
                    }
                }
            }
            else {
                
                let telephony = CTTelephonyNetworkInfo()
                if let data = telephony.subscriberCellularProvider{
                    print(data)
                    if data != nil {
                        primaryNetworkCode.append(data.mobileNetworkCode!)
                        primaryNetworkCode.append(getMsid())
                        operatorName.append(data.carrierName ?? "")
                    }
                }
            }
        }
        return (primaryNetworkCode,operatorName)
    }

}
