//
//  CustomAlertViewController.swift
//  BlueToothScreen
//
//  Created by vamsi on 4/3/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var customView: UIView!
    @IBOutlet var customalertTableView: UITableView!
    @IBOutlet var customTableHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        customView.layer.cornerRadius = 50
        customView.layer.masksToBounds = true
        
        customalertTableView.layer.cornerRadius = 40
        customalertTableView.layer.masksToBounds = true
        
        customTableHeightConstraint.constant = 320
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let identifier = "HeaderTableViewCell"
        var cellView: HeaderTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeaderTableViewCell
        if cellView == nil {
            tableView.register (UINib(nibName: "HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? HeaderTableViewCell
        }
        //cellView.imgIcon.image = UIImage(named: "bank")
        
        return cellView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let identifier = "FooterTableViewCell"
        var cellView: FooterTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? FooterTableViewCell
        if cellView == nil {
            tableView.register (UINib(nibName: "FooterTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? FooterTableViewCell
        }
        //cellView.imgIcon.image = UIImage(named: "bank")
        
        return cellView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      //  if indexPath.row == 1 {
      //      let cell = tableView.dequeueReusableCell(withIdentifier: "TitleTableViewCell") as! TitleTableViewCell
            //cell.serviceNameLabel.text = "\(phonenumberArray[indexPath.row])"
            
       //     return cell
     //   } else if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleImageTableViewCell") as! TitleImageTableViewCell
            //cell.serviceNameLabel.text = "\(phonenumberArray[indexPath.row])"
            
            return cell
     //   } else {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleTableViewCell") as! TitleTableViewCell
            //cell.serviceNameLabel.text = "\(phonenumberArray[indexPath.row])"
            
     //       return cell
      //  }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     //   if indexPath.row == 0
    //    {
            return 110
     //   }
    //    else if indexPath.row == 1{
            
    //        return 100
    //    }
    //    else{
    //        return 60
    //    }
    }
    
    
 
    
}
