//
//  ReportListViewController.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class ReportListViewController: UIViewController {
 var blueToothModelObj = [BluetoothDataModel]()
    var dateToSort = ""
   
    @IBOutlet weak var reportTable: UITableView!
    @IBOutlet var detailLabel: UILabel!{
           didSet {
               detailLabel.text = "Nearby Person".localized
           }
       }
    @IBOutlet var lblScanning: UILabel!{
        didSet {
            lblScanning.text = "Bluetooth starts discovering...".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    let btn = UIButton(type: .custom)
    
    //invoking timer for regular 7 sec
    var timer = Timer()
    var scanningButtonFired = false
    var timerCount = 1.0
    
    
    override func viewDidLoad() {

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]

        super.viewDidLoad()
          self.navigationController?.navigationBar.isHidden = true
        
        tablebottomContraint.constant = -20
        self.floatingButton()
        
        if ApplicationState.sharedAppState.currentUserCount.count + ApplicationState.sharedAppState.currentAudioUserCount.count>0{
            timerCount = 10.0
        }else{
            timerCount = 1.0
        }
        
        scheduledTimerWithTimeInterval()

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 7 seconds
        timer = Timer.scheduledTimer(timeInterval: timerCount, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        
        getDataFromCurrentUser()
        
//      //  if scanningButtonFired{
//            btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-150, width: 40, height: 40)
//            tablebottomContraint.constant = 60
//        let copy = blueToothModelObj
//            blueToothModelObj.removeAll()
//            self.reportTable.reloadData()
//            //
//           timer.invalidate()
//            let delayTime = DispatchTime.now() + 10.0
//            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
//
//                if ApplicationState.sharedAppState.currentUserCount.count>0{
//                    self.blueToothModelObj.removeAll()
//                    self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount
//                }
//                else{
//                    if ApplicationState.sharedAppState.currentUserCount.count == 0{
//                        self.blueToothModelObj = copy
//                    }else{
//                       self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount
//                    }
//                }
//
//                if self.blueToothModelObj.count>0{
//                    self.timerCount = 5.0
//                }else{
//                   self.timerCount = 1.0
//                }
//
//
//                self.scheduledTimerWithTimeInterval()
//                self.reportTable.reloadData()
//                self.tablebottomContraint.constant = -20
//                self.btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-100, width: 40, height: 40)
//            })
//      //  }
    }
    
    
    func getDataFromCurrentUser(){
        btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-150, width: 40, height: 40)
                   tablebottomContraint.constant = 60
               let copy = blueToothModelObj
                   blueToothModelObj.removeAll()
                   self.reportTable.reloadData()
                   //
                  timer.invalidate()
                   let delayTime = DispatchTime.now() + 10.0
                   DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                       
                       if ApplicationState.sharedAppState.currentUserCount.count>0{
                           self.blueToothModelObj.removeAll()
                        self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount + ApplicationState.sharedAppState.currentAudioUserCount
                       }
                       else{
                           if ApplicationState.sharedAppState.currentUserCount.count == 0{
                               self.blueToothModelObj = copy
                           }else{
                              self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount + ApplicationState.sharedAppState.currentAudioUserCount
                           }
                       }
                       
                       if self.blueToothModelObj.count>0{
                           self.timerCount = 10.0
                       }else{
                          self.timerCount = 1.0
                       }
                       
                       
                       self.scheduledTimerWithTimeInterval()
                       self.reportTable.reloadData()
                       self.tablebottomContraint.constant = -20
                       self.btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-100, width: 40, height: 40)
                   })
        
    }
    
    
    @objc func updateUI(){
        // let delayTime = DispatchTime.now() + 1.0
        //  DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
        
        self.blueToothModelObj.removeAll()
        self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount + ApplicationState.sharedAppState.currentAudioUserCount
        self.reportTable.reloadData()
        self.tablebottomContraint.constant = -20
        self.btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-100, width: 40, height: 40)
        // })
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            timer.invalidate()
          //  ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
        }
     // ApplicationState.sharedAppState.blueToothMainClassObj.stop()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount + ApplicationState.sharedAppState.currentAudioUserCount
            for i in 0..<self.blueToothModelObj.count{
                print(self.blueToothModelObj[i].bluetoothDate)
                
            }
            
            self.reportTable.delegate = self
            self.reportTable.dataSource = self
            self.reportTable.reloadData()
        }
    }
    
    
  func floatingButton(){
       
        btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-100, width: 40, height: 40)
        //btn.setTitle("", for: .normal)
     //   btn.setImage(UIImage(named: "bluetoothbutton"), for: .normal)
       btn.setBackgroundImage(UIImage(named: "bluetoothbutton"), for: .normal)
      //  btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btn.clipsToBounds = true
     //   btn.layer.cornerRadius = 30
      //  btn.layer.borderColor = #colorLiteral(red: 0.2091533542, green: 0.5110324025, blue: 0.798448503, alpha: 1)
      //  btn.layer.borderWidth = 2.0
    
    btn.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
    btn.layer.shadowOffset = CGSize(width: 0, height: 2)
    btn.layer.shadowOpacity = 1.0
    btn.layer.shadowRadius = 2.0
    btn.layer.masksToBounds = false
    
    btn.addTarget(self,action: #selector(self.buttonTapped), for:.touchUpInside)
        view.addSubview(btn)
    }
        
    @IBOutlet var tablebottomContraint: NSLayoutConstraint!
    @IBAction func backButtonClick(_ sender: Any) {
        // self.navigationController?.popViewController(animated: true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonTapped(){
        
//      btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-150, width: 40, height: 40)
//             tablebottomContraint.constant = 60
//            blueToothModelObj.removeAll()
//              self.reportTable.reloadData()
//      //
//
//              let delayTime = DispatchTime.now() + 4.0
//              DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
//
//                  if ApplicationState.sharedAppState.currentUserCount.count>0{
//                      self.blueToothModelObj.removeAll()
//                       self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount
//                  }
//                  else{
//                       self.blueToothModelObj = ApplicationState.sharedAppState.currentUserCount
//                  }
//
//                  self.reportTable.reloadData()
//                  self.tablebottomContraint.constant = -20
//                  self.btn.frame = CGRect(x: UIScreen.main.bounds.size.width-65, y: UIScreen.main.bounds.size.height-100, width: 40, height: 40)
//              })
        
        getDataFromCurrentUser()
        
    }
    
    
}


extension ReportListViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if blueToothModelObj.count>0{
            return blueToothModelObj.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReportListCell") as? ReportListCell else {
            return UITableViewCell()
        }
        cell.extraView.layer.cornerRadius = 8
        cell.extraView.layer.masksToBounds = true
        cell.extraView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.extraView.layer.shadowColor = UIColor.black.cgColor
        cell.extraView.layer.shadowOpacity = 0.23
        cell.extraView.layer.shadowRadius = 4
        
        cell.bluetoothId.textAlignment = .left
        cell.bluetoothId.lineBreakMode = .byCharWrapping
        cell.bluetoothId.sizeToFit()
        cell.bluetoothId.text = "MAC: " + blueToothModelObj[indexPath.row].bluetoothId
        cell.bluetoothName.text = "Name: " + blueToothModelObj[indexPath.row].bluetoothName
        cell.bluetoothrssiLabel.text = "RSSI: " + blueToothModelObj[indexPath.row].rssiID
        let signalstrength = blueToothModelObj[indexPath.row].bluetoothDateLessThanSixFeet
        
        if signalstrength == 1 {
            cell.signalstrengthLabel.text = "Signal Strength: " + "Excellent"
        }
        else if signalstrength == 2{
            cell.signalstrengthLabel.text = "Signal Strength: " + "Good"
        }
        else{
             cell.signalstrengthLabel.text = "Signal Strength: " + "Average"
        }
      //  cell.signalstrengthLabel.text = "Signal Strength: " + "\(blueToothModelObj[indexPath.row].bluetoothDateLessThanSixFeet)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
}


