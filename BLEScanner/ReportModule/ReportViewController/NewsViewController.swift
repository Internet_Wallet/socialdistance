//
//  NewsViewController.swift
//  BLEScanner
//
//  Created by OK$ on 05/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import WebKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet var detailLabel: UILabel!{
              didSet {
                  detailLabel.text = "News".localized
              }
          }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "News".localized
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]

        //https://www.bbc.com/pidgin/world-52160623
        
      
//        let url = URL(string: "https://www.bbc.com/pidgin/world-52160623")
//        webView.load(URLRequest(url: url!))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    //  Utility.alert(message: "", title: "Coming Soon", controller: self)
      
    }

}
