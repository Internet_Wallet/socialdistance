//
//  ReportViewController.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import CoreData

class ReportViewController: UIViewController {

    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var pickerContainer: UIView!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateWiseReportButtonOutlet: UIButton!
    @IBOutlet weak var reportBtnOutlet: UIButton!
    var blueToothModelObj = [BluetoothDataModel]()
    var dateToSend = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerContainer.isHidden = true
        retreiveData()
        pickerContainer.layer.cornerRadius = 10.0
        doneButtonOutlet.layer.cornerRadius = 10.0
        reportBtnOutlet.layer.cornerRadius = 10.0
        dateWiseReportButtonOutlet.layer.cornerRadius = 10.0
        datePicker.minimumDate = Date()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        dateToSend = ""
        container.isHidden = true
    }
    
    @IBAction func onClickDoneButton(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        dateToSend = formatter.string(from: datePicker.date)
        if #available(iOS 13.0, *) {
            guard let obj = self.storyboard?.instantiateViewController(identifier: "ReportListViewController")as? ReportListViewController else {
                return
            }
           // obj.blueToothModelObj = blueToothModelObj
            obj.dateToSort = dateToSend
            self.navigationController?.pushViewController(obj, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
    @IBAction func onclickShowAllRecords(_ sender: Any) {
        dateToSend = ""
        if #available(iOS 13.0, *) {
            guard let obj = self.storyboard?.instantiateViewController(identifier: "ReportListViewController")as? ReportListViewController else {
                return
            }
          //  obj.blueToothModelObj = blueToothModelObj
            obj.dateToSort = dateToSend
            self.navigationController?.pushViewController(obj, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    
    @IBAction func onClickDateWise(_ sender: Any) {
        if pickerContainer.isHidden{
            pickerContainer.isHidden = false
        }else{
            pickerContainer.isHidden = true
        }
    }
    
    
    func retreiveData(){
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
//        let managedContext = appDelegate.persistentContainer.viewContext
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BlueToothModel")
//        do {
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject]{
//                let newarray = data.value(forKey: "bluetoothData") as! BluetoothDataModels
//                self.blueToothModelObj = newarray.obj
//            }
//        }catch{
//            self.blueToothModelObj.removeAll()
//            print("Failed")
//            
//        }
        
    }
    

}
