//
//  ReportListCell.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class ReportListCell: UITableViewCell {

   
    @IBOutlet weak var bluetoothName: UILabel!
    @IBOutlet weak var bluetoothId: UILabel!
    @IBOutlet var bluetoothrssiLabel: UILabel!
    @IBOutlet var signalstrengthLabel: UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var extraView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //btn.setImage(UIImage(named: "bt_symbol"), for: .normal)
        imgView.image = UIImage(named: "bluetoothgreen")
      //  imgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
       // imgView.clipsToBounds = true
      //  imgView.layer.cornerRadius = 25
     //   imgView.layer.borderColor = #colorLiteral(red: 0.2091533542, green: 0.5110324025, blue: 0.798448503, alpha: 1)
     //   imgView.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
