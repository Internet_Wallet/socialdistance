//
//  ApiRequestClass.swift


import UIKit



protocol ApiRequestProtocol {
    func httpResponse(responseObj:Any,hasError: Bool)
}

class ApiRequestClass: NSObject{
    
    var customDelegate:ApiRequestProtocol?
    
    func sendHttpRequest(requestUrl:URL,requestData:Any,httpMethodName:String) -> Void {
        var request = URLRequest(url: requestUrl , cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        let jsonData = try! JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as String
        let dataToSend = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        request.httpBody = dataToSend
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = httpMethodName
            
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            
            guard error == nil else
            {
                self.customDelegate?.httpResponse(responseObj: (error?.localizedDescription)! as String, hasError: true)
                return
            }
            
            do {
                let result = try JSONSerialization.jsonObject(with: data!, options: [])
                print(result)
                self.customDelegate?.httpResponse(responseObj: result, hasError: false)
            }
            catch{
                self.customDelegate?.httpResponse(responseObj: error.localizedDescription, hasError: true)
            }
        }
        dataTask.resume()
    }
}
