//
//  ApplicationSettingModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 13/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
 
//this will hold all the appliation setting model
open class ApplicationSettingModel{
    var blueToothModel: BluetoothModelSetting?
    var ultraModel: UltraModel?
    var notificationModel: NotificationAlertModel?
    var deviceFlatModel: DeviceFlatModel?
    var dataSynModel: DataSyncModel?
    var nightModel: NightModeModel?
    
    public init(items:NSDictionary){
        if items.count>0{
            let array = items.value(forKey: "Bluetooth") as? NSArray
            let ultra = items.value(forKey: "Ultra") as? NSArray
            let notification = items.value(forKey: "NotificationAlert") as? NSArray
            let deviceFlat = items.value(forKey: "DeviceFlat") as? NSArray
            let datasync = items.value(forKey: "DataSync") as? NSArray
            let nightMode = items.value(forKey: "Nightmode") as? NSArray
            blueToothModel = BluetoothModelSetting(data: array?[0] as! NSDictionary)
            ultraModel = UltraModel(data: ultra?[0] as! NSDictionary)
            notificationModel = NotificationAlertModel(data: notification?[0] as! NSDictionary)
            deviceFlatModel = DeviceFlatModel(data: deviceFlat?[0] as! NSDictionary)
            dataSynModel = DataSyncModel(data: datasync?[0] as! NSDictionary)
            nightModel = NightModeModel(data: nightMode?[0] as! NSDictionary)
        }
    }
}


open class BluetoothModelSetting{
    var minRSSI: String = ""
    var maxRSSI: String = ""
    var alertValue: String = ""
    
    public init(data: NSDictionary){
        if data.count>0{
            self.minRSSI = data.value(forKey: "MAX_RSSI_VALUE") as? String ?? ""
            self.maxRSSI = data.value(forKey: "MAX_RSSI_VALUE_DATA") as? String ?? ""
            self.alertValue = data.value(forKey: "TimerAlertValue") as? String ?? ""
        }
    }
}


open class UltraModel{
    var timerAlertValue: String = ""

    public init(data: NSDictionary){
        if data.count>0{
            self.timerAlertValue = data.value(forKey: "TimerAlertValue") as? String ?? ""
        }
    }
}

open class NotificationAlertModel{
    var voiceCountMax: String = ""
    var autoIgnoreForIDCount: String = ""
    
    public init(data: NSDictionary){
        if data.count>0{
            self.voiceCountMax = data.value(forKey: "VoiceCountMax") as? String ?? ""
            self.autoIgnoreForIDCount = data.value(forKey: "AutoIgnoreForIDCount") as? String ?? ""
        }
    }
}


open class DeviceFlatModel{
    var stepsCountMin: String = ""
    var stepCountMix: String = ""
    var stepsSeconds: String = ""
    var voiceAlertMaxDay: String = ""
    var timeInterval: String = ""
    var updateDiff: String = ""
    
    
    public init(data: NSDictionary){
        if data.count>0{
            self.stepsCountMin = data.value(forKey: "StepCountMiN") as? String ?? ""
            self.stepCountMix = data.value(forKey: "StepCountMiX") as? String ?? ""
            self.stepsSeconds = data.value(forKey: "StepSeconds") as? String ?? ""
            self.voiceAlertMaxDay = data.value(forKey: "VoiceAlertMaxDay") as? String ?? ""
            self.timeInterval = data.value(forKey: "TimeInterval") as? String ?? ""
            self.updateDiff = data.value(forKey: "UpdateDiff") as? String ?? ""
        }
    }
}

open class DataSyncModel{
    var sysncTime = [String]()
    var countData: String = ""
    var stepsSeconds: String = ""
    
    public init(data: NSDictionary){
        if data.count>0{
            self.sysncTime = data.value(forKey: "SysncTime") as? [String] ?? []
            self.countData = data.value(forKey: "CountData") as? String ?? ""
            self.stepsSeconds = data.value(forKey: "StepSeconds") as? String ?? ""
        }
    }
}


open class NightModeModel {
    var startingTime: String = ""
    var endTime: String = ""
    
    
    public init(data: NSDictionary){
        if data.count>0{
            self.startingTime = data.value(forKey: "StartingTime") as? String ?? ""
            self.endTime = data.value(forKey: "EndTime") as? String ?? ""
        }
    }
}

