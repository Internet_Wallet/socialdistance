//
//  LoginViewModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class LoginViewModel: NSObject {
    let apiReqObj = ApiRequestClass()
    var delegate: ResponseDelegate? = nil
    
    func callLoginApi(mobileNumber: String, latitude:String,longitude:String,nearlocation:String,township:String,region:String,countryCode:String){
        let a = mobileNumber
        let last4 = String(a.suffix(8))
        print(last4)
       // ApplicationState.sharedAppState.currentUser.MobileNumber = mobileNumber
        ApplicationState.sharedAppState.currentUser.mobileNumberToDisplay = mobileNumber
        var mobilenum = mobileNumber
        if countryCode == "+95" {
            var myString = mobileNumber
            myString.remove(at: myString.startIndex)
            mobilenum = "0095"+myString
            print(mobilenum)
        } else {
            mobilenum = mobileNumber
        }
       // ApplicationState.sharedAppState.currentUser.MobileNumber = mobilenum
        apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getLoginApi())!, requestData: LoginFinalParam.getLoginParam(mobileNumber: mobilenum, ultrasound: last4,lat:latitude,long: longitude,nearloc: nearlocation,township: township,region: region), httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}

extension LoginViewModel :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool){
        //success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count>0{
                let modelObj = VMLoginResponseModel(dataDic: dataResponse)
                delegate?.getDataFromResponse(message: modelObj.Code ?? "", statusCode: "", title: modelObj.Message ?? "", OTP: ApplicationState.sharedAppState.currentUser.OTP, mobileNumber: modelObj.mobilenumber ?? "")
            }
        }else{
            delegate?.getDataFromResponse(message: "Error While Registering", statusCode: "", title:"", OTP: "", mobileNumber: "")
        }
        
    }
    
}

open class VMLoginResponseModel {
    
    var Code: String?
    var data: VMLoginModel?
    var Message: String?
    var statusCode: String?
    var mobilenumber:String?
    
    public init(dataDic: NSDictionary){
        if dataDic.count>0{
            self.Code = String(dataDic.value(forKey: "Code") as! Int)
            self.Message = dataDic["Message"] as? String
            

            if let dict =  dataDic.value(forKey: "Data") as? NSArray{
                let newValue = dict[0] as! NSDictionary
                self.data = VMLoginModel.init(dataDic: newValue)
                self.mobilenumber = newValue.value(forKey: "MobileNumber") as? String
            } else {
                //self.data = VMLoginModel.init(dataDic: dataDic)
            }
            //self.Message = dataDic["MobileNumber"] as? String
        }
    }
}


open class VMLoginModel {
    
    
    
    public init(dataDic: NSDictionary){
        if dataDic.count>0 {
            
            if let userId = dataDic.value(forKey: "UserId") as? String {
                ApplicationState.sharedAppState.currentUser.userID = userId
            }
            
            if let qrid = dataDic.value(forKey: "QRId") as? String {
               ApplicationState.sharedAppState.currentUser.qrID = qrid
            }
            
            if let mobilNumber = dataDic.value(forKey: "MobileNumber") as? String {
                ApplicationState.sharedAppState.currentUser.MobileNumber = mobilNumber
            }
            
            if let name = dataDic.value(forKey: "Name") as? String {
                ApplicationState.sharedAppState.currentUser.name = name
            }
           
            
            if let name = dataDic.value(forKey: "OTP") as? String {
                ApplicationState.sharedAppState.currentUser.OTP = name
            }
            
            if let name = dataDic.value(forKey: "UltraSoundId") as? String {
                 ApplicationState.sharedAppState.currentUser.UltraSoundId = name
             }
            
            if let name = dataDic.value(forKey: "GoogleToken") as? String {
                 ApplicationState.sharedAppState.currentUser.GoogleToken = name
             }
            
            
            
           
            
            if ApplicationState.sharedAppState.currentUser.qrID == "" {
                ApplicationState.sharedAppState.currentUser.generateButtonShow = false
            }else {
                ApplicationState.sharedAppState.currentUser.generateButtonShow = true
            }
        }
            
    }
    
}




 
 


