//
//  ApplicationSettingViewModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 12/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class ApplicationViewModel: NSObject {
    let apiReqObj = ApiRequestClass()
    var modelObj: ApplicationSettingModel? = nil
    
    
    func callAppSettingApi(mobileNumber: String){
        apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getAppSetting())!, requestData: ApplicationSettingParam.getParamForApplicationSetting(mobileNumber: mobileNumber), httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}


extension ApplicationViewModel: ApiRequestProtocol{
    
    func httpResponse(responseObj:Any,hasError: Bool){
       // success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count>0{
                if let responseObj =  dataResponse.value(forKey: "Data") as? NSDictionary{
                     modelObj = ApplicationSettingModel(items: responseObj)
                    if let hasData = modelObj{
                        ApplicationState.sharedAppState.currentUser.deviceTimeAlert = hasData.deviceFlatModel?.timeInterval ?? "0"
                        ApplicationState.sharedAppState.currentUser.deviceVoiceAlertMax = hasData.deviceFlatModel?.voiceAlertMaxDay ?? "0"
                    }
                
                }
            }
        }else{
        }
        
    }
    
}
