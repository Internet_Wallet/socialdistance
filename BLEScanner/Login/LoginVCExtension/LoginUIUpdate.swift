//
//  LoginUIUpdate.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation


extension HomeViewController{
    
    func setUI(){
        dismissKey()
      //  headingLabel.setPropertiesForLabel()
        mobileNumTF.setPropertiesForTextField()
        clearButton.isHidden = true
        phNumValidationsFile = getDataFromJSONFile() ?? []
        self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
        countryView = PaytoViews.updateView()
        let countryCode = String(format: "(%@)", "+95")
        countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
        countryView?.delegate = self
        self.mobileNumTF.leftViewMode = .always
        self.mobileNumTF.leftView     = countryView
        
    }
    
}
