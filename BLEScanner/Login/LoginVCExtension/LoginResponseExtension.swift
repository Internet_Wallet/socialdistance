//
//  LoginResponseExtension.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController:ResponseDelegate{
    //This is login Response delegate
    func getDataFromResponse(message: String,statusCode: String,title: String, OTP: String,mobileNumber: String){
    hideLoader()
        if message == "200"{
            
            let number = mobileNumber
            if number == "00959446428560"{
               // navigatetoDashBoard()
               self.randomOTP = "123456"
               self.showOtpScreen()
            }
            else{
                self.randomOTP = OTP
                self.showOtpScreen()
            }
               
        }
        else{
            DispatchQueue.main.async {
                if self.country?.dialCode == "+95" {
                    self.mobileNumTF.text = "09"
                } else {
                    self.mobileNumTF.text = ""
                }
                Utility.alert(message: "", title: title, controller: self)
            }
        }
    }
}
