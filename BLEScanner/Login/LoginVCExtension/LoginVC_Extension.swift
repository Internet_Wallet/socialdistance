//
//  Login_LeftView.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

extension HomeViewController: CountryViewControllerDelegate{
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.country = country
        self.mobileNumTF.leftView = nil
        
        let countryCode = String.init(format: "(%@)", country.dialCode)
        countryView?.wrapCountryViewData(img: country.code, str: countryCode)
        self.mobileNumTF.leftView = countryView
        list.dismiss(animated: true, completion: nil)
        
        switch self.country?.dialCode {
        case "+91":
            currentSelectedCountry = .indiaCountry
        case "+95":
            currentSelectedCountry = .myanmarCountry
        case "+66":
            currentSelectedCountry = .thaiCountry
        case "+86":
            currentSelectedCountry = .chinaCountry
        default:
            currentSelectedCountry  = .other
        }
        
        self.mobileNumTF.text = (self.country?.dialCode == "+95") ? "09" : ""
        self.mobileNumTF.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func publicCountryView() {
        guard let countryVC = countryViewController(delegate: self) else { return }
        countryVC.modalPresentationStyle = .fullScreen
        self.present(countryVC, animated: true, completion: nil)
    }
    
}
