//
//  LoginViewController.swift
//  BlueToothScreen
//
//  Created by vamsi on 3/29/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit
import CoreTelephony


class LoginViewController: UIViewController{//, CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions
    
    @IBOutlet var menuBarButton: UIBarButtonItem!
    @IBOutlet weak var clearButton: UIButton!
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var mobileNumTF: NoPasteTextField!{
        didSet {
            mobileNumTF.placeholder = "Enter Mobile Number".localized
        }
    }
    @IBOutlet var loginButton: UIButton!{
        didSet {
            self.loginButton.layer.cornerRadius = 4.0
            self.loginButton.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
            self.loginButton.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.loginButton.layer.shadowOpacity = 1.0
            self.loginButton.layer.shadowRadius = 4.0
            self.loginButton.layer.masksToBounds = false
            self.loginButton.isEnabled = false
            self.loginButton.setTitle("Login".localized, for: .normal)
        }
    }
    
    @IBOutlet var Geofence: UIButton!{
        didSet {
            self.Geofence.setRadiusWithShadow(12)
            self.Geofence.setTitle("Geofence".localized, for: .normal)
        }
    }
    
    @IBOutlet var registerButton: UIButton!{
        didSet {
            self.registerButton.layer.cornerRadius = 12.0
            self.registerButton.isEnabled = true
            self.registerButton.setTitle("Registration".localized, for: .normal)
        }
    }
    
    @IBOutlet var trackButton: UIButton!
        {
        didSet {
            self.trackButton.layer.cornerRadius = 12.0
            self.trackButton.isEnabled = true
        }
    }
    @IBOutlet var headingLabel: UILabel!
    
    var loginViewModel =  LoginViewModel()
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    
    var fromRegister = ""
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if fromRegister == "Registration"{
            backButton.isHidden = true
        }
        else{
            backButton.isHidden = false
        }
        //  setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loginButton.backgroundColor = .lightGray
        mobileNumTF.resignFirstResponder()
    }
    
    //this is textField Clear Button
    @IBAction func onClickClear(_ sender: Any) {
        self.clearButton.isHidden = true
        mobileNumTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        registerButton.setTitleColor(.orange, for: .normal)
        self.registerButton.backgroundColor = UIColor.lightGray
        self.mobileNumTF.becomeFirstResponder()
    }
    
    //Function to hide show button
    func clearButtonShowHide(count: Int) {
        if count > 0 {
            self.clearButton.isHidden = false
        } else {
            self.clearButton.isHidden = true
        }
    }
    
    //Navigate to geoFence
    @IBAction func GeofenceClick(_ sender: UIButton){
        if let viewController = UIStoryboard(name: "Geofence", bundle: nil).instantiateViewController(withIdentifier: "GeofenceNav") as? UINavigationController{
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func registerButton(_ sender: Any) {
        var homeVC: RegistrationViewController?
        homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    
    @IBAction func trackOnclick(_ : UIButton) {
        if #available(iOS 13.0, *) {
            let sb = UIStoryboard(name: "QRCode", bundle: nil)
            if let obj = sb.instantiateViewController(identifier: "ScanQR") as? ScanQR {
                self.navigationController?.pushViewController(obj, animated: false)
            }
        } else {
        }
        
    }
    
    @IBAction func genrateQROnclick(_ : UIButton) {
        if #available(iOS 13.0, *) {
            let sb = UIStoryboard(name: "QRCode", bundle: nil)
            if let obj = sb.instantiateViewController(identifier: "GenerateQR") as? GenerateQR {
                self.navigationController?.pushViewController(obj, animated: false)
            }
        } else {
            
            
        }
    }
    func navigatetoDashBoard() {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "QRCode", bundle: nil)
                if let baseObj = sb.instantiateViewController(identifier: "BaseTabController") as? BaseTabController {
                    baseObj.modalPresentationStyle = .fullScreen
                    baseObj.fromlogin = "LOGIN"
                    self.navigationController?.present(baseObj, animated: true, completion: nil)
                }
            } else {
                
                //  var homeVC: BaseTabController?
                
                //                         let homeVC = UIStoryboard(name: "QRCode", bundle: nil).instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
                let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
                
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if let objNav = objNav {
                    dashbord?.fromlogin = "LOGIN"
                    self.present(objNav, animated: true)
                }
                
            }
        }
    }
}


extension LoginViewController{
    @IBAction func menuAction(_ sender: UIBarButtonItem) {
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
        
    }
}







