//
//  OTPViewController.swift
//  OK
//
//  Created by Uma Rajendran on 8/14/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
protocol OTPDelegate: class {
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController)
}

protocol MyTextField1Delegate1 {
    func textFieldDidDelete()
}
class OTPViewController: UIViewController,MyTextField1Delegate1 {
   
    
    @IBOutlet weak var imgOTP: UIImageView!
    
    @IBOutlet var otpHeaderLabel: UILabel!
    @IBOutlet var otpDescriptionLabel: UILabel!
    
    @IBOutlet var otpContentView: UIView!
    
    @IBOutlet var firstNumber: MyTextField1!
    
    @IBOutlet var secondNumber: MyTextField1!
    
    @IBOutlet var thirdNumber: MyTextField1!
    
    @IBOutlet var fourthNumber: MyTextField1!
    
    @IBOutlet var fifthNumber: MyTextField1!
    
    @IBOutlet var sixthNumber: MyTextField1!
    @IBOutlet weak var lblOTP: UILabel!
    
    var activeField: UITextField!
    var delegate: OTPDelegate?
    var fullOTPNumber = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNumber.myDelegate = self
        firstNumber.delegate = self
        
        secondNumber.myDelegate = self
        secondNumber.delegate = self
        
        thirdNumber.myDelegate = self
        thirdNumber.delegate = self
        
        fourthNumber.myDelegate = self
        fourthNumber.delegate = self
        
        fifthNumber.myDelegate = self
        fifthNumber.delegate = self
        
        sixthNumber.myDelegate = self
        sixthNumber.delegate = self
        
        otpContentView.layer.cornerRadius = 10.0
        otpContentView.layer.masksToBounds = true
        lblOTP.text = "Enter the 6 digit code you received via SMS on your mobile number.".localized
        updateLocalizations()
      //  self.view.backgroundColor = kGradientGreyColor
       // loadFaceGIF()
    }
    
   
    
    func textFieldDidDelete() {
        
        if secondNumber.text?.count == 0 {
              firstNumber.text = ""
              firstNumber.becomeFirstResponder()
        }else if thirdNumber.text?.count == 0 {
              secondNumber.text = ""
              secondNumber.becomeFirstResponder()
        }else if fourthNumber.text?.count == 0 {
              thirdNumber.text = ""
              thirdNumber.becomeFirstResponder()
        }else if fifthNumber.text?.count == 0 {
              fourthNumber.text = ""
              fourthNumber.becomeFirstResponder()
        }else if sixthNumber.text?.count == 0 {
              fifthNumber.text = ""
              fifthNumber.becomeFirstResponder()
        }
    }
    
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        if !validateAllFields() {
            return
        }
        

        fullOTPNumber = "\(firstNumber.text!)" + secondNumber.text! + thirdNumber.text! + fourthNumber.text! + fifthNumber.text! + sixthNumber.text!
        print("full otp number :: \(fullOTPNumber)")
        activeField.resignFirstResponder()
        delegate?.selectedOTPNumber(otpNumber: fullOTPNumber, vc:self)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tapGesture(_ sender: Any) {
       // self.view.removeFromSuperview()
          self.dismiss(animated: false, completion: nil)
    }
    
    func validateAllFields() -> Bool {
        if ((firstNumber.text?.count)! > 0) && ((secondNumber.text?.count)! > 0) && ((thirdNumber.text?.count)! > 0) && ((fourthNumber.text?.count)! > 0) && ((fifthNumber.text?.count)! > 0) && ((sixthNumber.text?.count)! > 0) {
            return true
        }
        return false
    }
    func updateLocalizations() {
       // doneBtn.titleLabel?.font = UIFont.init(setLabelFont: 18)
       // doneBtn.titleLabel?.text = self.setLocStr(str: "Done")
       // doneBtn.backgroundColor = kYellowColor
       // otpHeaderLabel.font = UIFont.init(setLabelFont: 20)
       // otpDescriptionLabel.font = UIFont.init(setLabelFont: 16)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        firstNumber.text = ""
        secondNumber.text = ""
        thirdNumber.text = ""
        fourthNumber.text = ""
        fifthNumber.text = ""
        sixthNumber.text = ""
        firstNumber.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activeField.resignFirstResponder()
    }
}


extension OTPViewController: UITextFieldDelegate {
    //MARK: - TextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
     
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
       
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        print("textFieldShouldBeginEditing")
       
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        textField.text = ""
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing")
        if textField == sixthNumber {
            let btn = UIButton()
            self.doneBtnAction(btn)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("shouldChangeCharactersIn")
        
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        print("typing text :::: \(text)")
        
        // On inputing value to textfield
        if (textField.text!.count < 1  && string.count > 0){
            let nextTag = textField.tag + 1
            
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag);
            
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = string
            print("added numbers are ::: \(string)")
            nextResponder?.becomeFirstResponder()
            return false;
        }
        else if (textField.text!.count >= 1  && string.count == 0){
            // on deleting value from Textfield
            let previousTag = textField.tag - 1
            
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            print("removed numbers are ::: \(string)")
            previousResponder?.becomeFirstResponder()
            return false
        }else if (textField.text!.count >= 1  && string.count > 0) {
            
            return false
        }
        if textField == sixthNumber {
            let btn = UIButton()
            self.doneBtnAction(btn)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        
        textField.resignFirstResponder()
        return true
    }
    
}

class MyTextField1: UITextField {
    var myDelegate: MyTextField1Delegate1?
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}
