//
//  HomeViewController.swift
//  BLEScanner
//
//  Created by vamsi on 4/4/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import CoreTelephony

class HomeViewController: UIViewController, CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions {
    
    
    var randomOTP = ""
    var smsOtpTxt = ""
    var township : String?
    var devision : String?
    var googleAddress : String?
    var locationLat : String?
    var locationLong : String?
    
    var localBundle : Bundle?
    
    @IBOutlet var mobileNumTF: NoPasteTextField!{
        didSet {
            mobileNumTF.placeholder = "Enter Mobile Number".localized
        }
    }
    @IBOutlet var clearButton: UIButton!
    
    @IBOutlet var loginButton: UIButton!{
        didSet {
            self.loginButton.setTitle("Login".localized, for: .normal)
            self.loginButton.layer.cornerRadius = 4.0
            self.loginButton.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
            self.loginButton.layer.shadowOffset = CGSize(width: 2, height: 2)
            self.loginButton.layer.shadowOpacity = 1.0
            self.loginButton.layer.shadowRadius = 2.0
            self.loginButton.layer.masksToBounds = false
            self.loginButton.isEnabled = false
        }
    }
    
   
    
    @IBOutlet var maynamarView: UIView!
    
    @IBOutlet var unicodeView: UIView!
    
    @IBOutlet var englishView: UIView!
    var loginViewModel =  LoginViewModel()
    var APNSViewModel =  AppDelModel()

    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    
    var fromRegister = ""
    
    public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        setUI()
        location()
        //         maynamarView.backgroundColor = .systemBlue
        //         unicodeView.backgroundColor = .lightGray
        //         englishView.backgroundColor = .lightGray
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = .lightGray
        self.loginButton.isEnabled = false
        mobileNumTF.resignFirstResponder()
        
        
        let lang = ApplicationState.sharedAppState.currentUser.currentLanguage
        appDel.currentLanguage = lang
        if lang == "my" {
            appLang = "Myanmar"
            appFont = "Zawgyi-One"
            maynamarView.backgroundColor = .systemBlue
            unicodeView.backgroundColor = .lightGray
            englishView.backgroundColor = .lightGray
        } else if lang == "en" {
            appLang = "English"
            appFont = "Zawgyi-One"
            maynamarView.backgroundColor = .lightGray
            unicodeView.backgroundColor = .lightGray
            englishView.backgroundColor = .systemBlue
        } else if lang == "uni" {
            appLang = "Unicode"
            appFont = "Myanmar3"
            maynamarView.backgroundColor = .lightGray
            unicodeView.backgroundColor = .systemBlue
            englishView.backgroundColor = .lightGray
        }
        
        
    }
    
    @IBAction func clearButtonClick(_ sender: Any) {
        self.clearButton.isHidden = true
        mobileNumTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        self.loginButton.isEnabled = false
        self.mobileNumTF.becomeFirstResponder()
    }
    //Function to hide show button
    func clearButtonShowHide(count: Int) {
        if count > 0 {
            self.clearButton.isHidden = false
        } else {
            self.clearButton.isHidden = true
            self.loginButton.isEnabled = false
        }
    }
    
    @IBAction func loginClick(_ sender: Any) {
        
        if mobileNumTF.text == "09" || mobileNumTF.text == "" {
            Utility.alert(message: "Please Enter Mobile Number".localized, title: "", controller: self)
        }else{
            if NetWorkCode.checkifSimAvailableInPhone(){
                if Utility.isNetworkAvailable(controller: self){
                    showLoader()
                    loginViewModel.delegate = self as ResponseDelegate
                    loginViewModel.callLoginApi(mobileNumber: mobileNumTF.text ?? "",latitude:locationLat ?? "",longitude: locationLong ?? "",nearlocation: township ?? "",township: township ?? "",region:devision ?? "",countryCode: self.country?.dialCode ?? "")
                    //navigatetoDashBoard()
                }
            }else{
                Utility.alert(message: "Sim Not Available".localized, title: "", controller: self)
            }
        }
    }
    func location(){
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
            VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        if let street = addressDic["street"]  {
                            self.township = street
                        }
                        if let region = addressDic["region"]  {
                            self.devision = region
                        }
                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                }
            }
        }
        else{
            showLocationDisabledpopUp()
        }
    }
    
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled".localized, withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                self.navigationController?.dismiss(animated: true , completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }

    
    
    func navigatetoDashBoard() {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let sb = UIStoryboard(name: "QRCode", bundle: nil)
                if let baseObj = sb.instantiateViewController(identifier: "BaseTabController") as? BaseTabController {
                    baseObj.modalPresentationStyle = .fullScreen
                    baseObj.fromlogin = "LOGIN"
                    self.navigationController?.present(baseObj, animated: true, completion: nil)
                }
            } else {
                
                //  var homeVC: BaseTabController?
                
                //                         let homeVC = UIStoryboard(name: "QRCode", bundle: nil).instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
                let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
                let dashbord = homeVC.instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
                
                var objNav: UINavigationController? = nil
                if let dashbord = dashbord {
                    objNav = UINavigationController(rootViewController: dashbord)
                }
                if let objNav = objNav {
                    dashbord?.modalPresentationStyle = .fullScreen
                    dashbord?.fromlogin = "LOGIN"
                    self.navigationController?.present(objNav, animated: true)
                }
                
            }
        }
    }
    
    
    @IBAction func maynamarClick(_ sender: Any) {
        appDel.currentLanguage = "my"
        appLang = "Myanmar"
        appFont = "Zawgyi-One"

        ApplicationState.sharedAppState.currentUser.currentLanguage = "my"
        ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"

        if let path = Bundle.main.path(forResource: "my", ofType: "lproj") {
            localBundle = Bundle(path: path)
        }
        appDel.setSeletedlocaLizationLanguage(language: "my")
        maynamarView.backgroundColor = .systemBlue
        unicodeView.backgroundColor = .lightGray
        englishView.backgroundColor = .lightGray
        updateGUI()
    }
    
    @IBAction func unicodeClick(_ sender: Any) {
        appDel.currentLanguage = "uni"
        appLang = "Unicode"
        appFont = "Myanmar3"

        ApplicationState.sharedAppState.currentUser.currentLanguage = "uni"
        ApplicationState.sharedAppState.currentUser.currentFont = "Myanmar3"
        
        if let path = Bundle.main.path(forResource: "uni", ofType: "lproj") {
            localBundle = Bundle(path: path)
        }
        appDel.setSeletedlocaLizationLanguage(language: "uni")
        unicodeView.backgroundColor = .systemBlue
        maynamarView.backgroundColor = .lightGray
        englishView.backgroundColor = .lightGray
        updateGUI()
    }
    
    @IBAction func englishClick(_ sender: Any) {
        appDel.currentLanguage = "en"
        appLang = "English"
        appFont = "Zawgyi-One"

        ApplicationState.sharedAppState.currentUser.currentLanguage = "en"
        ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
        
        if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
            localBundle = Bundle(path: path)
        }
        appDel.setSeletedlocaLizationLanguage(language: "en")
        englishView.backgroundColor = .systemBlue
        unicodeView.backgroundColor = .lightGray
        maynamarView.backgroundColor = .lightGray
        updateGUI()
    }
    
    func updateGUI() {
      
        mobileNumTF.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        loginButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
       
        mobileNumTF.placeholder = "Enter Mobile Number".localized
        self.loginButton.setTitle("Login".localized, for: .normal)
    }
    
    @IBAction func registerClick(_ sender: Any) {
        var homeVC: RegistrationViewController?
        homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension HomeViewController: OTPDelegate{
    
    
    func showOtpScreen() {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let oTPViewController  = sb.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            oTPViewController.modalPresentationStyle = .overCurrentContext
            oTPViewController.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            oTPViewController.delegate = self
            self.present(oTPViewController, animated: false, completion: nil)
            self.smsOtpTxt = ""
        }
    }
    
    
    func selectedOTPNumber(otpNumber: String , vc:OTPViewController) {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        if randomOTP == otpNumber {
            navigatetoDashBoard()
            ApplicationState.sharedAppState.currentUser.LoginDate = Utility.getDateForCoreData()
            ApplicationState.sharedAppState.currentUser.refreshUserDate = Utility.getDateForCoreData()
            //setting data sync time
          
            ApplicationState.sharedAppState.currentUser.syncDataTime = Utility.getDateForCoreData()
            
                if NetWorkCode.checkifSimAvailableInPhone(){
                    if Utility.isNetworkAvailable(controller: self){
                        showLoader()
                        APNSViewModel.delegate = self as ResponseDelegate
                        APNSViewModel.APNSAPI(CountryCode:self.country?.dialCode ?? "")
                    }
                }else{
                    Utility.alert(message: "Sim Not Available".localized, title: "", controller: self)
                }
            
        } else {
            //otp doesn't match\
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "ALERT!".localized, withDescription: "Please enter valid OTP".localized, onController: self)
            let ok = SAlertAction()
            ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                self.showOtpScreen()
            })
            alertVC.addAction(action: [ok])
        }
    }
    
    
}
