//
//  LoginParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct LoginParam {
    
    static func getUserDetails(mobileNumber: String) -> NSMutableDictionary{
        let userDic = NSMutableDictionary()
        userDic.setValue(mobileNumber, forKey: "MobileNumber")
        return userDic
    }
    
}
