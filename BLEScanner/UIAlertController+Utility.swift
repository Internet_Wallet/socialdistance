//
//  UIAlertController+Utility.swift
//  BLEScanner
//
//  Created by HARRY G GOODWIN on 22/10/2017.
//  Copyright © 2017 GG. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func presentAlert(on viewController: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK".localized, style: .default)
        alert.addAction(okAction)
        viewController.present(alert, animated: true)
    }
}


extension UIColor {
    
    static func navigationPrimaryColor() -> UIColor{
        //        return UIColor.init(red: 50/255.0, green: 132/255.0, blue: 179/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
        
    }
    static func navigationSecondaryColor() -> UIColor{
        return UIColor.init(red: 61/255.0, green: 124/255.0, blue: 160/255.0, alpha: 1.0)
    }
    static func totalCartBackgroundColor() -> UIColor{
        //        return UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func totalCartTextColor() -> UIColor{
        return UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
    }
    static func messageBGColor() -> UIColor{
        return UIColor.init(red: 175/255.0, green: 64/255.0, blue: 68/255.0, alpha: 1.0)
    }
    static func categoryBGColor() -> UIColor{
        //return UIColor.init(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func productPriceColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToCartButtonTextColor() -> UIColor{
        return UIColor.white
    }
    static func addToCartButtonBackgroundColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToWishListButtonTextColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0)
    }
    static func addToWishListButtonBackgroundColor() -> UIColor{
        return UIColor.white
    }
    static func HomePageFooterButtonColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0) //orange color
    }
    static func primaryButtonColor() -> UIColor{
        return self.navigationPrimaryColor() //navigation color
    }
    static func secondaryButtonColor() -> UIColor{
        return UIColor.init(red: 249/255.0, green: 113/255.0, blue: 66/255.0, alpha: 1.0) //orange color
    }
    static func homePageBGColor() -> UIColor{
        //return UIColor.init(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func productPageBGColor() -> UIColor{
        //return UIColor.init(red: 234/255.0, green: 87/255.0, blue: 70/255.0, alpha: 1.0)
        return UIColor.init(red: 40/255.0, green: 115/255.0, blue: 240/255.0, alpha: 1.0)
    }
    static func subCategoryTableViewCell() -> UIColor{
        return UIColor.init(red: 204/255.0, green: 204/255.0, blue: 204/255.0, alpha: 1.0)
    }
    
    static func checkOutOptionBackgroundColor() -> UIColor{
        return UIColor.init(red: 8/255.0, green: 170/255.0, blue: 192/255.0, alpha: 1.0)
    }
    
    
    class func getColorWithHexString (_ hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0
        var g:CUnsignedInt = 0
        var b:CUnsignedInt = 0
        
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: CGFloat(1))
    }
    
    static func colorWithRedValue(redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
}

extension UIImageView {
    func applyshadowWithCorner(containerView : UIView, cornerRadious : CGFloat){
        containerView.clipsToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = cornerRadious
        containerView.layer.shadowPath = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: cornerRadious).cgPath
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadious
    }
}

extension UIButton {
    func BtnShadowToView () {
        self.layer.cornerRadius = 6
        self.layer.shadowOpacity = 0.25
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 2.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
    }
    
    func applyButtonGradient( colors : [UIColor]) {
        let frameAndStatusBar: CGRect = self.bounds
        //frameAndStatusBar.size.height += 5 // add 20 to account for the status bar
        setBackgroundImage(UIButton.ButtonGradient(size: frameAndStatusBar.size, colors: colors), for: .normal)
    }
    
    /// Creates a gradient image with the given settings
    static func ButtonGradient(size : CGSize, colors : [UIColor]) -> UIImage?{
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}
