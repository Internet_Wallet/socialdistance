//
//  RegisterModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//
//
import Foundation
//{   "Code": 200,
//    "Data": "",
//    "Message": "success"
//}
//
//"{
//    ""Code"": 500,
//    ""Data"": ""{'message':'Record Exists '}"",
//    ""Message"": ""failure""
//}"
//
//
//{
//    "Code": 500,
//    "Data": "{'message':'Conversion failed when converting from a character string to uniqueidentifier. '}",
//    "Message": "failure"
//}


open class RegisterationModel {
    var code: Int?
    var dataMessage: String?
    var titleMessgae: String?
    var mobilenumber:String?
    
    public init(dataDic: NSDictionary){
        if dataDic.count>0{
            self.code = dataDic.value(forKey: "Code") as? Int
            self.dataMessage = dataDic.value(forKey: "Data") as? String
            self.titleMessgae = dataDic.value(forKey: "Message") as? String
             self.mobilenumber = dataDic.value(forKey: "MobileNumber") as? String
        }
    }
    
}

