//
//  RegisterViewModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

protocol ResponseDelegate {
    func getDataFromResponse(message: String,statusCode: String,title: String,OTP: String,mobileNumber:String)
}

class RegisterViewModel: NSObject {
    let apiReqObj = ApiRequestClass()
    var delegate: ResponseDelegate? = nil
    
    
    func callRegisterationApi(mobileNumber: String,name: String,occupation: String,ultrasound: String){
        print(RegisterFinalParam.getRegisterationParam(mobileNumber: mobileNumber, name: name, occupation: occupation, ultrasonic: ultrasound))
           apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getRegisterApi())!, requestData: RegisterFinalParam.getRegisterationParam(mobileNumber: mobileNumber, name: name, occupation: occupation, ultrasonic: ultrasound), httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}

extension RegisterViewModel :ApiRequestProtocol {
   
    func httpResponse(responseObj:Any,hasError: Bool){
        //success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count>0{
                 let modelObj = RegisterationModel(dataDic: dataResponse)
                delegate?.getDataFromResponse(message: modelObj.dataMessage ?? "", statusCode: String(modelObj.code ?? 0), title: modelObj.titleMessgae ?? "", OTP: "", mobileNumber: modelObj.mobilenumber ?? "")
            }
        }else{
            delegate?.getDataFromResponse(message: "Error While Registering".localized, statusCode: "", title:"", OTP: "", mobileNumber: "")
        }
        
    }
        
}
