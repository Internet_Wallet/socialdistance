//
//  RegisterationParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct RegisterParam {
        
    static func getUserDetails(mobileNumber: String,name: String,occupationId: String, appID: String, ultrasound: String) -> NSMutableDictionary{
     let userDic = NSMutableDictionary()
     userDic.setValue(mobileNumber, forKey: "MobileNumber")
     userDic.setValue(name, forKey: "Name")
     userDic.setValue(occupationId, forKey: "OcupationId")
     userDic.setValue(appID, forKey: "AppId")
     userDic.setValue("NA", forKey: "FbId")
     userDic.setValue("NA", forKey: "Gmailid")
     userDic.setValue("NA", forKey: "ViberNumber")
     userDic.setValue("0", forKey: "RegDetails")
     userDic.setValue("1", forKey: "SendSms")
     userDic.setValue(ultrasound, forKey: "UltraSoundId")
         return userDic
    }
    
    //        "UserDetails": {
    //          "MobileNumber": "00959787190850",
    //          "Name": "Nanaji",
    //        "OcupationId": "784FB3D1-6C89-4CCA-B2E8-095E94CF5420",
    //        "AppId": "",
    //        "FbId": "fbtamilselvan@goautoticket.com",
    //        "Gmailid": "tamilselvan@goautoticket.com",
    //        "ViberNumber": "9123556967",
    //        "RegDetails":"0",
    //        "SendSms":"1",
    //          "UltraSoundId":"skjdf"
    //        },
            
    //        "Name": "Nanaji",
    //           "OcupationId": "784FB3D1-6C89-4CCA-B2E8-095E94CF5420",
    //           "AppId": "",
    //           "FbId": "fbtamilselvan@goautoticket.com",
    //           "Gmailid": "tamilselvan@goautoticket.com",
    //           "ViberNumber": "9123556967",
    //           "RegDetails":"0",
    //           "SendSms":"0",
    //             "UltraSoundId":"skjdf"
    
    static func getGpsInfo(lat: String,long: String,cellid: String,mcc: String,mnc: String,hspambps: String,nearLocation: String,Township: String,divisionName: String) -> NSMutableDictionary{
        let gpsDic = NSMutableDictionary()
        gpsDic.setValue(lat, forKey: "Lat")
        gpsDic.setValue(long, forKey: "Long")
        gpsDic.setValue(cellid, forKey: "Cellid")
        gpsDic.setValue(mcc, forKey: "MCC")
        gpsDic.setValue(mnc, forKey: "MNC")
        gpsDic.setValue(hspambps, forKey: "HSPAmbps")
        gpsDic.setValue(nearLocation, forKey: "NearLocation")
        gpsDic.setValue(Township, forKey: "TownshipName")
        gpsDic.setValue(divisionName, forKey: "DivisionName")
        return gpsDic
    }
    
    static func getBluetoothInfo(blueToothName: String,bluetoothId:String,newSelf: String) -> NSMutableDictionary{
        let bluetoothDic = NSMutableDictionary()
        bluetoothDic.setValue(blueToothName, forKey: "BluetoothName")
        bluetoothDic.setValue(blueToothName, forKey: "Bluetoothid")
        bluetoothDic.setValue(newSelf, forKey: "Self")
        
        return bluetoothDic
    }
    
    static func getWifiInfo(wifiName: String,wifiID: String) -> NSMutableDictionary{
        let wifiDic = NSMutableDictionary()
        wifiDic.setValue(wifiName, forKey: "WifiName")
        wifiDic.setValue(wifiID, forKey: "Wifiid")
        wifiDic.setValue("1", forKey: "Self")
        return wifiDic
    }
    
    static func getDeviceDetails(simidOne: String,simidTwo: String,msidOne: String,msidTwo: String,deviceID: String,gcmId: String,imei: String,phoneModel: String,phoneBrand: String,operatorNameOne: String,operationTwo: String,appID: String,osType: String,wifiDic:[NSMutableDictionary],blueToothDic:[NSMutableDictionary]) -> NSMutableDictionary{
      let deviceDic = NSMutableDictionary()
        deviceDic.setValue(simidOne, forKey: "Simidone")
        deviceDic.setValue(simidTwo, forKey: "SimidTwo")
        deviceDic.setValue(msidOne, forKey: "Msidone")
        deviceDic.setValue(msidTwo, forKey: "Msidtwo")
        deviceDic.setValue(deviceID, forKey: "DeviceId")
        deviceDic.setValue(gcmId, forKey: "GcmId")
        deviceDic.setValue(imei, forKey: "ImEi")
        deviceDic.setValue(phoneModel, forKey: "Phonemodel")
        deviceDic.setValue(phoneBrand, forKey: "Phonebrand")
        deviceDic.setValue(operatorNameOne, forKey: "Operatornameone")
        deviceDic.setValue(operationTwo, forKey: "Operatornametwo")
        deviceDic.setValue(appID, forKey: "AppId")
        deviceDic.setValue(osType, forKey: "OsType")
        deviceDic.setValue(wifiDic, forKey: "WifiInfo")
        deviceDic.setValue(blueToothDic, forKey: "BlueTooth")
       
        
        return deviceDic
    }
    
    
    
    
}
