//
//  FinalParam.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct RegisterFinalParam {
    static func getRegisterationParam(mobileNumber: String, name: String, occupation: String, ultrasonic: String) -> NSMutableDictionary{
          var wifiArray = [NSMutableDictionary]()
        let postMutableDic = NSMutableDictionary()
          
    postMutableDic.setValue(RegisterParam.getUserDetails(mobileNumber: mobileNumber, name: name, occupationId: occupation, appID: NetWorkCode.getTheAppID(), ultrasound:ultrasonic), forKey: "UserDetails")
          
          if NetWorkCode.getNetworkType() == "Wifi"{
            wifiArray.append(RegisterParam.getWifiInfo(wifiName: NetWorkCode.getNetworkInfos()[0], wifiID: NetWorkCode.getNetworkInfos()[safe : 1] ?? ""))
          }else{
              wifiArray.append(RegisterParam.getWifiInfo(wifiName: "", wifiID: ""))
          }
          
          if NetWorkCode.getMSIDForSimCard().0.count>0{
              let numberOfSim = NetWorkCode.getMSIDForSimCard().0[safe : 0] ?? ""
              if numberOfSim == "1"{
                postMutableDic.setValue(RegisterParam.getDeviceDetails(simidOne: Constant.uuid, simidTwo: Constant.uuid, msidOne: NetWorkCode.getMSIDForSimCard().0[safe : 1] ?? "", msidTwo: "", deviceID: Constant.uuid, gcmId: ApplicationState.sharedAppState.currentUser.APNSToken, imei: "", phoneModel: Constant.deviceModel, phoneBrand:"iPhone" , operatorNameOne: NetWorkCode.getMSIDForSimCard().1[safe : 0] ?? "", operationTwo: "", appID: NetWorkCode.getTheAppID(), osType: "1", wifiDic: wifiArray, blueToothDic: [RegisterParam.getBluetoothInfo(blueToothName: "iPhone", bluetoothId: "iPhone", newSelf: "1")]), forKey: "DeviceDetails")
              }else{
                postMutableDic.setValue(RegisterParam.getDeviceDetails(simidOne: Constant.uuid, simidTwo: Constant.uuid, msidOne: NetWorkCode.getMSIDForSimCard().0[safe : 1] ?? "", msidTwo: NetWorkCode.getMSIDForSimCard().0[safe : 2] ?? "", deviceID: Constant.uuid, gcmId: ApplicationState.sharedAppState.currentUser.APNSToken, imei: "", phoneModel: Constant.deviceModel, phoneBrand: "iPhone", operatorNameOne: NetWorkCode.getMSIDForSimCard().1[safe : 0] ?? "", operationTwo: NetWorkCode.getMSIDForSimCard().1[safe : 1] ?? "", appID: NetWorkCode.getTheAppID(), osType: "1", wifiDic: wifiArray, blueToothDic: [RegisterParam.getBluetoothInfo(blueToothName: "iPhone", bluetoothId: "iPhone", newSelf: "1")]), forKey: "DeviceDetails")
              }
              
          }
          
          postMutableDic.setValue(RegisterParam.getGpsInfo(lat: "16.76611328125", long: "96.17500019032727", cellid: "", mcc: "adads", mnc: "asdasd", hspambps: "", nearLocation: "Vintage Luxury Yatch", Township: "BotaTown", divisionName: "Yangon"), forKey: "GPS")
        
        return postMutableDic
           
      }
}

