//
//  OccupationTableView.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit


extension RegistrationViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.kOccupationCell, for: indexPath)
        cell.textLabel?.text = tableData[indexPath.row].name ?? ""
       return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userOccupation = tableData[indexPath.row].id ?? ""
        occupationTF.text = tableData[indexPath.row].name ?? ""
        crossButton.sendActions(for: .touchUpInside)
    }
    
}
