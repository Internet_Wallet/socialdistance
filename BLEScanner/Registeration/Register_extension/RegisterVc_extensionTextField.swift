//
//  RegisterVc_extension.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

//MARK:- TextField Delegate & Functions
extension RegistrationViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mobileNumTF {
            textField.text =  (self.country?.dialCode == "+95") ? "09" : ""
        }else if textField == occupationTF {
            occupationTF.tintColor = .clear
            self.view.endEditing(true)
            clearbutton.isHidden = true
            if registerButton.backgroundColor != UIColor.orange{
                Utility.alert(message: "Please Enter Mobile Number", title: "", controller: self)
            }else{
                if tableData.count > 0 {
                    self.showOccupationView(withFrame: Utility.setFrameForContactSuggestion(arrayCount: tableData.count, indexpath: IndexPath(item: 0, section: 0), onTextField: self.occupationTF, tableView: self.occupationListView), isHidden: false)
                }else{
                    if Utility.isNetworkAvailable(controller: self){
                        self.showLoader()
                        occupationViewModel.occupationDelegate = self
                        occupationViewModel.callOccupationApi(mobileNumber: mobileNumTF.text ?? "")
                        return false
                    }
                }
            }
            
        }else{
            clearbutton.isHidden = true
        }
        
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == mobileNumTF{
            
            if self.country?.dialCode == "+95" {
                textField.text = "09"
            } else {
                textField.text = ""
            }
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if textField == mobileNumTF {
            
                self.registerButton.isUserInteractionEnabled = true
                if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
                    let endOfDocument = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
                    return false
                }
                if currentSelectedCountry == .myanmarCountry {
                    if range.location <= 1 {
                        let newPosition = textField.endOfDocument
                        textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                        return false
                    }
                }
                
                if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
                switch currentSelectedCountry {
                case .indiaCountry:
                    
                    let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                    let textCount  = text.count
                    clearButtonShowHide(count: textCount)
                    if textCount > 10 {
                        
                        registerButton.backgroundColor = .orange
                        registerButton.setTitleColor(.white, for: .normal)
                       // self.registerButton.sendActions(for: .touchUpInside)
                        return false
                    } else {
                        registerButton.setTitleColor(.orange, for: .normal)
                        registerButton.backgroundColor = UIColor.lightGray
                        return true
                    }
                case .chinaCountry:
                    
                    let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                    let textCount  = text.count
                    clearButtonShowHide(count: textCount)
                    if textCount > 11  {
                        registerButton.setTitleColor(.white, for: .normal)
                        registerButton.backgroundColor = .orange
                       // self.registerButton.sendActions(for: .touchUpInside)
                        return false
                    } else {
                        registerButton.setTitleColor(.orange, for: .normal)
                        registerButton.backgroundColor = UIColor.lightGray
                        return true
                    }
                case .myanmarCountry:
                    if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
                    {
                        return false
                    }
                    
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        self.clearbutton.isHidden = true
                        return false
                    }
                    
                    let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                    let textCount  = text.count
                    if textCount > 2 {
                        self.clearbutton.isHidden = false
                    } else {
                        self.clearbutton.isHidden = true
                    }
                    
                    let object = validObj.getNumberRangeValidation(text)
                   // let validCount = object.min
                    
                    let  char = string.cString(using: String.Encoding.utf8)!
                    let isBackSpace = strcmp(char, "\\b")
                    
                    if (isBackSpace == -92) {
                        if textField.text == "09" {
                            textField.text = "09"
                            self.clearbutton.isHidden = true
                            return false
                        }
                    }
                    
                    if object.isRejected == true {
                        textField.text = "09"
                        self.clearbutton.isHidden = true
                        textField.resignFirstResponder()
                        
                        let text = "Invalid Mobile Number".localized
                       
                        Utility.alert(message: text, title: "", controller: self)
                      //  self.showAlert(text, image: nil, simChanges: false)
                        return false
                    }
                    
                    if textCount < object.min {
                        self.registerButton.isEnabled = false
                        self.registerButton.backgroundColor = .lightGray
                    } else if (textCount >= object.min && textCount < object.max) {
                        self.registerButton.isEnabled = true
                        registerButton.setTitleColor(.white, for: .normal)
                        self.registerButton.backgroundColor = .orange
                    } else if (textCount == object.max) {
                        textField.text = text
                        registerButton.setTitleColor(.white, for: .normal)
                        self.registerButton.backgroundColor = .orange
                       // self.registerButton.sendActions(for: .touchUpInside)
                        self.registerButton.isEnabled = true
                        return false
                    } else if textCount > object.max {
                        return false
                    }
                case .thaiCountry:
                    
                    let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                    let textCount  = text.count
                    clearButtonShowHide(count: textCount)
                    if textCount > 9  {
                        registerButton.setTitleColor(.white, for: .normal)
                        registerButton.backgroundColor = .orange
                        //self.registerButton.sendActions(for: .touchUpInside)
                        return false
                    }  else {
                        registerButton.setTitleColor(.orange, for: .normal)
                        registerButton.backgroundColor = UIColor.lightGray
                        return true
                    }

                    case .other:
                        
                        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                        let textCount  = text.count
                        clearButtonShowHide(count: textCount)
                        
                        if textCount == 13 {
                            registerButton.setTitleColor(.white, for: .normal)
                            registerButton.backgroundColor = .orange
                            self.registerButton.sendActions(for: .touchUpInside)
                            return false
                        }
                        
                        if textCount > 13 {
                            registerButton.setTitleColor(.white, for: .normal)
                            registerButton.backgroundColor = .orange
                            //self.registerButton.sendActions(for: .touchUpInside)
                            return false
                        }
                        
                        if textCount > 3  {
                            self.mobileNumTF.text = text
                            self.registerButton.isEnabled = true
                            registerButton.setTitleColor(.white, for: .normal)
                            self.registerButton.backgroundColor = .orange
                            //self.submitBtn.sendActions(for: .touchUpInside)
                            return false
                        }
                        else {
                            self.registerButton.isEnabled = false
                            self.registerButton.backgroundColor = .lightGray
                            return true
                        }
                    }
            

            
        }else if textField == occupationTF {
             if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "").inverted).joined(separator: "")) { return false }
        }
        
        return true
        
    }
    
}
