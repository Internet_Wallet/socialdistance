//
//  RegisterVC_Response.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit


//this will be get response when user will register
extension RegistrationViewController:ResponseDelegate{
  func getDataFromResponse(message: String,statusCode: String,title: String, OTP: String,mobileNumber: String){
        hideLoader()
        if statusCode == "200"{
            DispatchQueue.main.async {
                Utility.alertContoller(title: title, message: message, actionTitleFirst: "okay", actionTitleSecond: "", actionTitleThird: "", firstActoin: #selector(self.popBack), secondActoin: nil, thirdActoin: nil, controller: self)
            }
        }else{
            DispatchQueue.main.async {
                self.nameTF.text = ""
                self.occupationTF.text = ""
                self.userOccupation = ""
                
                if self.country?.dialCode == "+95" {
                    self.mobileNumTF.text = "09"
                } else {
                    self.mobileNumTF.text = ""
                }
                Utility.alert(message: message, title: title, controller: self)
            }
            
        }
        
    }
}

//this will get response when occupation service will hit

extension RegistrationViewController: OccupationResponse {
    func getOccupationData(occupationModelObj: OccupationModel?,message: String){
        hideLoader()
        if let data = occupationModelObj{
            if data.detailObj.count>0{
                tableData =  data.detailObj
                DispatchQueue.main.async {
                    self.occupationListView.delegate = self
                    self.occupationListView.dataSource = self
                    self.occupationListView.reloadData()
                    self.showOccupationView(withFrame: Utility.setFrameForContactSuggestion(arrayCount: data.detailObj.count, indexpath: IndexPath(item: 0, section: 0), onTextField: self.occupationTF, tableView: self.occupationListView), isHidden: false)
                }
            }else{
                DispatchQueue.main.async {
                    self.showOccupationView(withFrame: Utility.setFrameForContactSuggestion(arrayCount: data.detailObj.count, indexpath: IndexPath(item: 0, section: 0), onTextField: self.occupationTF, tableView: self.occupationListView), isHidden: true)
                }
            }
        }
        
    }
}
