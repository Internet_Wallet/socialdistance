//
//  RegisterUIUpdateExtension.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

extension RegistrationViewController{
    
    func showOccupationView(withFrame frame: CGRect,isHidden: Bool) {
        occupationListView.frame = CGRect(x: occupationTF.frame.origin.x, y: occupationTF.frame.origin.y - frame.height, width: frame.width, height: frame.height)
        crossButton.setImage(UIImage(named: "pt_close"), for: .normal)
        crossButton.frame = CGRect(x: occupationTF.frame.size.width - 20, y: occupationTF.frame.origin.y + 5, width: 35, height: 35)
        crossButton.addTarget(self, action: #selector(crossButtonAction), for: .touchUpInside)
        
        crossButton.isHidden = isHidden
        occupationListView.isHidden = isHidden
        occupationListView.layoutIfNeeded()
    }
    
    func setUI(){
        clearbutton.isHidden = true
        crossButton.isHidden = true
         phNumValidationsFile = getDataFromJSONFile() ?? []
        self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
        countryView = PaytoViews.updateView()
         let countryCode = String(format: "(%@)", "+95")
        occupationListView.cornerRadius = 7.5
        countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
        countryView?.delegate = self
        self.mobileNumTF.leftViewMode = .always
        self.mobileNumTF.leftView     = countryView
        dismissKey()
     //   headingLabel.setPropertiesForLabel()
        mobileNumTF.setPropertiesForTextField()
        nameTF.setPropertiesForTextField()
        occupationTF.setPropertiesForTextField()
        self.view.addSubview(crossButton)
        self.view.addSubview(occupationListView)
        self.view.bringSubviewToFront(occupationListView)
        self.view.bringSubviewToFront(crossButton)
        occupationListView.register(UITableViewCell.self, forCellReuseIdentifier: Constant.kOccupationCell)
    }
    
}
