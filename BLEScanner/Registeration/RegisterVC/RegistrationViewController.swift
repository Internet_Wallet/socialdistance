//
//  RegistrationViewController.swift
//  BLEScanner
//
//  Created by vamsi on 3/30/20.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import CoreLocation

class RegistrationViewController: UIViewController, CountryLeftViewDelegate, PhValidationProtocol,HelperFunctions{

    @IBOutlet weak var clearbutton: UIButton!
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var mobileNumTF: NoPasteTextField! {
        didSet {
            mobileNumTF.placeholder = "Enter Mobile Number".localized;
        }
    }
    @IBOutlet var nameTF: UITextField!{
        didSet {
            nameTF.placeholder = "Enter Name".localized;
        }
    }
    @IBOutlet var occupationTF: UITextField!{
        didSet {
            occupationTF.placeholder = "Enter Occupation".localized;
        }
    }
    @IBOutlet var registerButton: UIButton! {
        didSet {
            self.registerButton.setTitle("Registration".localized, for: .normal)
            self.registerButton.layer.cornerRadius = 4.0
            self.registerButton.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
            self.registerButton.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.registerButton.layer.shadowOpacity = 1.0
            self.registerButton.layer.shadowRadius = 4.0
            self.registerButton.layer.masksToBounds = false
            self.registerButton.isEnabled = true
        }
    }
    
    var countryView   : PaytoViews?
    var country       : Country?
    var registerViewModel =  RegisterViewModel()
    var occupationViewModel = GetOccupationViewModel()
    var tableData = [OccupationDetail]()
    var crossButton = UIButton()
    var occupationListView = UITableView()
    let validObj = PayToValidations.init()
    var userOccupation = ""
    
  public var currentSelectedCountry : countrySelected = .myanmarCountry
    enum countrySelected {
           case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
       }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
   
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func popBack(){
    //    self.navigationController?.popToRootViewController(animated: true)
        
        var homeVC: LoginViewController?
                      homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                      if let homeVC = homeVC {
                        homeVC.fromRegister = "Registration"
                          navigationController?.pushViewController(homeVC, animated: true)
                      }
        
    }
    
    @objc func crossButtonAction(sender: UIButton!) {
       self.showOccupationView(withFrame: Utility.setFrameForContactSuggestion(arrayCount: tableData.count, indexpath: IndexPath(item: 0, section: 0), onTextField: self.occupationTF, tableView: self.occupationListView), isHidden: true)
    }
    
    @IBAction func onClickClearButton(_ sender: Any) {
        self.clearbutton.isHidden = true
        mobileNumTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        registerButton.setTitleColor(.orange, for: .normal)
        self.registerButton.backgroundColor = UIColor.lightGray
        self.mobileNumTF.becomeFirstResponder()
    }
    
    func clearButtonShowHide(count: Int) {
        if count > 0 {
            self.clearbutton.isHidden = false
        } else {
            self.clearbutton.isHidden = true
        }
    }
   
    
    @IBAction func registerClick(_ sender: Any) {
        if mobileNumTF.text == "" {
            Utility.alert(message: "Please Enter MobileNumber".localized, title: "", controller: self)
        }
        else if nameTF.text == "" {
            Utility.alert(message: "Please Enter Name".localized, title: "", controller: self)
        }
        else if userOccupation == "" {
            Utility.alert(message: "Please Enter Occupation".localized, title: "", controller: self)
        }else{
            let defaults = UserDefaults()
            
            defaults .set(mobileNumTF.text, forKey: "RegisterNumber")
            defaults .set(nameTF.text, forKey: "RegisterName")
            defaults.synchronize()
            
//            if NetWorkCode.checkifSimAvailableInPhone(){
//                if registerButton.backgroundColor == UIColor.orange{
//                    if Utility.isNetworkAvailable(controller: self){
//                        showLoader()
//                        registerViewModel.delegate = self
//                        registerViewModel.callRegisterationApi(mobileNumber: mobileNumTF.text ?? "", name: nameTF.text ?? "", occupation: userOccupation, ultrasound: )
//                    }
//                }
//            }else{
//                Utility.alert(message: "Sim Not Available".localized, title: "", controller: self)
//            }
        }
    }
}












