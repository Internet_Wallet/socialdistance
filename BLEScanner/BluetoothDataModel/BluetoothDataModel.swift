//
//  BluetoothModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//




import Foundation
import CoreData

public class BluetoothDataModels: NSObject, NSCoding{
    
    public var obj: [BluetoothDataModel] = []
    
    enum Key:String {
        case obj = "bluetooth"
    }
    
    init(obj: [BluetoothDataModel]) {
        self.obj = obj
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(obj, forKey: Key.obj.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let mRanges = aDecoder.decodeObject(forKey: Key.obj.rawValue) as! [BluetoothDataModel]
        self.init(obj: mRanges)
    }
    
    
}

public class BluetoothDataModel: NSObject, NSCoding {
    
    public var bluetoothId: String = ""
    public var bluetoothName: String = ""
    public var bluetoothDate: String = ""
    public var bluetoothDateLessThanSixFeet: Int = 0
    public var peopleMeet: Int = 0
    public var userCategory: String = ""
    public var numberOfTimePeopleMeet: String = ""
    public var rssiID: String = ""
    public var mobileNumber: String = ""
    
    enum Key: String {
        case bluetoothId = "bluetoothId"
        case bluetoothName = "bluetoothName"
        case bluetoothDate = "bluetoothDate"
        case bluetoothDateLessThanSixFeet = "bluetoothDateLessThanSixFeet"
        case peopleMeet = "peopleMeet"
        case userCategory = "userCategory"
        case numberOfTimePeopleMeet = "numberOfTimePeopleMeet"
        case rssiID = "rssiID"
        case mobileNumber = "mobileNumber"
        
        
    }
    
    init(name: String,id: String,bluetoothDate: String,bluetoothDateLessThanSixFeet: Int,peopleMeet: Int,userCategory: String,numberOfTimePeopleMeet: String,rssiID: String,mobileNumber: String){
        self.bluetoothId = id
        self.bluetoothName = name
        self.bluetoothDate = bluetoothDate
        self.bluetoothDateLessThanSixFeet = bluetoothDateLessThanSixFeet
        self.peopleMeet = peopleMeet
        self.userCategory = userCategory
        self.numberOfTimePeopleMeet = numberOfTimePeopleMeet
        self.rssiID = rssiID
        self.mobileNumber = mobileNumber
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(bluetoothId, forKey: Key.bluetoothId.rawValue)
        aCoder.encode(bluetoothName, forKey: Key.bluetoothName.rawValue)
        aCoder.encode(bluetoothDate, forKey: Key.bluetoothDate.rawValue)
        aCoder.encode(bluetoothDateLessThanSixFeet, forKey: Key.bluetoothDateLessThanSixFeet.rawValue)
        aCoder.encode(peopleMeet, forKey: Key.peopleMeet.rawValue)
        aCoder.encode(userCategory, forKey: Key.userCategory.rawValue)
        aCoder.encode(numberOfTimePeopleMeet, forKey: Key.numberOfTimePeopleMeet.rawValue)
        aCoder.encode(rssiID, forKey: Key.rssiID.rawValue)
        aCoder.encode(mobileNumber, forKey: Key.mobileNumber.rawValue)
        
        
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let mbluetoothId = aDecoder.decodeObject(forKey: Key.bluetoothId.rawValue) as! String
        let mbluetoothName = aDecoder.decodeObject(forKey: Key.bluetoothName.rawValue) as! String
        let mbluetoothDate = aDecoder.decodeObject(forKey: Key.bluetoothDate.rawValue) as! String
        let userCategory = aDecoder.decodeObject(forKey: Key.userCategory.rawValue) as! String
        let boolValue = aDecoder.decodeInt32(forKey: Key.bluetoothDateLessThanSixFeet.rawValue)
        let peopleMeet = aDecoder.decodeInt32(forKey: Key.peopleMeet.rawValue)
        let numberOfPeople = aDecoder.decodeObject(forKey: Key.numberOfTimePeopleMeet.rawValue)  as! String
        let rssiId = aDecoder.decodeObject(forKey: Key.rssiID.rawValue)  as! String
        let mobileNumber = aDecoder.decodeObject(forKey: Key.mobileNumber.rawValue)  as! String
        self.init(name: String(mbluetoothName), id:
            String(mbluetoothId),bluetoothDate: mbluetoothDate,bluetoothDateLessThanSixFeet: Int(boolValue),peopleMeet: Int(peopleMeet),userCategory: String(userCategory),numberOfTimePeopleMeet: String(numberOfPeople),rssiID: String(rssiId),mobileNumber: String(mobileNumber))
    }
}

