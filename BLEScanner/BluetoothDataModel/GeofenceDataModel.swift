//
//  GeofenceDataModel.swift
//  BLEScanner
//
//  Created by iMac on 06/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import MapKit

public class GeofenceDataModels: NSObject, NSCoding{
    
    public var obj: [GeofenceDataModel] = []
    
    enum Key:String {
        case obj = "geofence"
    }
    
    init(obj: [GeofenceDataModel]) {
        self.obj = obj
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(obj, forKey: Key.obj.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let mRanges = aDecoder.decodeObject(forKey: Key.obj.rawValue) as! [GeofenceDataModel]
        
        self.init(obj: mRanges)
    }
    
    
}


public class GeofenceDataModel: NSObject, NSCoding {
    
    public var cordinate: CLLocationCoordinate2D?
    public var region: String = ""
    public var txtmsg: String = ""
    public var txtType: String = ""
    //    public var bluetoothDateLessThanSixFeet: Int = 0
    
    enum Key: String {
        case lat = "lat"
        case long = "long"
        case cordinate = "cordinate"
        case region = "region"
        case txtmsg = "txtmsg"
        case txtType = "txtType"
        //        case bluetoothDateLessThanSixFeet = "bluetoothDateLessThanSixFeet"
        
    }
    
    init(cordinate: CLLocationCoordinate2D, region: String, txtmsg: String , txtType: String){
        self.cordinate = cordinate
        self.region = region
        self.txtmsg = txtmsg
        self.txtType = txtType
        //        self.bluetoothDateLessThanSixFeet = bluetoothDateLessThanSixFeet
    }
    
    public override init() {
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(cordinate!.latitude , forKey: Key.lat.rawValue)
        aCoder.encode(cordinate!.longitude , forKey: Key.long.rawValue)
        aCoder.encode(region, forKey: Key.region.rawValue)
        aCoder.encode(txtmsg, forKey: Key.txtmsg.rawValue)
        aCoder.encode(txtType, forKey: Key.txtType.rawValue)
        //        aCoder.encode(bluetoothDateLessThanSixFeet, forKey: Key.bluetoothDateLessThanSixFeet.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let latitude = aDecoder.decodeDouble(forKey: Key.lat.rawValue) 
        let longitude = aDecoder.decodeDouble(forKey: Key.long.rawValue) 
        let cordinate = CLLocationCoordinate2D.init(latitude: latitude , longitude: longitude)
        let region = aDecoder.decodeObject(forKey: Key.region.rawValue) as! String
        let txtmsg = aDecoder.decodeObject(forKey: Key.txtmsg.rawValue) as! String
        let txttype = aDecoder.decodeObject(forKey: Key.txtType.rawValue) as! String
        //        let boolValue = aDecoder.decodeInt32(forKey: Key.bluetoothDateLessThanSixFeet.rawValue)
        self.init(cordinate: cordinate, region:
            region,txtmsg: txtmsg , txtType: txttype)
    }
}



