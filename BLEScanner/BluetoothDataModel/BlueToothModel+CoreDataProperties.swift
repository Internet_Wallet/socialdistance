//
//  BlueToothModel+CoreDataProperties.swift
//  
//
//  Created by Tushar Lama on 05/04/2020.
//
//

import Foundation
import CoreData


extension BlueToothModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BlueToothModel> {
        return NSFetchRequest<BlueToothModel>(entityName: "BlueToothModel")
    }

    @NSManaged public var bluetoothData: BluetoothDataModel?

}
