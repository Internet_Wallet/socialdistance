//
//  AppDelParam.swift
//  BLEScanner
//
//  Created by vamsi on 4/22/20.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct AppDelParam {
        
    static func getUserDetails(mobileNumber: String,OSType: String,simId: String,msId: String, appID: String) -> NSMutableDictionary{
     let userDic = NSMutableDictionary()
     userDic.setValue(mobileNumber, forKey: "MobileNumber")
     userDic.setValue(OSType, forKey: "OSType")
     userDic.setValue(simId, forKey: "simid")
     userDic.setValue(msId, forKey: "Msid")
     userDic.setValue(appID, forKey: "AppId")
     return userDic
    }
    static func getDeviceDetails(deviceID: String,gcmId: String) -> NSMutableDictionary{
      let deviceDic = NSMutableDictionary()
       
        deviceDic.setValue(deviceID, forKey: "DeviceId")
        deviceDic.setValue(gcmId, forKey: "GcmId")
        return deviceDic
    }
    
}
