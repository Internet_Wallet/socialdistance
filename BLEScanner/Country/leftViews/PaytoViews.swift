//
//  PaytoViews.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/10/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol CountryLeftViewDelegate : class {
     func publicCountryView()
}

class PaytoViews: UIView {
    
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryLabel: UILabel!
    
   weak var delegate : CountryLeftViewDelegate?
    
    class func updateView() -> PaytoViews? {
        let nameBundle = Bundle(for: PaytoViews.self)
        let nib   =  UINib(nibName: "PayToLeftView", bundle: nameBundle).instantiate(withOwner: nil, options: nil)[0] as? PaytoViews
        nib?.frame = .init(x: 0.00, y: 0.00, width: 120.00, height: 40.00)
        nib?.layoutUpdates()
        nib?.wrapCountryViewData(img: "Myanmar", str: "+95")
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func wrapCountryViewData(img: String, str: String) {
        let bundle = Bundle.init(for: PaytoViews.self)
        self.countryImage.image = UIImage.init(named: img, in: bundle, compatibleWith: nil)
        self.countryImage.contentMode = .scaleToFill
        if str.contains("(") {
            self.countryLabel.text  = str
        } else {
            self.countryLabel.text  = "(\(str))"
        }
        
        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        let vWidth = 65 + size.width
        self.countryLabel.textColor = .black
        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        self.layoutIfNeeded()
    }
    
    @IBAction func countryLeftViewAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.publicCountryView()
        }
    }
    
    deinit {
        self.delegate = nil
    }
}

