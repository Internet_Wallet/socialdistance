//
//  Protocols.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit
import CoreTelephony


 public protocol OKPaymentDelegates : class {
    func didEncounteredErrorWhilePayment(errMsg: NSError)
    func paymentSuccessfull(raw: Data?)
}

protocol PatternDelegate : class {
    func patterProcessed(_ hash: String)
}


 protocol HelperFunctions {}
 extension HelperFunctions {
     func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    if let ifaName = interface?.ifa_name {
                        if String(cString: ifaName) == "en0" {
                            var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                            getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                            address = String(cString: hostname)
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
    func getMsid() -> String {
        let networkInfo = CTTelephonyNetworkInfo.init()
        if #available(iOS 12.0, *) {
            
            guard let info = networkInfo.serviceSubscriberCellularProviders else { return "" }
            guard let network = info["serviceSubscriberCellularProvider"] else { return "" }
            guard let mnc = network.mobileNetworkCode else { return "" }
            return mnc

        } else {
            let carrier     = networkInfo.subscriberCellularProvider
            if let mnc = carrier?.mobileNetworkCode {
                return mnc
            }
            return ""
        }
    }
    
    func log<OK>(_ object: OK) {
        #if DEBUG
        print(object)
        #endif
    }
    
//    func xmlParsing(string: String) -> Dictionary<String, Any> {
//        let xml = SWXMLHash.parse(string)
//        var dictionary = Dictionary<String, Any>()
//        func enumerateMore(indexer: XMLIndexer) {
//            for child in indexer.children {
//                dictionary[child.element!.name] = child.element!.text
//                enumerateMore(indexer: child)
//            }
//        }
//
//        return dictionary
//    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            preLoginDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func countryViewController(delegate: CountryViewControllerDelegate?) -> CountryViewController? {
        let storyboardBundle = Bundle(for: CountryViewController.self)
        let storyboard = UIStoryboard.init(name: "Country", bundle: storyboardBundle)
        let vc       = storyboard.instantiateViewController(withIdentifier: "CountryViewController") as? CountryViewController
        vc?.delegate = delegate
        return vc
    }
    
    func setUniqueIDLogin() -> String {
        var appId: String = "OK_ID,"
        appId = appId + ("1.10.1,")
        appId = appId + (UIDevice.current.identifierForVendor?.uuidString)!
        appId = appId + (",")
        appId = appId + (UIDevice.current.model)
        appId = appId + (",")
        appId = appId + (UIDevice.current.model)
        appId = appId + (",")
        appId = appId + (UIDevice.current.systemVersion)
        appId = appId + (",")
        appId = appId + ("0")
        appId = appId + (",")
        appId = appId + ("0")
        log("full \(appId)")
        return appId
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                log(error.localizedDescription)
            }
        }
        return nil
    }

}
