//
//  CategoryViewController.swift
//  BLEScanner
//
//  Created by iMac on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

//data sync




import UIKit
import AudioToolbox.AudioServices
import AVFoundation

class CategoryViewController: SocialDistanceBaseVC {
    
    @IBOutlet weak var lblTotal: UILabel!{
        didSet {
            lblTotal.text = "Total".localized
        }
    }
    
    @IBOutlet weak var lblCurrentUserCount: UILabel!{
        didSet {
            lblCurrentUserCount.text = "Current People Around Me".localized
        }
    }
    
    @IBOutlet weak var lblPeopleIMet: UILabel!{
           didSet {
               lblPeopleIMet.text = "People I Met as on Date".localized
           }
       }

    @IBOutlet weak var currentUSerCount: UILabel!
    @IBOutlet weak var CategoaryTbl: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet var nowbutton: UIButton!
    
    var alert: UIAlertController? = nil
    var yesButton: UIAlertAction? = nil
    var noButton: UIAlertAction? = nil

    @IBOutlet weak var totalCount: UILabel!
    @IBOutlet var userImage: UIImageView!{
        didSet{
            userImage.applyshadowWithCorner(containerView: userImage.inputView ?? UIView(), cornerRadious: userImage.frame.width/2)
        }
    }
    
    var lessSixMore = 0
    var lessSixLess = 0
    let settingObj = ApplicationViewModel()
    
    var categoryArray = ["Less Than 6 Ft. More Than 2 Min.",
                         "Less Than 6 Ft. Less Than 2 Min.",
                         "More Than 6 Ft. More Than 2 Min.",
                         "More Than 6 Ft. Less Than 2 Min."]
   
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(userscount), name: NSNotification.Name(rawValue: "RefreshUser"), object: nil)
        
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        imgUser.layer.borderWidth = 1.0
        imgUser.layer.masksToBounds = false
        imgUser.layer.borderColor = UIColor.white.cgColor
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2
        imgUser.clipsToBounds = true
        showLoader()
        settingObj.callAppSettingApi(mobileNumber: ApplicationState.sharedAppState.currentUser.MobileNumber)
        
        let delayTime = DispatchTime.now() + 3.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.hideLoader()
            self.fetchData()
            self.setData()
            GeofencingRegion.shared.startUpdatingLocation()
            GeofencingRegion.shared.loadMonitoringRegions()
            
        })
        nowbutton.layer.cornerRadius =  15.0
    }
    
    @objc func userscount(){
        var audioCount = 0
        var bluetoothCount = 0
        if ApplicationState.sharedAppState.currentUserCount.count>0{
            
            let dataValue =  ApplicationState.sharedAppState.currentUserCount.filter({$0.bluetoothDateLessThanSixFeet == 1 || $0.bluetoothDateLessThanSixFeet == 2 || $0.bluetoothDateLessThanSixFeet == 10})
            bluetoothCount = dataValue.count
        }
        
        if ApplicationState.sharedAppState.currentAudioUserCount.count>0{
            let dataValue =  ApplicationState.sharedAppState.currentAudioUserCount.filter({$0.bluetoothDateLessThanSixFeet == 1 || $0.bluetoothDateLessThanSixFeet == 2})
            audioCount = dataValue.count
        }
        
        currentUSerCount.text = "\(bluetoothCount + audioCount)"
        self.totalCount.text = "0"
        setData()
    }
    
    func setData(){
        lessSixLess = 0
        lessSixLess = 0
        DispatchQueue.main.async {
           self.CategoaryTbl.reloadData()
        }
        
        if ApplicationState.sharedAppState.blueToothModelObj.count>0{
           let localModelObj = ApplicationState.sharedAppState.blueToothModelObj
            let value = localModelObj.filter({$0.bluetoothDateLessThanSixFeet == 1 || $0.bluetoothDateLessThanSixFeet == 2})
            lessSixMore = ApplicationState.sharedAppState.blueToothModelObj.filter({$0.bluetoothDateLessThanSixFeet == 2}).count
            lessSixLess = ApplicationState.sharedAppState.blueToothModelObj.filter({$0.bluetoothDateLessThanSixFeet == 1}).count
            
            DispatchQueue.main.async {
                self.totalCount.text = String(value.count)
                self.CategoaryTbl.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblPeopleIMet.text = "People I Met as on Date".localized
        lblCurrentUserCount.text = "Current People Around Me".localized
        lblTotal.text = "Total".localized
        lblPeopleIMet.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
        lblCurrentUserCount.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
        lblTotal.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0)
        
        //   fetchData()
        lblName.text = ApplicationState.sharedAppState.currentUser.mobileNumberToDisplay
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        view.addSubview(statusBarView)
        
        CategoaryTbl.delegate = self
        CategoaryTbl.dataSource = self
        self.animate()

        //Engine start & Stop
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
                appDel.startListening()
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
                appDel.invokeTimer()
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
               // appDel.stopListening()
            })
            
    }
    
    func fetchData(){
        let delayTime = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            ApplicationState.sharedAppState.currentUser.bluetoothScantime = Utility.getDateForCoreData()
          ApplicationState.sharedAppState.blueToothMainClassObj.isSartedCalled = false
          ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
        })
    }
    
    
    func animate() {
        self.CategoaryTbl.reloadData()
        let cells = self.CategoaryTbl.visibleCells
        let tableHeight: CGFloat = self.CategoaryTbl.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.3, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    @IBAction func nowbuttonClick(_ sender: Any) {
        
    }
    
    
}
//MARK: - UITableViewDataSource, UITableViewDelegate
extension CategoryViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: CategoaryViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as? CategoaryViewCell
       // cell.loadProgressView()
        cell.wrapCategoary(string: categoryArray[indexPath.row])
        
        switch indexPath.row {
        case 0:
            cell.lblProgress.text = "\(lessSixMore)"
        case 1:
            cell.lblProgress.text = "\(lessSixLess)"
        case 2:
            cell.lblProgress.text = "\(0)"
        case 3:
            cell.lblProgress.text = "\(0)"
        default:
            break
        }

        if indexPath.row == 0 {
            cell.progressView.progressTintColor = .red
        } else if indexPath.row == 1 {
            cell.progressView.progressTintColor = .orange
        }  else if indexPath.row == 2 {
                   cell.progressView.progressTintColor = .yellow

               } else if indexPath.row == 3 {
                   cell.progressView.progressTintColor = .green

               }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

class CategoaryViewCell: UITableViewCell {
    
    let progress = Progress(totalUnitCount: 100)
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet var lblheader: UILabel!{
        didSet {
            self.lblheader.text = self.lblheader.text
            self.lblheader.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    
    @IBOutlet var lblProgress: UILabel!{
        didSet {
            self.lblProgress.text = self.lblProgress.text
            self.lblProgress.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.imgPersonIcon?.layer.cornerRadius = (self.imgPersonIcon?.frame.width ?? 0) / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapCategoary(string: String) {
        lblheader.text = string.localized
        lblheader.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
    }
    
    func loadProgressView() {
        progressView.progress = 0.0
        progress.completedUnitCount = 0
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                return
            }
            self.progress.completedUnitCount += 1
            self.progressView.setProgress(Float(self.progress.fractionCompleted), animated: true)
            
        }
    }
}
