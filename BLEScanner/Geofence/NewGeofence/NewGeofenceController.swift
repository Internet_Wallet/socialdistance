//
//  NewGeofenceController.swift
//  BLEScanner
//
//  Created by iMac on 09/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class NewGeofenceController: UIViewController {
    
    
    var allRegions: [CircularRegion]! = nil
    let player = AVQueuePlayer()
    var state = ""
    let ENTERED_HOME_REGION_MESSAGE = "Welcome to Home geofence If the waves are good, you can try surfing!"
    let ENTERED_WORK_REGION_MESSAGE = "Welcome to work geofence If the waves are good, you can try surfing!"
    let ENTERED_REGION_NOTIFICATION_ID = "EnteredRegionNotification"
    let EXITED_Home_REGION_MESSAGE = "Bye! Hope you had a great day at the home grofence!"
    let EXITED_WORK_REGION_MESSAGE = "Bye! Hope you had a great day at the work grofence!"
    let EXITED_REGION_NOTIFICATION_ID = "ExitedRegionNotification"
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    var CHARSET = ""
    var myLanguage = ""
     
    
    @IBOutlet weak var searchView: CardView!{
        didSet{
            self.searchView.isHidden = true
        }
    }
    
    @IBOutlet weak var customAnnotionView: CardView!{
        didSet{
            self.customAnnotionView.isHidden = true
        }
    }
    
    
    @IBOutlet var editHomeLocationButton: UIButton?{
        didSet {
            self.editHomeLocationButton?.setTitle("Set My Home Location".localized, for: .normal)
            editHomeLocationButton?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var editWorkLocationButton: UIButton?{
        didSet {
            self.editWorkLocationButton?.setTitle("Add My Work Location".localized, for: .normal)
            editWorkLocationButton?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var myLocation: UIButton?{
        didSet {
            self.myLocation?.setTitle("OK".localized, for: .normal)
            //myLocation?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var workLocation: UIButton?{
        didSet {
            self.workLocation?.setTitle("OK".localized, for: .normal)
            //workLocation?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var quarintineLocation: UIButton?{
        didSet {
            self.quarintineLocation?.setTitle("OK".localized, for: .normal)
            //quarintineLocation?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var editLocationButton: UIButton?{
        didSet {
            // editLocationButton?.isHidden = true
            editLocationButton?.setRadiusWithShadow(22)
        }
    }
    
    @IBOutlet var deleteBtn: UIButton?{
        didSet {
            deleteBtn?.setRadiusWithShadow(10)
            deleteBtn?.isHidden = true
        }
    }
    
    @IBOutlet var deleteLbl : UILabel?{
        didSet {
            self.deleteLbl?.text = self.deleteLbl?.text?.localized
        }
    }
    
    
    @IBOutlet var AddressLbl : UILabel?
    @IBOutlet var searchLocation : UITextField?{
        didSet {
            searchLocation?.placeholder = "Search Location".localized
        }
    }
    
    
    
    @IBOutlet var myLocationBtnHeight: NSLayoutConstraint?
    @IBOutlet var workLocationBtnHeight: NSLayoutConstraint?
    @IBOutlet var quarintineBtnHeight: NSLayoutConstraint?
    
    @IBOutlet var deleteView : UIView!
    @IBOutlet var deleteBaseVeiw : UIView!
    @IBOutlet weak var mapView: MKMapView!
    var destinationString : String?
    
    var titleStr = ""
    var markerImageView: UIImageView?
    var annotation = MKPointAnnotation()
    var CustomMApView = MKAnnotationView()
    var AfterDragCoordinate: CLLocationCoordinate2D?
    
    
    func loadInitialize() {
        if myLanguage == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "
        }else if myLanguage == "uni"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀.@_ - "
        }else {
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ -"
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        self.loadInitialize()
        self.loadMultipleRegion()
        if self.destinationString == Constant.quarintine {
            self.title = "Quarantine".localized
        }
        else {
            if ApplicationState.sharedAppState.geofenceModelObj.count >= 2 {
                self.title = titleStr.localized
            } else {
                self.title = titleStr.localized
            }
        }
        
        if self.destinationString == Constant.geofenceHome {
            
            if let isPresent = self.checkForGeoFense(typeX: Constant.typeHomeLocation) as? Bool , isPresent == true {
                self.hideButtonOption(type: Constant.geofenceHome , isdeleteBtn: false)
            }
            else {
                self.geoFenceload()
                self.loadMapIcon()
                self.showButtonOption(type: Constant.geofenceHome, isdeleteBtn: true)
            }
        }
        else if (self.destinationString == Constant.geofenceWork) {
            
            if let isPresent = self.checkForGeoFense(typeX: Constant.typeWorkLocation) as? Bool , isPresent == true{
                self.hideButtonOption(type: Constant.geofenceWork , isdeleteBtn: false)
            }
            else {
                self.geoFenceload()
                self.loadMapIcon()
                self.showButtonOption(type: Constant.geofenceWork, isdeleteBtn: true)
            }
        }
        else {
            if let lastAddress = UserDefaults.standard.value(forKey: "lastQuirineAddress") as? String{
                self.AddressLbl?.text = lastAddress
            }
            self.showButtonOption(type: Constant.quarintine, isdeleteBtn: false)
        }
        
    }
    
    
    func checkForGeoFense(typeX : String) -> Bool {
        var isFound : Bool = false
        for obj in self.allRegions{
            if let type = obj.typeIdentifier as? String , type == typeX {
                isFound = true
                break
            }
        }
        return isFound
    }
    
    
    func showButtonOption(type : String , isdeleteBtn : Bool) {
        switch (type) {
        case Constant.geofenceHome:
            self.setSearchButton()
            self.editHomeLocationButton?.isHidden = false
            self.editWorkLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        case Constant.geofenceWork:
            self.setSearchButton()
            self.editWorkLocationButton?.isHidden = false
            self.editHomeLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        default:
            self.searchView.isHidden = true
            self.editHomeLocationButton?.isHidden = true
            self.editWorkLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        }
    }
    
    
    func hideButtonOption(type : String , isdeleteBtn : Bool) {
        switch (type) {
        case Constant.geofenceHome:
            self.editHomeLocationButton?.isHidden = true
            self.editWorkLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        case Constant.geofenceWork:
            self.editWorkLocationButton?.isHidden = true
            self.editHomeLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        default:
            self.editHomeLocationButton?.isHidden = true
            self.editWorkLocationButton?.isHidden = true
            self.deleteBtn?.isHidden = isdeleteBtn
        }
        
    }
    
    
    func loadMultipleRegion() {
        allRegions = CircularRegion.loadata()
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func geoFenceload(){
        GeofencingRegion.shared.delegate = self
        GeofencingRegion.shared.startUpdatingLocation()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.title = titleStr.localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]
        
        self.mapView.delegate = self
        self.deleteBaseVeiw.isHidden = true
        
        let gestureTap = UITapGestureRecognizer.init(target: self , action: #selector(removeBaseView))
        gestureTap.numberOfTapsRequired = 1
        self.deleteBaseVeiw.addGestureRecognizer(gestureTap)
        
        
        
        
        self.setBackButton()
        
        
        
        searchLocation?.delegate = self
        searchLocation?.addPadding(padding: .left(5))
        searchLocation?.addTarget(self, action: #selector(GenerateQR.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        
        
    }
    
    @objc func removeBaseView () {
        self.deleteBaseVeiw.isHidden = true
    }
    
    
    func location(lat: String,long:String){
        
        if Utility.isNetworkAvailable(controller: self) {
            showLoader()
            let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
            if isPermission {
                VMGeoLocationManager.shared.startUpdateLocation()
                VMGeoLocationManager.shared.getAddressFrom(lattitude:"\(lat)", longitude: "\(long)", language: "en")
                { (status, data) in
                    
                    self.hideLoader()
                    
                    
                    if let addressDic = data as? Dictionary<String, String> {
                        DispatchQueue.main.async {
                            
                            print(addressDic)
                            var address = ""
                            if let street = addressDic["street"] as? String {
                                print(street)
                                address += street
                            }
                            if let region = addressDic["region"] as? String {
                                print(region)
                                address += " , \(region)"
                            }
                            
                            print("\(addressDic)")
                            
                            self.AddressLbl?.text = address
                            
                            
                            if let region = addressDic["formatted_address"] as? String, address == ""  {
                                print(region)
                                self.AddressLbl?.text = region
                            }
                            
                        }
                    }
                }
            }
        }else {
            Utility.alert(message: "", title: "No Network Available".localized, controller: self)
        }
    }
    
    
    
    func getCordination(PlaceId: String){
        if Utility.isNetworkAvailable(controller: self) {
            showLoader()
            let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
            if isPermission {
                VMGeoLocationManager.shared.startUpdateLocation()
                
                VMGeoLocationManager.shared.getCordinateFromPlaceId(placeId: PlaceId, language: "en") { (status, data) in
                    if let addressDic = data as? NSDictionary {
                        DispatchQueue.main.async {
                            self.hideLoader()
                            print(addressDic)
                            if let addressDic = addressDic["location"] as? NSDictionary {
                                let lat = addressDic.value(forKey: "lat") as? Double
                                let lon = addressDic.value(forKey: "lng") as? Double
                                
                                
                                
                                let coordinates = CLLocationCoordinate2D(latitude:lat ?? 0.0
                                    , longitude:lon ?? 0.0)
                                self.currentCordinateShow(coordinate: coordinates)
                            }
                        }
                    }
                }
            }
        }else {
            Utility.alert(message: "", title: "No Network Available".localized, controller: self)
        }
    }
    
    
    
    func loadMapIcon(){
        markerImageView = UIImageView(image: UIImage(named: "placeholder"))
        //        markerImageView?.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        //        markerImageView?.center = mapView.center
        
        
        markerImageView?.frame = CGRect(x: (self.view.frame.size.width / 2)    , y: (self.view.frame.size.height / 2) - 50  , width: 45 , height: 45 )
        markerImageView?.center = CGPoint(x: (self.view.frame.size.width / 2)   , y: (self.view.frame.size.height / 2) - 50 )
        
        
        
        mapView.addSubview(markerImageView ?? UIImageView())
        markerImageView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.markerImageView?.transform = .identity
            },
                       completion: nil)
    }
    
    
    func AfterDragInitialRegion(radius: Int){
        if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
            //            self.loadMultipleRegion(latitute: "\(lat)", longitute: "\(long)", radius: Float(radius), msg: "AfterDragInitialRegion")
            if let overlays = mapView?.overlays {
                for overlay in overlays {
                    
                    mapView?.removeOverlay(overlay)
                    
                }
            }
            
            let geofenceRegionCenter = CLLocationCoordinate2DMake(lat, long);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: CLLocationDistance(radius), identifier: "CurrentLocation");
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
            self.mapView.setRegion(mapRegion, animated: true)
            let regionCircle = MKCircle(center: geofenceRegionCenter, radius: CLLocationDistance(radius))
            self.mapView.addOverlay(regionCircle)
            self.mapView.showsUserLocation = true;
            
            
            
        }
    }
    
    
    
    @objc func backClick(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBackButton(){
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        button.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        
    }
    
    
    func setSearchButton(){
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "searchBtn"), for: .normal)
        button.addTarget(self, action: #selector(searchClick), for: .touchUpInside)
        //        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = item
        
    }
    
    @objc func searchClick(){
        self.searchView.isHidden = false
    }
    
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled".localized, withDescription: "Please enable Location Services in your Settings".localized, onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
                self.dismiss(animated: true, completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Setting".localized, AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    @IBAction func CurrentLocationClick(_ sender: UIButton){
        self.geoFenceload()
    }
    
    
    @IBAction func deleteLocationClick(_ sender: UIButton){
        self.deleteBaseVeiw.isHidden = false
        if self.destinationString == Constant.quarintine {
            self.deleteLbl?.text = "Remove your Quarantine location ?".localized
            self.deleteView.isHidden = false
            self.deleteView.frame = CGRect(x: ((self.view.frame.size.width / 2) - 125 ) , y: ((self.view.frame.size.height / 2) - 75 ), width: 250 , height: 160 )
            self.myLocation?.isHidden = true
            self.workLocation?.isHidden = true
            self.quarintineLocation?.isHidden = false
            self.myLocationBtnHeight?.constant = 0
            self.workLocationBtnHeight?.constant = 0
            self.quarintineBtnHeight?.constant = 40
            self.deleteBaseVeiw.addSubview(deleteView)
        }
        else {
            self.deleteView.isHidden = false
            self.deleteView.frame = CGRect(x: ((self.view.frame.size.width / 2) - 125 ) , y: ((self.view.frame.size.height / 2) - 75 ), width: 250 , height: 160 )
            if self.destinationString == Constant.geofenceHome {
                self.deleteLbl?.text = "Remove Your Home Location ?".localized
                if let isPresent = self.checkForGeoFense(typeX: Constant.typeHomeLocation) as? Bool {
                    self.myLocation?.isHidden = false
                    self.workLocation?.isHidden = true
                    self.quarintineLocation?.isHidden = true
                    
                    self.myLocationBtnHeight?.constant = 40
                    self.workLocationBtnHeight?.constant = 0
                    self.quarintineBtnHeight?.constant = 0
                    
                    self.deleteBaseVeiw.addSubview(deleteView)
                }
            }
            else if (self.destinationString == Constant.geofenceWork) {
                self.deleteLbl?.text = "Remove Your Work Location ?".localized
                if let isPresent = self.checkForGeoFense(typeX: Constant.typeWorkLocation) as? Bool {
                    self.myLocation?.isHidden = true
                    self.workLocation?.isHidden = false
                    self.quarintineLocation?.isHidden = true
                    
                    self.myLocationBtnHeight?.constant = 0
                    self.workLocationBtnHeight?.constant = 40
                    self.quarintineBtnHeight?.constant = 0
                    
                    self.deleteBaseVeiw.addSubview(deleteView)
                }
            }
            else {
                
                self.myLocation?.isHidden = true
                self.workLocation?.isHidden = true
                self.quarintineLocation?.isHidden = false
                
                self.myLocationBtnHeight?.constant = 0
                self.workLocationBtnHeight?.constant = 0
                self.quarintineBtnHeight?.constant = 40
                
                self.deleteBaseVeiw.addSubview(deleteView)
            }
            
        }
        
    }
    
    
    
    
    @IBAction func AddHomeLocationClick(_ sender: UIButton){
        
        if editHomeLocationButton?.titleLabel?.text == "Set My Home Location".localized{
            
            if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                self.loadMultipleRegion(latitute: "\(lat)", longitute: "\(long)", radius: 600, msg: "", txtType: Constant.typeHomeLocation)
            }
            UserDefaults.standard.set(self.AddressLbl?.text, forKey: "lastHomeAddress")
            UserDefaults.standard.synchronize()
            self.editWorkLocationButton?.isEnabled = true
            self.editWorkLocationButton?.alpha = 1
            self.showSAert(message: "Home location added successfully".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
            
        }
        
    }
    
    @IBAction func AddWorkLocationClick(_ sender: UIButton){
        
        
        if sender.titleLabel?.text == "Add My Work Location".localized{
            if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                self.loadMultipleRegion(latitute: "\(lat)", longitute: "\(long)", radius: 600, msg: "", txtType: Constant.typeWorkLocation)
            }
            UserDefaults.standard.set(self.AddressLbl?.text, forKey: "lastWorkAddress")
            UserDefaults.standard.synchronize()
            self.editWorkLocationButton?.isEnabled = false
            self.editWorkLocationButton?.alpha = 1
            self.showSAert(message: "Work location added successfully".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
            
            
        }
        
    }
    
    
    @objc func addRegion() {
        
        
    }
    
    
    func loadMultipleRegion(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) {
        let allRegions = CircularRegion.setData(latitute: latitute, longitute: longitute, radius: radius, msg: msg, txtType: txtType)
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func setUpGeofenceForPlayaGrandeBeach(regions: [CircularRegion]) {
        for (index, _region) in regions.enumerated() {
            let regiondata = regions[index] as? CircularRegion
            let type = regiondata?.typeIdentifier
            if self.destinationString == Constant.geofenceHome {
                if (type == Constant.typeHomeLocation) {
                    let annotation = MKPointAnnotation()
                    let Coordinates = CLLocationCoordinate2D(latitude: regions[index].coordinate.latitude, longitude: regions[index].coordinate.longitude)
                    annotation.coordinate = Coordinates
                    mapView.addAnnotation(annotation)
                    let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
                    let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: regions[index].coordinate.radius, identifier: "CurrentLocation");
                    geofenceRegion.notifyOnExit = true;
                    geofenceRegion.notifyOnEntry = true;
                    let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
                    let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
                    self.mapView.setRegion(mapRegion, animated: true)
                    let regionCircle = MKCircle(center: geofenceRegionCenter, radius: regions[index].coordinate.radius)
                    self.mapView.addOverlay(regionCircle)
                    self.mapView.showsUserLocation = true;
                }
            }
            else if (self.destinationString == Constant.geofenceWork) {
                if (type == Constant.typeWorkLocation) {
                    let annotation = MKPointAnnotation()
                    let Coordinates = CLLocationCoordinate2D(latitude: regions[index].coordinate.latitude, longitude: regions[index].coordinate.longitude)
                    annotation.coordinate = Coordinates
                    mapView.addAnnotation(annotation)
                    let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
                    let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: regions[index].coordinate.radius, identifier: "CurrentLocation");
                    geofenceRegion.notifyOnExit = true;
                    geofenceRegion.notifyOnEntry = true;
                    let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
                    let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
                    self.mapView.setRegion(mapRegion, animated: true)
                    let regionCircle = MKCircle(center: geofenceRegionCenter, radius: regions[index].coordinate.radius)
                    self.mapView.addOverlay(regionCircle)
                    self.mapView.showsUserLocation = true;
                }
            }
            else {
                if type == Constant.typeQuarintineLocation {
                    let annotation = MKPointAnnotation()
                    let Coordinates = CLLocationCoordinate2D(latitude: regions[index].coordinate.latitude, longitude: regions[index].coordinate.longitude)
                    annotation.coordinate = Coordinates
                    mapView.addAnnotation(annotation)
                    let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
                    let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: regions[index].coordinate.radius, identifier: "CurrentLocation");
                    geofenceRegion.notifyOnExit = true;
                    geofenceRegion.notifyOnEntry = true;
                    let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
                    let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
                    self.mapView.setRegion(mapRegion, animated: true)
                    let regionCircle = MKCircle(center: geofenceRegionCenter, radius: regions[index].coordinate.radius)
                    self.mapView.addOverlay(regionCircle)
                    self.mapView.showsUserLocation = true;
                }
            }
        }
    }
    
    @objc func playSound(){
        self.dismiss(animated: true, completion: {
            
            //            if let url = Bundle.main.url(forResource: "please_do_not_break_quarantine", withExtension: "m4a") {
            //                self.player.volume = 10
            //                self.player.removeAllItems()
            //                self.player.insert(AVPlayerItem(url: url), after: nil)
            //                self.player.play()
            //            }
        })
        
        
    }
    
    // MARK:- delete the locationAction
    
    @IBAction func deleteMyHomeLocation(_ sender: UIButton){
        
        //        self.showSAert(message: "Are you sure, you want to delete location?".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "Yes".localized, actionTitleSecond: "No", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
        
        
        
        print(ApplicationState.sharedAppState.geofenceModelObj.count)
        // let objToDelet =  ApplicationState.sharedAppState.geofenceModelObj.filter({$0.txtType == Constant.typeHomeLocation})
        for i in 0..<ApplicationState.sharedAppState.geofenceModelObj.count{
            if ApplicationState.sharedAppState.geofenceModelObj[i].txtType == Constant.typeHomeLocation{
                ApplicationState.sharedAppState.geofenceModelObj.remove(at: i)
                break
            }
        }
        self.deleteBaseVeiw.isHidden = true
        self.showSAert(message: "Home location deleted successfully".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
    }
    
    @IBAction func deleteMyWorkLocation(_ sender: UIButton){
        
        
        
        print(ApplicationState.sharedAppState.geofenceModelObj.count)
        // let objToDelet =  ApplicationState.sharedAppState.geofenceModelObj.filter({$0.txtType == Constant.typeWorkLocation})
        for i in 0..<ApplicationState.sharedAppState.geofenceModelObj.count{
            if ApplicationState.sharedAppState.geofenceModelObj[i].txtType == Constant.typeWorkLocation{
                ApplicationState.sharedAppState.geofenceModelObj.remove(at: i)
                break
            }
        }
        self.deleteBaseVeiw.isHidden = true
        self.showSAert(message: "Work location deleted successfully".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
    }
    
    @IBAction func deleteMyQuarintineLocation(_ sender: UIButton){
        
        print(ApplicationState.sharedAppState.geofenceModelObj.count)
        // let objToDelet =  ApplicationState.sharedAppState.geofenceModelObj.filter({$0.txtType == Constant.typeQuarintineLocation})
        for i in 0..<ApplicationState.sharedAppState.geofenceModelObj.count{
            if ApplicationState.sharedAppState.geofenceModelObj[i].txtType == Constant.typeQuarintineLocation{
                ApplicationState.sharedAppState.geofenceModelObj.remove(at: i)
                break
            }
        }
        self.deleteBaseVeiw.isHidden = true
        ApplicationState.sharedAppState.currentUser.isQuarintine = false
        self.showSAert(message: "Quarantine location deleted successfully".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(playSound), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
    }
    
    @IBAction func EditLocationClick(_ sender: UIButton){
        self.editHomeLocationButton?.isHidden = false
        self.editWorkLocationButton?.isHidden = false
        sender.isHidden = true
        if sender.tag == 0{
            sender.titleLabel?.text = "Edit My Home Location".localized
        }else{
            sender.titleLabel?.text = "Edit My Work Location".localized
        }
    }
    
    func displayCustomAnnotation(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        // 1
        if view.annotation is MKUserLocation{
            // Don't proceed with custom callout
            return
        }
        
        self.customAnnotionView.isHidden = false
        self.customAnnotionView.center = CGPoint(x: view.bounds.size.width / 2, y: -self.customAnnotionView.bounds.size.height*0.52)
        view.addSubview(self.customAnnotionView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
}

extension NewGeofenceController: MKMapViewDelegate{
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case MapType.StandardMap.rawValue:
            self.mapView.mapType = .standard
        case MapType.SatelliteMap.rawValue:
            self.mapView.mapType = .satellite
        case MapType.HybridMap.rawValue:
            self.mapView.mapType = .hybrid
        default:
            break
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        //        let identifier = "MyPin"
        //        if annotation is MKUserLocation {
        //            return nil
        //        }
        //        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        //        if annotationView == nil {
        //            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        //            annotationView?.canShowCallout = true
        //
        //            annotationView?.image = UIImage(named: "location_map")
        //            // annotationView?.frame = CGRect(x: (annotationView?.center.x)!, y: (annotationView?.center.y)! + 100, width: 60, height: 60)
        //
        //            let detailButton = UIButton(type: .detailDisclosure)
        //            annotationView?.rightCalloutAccessoryView = detailButton
        //        } else {
        //            annotationView?.annotation = annotation
        //        }
        //        self.CustomMApView = annotationView ?? MKAnnotationView()
        //        return annotationView
        return nil
    }
    
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        //print("regionDidChangeAnimated")
        
        self.AfterDragCoordinate = mapView.centerCoordinate
        
        
        
        
        if self.destinationString == Constant.geofenceHome {
            
            if let isPresent = self.checkForGeoFense(typeX: Constant.typeHomeLocation) as? Bool , isPresent == true {
                // hide
                if let lastAddress = UserDefaults.standard.value(forKey: "lastHomeAddress") as? String{
                    self.AddressLbl?.text = lastAddress
                }
            }
            else {
                // show
                if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                    self.location(lat: "\(lat)", long: "\(long)")
                }
            }
        }
        else if (self.destinationString == Constant.geofenceWork) {
            
            if let isPresent = self.checkForGeoFense(typeX: Constant.typeWorkLocation) as? Bool , isPresent == true{
                // hide
                if let lastAddress = UserDefaults.standard.value(forKey: "lastWorkAddress") as? String{
                    self.AddressLbl?.text = lastAddress
                }
            }
            else {
                // show
                if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                    self.location(lat: "\(lat)", long: "\(long)")
                }
            }
        }
        
        
    }
    
    
    //MARK - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
        overlayRenderer.lineWidth = 4.0
        overlayRenderer.strokeColor = UIColor.red
        overlayRenderer.fillColor = UIColor(red: 76.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 0.7)
        return overlayRenderer
    }
}

extension NewGeofenceController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.setUpStreetList()
        self.showStreetView(text: textField.text ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if textField == searchLocation {
            
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
        if str == " " && textField.text?.last == " "{
            return false
        } else {
            if characterBeforeCursor(tf: textField) == " " && str == " " {
                return false
            } else {
                if characterAfterCursor(tf: textField) == " " && str == " " {
                    return false
                } else {
                    return true
                }
            }
        }
    }
    
    func characterBeforeCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
    func characterAfterCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
}

extension NewGeofenceController : StreetListViewDelegate {
    
    func setUpStreetList() {
        if !self.view.contains(tvStreetList)  {
            tvStreetList.frame = CGRect(x: 5, y: 130, width: self.view.frame.width - 10, height: 50)
            tvStreetList.backgroundColor = UIColor.clear
            tvStreetList.delegate = self
            tvStreetList.tvList.isHidden = true
            tvStreetList.isUserInteractionEnabled = true
            
            self.view.addSubview(tvStreetList)
            self.view.bringSubviewToFront(tvStreetList)
        }
    }
    
    func showStreetView(text: String) {
        if text.count > 0 {
            if self.view.contains(tvStreetList) {
            }else {
                setUpStreetList()
                tvStreetList.tvList.isHidden = false
            }
            tvStreetList.getAllAddressDetails(searchTextStr: text, yAsix: 130)
            self.view.bringSubviewToFront(tvStreetList)
        }else {
            if self.view.contains(tvStreetList) {
                self.removeStreetList()
            }
        }
    }
    
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        searchLocation?.text = street_Name.address
        self.getCordination(PlaceId: street_Name.placeID ?? "")
        searchLocation?.resignFirstResponder()
        self.removeStreetList()
    }
    
}


extension NewGeofenceController:GeofencingProtocol{
    
    
    func authorization(state: CLAuthorizationStatus) {
        
        if (state == CLAuthorizationStatus.authorizedAlways) {
            self.loadMultipleRegion()
            
        }else if (state == CLAuthorizationStatus.denied){
            showLocationDisabledpopUp()
        }
    }
    
    
    func currentCordinateShow(coordinate: CLLocationCoordinate2D){
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
        
        self.mapView.setRegion(mapRegion, animated: true)
        self.mapView.showsUserLocation = true;
    }
    
    
    func didEnterRegion() {
         
    }
    
    func didExitRegion() {
        
        
        
    }
     
    func getLatestCoordiante(location: CLLocation) {
         
        self.currentCordinateShow(coordinate: location.coordinate)
        GeofencingRegion.shared.stopUpdatingLocation()
         
    }
     
}


extension MKMapView {
    func topLeftCoordinate() -> CLLocationCoordinate2D {
        return convert(.zero, toCoordinateFrom: self)
    }
    
    func bottomRightCoordinate() -> CLLocationCoordinate2D {
        return convert(CGPoint(x: frame.width, y: frame.height), toCoordinateFrom: self)
    }
}
