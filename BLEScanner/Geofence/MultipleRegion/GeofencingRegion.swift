//
//  GeofencingRegion.swift
//  BLEScanner
//
//  Created by iMac on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications
import AVFoundation

public class GeofencingRegion: NSObject, CLLocationManagerDelegate {
    
    
    let player = AVQueuePlayer()
    
    // Declarations
    public static let shared = GeofencingRegion()
    
    public var allRegions: [CircularRegion] = []
    public lazy var locationManager: CLLocationManager? = { [weak self] in
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.delegate = self
        return locationManager
        }()
    public var currentLocation: CLLocation? {
        didSet {
            
            startGeofencing()
        }
    }
    public weak var delegate: GeofencingProtocol?
    private var enteredRegion: CLRegion?
    
    // MARK: Life Cycle
    public override init() {
        
        super.init()
        //ask for location permission
        
       // loadMonitoringRegions()
        
      
    }
    
    public func startUpdatingLocation() {
        GeofencePermission.enableLocationServices(locationManager: locationManager)
       
        locationManager?.startUpdatingLocation()
    }
    public func stopUpdatingLocation() {
        
        locationManager?.stopUpdatingLocation()
    }
    public func loadMonitoringRegions() {
        
        // Dummy data
        //        allRegions = CircularRegion.loadDummyData()
        
        allRegions = CircularRegion.loadata()
        
        /* Create a region centered on desired location,
         choose a radius for the region (in meters)
         choose a unique identifier for that region */
        for circularRegion in allRegions {
            let geofenceRegion = circularRegion.region
            geofenceRegion.notifyOnEntry = true
            geofenceRegion.notifyOnExit = true
            locationManager?.startMonitoring(for: geofenceRegion)
        }
    }
    public func evaluateClosestRegions(regions: [CircularRegion]) -> [CircularRegion] {
        
        var shortestRegions: [CircularRegion] = []
        //Calulate distance of each region's center to currentLocation
        shortestRegions = regions.map({ storableRegion in
            let distance = currentLocation?.distance(from: CLLocation(latitude: storableRegion.coordinate.latitude, longitude: storableRegion.coordinate.longitude))
            storableRegion.distance = distance ?? 0.0
            return storableRegion
        })
        
        
        //sort and get 20 closest
        let twentyNearbyRegions = regions
            .sorted{ $0.distance < $1.distance }
            .prefix(20)
        let twentyRegions: [CircularRegion] = Array(twentyNearbyRegions)
        return twentyRegions
    }
    
    func startGeofencing() {
        let twentyRegions = evaluateClosestRegions(regions: allRegions)
        stopMonitoringRegion()
        startMonitorRegion(twentyNearbyRegions: twentyRegions)
        
    }
    public func startMonitorRegion(twentyNearbyRegions: [CircularRegion]) {
        
        twentyNearbyRegions.forEach {
            locationManager?.startMonitoring(for: $0.region)
            locationManager?.requestState(for: $0.region)
        }
    }
    public func stopMonitoringRegion() {
        
        // Remove all regions you were tracking before
        guard let monitoredRegions = locationManager?.monitoredRegions else {
            return
        }
        
        for region in monitoredRegions {
            locationManager?.stopMonitoring(for: region)
        }
    }
}
// Location manager delegate methods
extension GeofencingRegion {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //   print("didUpdateLocations", locations.first?.coordinate.latitude ?? 0.0 ,locations.first?.coordinate.longitude ?? 0.0)
        currentLocation = locations.first
        delegate?.getLatestCoordiante(location: currentLocation!)
    }
    
    
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("didFailWithError",error)
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        enterGeofenceauth(state: status, manager: manager)
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        enterGeofence(geofence: region, manager: manager)
    }
    
    
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        exitGeofence(geofence: region, manager: manager)
    }
    
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        
        switch state {
        case .inside:
            enterGeofence(geofence: region, manager: manager)
            break
        case .outside:
            exitGeofence(geofence: region, manager: manager)
            break
        case .unknown:
            //print("Unknown state for geofence:",region)
            break
        default:
            break
        }
    }
    
    
    public func enterGeofenceauth(state: CLAuthorizationStatus, manager: CLLocationManager) {
        delegate?.authorization(state: state)
    }
    
    private func enterGeofence(geofence: CLRegion, manager: CLLocationManager) {
        
        print("didEnterRegion", geofence.identifier)
        if enteredRegion != geofence {
            enteredRegion = geofence
            delegate?.didEnterRegion()
            
            
             
            if geofence.identifier == Constant.typeHomeLocation{
                ApplicationState.sharedAppState.currentUser.isAtHome = true
                self.LocalNotification(message: Constant.ENTERED_HOME_REGION_MESSAGE, identifier: Constant.ENTERED_REGION_NOTIFICATION_ID)
            }
               
            else if geofence.identifier == Constant.typeWorkLocation{
                ApplicationState.sharedAppState.currentUser.isAtOffice = true
                self.LocalNotification(message: Constant.ENTERED_WORK_REGION_MESSAGE, identifier: Constant.ENTERED_REGION_NOTIFICATION_ID)
            }
            
            
            
        }
    }
    
    private func exitGeofence(geofence: CLRegion, manager: CLLocationManager) {
        
        print("didExitRegion", geofence.identifier)
        if enteredRegion == geofence {
            enteredRegion = nil
            currentLocation = manager.location
            delegate?.didExitRegion()
            
           
            if geofence.identifier == Constant.typeHomeLocation{
                ApplicationState.sharedAppState.currentUser.isAtHome = false
                self.LocalNotification(message: Constant.EXITED_Home_REGION_MESSAGE, identifier: Constant.EXITED_REGION_NOTIFICATION_ID)
            }
                
            else if geofence.identifier == Constant.typeWorkLocation{
                ApplicationState.sharedAppState.currentUser.isAtOffice = false
                self.LocalNotification(message: Constant.EXITED_WORK_REGION_MESSAGE, identifier: Constant.EXITED_REGION_NOTIFICATION_ID)
               
            }
            
        }
    }
    
    
    
    func LocalNotification(message: String, identifier: String) {
        //Create a local notification
        let content = UNMutableNotificationContent()
        content.body = message
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        // Deliver the notification
        
        
        // Schedule the notification
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
        }
    }
    
    
    func playSound(){
        if let url = Bundle.main.url(forResource: "please_put_mobile_in_your_poket", withExtension: "m4a") {
            player.removeAllItems()
            player.insert(AVPlayerItem(url: url), after: nil)
            player.play()
        }
    }
    
    
}
