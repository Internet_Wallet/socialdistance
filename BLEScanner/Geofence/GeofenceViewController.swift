//
//  GeofenceViewController.swift
//  BLEScanner
//
//  Created by iMac on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications
import AVFoundation

enum MapType: NSInteger {
    case StandardMap = 0
    case SatelliteMap = 1
    case HybridMap = 2
}

class GeofenceViewController: UIViewController,UIGestureRecognizerDelegate {
    
    
    
    
    
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var friendImage: UIImageView!
    
    
    var comingStatus : String?
    
    let player = AVQueuePlayer()
    let ENTERED_REGION_MESSAGE = "Welcome to geofence If the waves are good, you can try surfing!"
    let ENTERED_REGION_NOTIFICATION_ID = "EnteredRegionNotification"
    let EXITED_REGION_MESSAGE = "Bye! Hope you had a great day at the grofence!"
    let EXITED_REGION_NOTIFICATION_ID = "ExitedRegionNotification"
    var markerImageView: UIImageView?
    
    var alert: UIAlertController? = nil
    var yesButton: UIAlertAction? = nil
    var noButton: UIAlertAction? = nil
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var AddGeofenceBarButton: UIBarButtonItem?{
        didSet {
            AddGeofenceBarButton?.customView?.setRadiusWithShadow(10)
        }
    }
    
    @IBOutlet var currentlocationBtn: UIButton!{
        didSet {
            currentlocationBtn?.setRadiusWithShadow()
        }
    }
    
    @IBOutlet var AddlocationBtn: UIButton!{
        didSet {
            AddlocationBtn?.setRadiusWithShadow()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.AddGeofenceBarButton = UIBarButtonItem.init(image: UIImage.init(named: "ic_add_circle_outline_white"), style: .done, target: self, action: #selector(AddGeofenceAction(_:)))
        //        self.navigationItem.rightBarButtonItem?.tintColor = .white
        //        self.navigationItem.rightBarButtonItem  = self.AddGeofenceBarButton
        
        // self.listOfVoiceAvalible()
        self.loadMultipleRegion()
        
        
    }
    
    
    func listOfVoiceAvalible(){
        if let arr = NSLocale.availableLocaleIdentifiers as? [String]{
            print(arr)
        }
    }
    
    @IBAction func AddGeofenceAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "AddGeofenceView", sender: self)
    }
    
    //Navigate to geoFence
    @IBAction func AddGeofenceClick(_ sender: UIButton){
        if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlusGeofence") as? AddGeofenceViewController{
            ViewController.Delegate = self
            self.navigationController?.pushViewController(ViewController, animated: true)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddGeofenceView" {
            if let vc = segue.destination as? AddGeofenceViewController {
                vc.Delegate = self
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Your Location".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]
        self.setBackButton()
        self.mapView.delegate = self
        GeofencingRegion.shared.delegate = self
        GeofencingRegion.shared.startUpdatingLocation()
        //self.loadMapIcon()
        //        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(Gesture:)))
        //        gesture.delegate = self
        //        gesture.view?.center = mapView.center
        //        mapView.addGestureRecognizer(gesture)
        
        UNUserNotificationCenter.current().delegate = self
        
    }
    
    
    
    
    func loadMapIcon(){
        markerImageView = UIImageView(image: UIImage(named: "location"))
        markerImageView?.frame = CGRect(x: mapView.center.x, y: mapView.center.y, width: 60, height: 60)
        markerImageView?.center = mapView.center
        mapView.addSubview(markerImageView ?? UIImageView())
        markerImageView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.markerImageView?.transform = .identity
            },
                       completion: nil)
    }
    
    //    @objc func didDragMap(Gesture:UIGestureRecognizer){
    //        if (Gesture.state == .ended){
    //            print(Gesture)
    //        }
    //    }
    
    
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        return true
    //    }
    
    //    static var mapChangedFromUserInteraction: Bool = false
    //
    //
    //    func mapViewRegionDidChangeFromUserInteraction() -> Bool{
    //
    //
    //        let view = self.mapView.subviews.first
    //
    //        for recognizer in view!.gestureRecognizers!{
    //            if recognizer.state == .began  ||  recognizer.state == .ended{
    //                return true
    //            }
    //        }
    //        return false
    //
    //    }
    
    //    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
    //
    //        GeofenceViewController.mapChangedFromUserInteraction = self.mapViewRegionDidChangeFromUserInteraction()
    //
    //        if (GeofenceViewController.mapChangedFromUserInteraction) {
    //            // user changed map region
    //            print(mapView)
    //        }
    //    }
    
    
    //    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
    //        if (GeofenceViewController.mapChangedFromUserInteraction) {
    //            // user changed map region
    //        }
    //    }
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {})
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    //    private func setCurrentLocation() {
    //
    //        guard let exposedLocation = GeofencingRegion.shared.currentLocation else {
    //            print("*** Error in \(#function): exposedLocation is nil")
    //            return
    //        }
    //
    //        self.getPlace(for: exposedLocation) { placemark in
    //            guard let placemark = placemark else { return }
    //
    //            var output = "Our location is:"
    //            if let country = placemark.country {
    //                output = output + "\n\(country)"
    //            }
    //            if let state = placemark.administrativeArea {
    //                output = output + "\n\(state)"
    //            }
    //            if let town = placemark.locality {
    //                output = output + "\n\(town)"
    //            }
    //            self.customAlert(title: output, msg: "\(exposedLocation)")
    //            print(placemark)
    //        }
    //    }
    
    func customAlert(title:String,msg:String){
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setBackButton(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
    }
    
    @objc func backClick(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case MapType.StandardMap.rawValue:
            self.mapView.mapType = .standard
        case MapType.SatelliteMap.rawValue:
            self.mapView.mapType = .satellite
        case MapType.HybridMap.rawValue:
            self.mapView.mapType = .hybrid
        default:
            break
        }
    }
    
    
    @IBAction func CurrentLocationClick(_ sender: UIButton){
        GeofencingRegion.shared.delegate = self
        GeofencingRegion.shared.startUpdatingLocation()
    }
    
    
    
}

extension GeofenceViewController: MKMapViewDelegate{
    
    func loadMultipleRegion() {
        let allRegions = CircularRegion.loadata()
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func loadMultipleRegion(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) {
        let allRegions = CircularRegion.setData(latitute: latitute, longitute: longitute, radius: radius, msg: msg, txtType: txtType)
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    //    func loadMapView(regions: [CircularRegion]) {
    //        let currentLocation = GeofencingRegion.shared.currentLocation
    //        guard let coordinate = currentLocation?.coordinate else { return }
    //        let annotation = MKPointAnnotation()
    //        annotation.coordinate = coordinate
    //        annotation.title = "Current"
    //        annotation.subtitle = "My Location"
    //        mapView.addAnnotation(annotation)
    //        for (index, _region) in regions.enumerated() {
    //            let annotation = MKPointAnnotation()
    //            annotation.coordinate = CLLocationCoordinate2D(latitude: _region.coordinate.latitude, longitude: _region.coordinate.longitude)
    //            annotation.title = "Region\(index)"
    //            //print(annotation.title)
    //            mapView.addAnnotation(annotation)
    //            setRegion(coordinate: annotation.coordinate)
    //        }
    //
    //
    //    }
    
    func setRegion(coordinate: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    
    func currentCordinateShow(coordinate: CLLocationCoordinate2D){
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
        
        self.mapView.setRegion(mapRegion, animated: true)
        self.mapView.showsUserLocation = true;
    }
    
    
    
    
    func setUpGeofenceForPlayaGrandeBeach(regions: [CircularRegion]) {
        for (index, _region) in regions.enumerated() {
            let annotation = MKPointAnnotation()
            let Coordinates = CLLocationCoordinate2D(latitude: regions[index].coordinate.latitude, longitude: regions[index].coordinate.longitude)
            annotation.coordinate = Coordinates
            mapView.addAnnotation(annotation)
            let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: regions[index].coordinate.radius, identifier: "CurrentLocation");
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
            self.mapView.setRegion(mapRegion, animated: true)
            let regionCircle = MKCircle(center: geofenceRegionCenter, radius: regions[index].coordinate.radius)
            self.mapView.addOverlay(regionCircle)
            self.mapView.showsUserLocation = true;
        }
        self.playSound()
    }
    
    
    func playSound(){
        if let url = Bundle.main.url(forResource: "please_put_mobile_in_your_poket", withExtension: "m4a") {
            player.removeAllItems()
            player.insert(AVPlayerItem(url: url), after: nil)
            player.play()
        }
    }
    
    func createLocalNotification(message: String, identifier: String) {
        //Create a local notification
        let content = UNMutableNotificationContent()
        content.body = message
        content.sound = UNNotificationSound.default
        
        // Deliver the notification
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: nil)
        
        // Schedule the notification
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
        }
    }
    
    //MARK - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
        overlayRenderer.lineWidth = 4.0
        overlayRenderer.strokeColor = UIColor.red
        overlayRenderer.fillColor = UIColor(red: 76.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 0.7)
        return overlayRenderer
    }
    
    //    // MARK: - Get Placemark
    //    func getPlace(for location: CLLocation,
    //                  completion: @escaping (CLPlacemark?) -> Void) {
    //
    //        let geocoder = CLGeocoder()
    //        geocoder.reverseGeocodeLocation(location) { placemarks, error in
    //
    //            guard error == nil else {
    //                print("*** Error in \(#function): \(error!.localizedDescription)")
    //                completion(nil)
    //                return
    //            }
    //
    //            guard let placemark = placemarks?[0] else {
    //                print("*** Error in \(#function): placemark is nil")
    //                completion(nil)
    //                return
    //            }
    //
    //            completion(placemark)
    //        }
    //    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "MyPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            
            annotationView?.image = UIImage(named: "location")
            // annotationView?.frame = CGRect(x: (annotationView?.center.x)!, y: (annotationView?.center.y)! + 100, width: 60, height: 60)
            
            let detailButton = UIButton(type: .detailDisclosure)
            annotationView?.rightCalloutAccessoryView = detailButton
        } else {
            annotationView?.annotation = annotation
        }
        
        
        return annotationView
    }
    
    
    
}

extension GeofenceViewController: GeofencingProtocol{
    
    func authorization(state: CLAuthorizationStatus) {
        if (state == CLAuthorizationStatus.authorizedAlways) {
            self.loadMultipleRegion()
            
        }else if (state == CLAuthorizationStatus.denied){
            showLocationDisabledpopUp()
        }
    }
    
    func didEnterRegion() {
        self.message.text = ENTERED_REGION_MESSAGE
        self.createLocalNotification(message: ENTERED_REGION_MESSAGE, identifier: ENTERED_REGION_NOTIFICATION_ID)
        //        self.customAlert()
    }
    
    func didExitRegion() {
        self.message.text = EXITED_REGION_MESSAGE
        self.createLocalNotification(message: EXITED_REGION_MESSAGE, identifier: EXITED_REGION_NOTIFICATION_ID)
        //        self.customAlert()
    }
    
    func getLatestCoordiante(location: CLLocation) {
        
        self.currentCordinateShow(coordinate: location.coordinate)
        GeofencingRegion.shared.stopUpdatingLocation()
    }
    
    
    
    
    func customAlert(){
        
        
        alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
        
        let messagelabel = UILabel(frame: CGRect(x: 20, y: 70, width: 210, height: 80))
        messagelabel.text = "Are you Sure? You want to add this id as a family member.".localized
        messagelabel.textAlignment = .center
        messagelabel.numberOfLines = 4
        messagelabel.lineBreakMode = .byTruncatingTail
        messagelabel.textColor = UIColor.black
        
        let namelabel = UILabel(frame: CGRect(x: 40, y: 160, width: 190, height: 30))
        namelabel.text = "vamsi"
        namelabel.textAlignment = .center
        namelabel.numberOfLines = 4
        namelabel.lineBreakMode = .byTruncatingTail
        namelabel.textColor = UIColor.black
        alert?.view.addSubview(messagelabel)
        alert?.view.addSubview(namelabel)
        imageView.image = UIImage(named: "AlertIcon") // Your image here...
        alert?.view.addSubview(imageView)
        let height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        let width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        alert!.view.addConstraint(height)
        alert!.view.addConstraint(width)
        
        yesButton = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
        })
        
        noButton = UIAlertAction(title: "No", style: .default, handler: { action in
            
            
        })
        yesButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        noButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        
        alert?.addAction(yesButton!)
        alert?.addAction(noButton!)
        self.present(alert!, animated: true, completion: nil)
    }
}

extension GeofenceViewController: AddGeofenceViewDelegate{
    func SubmitClick(latitute: String, longitute: String, radius: Float, msg: String,txtType: String) {
        self.loadMultipleRegion(latitute: latitute, longitute: longitute, radius: radius, msg: msg, txtType: txtType)
    }
}

extension GeofenceViewController:UNUserNotificationCenterDelegate{
    
    
    @IBAction func triviaBtnTapped(_ sender: Any) {
        
        let friendsContent = UNMutableNotificationContent()
        friendsContent.title = "Friends Quiz!"
        friendsContent.subtitle = "Can you identify the Friend with his quote?"
        friendsContent.body = "'PIVOTTTT!!'"
        //        friendsContent.badge = 1
        friendsContent.categoryIdentifier = "friendsQuizCategory"
        friendsContent.sound = UNNotificationSound.default
        
        let quizTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        let quizRequestIdentifier = "friendsQuiz"
        let request = UNNotificationRequest(identifier: quizRequestIdentifier, content: friendsContent, trigger: quizTrigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print(error as Any)
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == "ross" {
            responseLabel.text = "That's the correct answer!"
            friendImage.image = #imageLiteral(resourceName: "ross")
        } else if response.actionIdentifier == "chandler" {
            responseLabel.text = "Could you BE more wrong!?"
            friendImage.image = #imageLiteral(resourceName: "chandler")
        } else {
            responseLabel.text = "Try again... or go eat a sandwich."
            friendImage.image = #imageLiteral(resourceName: "joey")
        }
        completionHandler()
    }
    
}



