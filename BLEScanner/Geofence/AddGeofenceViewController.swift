//
//  AddGeofenceViewController.swift
//  BLEScanner
//
//  Created by iMac on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import MapKit

//MARK: step 1 Add Protocol here.
protocol AddGeofenceViewDelegate: class {
    func SubmitClick(latitute: String, longitute: String, radius: Float, msg: String,txtType: String)
}



//MARK: step another Add Protocol here.
protocol QuirintineViewControllerDelegate: class {
    func SubmitClick(latitute: String, longitute: String, radius: Float, msg: String,txtType: String)
}



class AddGeofenceViewController: UIViewController {
    
    
    
    var Screen: String?
    
    weak var Delegate: AddGeofenceViewDelegate?
    weak var QuirintineDelegate: QuirintineViewControllerDelegate?
    
    
    var markerImageView: UIImageView?
    var AfterDragCoordinate: CLLocationCoordinate2D?
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var mapCardView: UIView!{
        didSet{
            mapCardView.isHidden = true
        }
    }
    
    @IBOutlet var latituteTxt: UITextField!{
        didSet {
            self.latituteTxt.setPropertiesForTextField()
            self.latituteTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet var longituteTxt: UITextField!{
        didSet {
            self.longituteTxt.setPropertiesForTextField()
            self.longituteTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet var titleTxt: UITextField!{
        didSet {
            self.titleTxt.setPropertiesForTextField()
        }
    }
    @IBOutlet var commentTxt: UITextView!{
        didSet {
            self.commentTxt.setPropertiesForTextField()
        }
    }
    
    @IBOutlet var msgTxt: UITextField!{
        didSet {
            self.msgTxt.setPropertiesForTextField()
            self.msgTxt.placeholder = "We arrived! :]".localized
        }
    }
    
    @IBOutlet var GeofenceProceedBtn: UIButton!{
        didSet {
            self.GeofenceProceedBtn.setRadiusWithShadow(12)
        }
    }
    
    @IBOutlet var btnContinue: UIButton!{
        didSet {
            btnContinue.setTitle("CONTINUE".localized, for: .normal)
        }
    }
    
    @IBOutlet var radiusSlider: UISlider!{
        didSet {
            self.radiusSlider.value = 200
        }
    }
    
    @IBOutlet weak var headerlbl: UILabel!{
        didSet{
            self.headerlbl.text = "Add Your Location?".localized
        }
    }
    
    @IBOutlet weak var meterlbl: UILabel!{
        didSet{
            self.meterlbl.text = "200" + " meters".localized
        }
    }
    
    @IBOutlet weak var draglbl: UILabel!{
        didSet{
            self.draglbl.isHidden = false
            self.draglbl.text = "Drag the map to set the location".localized
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Your Location".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]
        //        GeofencingRegion.shared.delegate = self
        //        GeofencingRegion.shared.startUpdatingLocation()
        
        
        self.mapView.delegate = self
        self.loadMapIcon()
        
        self.navigationController?.navigationBar.tintColor = UIColor.init(red: 67.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.init(red: 67.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        self.setBackButton()
        
        GeofencingRegion.shared.delegate = self
        GeofencingRegion.shared.startUpdatingLocation()
        
    }
    
    
    func loadMapIcon(){
        markerImageView = UIImageView(image: UIImage(named: "location"))
        markerImageView?.frame = CGRect(x: mapView.center.x, y: mapView.center.y, width: 60, height: 60)
        markerImageView?.center = mapView.center
        mapView.addSubview(markerImageView ?? UIImageView())
        markerImageView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.markerImageView?.transform = .identity
            },
                       completion: nil)
    }
    
    
    
    
    func setBackButton(){
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
    }
    
    @objc func backClick(){
        
        if self.Screen == "QuirintineViewController"{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        let step: Float = 200
        
        let roundedValue = round(sender.value/step) * step
        sender.value = roundedValue
        
        self.meterlbl.text = "\(Int(roundedValue))" + " meters".localized
        print("\(round(sender.value/step) * step)")
        
        
        self.AfterDragInitialRegion(radius: (Int(roundedValue)))
        
    }
    
    @IBAction func submitTapAction(_ sender: Any) {
        if let del = self.Delegate { 
            if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                del.SubmitClick(latitute: "\(lat)", longitute: "\(long)", radius: radiusSlider.value, msg: self.msgTxt.text ?? "", txtType: "")
            }
            self.backClick()
        }
    }
    
    
    @IBAction func continueTapAction(_ sender: UIButton) {
        
        if self.Screen == "QuirintineViewController"{
            
            if let del = self.QuirintineDelegate {
                
                
                self.AfterDragInitialRegion(radius: Int(self.radiusSlider.minimumValue))
                
                
                if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                    del.SubmitClick(latitute: "\(lat)", longitute: "\(long)", radius: radiusSlider.value, msg: self.msgTxt.text ?? "", txtType: "")
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                    self?.backClick()
                }
            }
            
            
            
        }else{
            
            if sender.tag == 0{
                sender.tag = 1
                self.headerlbl.text = "Choose the radius".localized
                self.draglbl.isHidden = true
                self.mapCardView.isHidden = false
                self.AfterDragInitialRegion(radius: Int(self.radiusSlider.minimumValue))
                
                
            }else if sender.tag == 1{
                sender.tag = 2
                self.headerlbl.text = "Enter the message".localized
                self.draglbl.isHidden = true
                self.mapCardView.isHidden = true
                self.msgTxt.isHidden = false
                self.msgTxt.resignFirstResponder()
            }else{
                
                if self.msgTxt.text != "" && self.msgTxt.text != nil{
                    if let del = self.Delegate {
                        if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
                            del.SubmitClick(latitute: "\(lat)", longitute: "\(long)", radius: radiusSlider.value, msg: self.msgTxt.text ?? "", txtType: "")
                        }
                        self.backClick()
                    }
                }else{
                    AppUtility.showToastlocal(message: "Required".localized, view: self.view)
                }
                
                
            }
        }
    }
    
    
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case MapType.StandardMap.rawValue:
            self.mapView.mapType = .standard
        case MapType.SatelliteMap.rawValue:
            self.mapView.mapType = .satellite
        case MapType.HybridMap.rawValue:
            self.mapView.mapType = .hybrid
        default:
            break
        }
    }
    
    
    func AfterDragInitialRegion(radius: Int){
        if let lat = self.AfterDragCoordinate?.latitude,let long = self.AfterDragCoordinate?.longitude{
            //            self.loadMultipleRegion(latitute: "\(lat)", longitute: "\(long)", radius: Float(radius), msg: "AfterDragInitialRegion")
            
            if let overlays = mapView?.overlays {
                for overlay in overlays {
                    
                    mapView?.removeOverlay(overlay)
                    
                }
            }
            
            let geofenceRegionCenter = CLLocationCoordinate2DMake(lat, long);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: CLLocationDistance(radius), identifier: "CurrentLocation");
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
            self.mapView.setRegion(mapRegion, animated: true)
            let regionCircle = MKCircle(center: geofenceRegionCenter, radius: CLLocationDistance(radius))
            self.mapView.addOverlay(regionCircle)
            self.mapView.showsUserLocation = true;
            
            
            
        }
    }
    
    
    func loadMultipleRegion(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) {
        let allRegions = CircularRegion.setData(latitute: latitute, longitute: longitute, radius: radius, msg: msg, txtType: txtType)
        let closestRegion = GeofencingRegion.shared.evaluateClosestRegions(regions: allRegions)
        self.setUpGeofenceForPlayaGrandeBeach(regions: closestRegion)
    }
    
    
    func setUpGeofenceForPlayaGrandeBeach(regions: [CircularRegion]) {
        for (_, _region) in regions.enumerated() {
            let geofenceRegionCenter = CLLocationCoordinate2DMake(_region.coordinate.latitude, _region.coordinate.longitude);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 200, identifier: "CurrentLocation");
            geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
            self.mapView.setRegion(mapRegion, animated: true)
            let regionCircle = MKCircle(center: geofenceRegionCenter, radius: 200)
            self.mapView.addOverlay(regionCircle)
            self.mapView.showsUserLocation = true;
        }
        
    }
    
}



extension AddGeofenceViewController: MKMapViewDelegate{
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        print("regionDidChangeAnimated")
        
        self.AfterDragCoordinate = mapView.centerCoordinate
        
    }
    
    
    //MARK - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
        overlayRenderer.lineWidth = 4.0
        overlayRenderer.strokeColor = UIColor.red
        overlayRenderer.fillColor = UIColor(red: 76.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 0.7)
        return overlayRenderer
    }
    
}




extension AddGeofenceViewController:GeofencingProtocol{
    
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled", withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {})
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    
    func authorization(state: CLAuthorizationStatus) {
        if (state == CLAuthorizationStatus.authorizedAlways) {
            //            self.loadMultipleRegion()
            
        }else if (state == CLAuthorizationStatus.denied){
            showLocationDisabledpopUp()
        }
    }
    
    func didEnterRegion() {
        
    }
    
    func didExitRegion() {
        
    }
    
    func getLatestCoordiante(location: CLLocation) {
        self.currentCordinateShow(coordinate: location.coordinate)
        GeofencingRegion.shared.stopUpdatingLocation()
    }
    
    func currentCordinateShow(coordinate: CLLocationCoordinate2D){
        
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
        
        self.mapView.setRegion(mapRegion, animated: true)
        self.mapView.showsUserLocation = true;
    }
}



extension AddGeofenceViewController: UITextViewDelegate{
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
}





