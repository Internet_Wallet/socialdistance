//
//  CustomAlert.swift
//  BLEScanner
//
//  Created by Vinod Kumar on 07/04/20.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

var alert: UIAlertController? = nil
var yesButton: UIAlertAction? = nil
var noButton: UIAlertAction? = nil
var messagelabel = UILabel()
var namelabel = UILabel()
var height = NSLayoutConstraint()
var width = NSLayoutConstraint()
var imageView  = UIImageView()

extension UIViewController  {
    
    func customAlerttwobuttons() {
        
        alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
        
        messagelabel = UILabel(frame: CGRect(x: 20, y: 70, width: 210, height: 80))
        messagelabel.text = "Are you Sure? You want to add this id as a family member."
        messagelabel.textAlignment = .center
        messagelabel.numberOfLines = 4
        messagelabel.lineBreakMode = .byTruncatingTail
        messagelabel.textColor = UIColor.black
        
        namelabel = UILabel(frame: CGRect(x: 40, y: 160, width: 190, height: 30))
        namelabel.text = ""
        namelabel.textAlignment = .center
        namelabel.numberOfLines = 4
        namelabel.lineBreakMode = .byTruncatingTail
        namelabel.textColor = UIColor.black
        alert?.view.addSubview(messagelabel)
        alert?.view.addSubview(namelabel)
        imageView.image = UIImage(named: "AlertIcon") // Your image here...
        alert?.view.addSubview(imageView)
        height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        alert!.view.addConstraint(height)
        alert!.view.addConstraint(width)
        
        yesButton = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            
        })
        
        noButton = UIAlertAction(title: "No", style: .default, handler: { action in
        })
        yesButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        noButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        
        alert?.addAction(yesButton!)
        alert?.addAction(noButton!)
        self.present(alert!, animated: true, completion: nil)
        
        
    }
    
    func customAlertSingleButton() {
        
        alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
        
        messagelabel = UILabel(frame: CGRect(x: 10, y: 70, width: 220, height: 60))
        messagelabel.text = ""
        messagelabel.textAlignment = .center
        messagelabel.numberOfLines = 4
        messagelabel.lineBreakMode = .byTruncatingTail
        messagelabel.textColor = UIColor.black
        
        namelabel = UILabel(frame: CGRect(x: 30, y: 160, width: 200, height: 60))
        namelabel.text = ""
        namelabel.textAlignment = .center
        namelabel.numberOfLines = 4
        namelabel.lineBreakMode = .byTruncatingTail
        namelabel.textColor = UIColor.black
        alert?.view.addSubview(messagelabel)
        alert?.view.addSubview(namelabel)
        imageView.image = UIImage(named: "AlertIcon") // Your image here...
        alert?.view.addSubview(imageView)
        height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        alert!.view.addConstraint(height)
        alert!.view.addConstraint(width)
        
        yesButton = UIAlertAction(title: "OK".localized, style: .default, handler: { action in
            
            
        })
        
        yesButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        
        alert?.addAction(yesButton!)
        self.present(alert!, animated: true, completion: nil)
    }

    
    func showSAert(message: String, alertIcon: String, subTitle : String , Title : String , actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, actionTitleFourth: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?,fourthAction: Selector?, controller: UIViewController) {
         var alertHeight = 0
         alert = UIAlertController.init(title: Title , message: "" , preferredStyle: .alert)
         imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
         alertHeight += 70
         messagelabel = UILabel(frame: CGRect(x: 25, y: alertHeight , width: 220, height: 20))
         messagelabel.text = message
         messagelabel.textAlignment = .center
         messagelabel.numberOfLines = 0
         messagelabel.lineBreakMode = .byTruncatingTail
         messagelabel.textColor = UIColor.black
         messagelabel.frame.size.height = messagelabel.optimalHeight + 10
         messagelabel.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
         alertHeight = alertHeight + Int(messagelabel.frame.size.height) + 10
         
         namelabel = UILabel(frame: CGRect(x: 30, y: alertHeight , width: 200, height: 20))
         namelabel.text = subTitle
         namelabel.textAlignment = .center
         namelabel.numberOfLines = 0
         namelabel.lineBreakMode = .byTruncatingTail
         namelabel.textColor = UIColor.black
         namelabel.frame.size.height = namelabel.optimalHeight
         alertHeight = alertHeight + Int(namelabel.frame.size.height) + 10
         
         alert?.view.addSubview(messagelabel)
         alert?.view.addSubview(namelabel)
        
        if alertIcon.count != 0 {
          imageView.image = UIImage(named: alertIcon)
          alert?.view.addSubview(imageView)
        }
        else {
            imageView.image = UIImage(named: "AlertIcon")
            alert?.view.addSubview(imageView)
        }
        if actionTitleThird == "" {
            
            alertHeight += 50
        }
        else if (actionTitleFourth != "") {
            alertHeight += 200
        }
        else {
             alertHeight += 150
        }
        height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(alertHeight))
         width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
         alert!.view.addConstraint(height)
         alert!.view.addConstraint(width)
        
        if(!actionTitleFirst.isEmpty) {
              let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                  if(firstActoin != nil){
                      controller.perform(firstActoin)
                  }
              }
             
             firstButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            
            
            
//            let attributedText = NSMutableAttributedString(string: actionTitleFirst)
//            let range = NSRange(location: 0, length: attributedText.length)
//            attributedText.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
//            attributedText.addAttribute(NSAttributedString.Key.kern, value: UIFont(name: "Zawgyi-One", size: 15.0)!, range: range)
//            
             
             
//            var messageMutableString = NSMutableAttributedString()
//            messageMutableString = NSMutableAttributedString(string: actionTitleFirst, attributes: [NSAttributedString.Key.font:UIFont(name: "Zawgyi-One", size: 15.0)!])
//            firstButtonAction.setValue(attributedText, forKey: "titleTextColor")

            
            
            
            
            alert!.addAction(firstButtonAction)
          }
          
          if(!actionTitleSecond.isEmpty) {
              let secondButtonAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                  //            NSLog("You pressed button two")
                  if(secondActoin != nil){
                      controller.perform(secondActoin)
                  }
              }
             secondButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(secondButtonAction)
          }
          
          if(!actionTitleThird.isEmpty) {
            let thirdButtonAction = UIAlertAction(title: actionTitleThird , style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button three")
                if(thirdActoin != nil){
                    controller.perform(thirdActoin)
                }
            }
             thirdButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(thirdButtonAction)
          }
        if(!actionTitleFourth.isEmpty) {
          let fourthButtonAction = UIAlertAction(title: actionTitleFourth , style: .default) { (alert: UIAlertAction!) -> Void in
              //            NSLog("You pressed button three")
              if(fourthAction != nil){
                  controller.perform(fourthAction)
              }
          }
           fourthButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
          alert!.addAction(fourthButtonAction)
        }
         self.present(alert!, animated: true, completion: nil)
     }
    

    
    
    func showSAertForFive(message: String, alertIcon: String, subTitle : String , Title : String , actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, actionTitleFourth: String,actionTitlefifth: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?,fourthAction: Selector?,fifthAction: Selector?, controller: UIViewController) {
         var alertHeight = 0
         alert = UIAlertController.init(title: Title , message: "" , preferredStyle: .alert)
         imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
         alertHeight += 70
         messagelabel = UILabel(frame: CGRect(x: 10, y: alertHeight , width: 220, height: 20))
         messagelabel.text = message
         messagelabel.textAlignment = .center
         messagelabel.numberOfLines = 0
         messagelabel.lineBreakMode = .byTruncatingTail
         messagelabel.textColor = UIColor.black
         messagelabel.frame.size.height = messagelabel.optimalHeight + 10
         alertHeight = alertHeight + Int(messagelabel.frame.size.height) + 10
         
         namelabel = UILabel(frame: CGRect(x: 30, y: alertHeight , width: 200, height: 20))
         namelabel.text = subTitle
         namelabel.textAlignment = .center
         namelabel.numberOfLines = 0
         namelabel.lineBreakMode = .byTruncatingTail
         namelabel.textColor = UIColor.black
         namelabel.frame.size.height = namelabel.optimalHeight
         alertHeight = alertHeight + Int(namelabel.frame.size.height) + 10
         
         alert?.view.addSubview(messagelabel)
         alert?.view.addSubview(namelabel)
        
        if alertIcon.count != 0 {
          imageView.image = UIImage(named: alertIcon)
          alert?.view.addSubview(imageView)
        }
        else {
            imageView.image = UIImage(named: "AlertIcon")
            alert?.view.addSubview(imageView)
        }
        if actionTitleThird == "" {
            
            alertHeight += 50
        }
        else if (actionTitleFourth != "") {
            alertHeight += 200
        }
        else {
             alertHeight += 150
        }
        height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(alertHeight))
         width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
         alert!.view.addConstraint(height)
         alert!.view.addConstraint(width)
        
        if(!actionTitleFirst.isEmpty) {
              let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                  if(firstActoin != nil){
                      controller.perform(firstActoin)
                  }
              }
             firstButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(firstButtonAction)
          }
          
          if(!actionTitleSecond.isEmpty) {
              let secondButtonAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                  //            NSLog("You pressed button two")
                  if(secondActoin != nil){
                      controller.perform(secondActoin)
                  }
              }
             secondButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(secondButtonAction)
          }
          
          if(!actionTitleThird.isEmpty) {
            let thirdButtonAction = UIAlertAction(title: actionTitleThird , style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button three")
                if(thirdActoin != nil){
                    controller.perform(thirdActoin)
                }
            }
             thirdButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(thirdButtonAction)
          }
        if(!actionTitleFourth.isEmpty) {
          let fourthButtonAction = UIAlertAction(title: actionTitleFourth , style: .default) { (alert: UIAlertAction!) -> Void in
              //            NSLog("You pressed button three")
              if(fourthAction != nil){
                  controller.perform(fourthAction)
              }
          }
           fourthButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
          alert!.addAction(fourthButtonAction)
        }
        
        
        if(!actionTitlefifth.isEmpty) {
          let fifthButtonAction = UIAlertAction(title: actionTitlefifth , style: .default) { (alert: UIAlertAction!) -> Void in
              //            NSLog("You pressed button three")
              if(fifthAction != nil){
                  controller.perform(fifthAction)
              }
          }
           fifthButtonAction.setValue(UIColor.systemRed, forKey: "titleTextColor")
          alert!.addAction(fifthButtonAction)
        }
        
         self.present(alert!, animated: true, completion: nil)
     }
    
    
}



extension UILabel
{
var optimalHeight : CGFloat
    {
        get
        {
            let label = UILabel(frame: CGRect.init(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
         }
    }
}


/*
 
 alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
 let imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))

 let messagelabel = UILabel(frame: CGRect(x: 20, y: 70, width: 210, height: 80))
 messagelabel.text = "Are you Sure? You want to add this id as a family member."
 messagelabel.textAlignment = .center
 messagelabel.numberOfLines = 4
 messagelabel.lineBreakMode = .byTruncatingTail
 messagelabel.textColor = UIColor.black

 let namelabel = UILabel(frame: CGRect(x: 40, y: 160, width: 190, height: 30))
 // namelabel.text = "vamsi"
 namelabel.textAlignment = .center
 namelabel.numberOfLines = 4
 namelabel.lineBreakMode = .byTruncatingTail
 namelabel.textColor = UIColor.black
 alert?.view.addSubview(messagelabel)
 alert?.view.addSubview(namelabel)
 imageView.image = UIImage(named: "AlertIcon") // Your image here...
 alert?.view.addSubview(imageView)
 let height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
 let width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
 alert!.view.addConstraint(height)
 alert!.view.addConstraint(width)

 yesButton = UIAlertAction(title: "Yes", style: .default, handler: { action in
 // self.favoriteAPICall()
 // self.callBlockBluetoothDevice()
 // if ApplicationState.sharedAppState.blueToothModelObj.indices.contains(sender.tag){
 // //removing from database to add new value
 // let copy = ApplicationState.sharedAppState.blueToothModelObj[sender.tag]
 // ApplicationState.sharedAppState.blueToothModelObj.remove(at: sender.tag)
 // ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: copy.bluetoothName, id: copy.bluetoothId, bluetoothDate: copy.bluetoothDate, bluetoothDateLessThanSixFeet: copy.bluetoothDateLessThanSixFeet, soundPlayedTimes: 1))
 // self.localModelObj.removeAll()
 // self.localModelObj = ApplicationState.sharedAppState.blueToothModelObj.filter({$0.bluetoothDateLessThanSixFeet >= 2})
 // self.familyMembersTableView.reloadData()
 // }




 })

 noButton = UIAlertAction(title: "No", style: .default, handler: { action in


 })
 yesButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")
 noButton!.setValue(UIColor.systemBlue, forKey: "titleTextColor")

 alert?.addAction(yesButton!)
 alert?.addAction(noButton!)
 self.present(alert!, animated: true, completion: nil)*/
