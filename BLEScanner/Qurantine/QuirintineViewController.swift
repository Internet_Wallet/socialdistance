//
//  QuirintineViewController.swift
//  BLEScanner
//
//  Created by Avaneesh Kumar on 06/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class QuirintineViewController: UIViewController, MyTextFieldDelegate, TownshipVCDelegate,NRCOptionDelegte {
    
    
    @IBOutlet weak var btnNRCClose: UIButton!
    
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet weak var btnNRC: UIButton!
    
    @IBOutlet var name : UILabel!{
        didSet {
            name.text = "Name".localized
        }
    }
    @IBOutlet var fatherName : UILabel!{
        didSet {
            fatherName.text = "Father Name".localized
        }
    }
    @IBOutlet var NRC : UILabel!{
        didSet {
            NRC.text = "NRC".localized
        }
    }
    
    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtFatherName : UITextField!
    @IBOutlet var txtNRC : MyTextField!
    
    @IBOutlet var ProfileView : UIView!
    @IBOutlet var imgProfile : UIImageView!
    
    @IBOutlet var openProfilePicButtonView : CardView!
    
    @IBOutlet var backGroundView : CardView!
    @IBOutlet var saveButton : UIButton!{
        didSet {
            self.saveButton.setRadiusWithShadow(12)
            self.saveButton.setTitle("Save".localized, for: .normal)
        }
    }
    var btnnrc = UIButton()
    var nrcString = ""
    var apiName: String? = ""
    let apiObj = ApiRequestClass()
    let player = AVQueuePlayer()
    
    var township : String?
    var devision : String?
    var googleAddress : String?
    var addressQuarintine : String = ""
    
    var locationLat : String?
    var locationLong : String?
    
    var myLanguage = ""
    
    @IBOutlet var GeofenceBtn: UIButton!{
        didSet {
            self.GeofenceBtn.setRadiusWithShadow(12)
            self.GeofenceBtn.setTitle("Geofence".localized, for: .normal)
        }
    }
    
    
    
    @IBOutlet var leftRadioBtn: RadioButton!{
        didSet {
            self.leftRadioBtn.setRadiusWithShadow(12)
            self.leftRadioBtn.setTitle("Current Location".localized, for: .normal)
        }
    }
    
    @IBOutlet var rightRadioBtn: RadioButton!{
        didSet {
            self.rightRadioBtn.setRadiusWithShadow(12)
            self.rightRadioBtn.setTitle("Choose Location".localized, for: .normal)
        }
    }
    
    
    override func awakeFromNib() {
        self.view.layoutIfNeeded()

        leftRadioBtn.isSelected = true
        rightRadioBtn.isSelected = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMarqueLabelInNavigation()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20) ?? "Zawgyi-One"]

        txtName.layer.cornerRadius = 5.0
        txtFatherName.layer.cornerRadius = 5.0
        txtNRC.layer.cornerRadius = 5.0
        txtNRC.delegate = self
        txtName.delegate = self
        txtFatherName.delegate = self
        
        txtName.placeholder = "Eg: John, Ka won etc.".localized
        txtFatherName.placeholder = "Eg: John, Ka won etc.".localized
        txtNRC.placeholder = "Eg: 12/LaMaNa(N)222222".localized
        
        txtName.addPadding(padding: .left(5))
        txtFatherName.addPadding(padding: .left(5))
        txtNRC.addPadding(padding: .left(5))
        txtNRC.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        txtNRC.myDelegate = self
        ProfileView.layer.borderWidth = 1.0
        ProfileView.layer.masksToBounds = false
        ProfileView.layer.borderColor = UIColor.lightGray.cgColor
        ProfileView.layer.cornerRadius = ProfileView.frame.size.width / 2
        ProfileView.clipsToBounds = true
        
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.borderColor = UIColor.white.cgColor
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.clipsToBounds = true
        
        
        openProfilePicButtonView.layer.borderWidth = 1.0
        openProfilePicButtonView.layer.masksToBounds = false
        openProfilePicButtonView.layer.borderColor = UIColor.lightGray.cgColor
        openProfilePicButtonView.layer.cornerRadius = openProfilePicButtonView.frame.size.width / 2
        openProfilePicButtonView.clipsToBounds = true
        
        
        btnNRCClose.isHidden = true
        imgProfile.image = UIImage(named: "quarantine_user")
        self.navigationItem.title = "Quarantine"
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
                   myLanguage = "my"
               }else{
                    myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
               }
        
        leftRadioBtn?.alternateButton = [rightRadioBtn!]
        rightRadioBtn?.alternateButton = [leftRadioBtn!]
        
        
        /* for element in arrString {
                                               if element == "route" {
                                                   finalDict["street"] = item.object(forKey: "long_name") as? String
                                               }
                                               
                                               if element == "sublocality_level_1" {
                                                   finalDict["township"] = item.object(forKey: "long_name") as? String
                                               }
                                               
                                               if element == "administrative_area_level_1" {
                                                   finalDict["region"] = item.object(forKey: "long_name") as? String
                                               }
                                               
                                               if element == "locality" {
                                                   finalDict["city"] = item.object(forKey: "long_name") as? String
                                               }
                                               
                                               if element == "country" {
                                                   finalDict["Country"] = item.object(forKey: "short_name") as? String
                                               }*/
            
        let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
        if isPermission {
            VMGeoLocationManager.shared.startUpdateLocation()
                         VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
                             { (status, data) in
                                 if let addressDic = data as? Dictionary<String, String> {
                                     DispatchQueue.main.async {
                                       print(addressDic)
                                             if let street = addressDic["street"] as?  String  {
                                                self.addressQuarintine = "\(street)"
                                              self.township = street
                                             }
                                           if let town = addressDic["township"] as?  String  {
                                             self.addressQuarintine = " \(self.addressQuarintine) , \(town)"
                                            }
                                           if let region = addressDic["region"] as?  String  {
                                               self.devision = region
                                               self.addressQuarintine = " \(self.addressQuarintine) , \(region)"
                                           }
                                          if let city = addressDic["city"] as?  String  {
                                            self.addressQuarintine = " \(self.addressQuarintine) , \(city)"
                                          }
                                         self.googleAddress = "\(addressDic)"
                                       self.locationLat = VMGeoLocationManager.shared.currentLatitude
                                       self.locationLong = VMGeoLocationManager.shared.currentLongitude
                                     }
                                 }
                             }
                      }
        else {
            showLocationDisabledpopUp()
        }
      }
    
    private func setMarqueLabelInNavigation() {
          let lblMarque = MarqueeLabel()
          lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
          lblMarque.textColor = UIColor.white
          lblMarque.textAlignment = .center
          lblMarque.font =  UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 18)
          lblMarque.text = "Quarantine".localized
          self.navigationItem.titleView = nil
          self.navigationItem.titleView = lblMarque
      }
    
    
    @IBAction func radioSelection(sender: RadioButton){
        if sender == leftRadioBtn{
            
            GeofencingRegion.shared.delegate = self
            GeofencingRegion.shared.startUpdatingLocation()
            
        }else{
            self.AddGeofenceClick(sender)
        }
    }
    
    
    func nrcbutton(nrc : String) {
        let stringSize: CGSize = nrc.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
        btnnrc.setTitleColor(.black, for: .normal)
        btnnrc.titleLabel?.font =  UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 16)
        btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
        btnnrc.setTitle(nrc, for: UIControl.State.normal)
        let btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
        btnView.addSubview(btnnrc)
        txtNRC.leftView = nil
        txtNRC.leftView = btnView
        txtNRC.leftViewMode = UITextField.ViewMode.always
    }
    
  
    
    @IBAction func backAction (sender : UIButton) {
        self.navigationController?.dismiss(animated: true , completion: nil)
    }
    
    @IBAction func onClickCloseNRCAction(_ sender: UIButton) {
        txtNRC.text = ""
        txtNRC.leftView = nil
        txtNRC.resignFirstResponder()
        btnNRC.isHidden = false
        btnNRCClose.isHidden = true
        txtNRC.addPadding(padding: .left(5))
    }
    
    
    @IBAction func updateProfileImage (sender : UIButton) {
//        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
//            self.OpenCamera()
//        } else {
//            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
//                if granted {
//                    self.OpenCamera()
//                } else {
//                    self.OpenSettings(message: "Please Allow Camera Access".localized)
//                }
//            })
//        }
    }
    
    @IBAction func saveDetailInfo (sender : UIButton) {
        if let textName = self.txtName.text , textName.count > 0 {
            if let txtFather = self.txtFatherName.text , txtFather.count > 0 {
                if let txtNRC = self.txtNRC.text , txtNRC.count == 6 {
                    let isPermission =  VMGeoLocationManager.shared.setUpLocationManager()
                    if isPermission {
                        CircularRegion.setData(latitute: locationLat ?? "0.0", longitute: locationLong ?? "0.0" , radius: 200, msg: "", txtType: Constant.typeQuarintineLocation)
                        self.addQuarintineMember()
                    }
                    else {
                        self.showLocationDisabledpopUp()
                    }
                } else { self.showSAert(message: "Please Enter Valid NRC Details".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(alertOk), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self) }
            }
            else { self.showSAert(message: "Please Enter Father Name".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(alertOk), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self) }
        } else { self.showSAert(message: "Please Enter Name".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(alertOk), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)}
    }
    
    @objc func alertOk () {
        
        
    }
}

extension QuirintineViewController: QuirintineViewControllerDelegate{
    func SubmitClick(latitute: String, longitute: String, radius: Float, msg: String, txtType: String) {
        //        self.loadMultipleRegion(latitute: latitute, longitute: longitute, radius: radius, msg: msg)
        
        print("Lat = \(latitute), Long = \(longitute), Radius = \(radius)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            //            self?.showAlert(msg: "Lat = \(latitute), Long = \(longitute), Radius = \(radius)")
            VMGeoLocationManager.shared.getAddressFrom(lattitude:latitute, longitude: longitute, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                        print(addressDic)
                        
                        if let street = addressDic["street"] as?  String  {
                            self?.township = street
                        }
                        
                        if let region = addressDic["region"] as?  String  {
                            self?.devision = region
                        }
                        self?.googleAddress = "\(addressDic)"
                        
                        self?.locationLat = latitute
                        self?.locationLong = longitute
                        
                    }
                }
            }
            
        }
        
        
        
        
    }
    
    
    //Navigate to geoFence
    @IBAction func AddGeofenceClick(_ sender: RadioButton){
        //          if let viewController = UIStoryboard(name: "Geofence", bundle: nil).instantiateViewController(withIdentifier: "PlusGeofence") as? AddGeofenceViewController{
        //            viewController.QuirintineDelegate = self
        //              let  navController = UINavigationController.init(rootViewController: viewController)
        //              navController.navigationBar.tintColor = UIColor.init(red: 67.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        //              navController.navigationBar.backgroundColor = UIColor.init(red: 67.0/255.0, green: 123.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        //              viewController.Screen = "QuirintineViewController"
        //              navController.modalPresentationStyle = .fullScreen
        //              self.present(navController, animated: true, completion: nil)
        //          }
        
        if let viewController = UIStoryboard(name: "Geofence", bundle: nil).instantiateViewController(withIdentifier: "GeofenceNav") as? UINavigationController{
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
}


extension QuirintineViewController :  UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func OpenCamera(){
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.title = "Camera".localized
            imagePicker.sourceType = .camera
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionaryFacePhotoDeposide(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            
            self.imgProfile.image = image
            appDel.userImage = image
            
        }
        dismiss(animated: false, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.imgProfile.image = pickedImage
            appDel.userImage = pickedImage
            
        }
        dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: false, completion: nil)
    }
    
    func OpenSettings(message: String) {
        
        let alert = UIAlertController(title: "", message: "Allow to access Camera".localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Settings".localized, style: UIAlertAction.Style.default, handler: { action in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary1111([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension QuirintineViewController: GeofencingProtocol{
     func authorization(state: CLAuthorizationStatus) {
           if (state == CLAuthorizationStatus.authorizedAlways) {
                
               
           }else if (state == CLAuthorizationStatus.denied){
               showLocationDisabledpopUp()
           }
       }
    
    func showLocationDisabledpopUp() {
        
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Location Services are disabled".localized, withDescription: "Please enable Location Services in your Settings", onController: self)
            
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel", AlertType: .defualt, withComplition: {
                self.navigationController?.dismiss(animated: true , completion: nil)
            })
            let openAction = SAlertAction()
            openAction.action(name: "Open Setting", AlertType: .defualt, withComplition: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            })
            
            alertVC.addAction(action: [cancelAction,openAction])
        }
    }
    
    func didEnterRegion() {
         
    }
    
    func didExitRegion() {
         
    }
    
    func getLatestCoordiante(location: CLLocation) {
        GeofencingRegion.shared.stopUpdatingLocation()
        print(location)
        
     //  self.showAlert(msg: "\(location)")
        VMGeoLocationManager.shared.setUpLocationManager()
        VMGeoLocationManager.shared.startUpdateLocation()
        VMGeoLocationManager.shared.getAddressFrom(lattitude:VMGeoLocationManager.shared.currentLatitude, longitude: VMGeoLocationManager.shared.currentLongitude, language: "en")
            { (status, data) in
                if let addressDic = data as? Dictionary<String, String> {
                    DispatchQueue.main.async {
                      print(addressDic)
                        
               if let street = addressDic["street"] as?  String  {
                    self.township = street
                      }
               if let region = addressDic["region"] as?  String  {
                      self.devision = region
                     }
                        self.googleAddress = "\(addressDic)"
                        
                        self.locationLat = VMGeoLocationManager.shared.currentLatitude
                        self.locationLong = VMGeoLocationManager.shared.currentLongitude
                    }
                }
            }

    }
    
    func showAlert(msg:String){
        let alert = UIAlertController(title: "Location", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
 
             
        }))
        
//        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        self.present(alert, animated: true, completion: nil)
    }
}


extension QuirintineViewController : UITextFieldDelegate {
    
    func textFieldDidDelete() {
        if self.txtNRC.text?.count == 0 {
            let btn = UIButton()
            self.BtnNrcTypeAction(button: btn)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtNRC {
            textField.keyboardType = .numberPad
        }
    }
    
    @objc func textFieldDidEditChanged(_ textField : UITextField) {
        if textField == txtNRC {
            if textField.text?.count ?? 0 > 0 {
                btnNRCClose.isHidden = false
            }else {
                btnNRCClose.isHidden = true
            }
            if textField.text?.count ?? 0 ==  6 {
                textField.resignFirstResponder()
            }else if textField.text?.count ?? 0 > 6 {
                textField.resignFirstResponder()
                textField.text?.removeLast()
            }
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         var CHARSET : String = " "
              if myLanguage == "my" {
                   CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
               }
              else if myLanguage == "uni"{
                   CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
               }else {
                    CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy "
                }
               
        
        if textField == txtNRC {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            if textField.text?.count ?? 0 > 6 {
                if string == "" {
                    return true
                }
                textField.resignFirstResponder()
                return false
            }
        }else if textField == txtName{
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if textField.text?.count ?? 0 > 40 {
                if string == "" {
                    return true
                }
                return false
            }
        }else if textField == txtFatherName {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if textField.text?.count ?? 0 > 40 {
                if string == "" {
                    return true
                }
                return false
            }
        }
        //
        return restrictMultipleSpaces(str: string, textField: textField)
        
    }
    
    //MARK: - Global Methods
       public func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
          
           if str == " " && textField.text?.last == " "{
               return false
           } else {
               if characterBeforeCursor(tf: textField) == " " && str == " " {
                   return false
               } else {
                   if characterAfterCursor(tf: textField) == " " && str == " " {
                       return false
                   } else {
                       return true
                   }
               }
           }
       }

       func characterBeforeCursor(tf : UITextField) -> String? {
           if let cursorRange = tf.selectedTextRange {
               // get the position one character before the cursor start position
               if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                   let range = tf.textRange(from: newPosition, to: cursorRange.start)
                   return tf.text(in: range!)
               }
           }
           return nil
       }

       func characterAfterCursor(tf : UITextField) -> String? {
           if let cursorRange = tf.selectedTextRange {
               // get the position one character before the cursor start position
               if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                   let range = tf.textRange(from: newPosition, to: cursorRange.start)
                   return tf.text(in: range!)
               }
           }
           return nil
       }
    
    
    @IBAction func onClickNRCButton(_ sender: UIButton) {
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Qurantine", bundle: nil)
        let division = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
        division.delegate = self
        self.navigationController?.pushViewController(division, animated: true)
    }
    
    
    func divisionSetNRC(nrc: String) {
        self.btnNRC.isHidden = true
        self.txtNRC.text = ""
        self.nrcbutton(nrc: nrc)
        self.nrcString = nrc
        // txtNRC.becomeFirstResponder()
        self.navigationController?.popToViewController(self, animated: true)
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        let nrcOption = self.storyboard?.instantiateViewController(withIdentifier: "NRCOption") as! NRCOption
        let str = nrcString.components(separatedBy: "(")
        
        if str[0].count == 0 {
            return
        }
        var myLanguage = ""
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            myLanguage = "my"
        }else{
             myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
        }
        
        if myLanguage == "my" {
            let strN = str [0] + "(ႏိုင္)"
            let strP = str [0] + "(ျပဳ)"
            let strE = str [0] + "(ဧည့္)"
            let strT = str [0] + "(သာ)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }else if myLanguage == "en" {
            let strN = str [0] + "(N)"
            let strP = str [0] + "(P)"
            let strE = str [0] + "(E)"
            let strT = str [0] + "(T)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }else {
            let strN = str [0] + "(နိုင်)"
            let strP = str [0] + "(ပြု)"
            let strE = str [0] + "(ဧည့်)"
            let strT = str [0] + "(သာ)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }
        
        nrcOption.delegate = self
        self.navigationController?.pushViewController(nrcOption, animated: true)
    }
    
    func setNRCNubmer(nrc: String) {
        self.btnNRC.isHidden = true
        self.btnNRCClose.isHidden = true
        self.txtNRC.text = ""
        self.nrcbutton(nrc: nrc)
        self.nrcString = nrc
        // txtNRC.becomeFirstResponder()
        self.navigationController?.popToViewController(self, animated: true)
        
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionaryFacePhotoDeposide(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}



fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary1111(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}



protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
    
}

extension QuirintineViewController : ApiRequestProtocol {
    
    func addQuarintineMember() {
        if Utility.isNetworkAvailable(controller: self) {
            let NRCDetails = "\(btnnrc.titleLabel!.text!)\(self.txtNRC.text!)"
            let dic = AddQuarintineMember.getTheParmsForQuarintine(name: self.txtName.text ?? "", fatherName: self.txtFatherName.text ?? "", nrcDetails:  NRCDetails , townShip: township ?? "" , devision: devision ?? "", googleAddres: googleAddress ?? "" )
            print(dic)
            showLoader()
            apiObj.customDelegate = self
            self.apiName = "AddQuarintine"
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.updateQuarintine())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
        }else {
            Utility.alert(message: "", title: "No Network Available".localized, controller: self)
        }
    }
    
    func httpResponse(responseObj:Any,hasError: Bool) {
            hideLoader()
         if !hasError {
            if self.apiName == "AddQuarintine"  {
                let dataResponse = responseObj as! NSDictionary
                      if dataResponse.count > 0 {
                         DispatchQueue.main.async {
                            if let messageCode = dataResponse["Code"] as? Int , messageCode == 200 {
                                print("\(String(describing: dataResponse["Message"]!))")
                                DispatchQueue.main.async {
                                    ApplicationState.sharedAppState.currentUser.isQuarintine = true
                                    UserDefaults.standard.set(self.addressQuarintine, forKey: "lastQuirineAddress")
                                    UserDefaults.standard.synchronize()
                                    self.showSAert(message: "Successfully added to quarantine".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "OK".localized, actionTitleSecond: "", actionTitleThird: "", actionTitleFourth: "", firstActoin: #selector(self.GoToPreviousScreen), secondActoin: nil, thirdActoin: nil, fourthAction: nil, controller: self)
                                }
                            }
                         }
                     }
            }
         }else {
            Utility.alert(message: "", title: "API Failed".localized, controller: self)
         }
     }
    
    @objc func GoToPreviousScreen () {
        self.navigationController!.dismiss(animated: true , completion: {
                    if let url = Bundle.main.url(forResource: "please_do_not_break_quarantine", withExtension: "m4a") {
                        self.player.volume = 10
                        self.player.removeAllItems()
                        self.player.insert(AVPlayerItem(url: url), after: nil)
                        self.player.play()
                    }
                })
    }
}

class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?

    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 2.0
        self.layer.masksToBounds = true
    }

    func unselectAlternateButtons() {
        if alternateButton != nil {
            self.isSelected = true

            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        } else {
            toggleButton()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }

    func toggleButton() {
        self.isSelected = !isSelected
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.layer.borderColor = UIColor.gray.cgColor
                
                
            } else {
                self.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
}

