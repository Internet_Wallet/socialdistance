//
//  NRCOption.swift
//  BLEScanner
//
//  Created by Sam on 07/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

protocol NRCOptionDelegte {
    func setNRCNubmer(nrc: String)
}


class NRCOption: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var delegate: NRCOptionDelegte?
    var listNRCOption : [String]?
    @IBOutlet weak var tvNRCOption: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMarqueLabelInNavigation()
        tvNRCOption.tableFooterView = UIView()
        tvNRCOption.reloadData()
    }
    private func setMarqueLabelInNavigation() {
        //btnBack.titleLabel?.font = UIFont(name: appName, size: 18)
        btnBack.setTitle("Back".localized, for: .normal)
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.textColor = UIColor.white
        lblMarque.textAlignment = .center
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.text = "NRC Type".localized
        self.navigationItem.titleView = nil
        self.navigationItem.titleView = lblMarque
    }

    
    @IBAction func onClickBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return 4
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NRCOptionCell", for: indexPath) as! NRCOptionCell
        if let nrc = listNRCOption?[indexPath.row] {
         cell.lblDivison.text = nrc
        }
        cell.selectionStyle = .none
        return cell
     }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let del = delegate {
            if let nrc = listNRCOption?[indexPath.row] {
                 del.setNRCNubmer(nrc: nrc)
            }
        }
    }
    
    
}



 class NRCOptionCell: UITableViewCell {

     @IBOutlet weak var lblDivison: UILabel!
   override func awakeFromNib() {
     super.awakeFromNib()
      // lblDivison.font = UIFont.systemFont(ofSize: 17)
    //lblDivison.font = UIFont(name: appFont, size: 17)

 }

}
