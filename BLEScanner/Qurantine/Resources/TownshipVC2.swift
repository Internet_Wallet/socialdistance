//
//  TownshipVC2.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 10/3/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
protocol TownshipDelegate {
    func nrcValueString(nrcSting : String)
}


class TownshipVC2: UIViewController, NRCOptionDelegte {
  
    
    var delegate : TownshipDelegate?
    
    @IBOutlet weak var tvTownship: UITableView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var isSearch = false
    var tfSearch = UITextField()
    var selectedDivState: LocationDetail?
    var townshipArr : [TownShipDetail]?
    var array = [TownShipDetail]()
    var CHARSET = ""
    var myLanguage = ""
    @IBOutlet weak var lblNoRecord: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //btnBack.titleLabel?.font = UIFont(name: appName, size: 18)
        btnBack.setTitle("Back".localized, for: .normal)
        btnSearch.tag = 1
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            myLanguage = "my"
        }else{
             myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setMarqueLabelInNavigation()
        self.loadInitialize()
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        isSearch = false
        self.tvTownship.reloadData()
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: "Zawgyi-One", size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = UIImage(named: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.titleLabel?.font = UIFont(name: "Zawgyi-One", size: 13)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2 {
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            tfSearch.text = ""
            lblNoRecord.isHidden = true
            isSearch = false
            self.navigationItem.titleView = nil
            self.setMarqueLabelInNavigation()
            tvTownship.reloadData()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            lblNoRecord.isHidden = true
            tfSearch.text = ""
            isSearch = false
            tvTownship.reloadData()
        }
    }
    private func setMarqueLabelInNavigation() {
        lblNoRecord.isHidden = true
        lblNoRecord.text = "No Record Found".localized
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.textColor = UIColor.white
        lblMarque.textAlignment = .center
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.text = "Select NRC Code ".localized
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(UIImage(named: "searchBtn"), for: .normal)
        self.navigationItem.titleView = nil
        self.navigationItem.titleView = lblMarque
    }
    
    
    func loadInitialize() {
        
        if myLanguage == "my" {
            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameMY < $1.townShipNameMY} )
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if myLanguage == "en"{
            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameEN < $1.townShipNameEN} )
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
        }else if myLanguage == "uni"{
            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameUni < $1.townShipNameUni} )
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else {
              CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }
        lblNoRecord.isHidden = true
        tvTownship.delegate = self
        tvTownship.dataSource = self
        tvTownship.tableFooterView = UIView(frame: CGRect.zero)
        tvTownship.reloadData()
    }
 
    @IBAction func btnBackAction(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: false)
    }
 
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
         if str == " " && textField.text?.last == " "{
             return false
         } else {
             if characterBeforeCursor(tf: textField) == " " && str == " " {
                 return false
             } else {
                 if characterAfterCursor(tf: textField) == " " && str == " " {
                     return false
                 } else {
                     return true
                 }
             }
         }
     }
     
     func characterBeforeCursor(tf : UITextField) -> String? {
         if let cursorRange = tf.selectedTextRange {
             if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                 let range = tf.textRange(from: newPosition, to: cursorRange.start)
                 return tf.text(in: range!)
             }
         }
         return nil
     }
     
     func characterAfterCursor(tf : UITextField) -> String? {
         if let cursorRange = tf.selectedTextRange {
             if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                 let range = tf.textRange(from: newPosition, to: cursorRange.start)
                 return tf.text(in: range!)
             }
         }
         return nil
     }
    
}

extension TownshipVC2 : UITextFieldDelegate {
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
         tfSearch.resignFirstResponder()
         return true
     }
     
     public func textFieldShouldClear(_ textField: UITextField) -> Bool {
         lblNoRecord.isHidden = true
         tfSearch.text = ""
         btnSearch.tag = 2
         isSearch = false
         tvTownship.reloadData()
         return true
     }

     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
         if range.location == 0 && string == " " {
             return false
         }
         if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
         
         if let text = textField.text, let textRange = Range(range, in: text) {
             let updatedText = text.replacingCharacters(in: textRange, with: string)
             if updatedText == "" {
                 isSearch = false
                 lblNoRecord.isHidden = true
                 tvTownship.reloadData()
             }
             else{
               //  lblNoRecord.isHidden = true
                 getSearchArrayContains(updatedText)
             }
         }
            return restrictMultipleSpaces(str: string, textField: textField)
     }
     
     func getSearchArrayContains(_ text : String) {
         if myLanguage == "my" {
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeMY.lowercased().contains(text.lowercased())}))!
         }else if myLanguage == "en"{
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeEN.lowercased().contains(text.lowercased())}))!
         }else {
            array = (self.townshipArr?.filter({$0.nrcDisplayCodeUni.lowercased().contains(text.lowercased())}))!
         }
     
         if array.count == 0 {
             lblNoRecord.isHidden = false
             self.btnSearch.tag = 2
             self.btnSearch.tintColor = UIColor.lightGray
         }else {
            lblNoRecord.isHidden = true
         }
         isSearch = true
         tvTownship.reloadData()
     }
}


extension TownshipVC2 : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
               return array.count
            }else{
                return (townshipArr?.count)!
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TownshipCell", for: indexPath) as! TownshipCell
    
        if isSearch {
            let model = array[indexPath.row]
            if myLanguage == "my" {
                cell.lblTownship.text = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)"
                cell.town.text = model.townShipNameMY
            }else if myLanguage == "en" {
                cell.lblTownship.text = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)"
                cell.town.text = model.townShipNameEN
            }else {
                cell.lblTownship.text = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)"
                cell.town.text = model.townShipNameUni
            }
        }else {
            let model = townshipArr![indexPath.row]
            if myLanguage == "my" {
                cell.lblTownship.text = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)"
                cell.town.text = model.townShipNameMY
            }else if myLanguage == "en" {
                cell.lblTownship.text = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)"
                cell.town.text = model.townShipNameEN
            }else {
                cell.lblTownship.text = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)"
                cell.town.text = model.townShipNameUni
            }
        }
        cell.selectionStyle = .none
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var nrcStr = ""
        if isSearch {
            let model = array[indexPath.row]
            if myLanguage == "my"{
                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)"
            }else if myLanguage == "en" {
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)"
            }else {
                nrcStr = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)"
            }
        }else {
            let model = townshipArr![indexPath.row]
            nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)"
            if myLanguage == "my"{
                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)"
            }else if myLanguage == "en" {
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)"
            }else {
                nrcStr = "\(model.nrcCodeNumberUni)" + "/" + "\(model.nrcBUCode)"
            }
        }
        
        let nrcOption = self.storyboard?.instantiateViewController(withIdentifier: "NRCOption") as! NRCOption
        
        if myLanguage == "my" {
            let strN = nrcStr + "(ႏိုင္)"
            let strP = nrcStr + "(ျပဳ)"
            let strE = nrcStr + "(ဧည့္)"
            let strT = nrcStr + "(သာ)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }else if myLanguage == "en" {
            let strN = nrcStr + "(N)"
            let strP = nrcStr + "(P)"
            let strE = nrcStr + "(E)"
            let strT = nrcStr + "(T)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }else {
            let strN = nrcStr + "(နိုင်)"
            let strP = nrcStr + "(ပြု)"
            let strE = nrcStr + "(ဧည့်)"
            let strT = nrcStr + "(သာ)"
            nrcOption.listNRCOption = [strN,strP,strE,strT]
        }
        nrcOption.delegate = self
        self.navigationController?.pushViewController(nrcOption, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func setNRCNubmer(nrc: String) {
        if let del = delegate {
            del.nrcValueString(nrcSting: nrc)
        }
    }
    
}


 class TownshipCell: UITableViewCell {

 @IBOutlet weak var lblTownship: UILabel!
 @IBOutlet weak var town: UILabel!
    
   override func awakeFromNib() {
     super.awakeFromNib()
       //lblTownship.font = UIFont.systemFont(ofSize: 17)
        //town.font = UIFont.systemFont(ofSize: 17)
 }

}



