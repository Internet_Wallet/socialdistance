 //
 //  TownshipVC.swift
 //  DemoUpdateProfile
 //
 //  Created by SHUBH on 8/21/17.
 //  Copyright © 2017 SHUBH. All rights reserved.
 //
 
 import UIKit
 
 protocol TownshipVCDelegate {
    func divisionSetNRC(nrc : String)
 }
 
 class TownshipVC: UIViewController,TownshipDelegate {
    var delegate: TownshipVCDelegate?
    var allLocationsList = [LocationDetail]()
    var locationDetails: LocationDetail?
    @IBOutlet weak var tvDivision: UITableView!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    var tfSearch = UITextField()
    var isSearch = false
    var arr_city = [LocationDetail]()
    var CHARSET = ""
    var myLanguage = ""
    var array = [LocationDetail]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tvDivision.tableFooterView = UIView.init(frame: CGRect.zero)
        btnSearch.titleLabel?.font = UIFont(name: appName, size: 13)
        //btnBack.titleLabel?.font = UIFont(name: appName, size: 18)
        btnBack.setTitle("Back".localized, for: .normal)
        btnSearch.tag = 1
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            myLanguage = "my"
        }else{
             myLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
        }

        loadInitialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setMarqueLabelInNavigation()
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        isSearch = false
        self.tvDivision.reloadData()
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.tintColor = UIColor.init(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            tfSearch.font = UIFont(name: "Zawgyi-One", size: 17)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.placeholder = "Search".localized
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = UIImage(named: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2{
            btnSearch.tag = 1
            self.navigationController?.view.endEditing(false)
            isSearch = false
            
            tfSearch.text = ""
            lblNoRecordFound.isHidden = true
            tvDivision.reloadData()
            self.setMarqueLabelInNavigation()
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            lblNoRecordFound.isHidden = true
            tfSearch.text = ""
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
            isSearch = false
            tvDivision.reloadData()
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: "Zawgyi-One", size: 18)
        lblMarque.text = "Select NRC Code ".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        btnSearch.setTitle("", for: .normal)
        btnSearch.setImage(UIImage(named: "searchBtn"), for: .normal)
        lblNoRecordFound.isHidden = true
        lblNoRecordFound.text = "No Record Found".localized
    }
    
    
    func loadInitialize() {
        if myLanguage == "my"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else if myLanguage == "en"{
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }else if myLanguage == "uni"{
            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀ "
        }else {
          CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        }
        arr_city = TownshipManager.allLocationsList
        DispatchQueue.main.async {
            self.tvDivision.reloadData()
        }
    }
    
    func nrcValueString(nrcSting: String) {
        if let del = delegate {
            del.divisionSetNRC(nrc: nrcSting)
        }
    }
    
    func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
        if str == " " && textField.text?.last == " "{
            return false
        } else {
            if characterBeforeCursor(tf: textField) == " " && str == " " {
                return false
            } else {
                if characterAfterCursor(tf: textField) == " " && str == " " {
                    return false
                } else {
                    return true
                }
            }
        }
    }
    
    func characterBeforeCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
    func characterAfterCursor(tf : UITextField) -> String? {
        if let cursorRange = tf.selectedTextRange {
            if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
                let range = tf.textRange(from: newPosition, to: cursorRange.start)
                return tf.text(in: range!)
            }
        }
        return nil
    }
    
 }
 
 extension TownshipVC : UITextFieldDelegate {
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tvDivision.reloadData()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 0 && string == " " {
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                lblNoRecordFound.isHidden = true
                tvDivision.reloadData()
            }else {
                getSearchArrayContains(updatedText)
            }
        }
        return restrictMultipleSpaces(str: string, textField: textField)
    }
    
    func getSearchArrayContains(_ text : String) {
        if myLanguage == "my" {
            array = self.arr_city.filter({$0.nrcDisplayStateDivMY.lowercased().contains(text.lowercased())})
        }else if myLanguage == "en"{
            array = self.arr_city.filter({$0.nrcDisplayStateDivEN.lowercased().contains(text.lowercased())})
        }else if myLanguage == "uni"{
            array = self.arr_city.filter({$0.nrcDisplayStateDivUni.lowercased().contains(text.lowercased())})
        }else {
            array = self.arr_city.filter({$0.nrcDisplayStateDivEN.lowercased().contains(text.lowercased())})
        }
        
        if array.count == 0 {
            lblNoRecordFound.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
            lblNoRecordFound.isHidden = true
        }
        isSearch = true
        tvDivision.reloadData()
    }
 }
 
 
 extension TownshipVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else {
            return arr_city.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TownshipDivisionnCell", for: indexPath) as! TownshipDivisionnCell
        if isSearch {
            let dic = array[safe : indexPath.row]
            if myLanguage == "my" {
                cell.lblDivison.text = dic?.nrcDisplayStateDivMY
            }else if myLanguage == "en" {
                cell.lblDivison.text = dic?.nrcDisplayStateDivEN
            }else if myLanguage == "uni"{
                cell.lblDivison.text = dic?.nrcDisplayStateDivUni
            }else {
                cell.lblDivison.text = dic?.nrcDisplayStateDivEN
            }
        }else {
            let dic = arr_city[safe : indexPath.row]
            if myLanguage == "my" {
                cell.lblDivison.text = dic?.nrcDisplayStateDivMY
            }else if myLanguage == "en" {
                cell.lblDivison.text = dic?.nrcDisplayStateDivEN
            }else if myLanguage == "uni" {
                cell.lblDivison.text = dic?.nrcDisplayStateDivUni
            }else {
               cell.lblDivison.text = dic?.nrcDisplayStateDivEN
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let townshipVC2 = self.storyboard?.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
        if isSearch {
            townshipVC2.selectedDivState = array[safe : indexPath.row]
        }else {
            townshipVC2.selectedDivState = arr_city[safe : indexPath.row]
        }
        btnSearch.tag = 1
        townshipVC2.delegate = self
        self.navigationController?.pushViewController(townshipVC2, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
 }
 
 
 
 
 class TownshipDivisionnCell: UITableViewCell {
    
    @IBOutlet weak var lblDivison: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //lblDivison.font = UIFont.systemFont(ofSize: 17)
        
    }
    
 }
 
