//
//  DataSynViewModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 04/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class DataSynViewModel: NSObject{
    
     let apiReqObj = ApiRequestClass()
    
    
    
  
    
    func callDataSync(){
        apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getSyncApi())!, requestData: DataSynParam.makeParamForDataSync(), httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}

extension DataSynViewModel :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool){
        //success Block
        if !hasError {
        }else{
        }
        
    }
    
}
