//
//  AppDelFinalParam.swift
//  BLEScanner
//
//  Created by vamsi on 4/22/20.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

struct AppDelFinalParam {
    static func getAppDelParam(CountryCode:String) -> NSMutableDictionary{
     
        let postMutableDic = NSMutableDictionary()
        var mobilenum = ApplicationState.sharedAppState.currentUser.MobileNumber
                      
        if NetWorkCode.getMSIDForSimCard().0.count>0{
            let numberOfSim = NetWorkCode.getMSIDForSimCard().0[0]
            if numberOfSim == "1"{
               
                 postMutableDic.setValue(AppDelParam.getUserDetails(mobileNumber: mobilenum,OSType:"1",simId:Constant.uuid,msId:NetWorkCode.getMSIDForSimCard().0[1], appID: NetWorkCode.getTheAppID()), forKey: "UserDetails")
            }else{
                 postMutableDic.setValue(AppDelParam.getUserDetails(mobileNumber:mobilenum,OSType:"1",simId:Constant.uuid,msId:NetWorkCode.getMSIDForSimCard().0[safe:1] ?? "", appID: NetWorkCode.getTheAppID()), forKey: "UserDetails")
            }
            
        }
        postMutableDic.setValue(AppDelParam.getDeviceDetails(deviceID:Constant.uuid, gcmId: ApplicationState.sharedAppState.currentUser.APNSToken), forKey: "DeviceDetails")
        
        return postMutableDic
        
    }
}
