//
//  GetOccupationViewModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

protocol OccupationResponse {
    func getOccupationData(occupationModelObj: OccupationModel?,message: String)
}

class GetOccupationViewModel: NSObject{
    
    let apiReqObj = ApiRequestClass()
    var delegate: ResponseDelegate? = nil
    var occupationModelObj: OccupationModel?
    var occupationDelegate: OccupationResponse? = nil
    
    
    func callOccupationApi(mobileNumber: String){
        apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getOccupationApi())!, requestData: GetOccupationParam.getOccupationParam(mobile: mobileNumber, simID: Constant.uuid, msID: NetWorkCode.getMSIDForSimCard().0[1], appID: NetWorkCode.getTheAppID()), httpMethodName: NetWorkCode.serviceTypePost)
    }
    
}




extension GetOccupationViewModel :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool){
        //success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count>0{
                let modelObj = OccupationModel(dicData: dataResponse)
                //this block will define server error
                if modelObj.statusCode != "200"{
                    occupationDelegate?.getOccupationData(occupationModelObj: modelObj,message: modelObj.message ?? "")
                }else{
                    occupationDelegate?.getOccupationData(occupationModelObj: modelObj,message: "")
                }
            }
        }else{
            occupationDelegate?.getOccupationData(occupationModelObj: nil,message: "Error from Server")
        }
        
    }
    
}
