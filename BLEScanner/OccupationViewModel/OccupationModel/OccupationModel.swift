//
//  OccupationModel.swift
//  BLEScanner
//
//  Created by Tushar Lama on 01/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

open class OccupationModel {
    var statusCode: String?
    var message: String?
    var detailObj = [OccupationDetail]()
    
    public init(dicData: NSDictionary){
        if dicData.count>0{
            self.statusCode = dicData.value(forKey: "Code") as? String
            self.message = dicData.value(forKey: "Message") as? String
            let arrayValue = dicData.value(forKey: "Data") as? NSArray
            
            if let arr = arrayValue{
                if arr.count>0{
                    self.detailObj = arr.compactMap{OccupationDetail(dicValue: $0 as! NSDictionary)}
                }
            }
        }
    }
}

open class OccupationDetail{
    var name: String?
    var id: String?
    public init(dicValue: NSDictionary){
        if dicValue.count>0{
            self.name = dicValue.value(forKey: "name") as? String
            self.id = dicValue.value(forKey: "id") as? String
        }
    }
}

