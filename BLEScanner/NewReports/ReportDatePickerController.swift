//
//  ReportDatePickerController.swift
//  OK
//

import UIKit
protocol ReportDatePickerDelegate {
    func processWithDate(fromDate: String?, toDate: String?, dateMode: DateMode)
    func resetDate()
    func cancelPicker()
}
extension ReportDatePickerDelegate {
    func resetDate() { }
}
enum DateMode {
    case fromToDate, month, singleDate
}
class ReportDatePickerController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet {
            btnSubmit.setTitle("Submit".localized, for: .normal)
            btnSubmit.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet {
            btnCancel.setTitle("Cancel".localized, for: .normal)
            btnCancel.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet {
            btnReset.setTitle("Reset".localized, for: .normal)
            btnReset.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblFromDateTitle: UILabel!{
        didSet {
            lblFromDateTitle.text = "From Date".localized
            lblFromDateTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblToDateTitle: UILabel!{
        didSet {
            lblToDateTitle.text = "To Date".localized
            lblToDateTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnFromDateValue: UIButton!
    @IBOutlet weak var btnToDateValue: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var constraintWDateContainer: NSLayoutConstraint!
    @IBOutlet weak var constraintTDatePicker: NSLayoutConstraint!
    @IBOutlet weak var constraintTToLabel: NSLayoutConstraint!
    
    enum ViewMode {
        case landscape, portrait
    }
    //MARK: - Properties
    var dateMode = DateMode.fromToDate
    var delegate: ReportDatePickerDelegate?
    var viewOrientation = ViewMode.landscape
    var isResetNeeded = false
    
    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.calendar = Calendar(identifier: .gregorian)
        datePicker.timeZone = TimeZone(abbreviation: "GMT")
        configureButtonAndLabels()//Update font & title
        addGesture()
        setViewForOrientationMode()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func addGesture() {
        let tapGestToRemoveView = UITapGestureRecognizer(target: self, action: #selector(shadowIsTapped))
        viewShadow.addGestureRecognizer(tapGestToRemoveView)
    }
    
    private func setViewForOrientationMode() {
        switch viewOrientation {
        case .portrait:
            constraintWDateContainer.constant = UIScreen.main.bounds.size.width
            constraintTDatePicker.constant = 10
            constraintTToLabel.constant = -20
            switch dateMode {
            case .fromToDate:
                constraintTToLabel.constant = -5
            default:
                break
            }
            self.view.layoutIfNeeded()
        default:
            break
        }
    }
    
    private func configureButtonAndLabels() {
        btnSubmit.layer.cornerRadius = 15
        btnCancel.layer.cornerRadius = 15
        btnReset.layer.cornerRadius = 15
        btnSubmit.addTarget(self, action: #selector(filterSelectedDate), for: .touchUpInside)
        btnCancel.addTarget(self, action: #selector(dismissPickerController), for: .touchUpInside)
        btnReset.addTarget(self, action: #selector(resetDate), for: .touchUpInside)
        
        lblFromDateTitle.text = "From Date".localized
        lblToDateTitle.text = "To Date".localized
        btnSubmit.setTitle("Submit".localized, for: .normal)
        btnCancel.setTitle("Cancel".localized, for: .normal)
        btnReset.setTitle("Reset".localized, for: .normal)
        lblFromDateTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        lblToDateTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        btnSubmit.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        btnCancel.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        btnReset.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)

        datePicker.calendar = Calendar(identifier: .gregorian)
        datePicker.datePickerMode = .date
        
        switch dateMode {
        case .fromToDate:
            btnFromDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            configureDatePicker(tag: 0)
        case .month:
            btnFromDateValue.setTitle(nil, for: .normal)
            btnFromDateValue.isHidden = true
            lblFromDateTitle.isHidden = true
            lblToDateTitle.text = "Month".localized
            btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "MMM yyyy"), for: .normal)
            configureMonthPicker()
        case .singleDate:
            btnFromDateValue.setTitle(nil, for: .normal)
            btnFromDateValue.isHidden = true
            lblFromDateTitle.isHidden = true
            lblToDateTitle.text = "Selected Date".localized
            btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            configureSingleDatePicker()
        }
        
        if isResetNeeded {
            btnReset.isHidden = false
        } else {
            btnReset.isHidden = true
        }
    }
    
    private func configureDatePicker(tag: Int) {
        switch tag {
        case 0:
            btnFromDateValue.setTitleColor(Constant.datePickerColor, for: .normal)
            btnToDateValue.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(fromDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let endDateStr = btnToDateValue.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.maximumDate = endDate
            } else {
                datePicker.maximumDate = nil
            }
            if let currentDateStr = btnFromDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                btnFromDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        case 1:
            btnFromDateValue.setTitleColor(UIColor(red: 30/255.0, green: 30/255, blue: 30/255, alpha: 1.0), for: .normal)
            btnToDateValue.setTitleColor(Constant.datePickerColor, for: .normal)
            datePicker.maximumDate = Date()
            datePicker.removeTarget(nil, action: nil, for: .allEvents)
            datePicker.addTarget(self, action: #selector(toDatePickerChanged), for: .valueChanged)
            datePicker.minimumDate = nil
            if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                datePicker.date = currentDate
            } else {
                btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        default:
            break
        }
    }
    
    private func configureMonthPicker() {
        btnToDateValue.setTitleColor(Constant.datePickerColor, for: .normal)
        datePicker.maximumDate = Date()
        datePicker.date = Date()
        datePicker.removeTarget(nil, action: nil, for: .allEvents)
        datePicker.addTarget(self, action: #selector(monthPickerChanged), for: .valueChanged)
        datePicker.minimumDate = nil
        if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "MMM yyyy") {
            datePicker.date = currentDate
        } else {
            btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "MMM yyyy"), for: .normal)
        }
    }
    
    private func configureSingleDatePicker() {
        btnToDateValue.setTitleColor(Constant.datePickerColor, for: .normal)
        datePicker.maximumDate = Date()
        datePicker.date = Date()
        datePicker.removeTarget(nil, action: nil, for: .allEvents)
        datePicker.addTarget(self, action: #selector(dingleDatePickerChanged), for: .valueChanged)
        datePicker.minimumDate = nil
        if let currentDateStr = btnToDateValue.title(for: .normal), let currentDate = currentDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            datePicker.date = currentDate
        } else {
            btnToDateValue.setTitle(Date().stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    //MARK: - Target Methods
    @objc func filterSelectedDate() {
        var fromDate: String?
        var toDate: String?
        switch dateMode {
        case .fromToDate:
            if let dateStartInStr = btnFromDateValue.title(for: .normal) {
                fromDate = dateStartInStr//dateStartInStr.dateValue(dateFormatIs: "dd MMM yyyy")
            }
            if let dateEndInStr = btnToDateValue.title(for: .normal) {
                toDate = dateEndInStr//dateEndInStr.dateValue(dateFormatIs: "dd-MM-yyyy HH:mm")
            }
        case .month:
            if let monthInStr = btnToDateValue.title(for: .normal) {
                toDate = monthInStr//.dateValue(dateFormatIs: "MMM yyyy")
            }
        case .singleDate:
            if let monthInStr = btnToDateValue.title(for: .normal) {
                toDate = monthInStr//.dateValue(dateFormatIs: "dd MMM yyyy")
            }
        }
        delegate?.processWithDate(fromDate: fromDate, toDate: toDate, dateMode: dateMode)
        dismissPickerController()
    }
    
    @objc func dismissPickerController() {
        delegate?.cancelPicker()
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func resetDate() {
        delegate?.resetDate()
        dismissPickerController()
    }
    
    @objc func fromDatePickerChanged(datePicker:UIDatePicker) {
        btnFromDateValue.setTitle(datePicker.date.stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
        //datePicker.minimumDate = datePicker.date
    }
    
    @objc func toDatePickerChanged(datePicker: UIDatePicker) {
        if let startDateStr = btnFromDateValue.title(for: .normal), let startDate = startDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
            datePicker.minimumDate = startDate
            switch datePicker.date.compare(startDate) {
            case .orderedAscending:
                if let endDateStr = btnToDateValue.title(for: .normal), let endDate = endDateStr.dateValue(dateFormatIs: "dd MMM yyyy") {
                    datePicker.date = endDate
                }
            case .orderedDescending, .orderedSame:
                btnToDateValue.setTitle(datePicker.date.stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
            }
        } else {
            btnToDateValue.setTitle(datePicker.date.stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
        }
    }
    
    @objc func monthPickerChanged(datePicker: UIDatePicker) {
        btnToDateValue.setTitle(datePicker.date.stringValue(dateFormatIs: "MMM yyyy"), for: .normal)
    }
    
    @objc func dingleDatePickerChanged(datePicker: UIDatePicker) {
        btnToDateValue.setTitle(datePicker.date.stringValue(dateFormatIs: "dd MMM yyyy"), for: .normal)
    }
    
    @objc func shadowIsTapped(sender: UITapGestureRecognizer) {
        dismissPickerController()
    }
    
    //MARK: - Button Action Methods
    @IBAction func configureDatePickerOne(_ sender: UIButton) {
        configureDatePicker(tag: 0)
    }
    
    @IBAction func configureDatePickerTwo(_ sender: UIButton) {
        if btnFromDateValue.isHidden {
            configureMonthPicker()
        } else {
            configureDatePicker(tag: 1)
        }
    }
    
    deinit {
        if let recognizers = viewShadow.gestureRecognizers {
            for recognizer in recognizers {
                if let recognizer = recognizer as? UITapGestureRecognizer {
                    viewShadow.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
}
