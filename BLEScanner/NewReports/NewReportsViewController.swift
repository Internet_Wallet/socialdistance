//
//  NewReportsViewController.swift
//  BLEScanner
//
//  Created by Tushar Lama on 10/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit
import TGPControls

class NewReportsViewController: UIViewController {
    
    private var multiButton : ActionButton?

    @IBOutlet var newreportsTableView: UITableView!
    @IBOutlet var lblFeetRange: UILabel!{
        didSet {
            lblFeetRange.text = "Feet Range Filter".localized
        }
    }
    @IBOutlet var lblSelectFeet: UILabel!{
        didSet {
            lblSelectFeet.text = "Select Feet".localized
        }
    }

    @IBOutlet var feetrangeView: UIView!
    @IBOutlet weak var oneTo10Labels: TGPCamelLabels!
    @IBOutlet weak var oneTo10Slider: TGPDiscreteSlider!
    var effectView: UIVisualEffectView!
    var touchStr = ""
    let validObj = PayToValidations.init()
    @IBOutlet var okButton: UIButton!{
        didSet{
            self.okButton.setTitle("OK".localized, for: .normal)
            self.okButton.layer.cornerRadius = 8.0
            self.okButton.layer.masksToBounds = false
            self.okButton.isEnabled = true
        }
    }
    var localModelObj = [BluetoothDataModel]()
    @IBOutlet var lblTitle: UILabel!{
        didSet {
            lblTitle.text = "Reports".localized
        }
    }

    @IBOutlet weak var lblNoRecordFound: UILabel!
    var dateStringToFilter = Date().stringValue(dateFormatIs: "yyyy-MM-dd")
    var fromDateString = Date().stringValue(dateFormatIs: "yyyy-MM-dd")
    var toDateString = Date().stringValue(dateFormatIs: "yyyy-MM-dd")
    var value = 1.0//For Feet Filter Default
    var slidermove = ""
    var blueToothId = ""
    var blueTothDate = ""
    
    //MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lblTitle.text = "Reports".localized
        lblNoRecordFound.text = "No Record Found".localized
        lblSelectFeet.text = "Select Feet".localized
        lblFeetRange.text = "Feet Range Filter".localized
        lblTitle.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        lblNoRecordFound.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        lblSelectFeet.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        lblFeetRange.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)

        self.loadFloatButtons()

        getArrayWithSortedData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.systemBlue
        self.navigationController?.navigationBar.tintColor = UIColor.systemBlue
        self.navigationController?.navigationItem.title = "Reports".localized
        self.navigationController?.navigationBar.tintColor = UIColor.systemBlue
        
        // Do any additional setup after loading the view.
        feetrangeView.isHidden = true
        lblNoRecordFound.isHidden = true
        lblNoRecordFound.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 20)
        
        oneTo10Slider.addTarget(self, action: #selector(NewReportsViewController.valueChanged(_:event:)), for: .valueChanged)
        
//        self.newreportsTableView.estimatedRowHeight = 230
//        self.newreportsTableView.rowHeight = UITableView.automaticDimension
//        self.newreportsTableView.setNeedsLayout()
//        self.newreportsTableView.layoutIfNeeded()
//        self.newreportsTableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        
    }
    
    
    func getArrayWithSortedData() {
        localModelObj.removeAll()
        localModelObj = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let ready = localModelObj.sorted(by: { dateFormatter.date(from:$0.bluetoothDate)!.compare(dateFormatter.date(from:$1.bluetoothDate)!) == .orderedDescending })
        localModelObj.removeAll()
        localModelObj = ready
       newreportsTableView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        if touch?.view == self.feetrangeView {
            
            feetrangeView.isHidden = false
        }
        else
        {
            if touchStr == "1"{
                feetrangeView.isHidden = true
                self.removeBlur()
                touchStr = "0"
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if touchStr == "1"{
            feetrangeView.isHidden = true
            self.removeBlur()
            touchStr = "0"
        }else{
      //       multiButton?.notifyBlurDelegate = self as? NotifyBlurDelegate
            multiButton?.hideBlur()
        }
        
    }
    // MARK: - Custom Methods
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let all = ActionButtonItem(title: "All".localized, image: #imageLiteral(resourceName: "filter_all"))
        all.action = { item in
            self.multiButton?.toggleMenu()
            self.getArrayWithSortedData()
        }
        let bluetooth = ActionButtonItem(title: "Bluetooth Filter".localized, image: #imageLiteral(resourceName: "filter_bluetooth"))
        bluetooth.action = { item in
            self.multiButton?.toggleMenu()
            self.localModelObj.removeAll()
            let data = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
            self.localModelObj = data.filter({$0.bluetoothId.count > 10})
            self.newreportsTableView.reloadData()
            print("Bluetooth Filter")
        }
        
        let QR = ActionButtonItem(title: "QR Filter".localized, image: #imageLiteral(resourceName: "filter_qr"))
        QR.action = { item in
            self.multiButton?.toggleMenu()
            print("QR Filter")
            self.localModelObj.removeAll()
            let data = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
            //self.localModelObj = data.filter({$0.bluetoothName == "Audio QR"})
            self.localModelObj = data.filter({$0.bluetoothId.count < 10})
            self.newreportsTableView.reloadData()
        }
        
        let feetFilter = ActionButtonItem(title: "Filter By Feet".localized, image: #imageLiteral(resourceName: "filter_feet"))
        feetFilter.action = { item in
            self.multiButton?.toggleMenu()
            //self.oneTo10Slider.ticksListener = self.oneTo10Labels
            self.feetrangeView.isHidden = false
            self.addBlur()
            self.touchStr = "1"
            self.view.addSubview(self.feetrangeView)
            self.oneTo10Labels.names = ["1","2","3","4","5","6","7","8","9","10"]
            self.oneTo10Slider.ticksListener = self.oneTo10Labels
            self.oneTo10Slider.addTarget(self, action: #selector(NewReportsViewController.valueChanged(_:event:)), for: .valueChanged)
            print("Filter By Feet")
            
        }
        
        let dateFilter = ActionButtonItem(title: "Date Filter".localized, image: #imageLiteral(resourceName: "filter_date"))
        dateFilter.action = { item in
            self.multiButton?.toggleMenu()
            
            self.addBlur()
            print("Date Filter")
            guard let reportDatePickerController = UIStoryboard(name: "NewReports", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReportDatePickerController") as? ReportDatePickerController else { return }
            reportDatePickerController.dateMode = .fromToDate
            reportDatePickerController.isResetNeeded = true
            reportDatePickerController.delegate = self
            reportDatePickerController.viewOrientation = .portrait
            reportDatePickerController.modalPresentationStyle = .overCurrentContext
            self.present(reportDatePickerController, animated: false, completion: nil)
        }
        
        buttonItems = [dateFilter,feetFilter,QR,bluetooth, all]
        
        self.multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        self.multiButton?.notifyBlurDelegate = self as? NotifyBlurDelegate
        self.multiButton?.action = {
            button in button.toggleMenu()
        }
        self.multiButton?.setImage(#imageLiteral(resourceName: "filter"), forState: .normal)
        
    }
    
    
    @IBAction func okbuttonClick(_ sender: Any) {
        feetrangeView.isHidden = true
        self.removeBlur()
        //Default Value 1
        if slidermove == ""{
           // value = Double(value)
            print("@@@@@",value)
            var sortrssi = 0
            self.localModelObj.removeAll()
            for rssi in ApplicationState.sharedAppState.blueToothModelObj {
               sortrssi = Int(rssi.rssiID) ?? 0
                if value > 0 && value <= 1 {
                if sortrssi < 0 && sortrssi >= -13 {
                print("#######", sortrssi)
                self.localModelObj.append(rssi)
                  }
              }
            }
              self.newreportsTableView.reloadData()
        }
        else{
            //
        }
       
    }
    func addBlur() {
        
        let effect = UIBlurEffect(style: UIBlurEffect.Style.light)
        effectView = UIVisualEffectView(effect: effect)
        effectView.frame = self.view.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(effectView)
        
    }
    func removeBlur() {
        effectView.removeFromSuperview()
    }
    
   
    
    @objc func valueChanged(_ sender: TGPDiscreteSlider, event:UIEvent) {
       //    controlEventsLabel.text = "valueChanged"
       //    stepper.value = Double(sender.value)
        slidermove = "1"
        value = Double(sender.value)
        print("@@@@@",value)
        var sortrssi = 0
        self.localModelObj.removeAll()
        
        for rssi in ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray {
                     
                 sortrssi = Int(rssi.rssiID) ?? 0
            if value > 0 && value <= 1 {
                 if sortrssi < 0 && sortrssi >= -13 {
                     print("#######", sortrssi)
                     
                     self.localModelObj.append(rssi)
                 }
            } else if value > 1 && value <= 2 {
                  if sortrssi <= -13  && sortrssi > -26 {
                    print("#######", sortrssi)
                    self.localModelObj.append(rssi)
                  }
            } else if value > 2 && value <= 3 {
                  if sortrssi <= -26  && sortrssi > -39 {
                      print("#######", sortrssi)
                     self.localModelObj.append(rssi)
                  }
            }else if value > 3 && value <= 4 {
                   if sortrssi <= -39  && sortrssi > -52 {
                        print("#######", sortrssi)
                        self.localModelObj.append(rssi)
                  }
            }else if value > 4 && value <= 5 {
                  if sortrssi <= -52  && sortrssi > -65 {
                  print("#######", sortrssi)
                  self.localModelObj.append(rssi)
                 }
            } else if value > 5 && value <= 6 {
                 if sortrssi <= -65  && sortrssi > -78 {
                     print("#######", sortrssi)
                    self.localModelObj.append(rssi)
                }
            }
            
            else if value > 6 && value <= 7 {
                if sortrssi <= -78  && sortrssi > -91 {
                    print("#######", sortrssi)
                      self.localModelObj.append(rssi)
                 }
              }
            else if value > 7 && value <= 8 {
                  if sortrssi <= -91  && sortrssi > -104 {
                  print("#######", sortrssi)
                  self.localModelObj.append(rssi)
                }
            }
            else if value > 8 && value <= 9 {
                  if sortrssi <= -104  && sortrssi > -117 {
                  print("#######", sortrssi)
                  self.localModelObj.append(rssi)
                }
            }
            else if value > 9 && value <= 10 {
                if sortrssi <= -117  && sortrssi > -130 {
                  print("#######", sortrssi)
                   self.localModelObj.append(rssi)
                  }
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let ready = localModelObj.sorted(by: { dateFormatter.date(from:$0.bluetoothDate)!.compare(dateFormatter.date(from:$1.bluetoothDate)!) == .orderedDescending })
        localModelObj.removeAll()
        localModelObj = ready
        self.newreportsTableView.reloadData()
        
    }
}


extension NewReportsViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: String?, toDate: String?, dateMode: DateMode) {
        // print(fromDate,toDate,dateMode)
        removeBlur()
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd MMM yyyy"
        let fromtime = dateFormatter.date(from: fromDate!) //timeStr
        let totime = dateFormatter.date(from: toDate!) //timeStr
        
        dateFormatter.dateFormat = "dd-MM-yyyy"//"E, d MMM yyyy hh:mm a"//HH:mm:ss\\\
        let fromDatePicker = dateFormatter.string(from: fromtime!)
        let toDatePicker = dateFormatter.string(from: totime!)
        let previousCurrent = ApplicationState.sharedAppState.blueToothModelObj + ApplicationState.sharedAppState.previousReportsArray
        if fromDatePicker == toDatePicker{
            self.localModelObj.removeAll()
            self.localModelObj = previousCurrent.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == fromDatePicker})
        }else{
            let fromdateValue = previousCurrent.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == fromDatePicker})
            let todateValue = previousCurrent.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == toDatePicker})
            let mergedObj = fromdateValue + todateValue
            self.localModelObj.removeAll()
            self.localModelObj = mergedObj
        }
        
        self.newreportsTableView.reloadData()
        
        
    }
    
    func cancelPicker() {
        removeBlur()
    }
    
    func resetDate() {
        removeBlur()
        fromDateString = Date().stringValue(dateFormatIs: "yyyy-MM-dd")
        toDateString = Date().stringValue(dateFormatIs: "yyyy-MM-dd")

        self.localModelObj.removeAll()
        self.localModelObj = ApplicationState.sharedAppState.blueToothModelObj
        self.newreportsTableView.reloadData()
    }
}

extension NewReportsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if localModelObj.count == 0 {
                lblNoRecordFound.isHidden = false
        }else {
                lblNoRecordFound.isHidden = true
        }
        return localModelObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewReportsTableViewCell") as? NewReportsTableViewCell else {
            return UITableViewCell()
        }
        
        cell.addfromcontactsButton.setTitle("Choose Contact".localized, for: .normal)
        cell.addmobilenumberbutton.setTitle("Add Contact".localized, for: .normal)
        cell.addmobilenumberbutton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)
        cell.addfromcontactsButton.titleLabel?.font = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 15.0)

        cell.extraView.layer.cornerRadius = 8
        cell.extraView.layer.masksToBounds = true
        cell.extraView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.extraView.layer.shadowColor = UIColor.black.cgColor
        cell.extraView.layer.shadowOpacity = 0.23
        cell.extraView.layer.shadowRadius = 4
        
        cell.addmobilenumberbutton.layer.cornerRadius =  20
        cell.addmobilenumberbutton.layer.borderWidth = 2
        cell.addmobilenumberbutton.layer.borderColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
        
        cell.addfromcontactsButton.layer.cornerRadius =  20
        cell.addfromcontactsButton.layer.borderWidth = 2
        cell.addfromcontactsButton.layer.borderColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
        let name = localModelObj[safe: indexPath.row]?.bluetoothName ?? ""
        print("@@@@@",name)
         print("vvvvvv",localModelObj[indexPath.row].mobileNumber)
        var labelname = ""
        if localModelObj[indexPath.row].mobileNumber != "" && name != "No Name"{
            //hide biutton
            cell.labelTopConstraint.constant = 40
            cell.addmobilenumberbutton.isHidden = true
            cell.addfromcontactsButton.isHidden = true
            labelname = "Name: "
        }else {
            cell.labelTopConstraint.constant = 10
            cell.addmobilenumberbutton.isHidden = false
            cell.addfromcontactsButton.isHidden = false
            labelname = "Device Name: "
        }
       
        
        cell.idlabel.textAlignment = .left
        cell.idlabel.lineBreakMode = .byCharWrapping
        cell.idlabel.sizeToFit()
        cell.deviceNameLabel.text = labelname + name
        cell.idlabel.text = "Id : " +  (localModelObj[safe: indexPath.row]?.bluetoothId ?? "")
       
        let rssi = localModelObj[safe: indexPath.row]?.rssiID ?? ""
        
        if rssi != ""
        {
         cell.rssiLabel.isHidden = false
         var sortrssi = 0
         sortrssi = Int(rssi) ?? 0
        if sortrssi < 0 && sortrssi >= -13 {
            print("#######", sortrssi)
            
             cell.rssiLabel.text = "Distance : " + "1 Feet"
            
        }
       else if sortrssi <= -13  && sortrssi > -26 {
            print("#######", sortrssi)
             cell.rssiLabel.text = "Distance : " + "2 Feet"
        }
        else if sortrssi <= -26  && sortrssi > -39 {
             print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "3 Feet"
        }
       else if sortrssi <= -39  && sortrssi > -52 {
            print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "4 Feet"
         }
       else if sortrssi <= -52  && sortrssi > -65 {
            print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "5 Feet"
        }
       else if sortrssi <= -65  && sortrssi > -78 {
            print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "6 Feet"
        }
       else if sortrssi <= -78  && sortrssi > -91 {
             print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "7 Feet"
        }
       else if sortrssi <= -91  && sortrssi > -104 {
              print("#######", sortrssi)
            cell.rssiLabel.text = "Distance : " + "8 Feet"
        }
       else if sortrssi <= -104  && sortrssi > -117 {
              print("#######", sortrssi)
              cell.rssiLabel.text = "Distance : " + "9 Feet"
        }
        else if sortrssi <= -117  && sortrssi > -130 {
              print("#######", sortrssi)
             cell.rssiLabel.text = "Distance : " + "10 Feet"
        }
    }
//        if rssi != ""
//        {
//             cell.rssiLabel.isHidden = false
//             cell.rssiLabel.text = "RSSI : " + rssi
//        }
        else{
            cell.rssiLabel.isHidden = true
        }
        

        let myString = localModelObj[safe: indexPath.row]?.bluetoothDate ?? ""
        let convertedDate: String = Utility.makeDateFormatForSyncWithAMPM(date: myString)
       
        print("@@@@@@@",convertedDate)
        
        let myStringArr = convertedDate.components(separatedBy: " ")
        let date: String = myStringArr [0]
        let time: String = myStringArr [1]
        let formate: String =  myStringArr [2]
        
        
         cell.dateLabel.text = "Date : " + (date)
         cell.timeLabel.text  = "Time : " + (time) +  " " + (formate)
        
        if localModelObj[safe: indexPath.row]?.mobileNumber ?? "" == ""{
            cell.idlabel.text = "Id : " +  (localModelObj[safe: indexPath.row]?.bluetoothId ?? "")
        }else{
            cell.idlabel.text = "Mobile No : " +  (localModelObj[safe: indexPath.row]?.mobileNumber ?? "")
        }
       
     //   cell.dateLabel.text = "Date: " + (localModelObj[safe: indexPath.row]?.bluetoothDate ?? "")

        
        cell.addmobilenumberbutton.tag = indexPath.row
        cell.addmobilenumberbutton.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        cell.addfromcontactsButton.tag = indexPath.row
        cell.addfromcontactsButton.addTarget(self, action: #selector(contactbuttonSelected), for: .touchUpInside)
        
      //  cell.checkmarkbutton.tag = indexPath.row
     //   cell.checkmarkbutton.addTarget(self, action: #selector(checkmarkbuttonSelected), for: .touchUpInside)
     //   cell.checkmarkbutton.isHidden = true
     //   cell.ignoreLabel.isHidden = true
        
//        if localModelObj[indexPath.row].userCategory == CustomNotification.DefaultValue.description {
//            cell.checkmarkbutton.setImage(UIImage(named: "uncheckMark"), for: .normal)
//        }else{
//             cell.checkmarkbutton.setImage(UIImage(named: "CheckMark"), for: .normal)
//        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230.0
    }
    
    @objc func buttonSelected(sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "FromContact")
       blueToothId = localModelObj[safe: sender.tag]?.bluetoothId ?? ""
        blueTothDate = localModelObj[safe: sender.tag]?.bluetoothDate ?? ""
       // UserDefaults.standard.set(b_id, forKey: "Bluetooth_ID_Contact")
      //  UserDefaults.standard.synchronize()
        self.navigateToReportContact(bluetoothId: blueToothId,bluetoothDate: blueTothDate)
    }
    @objc func contactbuttonSelected(sender: UIButton) {
//        let b_id = localModelObj[safe: sender.tag]?.bluetoothId ?? ""
//        UserDefaults.standard.set(b_id, forKey: "Bluetooth_ID_Contact")
        
        blueToothId = localModelObj[safe: sender.tag]?.bluetoothId ?? ""
        blueTothDate = localModelObj[safe: sender.tag]?.bluetoothDate ?? ""
        self.showContactPickerView()
    }
    @objc func checkmarkbuttonSelected(sender: UIButton) {
        let items = ApplicationState.sharedAppState.blueToothModelObj
        if sender.currentImage == UIImage(named: "CheckMark") {
            if items.indices.contains(sender.tag) {
                let local =  ApplicationState.sharedAppState.blueToothModelObj[sender.tag]
                ApplicationState.sharedAppState.blueToothModelObj.remove(at: sender.tag)
                ApplicationState.sharedAppState.blueToothModelObj.insert(BluetoothDataModel(name: local.bluetoothName, id: items[sender.tag].bluetoothId, bluetoothDate: items[sender.tag].bluetoothDate, bluetoothDateLessThanSixFeet: items[sender.tag].bluetoothDateLessThanSixFeet, peopleMeet: items[sender.tag].peopleMeet, userCategory: CustomNotification.DefaultValue.description, numberOfTimePeopleMeet: items[sender.tag].numberOfTimePeopleMeet, rssiID: items[sender.tag].rssiID, mobileNumber: local.mobileNumber), at: sender.tag)
                localModelObj.remove(at: sender.tag)
                localModelObj.insert(BluetoothDataModel(name: local.bluetoothName, id: items[sender.tag].bluetoothId, bluetoothDate: items[sender.tag].bluetoothDate, bluetoothDateLessThanSixFeet: items[sender.tag].bluetoothDateLessThanSixFeet, peopleMeet: items[sender.tag].peopleMeet, userCategory: CustomNotification.DefaultValue.description, numberOfTimePeopleMeet: items[sender.tag].numberOfTimePeopleMeet, rssiID: items[sender.tag].rssiID, mobileNumber: local.mobileNumber), at: sender.tag)
                
            }
            
        }else{
            if items.indices.contains(sender.tag) {
                let local =  ApplicationState.sharedAppState.blueToothModelObj[sender.tag]
                ApplicationState.sharedAppState.blueToothModelObj.remove(at: sender.tag)
                ApplicationState.sharedAppState.blueToothModelObj.insert(BluetoothDataModel(name: local.bluetoothName, id: items[sender.tag].bluetoothId, bluetoothDate: items[sender.tag].bluetoothDate, bluetoothDateLessThanSixFeet: items[sender.tag].bluetoothDateLessThanSixFeet, peopleMeet: items[sender.tag].peopleMeet, userCategory: CustomNotification.Others.description, numberOfTimePeopleMeet: items[sender.tag].numberOfTimePeopleMeet, rssiID: items[sender.tag].rssiID, mobileNumber: local.mobileNumber), at: sender.tag)
                
                localModelObj.remove(at: sender.tag)
                localModelObj.insert(BluetoothDataModel(name: local.bluetoothName, id: items[sender.tag].bluetoothId, bluetoothDate: items[sender.tag].bluetoothDate, bluetoothDateLessThanSixFeet: items[sender.tag].bluetoothDateLessThanSixFeet, peopleMeet: items[sender.tag].peopleMeet, userCategory: CustomNotification.Others.description, numberOfTimePeopleMeet: items[sender.tag].numberOfTimePeopleMeet, rssiID: items[sender.tag].rssiID, mobileNumber: local.mobileNumber), at: sender.tag)
            }
            
        }
        
       // newreportsTableView.reloadData()
        
        localModelObj.removeAll()
        localModelObj = ApplicationState.sharedAppState.blueToothModelObj
        if let _ = newreportsTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? NewReportsTableViewCell {
            newreportsTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        }
       }
    
    
    func navigateToReportContact(bluetoothId: String,bluetoothDate: String) {
        if #available(iOS 13.0, *) {
            let sb = UIStoryboard(name: "QRCode", bundle: nil)
            let baseObj = sb.instantiateViewController(identifier: "MobileNumberViewController") as? MobileNumberViewController
            if let isObjAvaliable = baseObj{
                isObjAvaliable.modalPresentationStyle = .fullScreen
                isObjAvaliable.blueToothId = bluetoothId
                 isObjAvaliable.blueToothDate = bluetoothDate
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(isObjAvaliable, animated: false, completion: nil)
                }
                self.blueToothId = ""
                self.blueTothDate = ""
            }
        }else {
            let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
            let dashbord = homeVC.instantiateViewController(withIdentifier: "MobileNumberViewController") as? MobileNumberViewController
            var objNav: UINavigationController? = nil
            if let dashbord = dashbord {
                objNav = UINavigationController(rootViewController: dashbord)
            }
            if let objNav = objNav {
                if let isAvailableObj = dashbord{
                    isAvailableObj.blueToothId = bluetoothId
                    isAvailableObj.blueToothDate = bluetoothDate
                    isAvailableObj.modalPresentationStyle = .fullScreen
                }
                self.present(objNav, animated: false)
                self.blueToothId = ""
                self.blueTothDate = ""
            }
        }
    }
    
    
    func showContactPickerView() {
         let nav = Utility.openContact(multiSelection: false, self)
           nav.modalPresentationStyle = .fullScreen
           self.present(nav, animated: false, completion: nil)
     }
}
