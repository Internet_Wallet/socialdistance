//
//  ReportContactExtension.swift
//  BLEScanner
//
//  Created by Sam on 15/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

extension NewReportsViewController:  ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
       func contact(_: ContactPickersPicker, didCancel error: NSError){}
       func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
       func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
       
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        print(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        if tempPhoneNumber.count > 3 {
            if countryCode == "95" {
                let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                let dic = ["country": "Myanmar","flag": countryData.countryFlag,"code": countryData.countryCode,"number":noToCheck,"name":  contact.firstName]
                UserDefaults.standard.set(dic, forKey: "ContactDetails")
                UserDefaults.standard.set(true, forKey: "FromContact")
                UserDefaults.standard.synchronize()
                self.navigateToReportContact(bluetoothId: blueToothId, bluetoothDate: blueTothDate)
                //                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                //                if !mbLength.isRejected {
                //                    if noToCheck.count < mbLength.min || noToCheck.count > mbLength.max {
                //                        Utility.alert(message: "", title: "You selected invalid number, Please try again".localized, controller: self)
                //                    }else {
                //                        let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                //                        let dic = ["country": "Myanmar","flag": countryData.countryFlag,"code": countryData.countryCode,"number":noToCheck,"name":  contact.firstName]
                //                        UserDefaults.standard.set(dic, forKey: "ContactDetails")
                //                        UserDefaults.standard.set(true, forKey: "FromContact")
                //                        UserDefaults.standard.synchronize()
                //                        self.navigateToReportContact()
                //                    }
                //                }else {
                //                    Utility.alert(message: "", title: "You selected invalid number, Please try again".localized, controller: self)
                //                }
            }else {
                let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                let dic = ["country": countryData.countryFlag,"flag": countryData.countryFlag,"code": countryData.countryCode,"number":noToCheck,"name":  contact.firstName]
                UserDefaults.standard.set(dic, forKey: "ContactDetails")
                UserDefaults.standard.set(true, forKey: "FromContact")
                UserDefaults.standard.synchronize()
                
                self.navigateToReportContact(bluetoothId: blueToothId, bluetoothDate: blueTothDate)
                //                if tempPhoneNumber.count > 14 {
                //                    DispatchQueue.main.async {
                //                                  Utility.alert(message: "", title: "You selected invalid number, Please try again".localized, controller: self)
                //                              }
                //                }else {
                //                    let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                //                            let dic = ["country": countryData.countryFlag,"flag": countryData.countryFlag,"code": countryData.countryCode,"number":noToCheck,"name":  contact.firstName]
                //                            UserDefaults.standard.set(dic, forKey: "ContactDetails")
                //                            UserDefaults.standard.set(true, forKey: "FromContact")
                //                            UserDefaults.standard.synchronize()
                //                            self.navigateToReportContact()
                //                }
            }
        }else {
            DispatchQueue.main.async {
                Utility.alert(message: "", title: "You selected invalid number, Please try again".localized, controller: self)
            }
        }
    }
    
       
       func identifyCountry(withPhoneNumber prefix: String) -> (countryCode: String, countryFlag: String) {
           let countArray = revertCountryArray() as! Array<NSDictionary>
           var finalCodeString = ""
           var flagCountry = ""
           for dic in countArray {
               if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic .object(forKey: "CountryFlagCode") as? String {
                   if prefix.hasPrefix(code) {
                       finalCodeString = code
                       flagCountry  = flag
                   }
               }
           }
           return (finalCodeString,flagCountry)
       }
       
       func identifyCountryByCode(withPhoneNumber prefix: String) -> (countryCode: String, countryFlag: String) {
           let countArray = revertCountryArray() as! Array<NSDictionary>
           var finalCodeString = ""
           var flagCountry = ""
           for dic in countArray {
               if let code = dic.object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String {
                   if prefix == code {
                       finalCodeString = code
                       flagCountry  = flag
                   }
               }
           }
           return (finalCodeString,flagCountry)
       }
       
       func revertCountryArray() -> NSArray {
           var arrCountry = NSArray.init()
           do {
               if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                   let data = try Data(contentsOf: file)
                   let json = try JSONSerialization.jsonObject(with: data, options: [])
                   if let object = json as? NSDictionary {
                       if let arr = object.object(forKey: "data") as? NSArray {
                           arrCountry = arr
                           return arrCountry
                       }
                   } else if let object = json as? [Any] {
                       print(object)
                   } else {
                       print("JSON is invalid")
                   }
               } else {
                   print("no file")
               }
           } catch {
               print(error.localizedDescription)
           }
           return arrCountry
       }

    
}
