//
//  NewReportsTableViewCell.swift
//  BLEScanner
//
//  Created by Tushar Lama on 10/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class NewReportsTableViewCell: UITableViewCell {
    
    @IBOutlet var labelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var extraView: UIView!
    
    @IBOutlet weak var idlabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var deviceNameLabel: UILabel!
    
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var rssiLabel: UILabel!
    @IBOutlet var addmobilenumberbutton: UIButton!{
        didSet {
            addmobilenumberbutton.setTitle("Add Contact".localized, for: .normal)
        }
    }
    
    @IBOutlet var addfromcontactsButton: UIButton!{
        didSet {
            addfromcontactsButton.setTitle("Choose Contact".localized, for: .normal)
        }
    }
    
    @IBOutlet var checkmarkbutton: UIButton!
    
    @IBOutlet var ignoreLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
