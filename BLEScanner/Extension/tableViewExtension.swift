//
//  tableViewExtension.swift
//  BLEScanner
//
//  Created by Tushar Lama on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
     public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.borderColor  = UIColor.lightGray.cgColor
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
}
