//
//  textFieldExtension.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
    func setPropertiesForTextField(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 0.5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 1.0
        self.layer.cornerRadius = 12.0
    }
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
   // use like below where ever you want
    
    // 1.  To add left padding
  //  yourTextFieldName.addPadding(.left(20))

    // 2.  To add right padding
 //   yourTextFieldName.addPadding(.right(20))

    // 3. To add left & right padding both
 //   yourTextFieldName.addPadding(.both(20))
    
    
    func addPadding( padding: PaddingSide) {

        self.leftViewMode = .always
        self.layer.masksToBounds = true


        switch padding {

        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always

        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always

        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
}

extension UITextView{
    
    func setPropertiesForTextField(){
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 0.5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 1.0
        self.layer.cornerRadius = 12.0
    }
}


class NoPasteTextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(copy(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 2.5, width: 108 , height: 40)
       }
}

 
