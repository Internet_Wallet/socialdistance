////
////  UltraSonicListVC.swift
////  BLEScanner
////
////  Created by OK$ on 30/03/2020.
////  Copyright © 2020 GG. All rights reserved.
////
//
//import UIKit
//import AVFoundation
//import Foundation
////import QuietModemKit
//
//class UltraSonicListVC: UIViewController {
//    
//    var timer = Timer()
//    @IBOutlet weak var contactsTableView: UITableView!
//    var phonenumberArray = [String]()
//
//    var tx: QMFrameTransmitter = {
//        let txConf: QMTransmitterConfig = QMTransmitterConfig(key:"ultrasonic") //ultrasonic-experimental
//        let tx: QMFrameTransmitter = QMFrameTransmitter(config: txConf)
//        return tx
//    }()
//    var rx: QMFrameReceiver?
//    
//let currentRoute = AVAudioSession.sharedInstance().currentRoute
//
//    //MARK: - Views life cycle methods
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
//                   if granted {
//                      // Granted
//                   } else {
//                       print("Permission to record not granted")
//                       let alert: UIAlertController
//                       
//                    alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
//                       
//                    alert.addAction(UIAlertAction(title: "Setting".localized, style: .default, handler: { (_) in
//                           DispatchQueue.main.async {
//                               
//                               //UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
//                               
//                           }
//                       }))
//                       self.present(alert, animated: true, completion: nil)
//                   }
//               })
//
//    }
//    
//    //Hide/View Balance
//    @IBAction func btnTransmitAction(_ sender: Any) {
//        transmit()
//    }
//    
//    //Hide/View Balance
//    @IBAction func btnReceiveAction(_ sender: Any) {
//        receive()
//    }
//    
//       //MARK: Transmit
//       private func transmit() {
//           
//           contactsTableView.reloadData()
//           
//        let frame_str =  (UserDefaults.standard.value(forKey: "MobileNumber")  as? String ?? "9763548530") + "#" + "Gauri"
//           let data = frame_str.data(using: .utf8)
//           self.tx.send(data)
//       }
//
//
//       //MARK: Receive
//       private func receive() {
//
//           AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
//               if granted {
//                 //  self.contactsView.isHidden = false
//                   if self.rx == nil {
//                       let rxConf: QMReceiverConfig = QMReceiverConfig (key:"ultrasonic")   //ultrasonic-experimental
//                       self.rx = QMFrameReceiver(config: rxConf)
//                   }
//                   self.rx?.setReceiveCallback(self.receiveCallback)
//               } else {
//                   print("Permission to record not granted")
//                   let alert: UIAlertController
//                   
//                alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
//                   
//                alert.addAction(UIAlertAction(title: "Setting".localized, style: .default, handler: { (_) in
//                       DispatchQueue.main.async {
//                           
//                           //UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
//                   
//                       }
//                   }))
//                self.present(alert, animated: true, completion: nil)
//               }
//           })
//         
//       }
//       
//    //MARK: Receive Callback
//       func receiveCallback(frame: Data?) {
//           let msg = String(data: frame ?? Data(), encoding: String.Encoding.utf8) ?? "data could not be decoded"
//           
//               let receiverDetailArray = msg.components(separatedBy: "#")
//               if receiverDetailArray.count > 1 {
//                  print(receiverDetailArray[0])
//                       
//                self.phonenumberArray.append(receiverDetailArray[0])
//                self.contactsTableView.reloadData()
//
//                   }
//       }
//    
//}
//
//extension UltraSonicListVC: UITableViewDataSource,UITableViewDelegate  {
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceTableViewCell
//        cell.serviceNameLabel.text = "\(phonenumberArray[indexPath.row])"
//        
//        return cell
//    }
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//               return 1
//       }
//       
//       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//           
//                   return phonenumberArray.count
//    }
//}
