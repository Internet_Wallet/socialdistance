//
//  CodePRelogin.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

struct CodePreLogin {
    var country = "Myanmar"
    var code    = "+95"
    var flag    = "myanmar"
    
    init(cName: String, codes: String, flag: String) {
        self.country = cName
        self.code = codes
        self.flag  = flag
    }
}

struct NetworkMCCMNC {
    var mnc = ""
    var mcc = ""
    
    init(_ mnc: String, _ mcc: String) {
        self.mnc = mnc
        self.mcc = mcc
    }
}

struct NetworkPreLogin {
    
    var status       = ""
    var networkName  = ""
    var operatorName = ""

    init(status: String, netName: String,opeName: String) {
        self.status       = status
        self.networkName  = netName
        self.operatorName = opeName
     }
}

enum CountryName {
    case Myanmar
    case Other
}

enum countrySelected {
    case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
}
