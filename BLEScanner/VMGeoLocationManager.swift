
//
//  VMGeoLocationManager.swift
//  VMart
//
//  Created by Kethan Kumar on 08/11/18.
//  Copyright © 2018 Kethan Kumar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class VMGeoLocationManager : NSObject, CLLocationManagerDelegate {
    static let shared = VMGeoLocationManager()
    var locationManager: CLLocationManager!
    var geoCoder: CLGeocoder!
    var placeMark: CLPlacemark!
    var currentLatitude: String! = "0.0"
    var currentLongitude: String! = "0.0"
    var currentLocation: CLLocation!
    
    
    override init() {
        super.init()
       // self.setUpLocationManager()
    }
    
    func setUpLocationManager() -> Bool {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.authorizationStatus() == .denied {
            return false
        }
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        startUpdateLocation()
        geteCurrentLocationDetails()
        return true
    }
    
    func startUpdateLocation() {
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdateLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func geteCurrentLocationDetails() {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location
        }
        //lGeoLocation
        if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
            currentLatitude = String(lat)
            currentLongitude = String(long)
        }
    }
    
    
    func ReturnCurrentLocationDetails() ->  CLLocation{
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location
        }
        
        //lGeoLocation
        if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
            currentLatitude = String(lat)
            currentLongitude = String(long)
        }
        
        return currentLocation
    }
    
    
    func ReturnDistanceDetails() ->  Bool{
        
        
        self.setUpLocationManager()
        
        //        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {  currentLocation = locationManager.location  }
        
        
        if let dictValue = UserDefaults.standard.object(forKey: "BussinessDoneCurrentLocation") as? [String: Any] {
            let latitude =  dictValue["lat"] as? NSNumber ?? 0
            let longitude = dictValue["long"] as? NSNumber ?? 0
            let coordinate:CLLocation = CLLocation(latitude: latitude.doubleValue, longitude: longitude.doubleValue)
            let distanceInMeters = coordinate.distance(from: currentLocation)
            if(distanceInMeters <= 160){
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    func getLatLongByName(cityname: String, completionHandler: @escaping (Bool, String?, String?) -> Void) {
        let googleUrlStr = "http://maps.google.com/maps/api/geocode/json?address=\(cityname)&sensor=false"
        guard let escapedUrlStr = googleUrlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
        guard let url = URL.init(string: escapedUrlStr) else { return }
        getGoogleApiResponse(apiUrl: url, compilationHandler: { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                  //  println_debug("error message from google api ::::: \(errorMsg)")
                    print(errorMsg)
                }else{
                  //  println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil, nil)
                return
            }
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String, Any> {
                        if let geometry = dic["geometry"] as? Dictionary<String, Any> {
                            if let location = geometry["location"] as? Dictionary<String, Any> {
                                if let locLat = location["lat"] as? Double, let locLong = location["lng"] as? Double {
                                    completionHandler(true, String(locLat), String(locLong) )
                                    return
                                }
                            }
                        }
                    }
                }
            }
            completionHandler(false, nil, nil)
        })
    }
    
    func getLocationNameByLatLong(lattitude: String, longitude: String, isForCashinCashout : Bool = false, completionHandler: @escaping (Bool, Any?) -> Void) {
        
        var googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\("AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE")"
        
        googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\("AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE")"
        
        guard let url = URL.init(string: googleUrlStr) else { return }
        getGoogleApiResponse(apiUrl: url) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                   // println_debug("error message from google api ::::: \(errorMsg)")
                    print(errorMsg)
                }else{
                   // println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String, Any> {
                        if isForCashinCashout {
                            completionHandler(true, dic["formatted_address"])
                            return
                        }
                        if let typesArr = dic["types"] as? [String] {
                            let itemExists = typesArr.contains(where: {
                                $0.range(of: "neighborhood", options: .caseInsensitive) != nil
                            })
                            if itemExists == true {
                                completionHandler(true, dic["formatted_address"])
                                return
                                /*
                                 if let geometryDic = dic["geometry"] as? Dictionary<String, Any> {
                                 if let locType = geometryDic["location_type"] as? String {
                                 if locType.lowercased() == "geometric_center" {
                                 completionHandler(true, dic["formatted_address"])
                                 return
                                 }
                                 }
                                 }
                                 */
                            }
                            
                        }
                    }
                }
            }
            completionHandler(false, nil)
        }
    }
    
    //language=hi
    func getAddressFrom(lattitude: String, longitude: String,language : String , completionHandler: @escaping (Bool, Any?) -> Void) {
        
//
//        https://maps.googleapis.com/maps/api/geocode/json?latlng=7.00002,93.81108&key=AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE
//
        
        
        
        //   language = "my"
        var googleUrlStr = "" //"https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=\("AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE")"
        
        // googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\("AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE")"
        print("GoogleURL : \(googleUrlStr)")
        guard let url = URL.init(string: googleUrlStr) else { return }
        getGoogleApiResponse(apiUrl: url) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    // println_debug("error message from google api ::::: \(errorMsg)")
                    print(errorMsg)
                }else{
                    //  println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
            var finalDict = Dictionary<String,String>()
            
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String,Any> {
                        
                        
                        if let longNameString = dic["formatted_address"] as? String {
                            finalDict["formatted_address"] = longNameString
                        }
                        
                        
                        if let arr = dic["address_components"] as? Array<NSDictionary> {
                            for item in arr {
                                
                                
                                
                                
                                if let arrString = item.object(forKey: "types") as? Array<String> {
                                    for element in arrString {
                                        if element == "route" {
                                            finalDict["street"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "sublocality_level_1" {
                                            finalDict["township"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "administrative_area_level_1" {
                                            finalDict["region"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "locality" {
                                            finalDict["city"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "country" {
                                            finalDict["Country"] = item.object(forKey: "short_name") as? String
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                completionHandler(true,finalDict)
            }else{
                completionHandler(false, nil)
            }
        }
        
        // https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&language=my&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
    }
    
    
    
    func getCordinateFromPlaceId(placeId: String,language : String , completionHandler: @escaping (Bool, Any?) -> Void) {
        
        
        var googleUrlStr = ""
        googleUrlStr = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&components=country:mm&key=\(Google_API_KEY)"
        print("GoogleURL : \(googleUrlStr)")
        guard let url = URL.init(string: googleUrlStr) else { return }
        getGoogleApiResponse(apiUrl: url) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                   // println_debug("error message from google api ::::: \(errorMsg)")
                    print(errorMsg)
                }else{
                  //  println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
             
            
            if let results = response as? NSDictionary {
                if results.count>0{
                    completionHandler(true,response)
                }
                
            }
            completionHandler(false, nil)
        }
        
        // https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&language=my&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
    }
    
    
    
    func getGoogleApiResponse(apiUrl: URL , compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        let request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return }
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                    // println_debug(json)
                    if (json["status"] as? String)?.lowercased() == "ok" {
                        if let resultArr = json["results"] as? Array<Any> {
                            if resultArr.count > 0 {
//                                if let resultArr = resultArr["geometry"] as? <Any> {
//                                }
                                compilationHandler(true, resultArr)
                            }else {
                                compilationHandler(false, json["error_message"])
                            }
                        }
                        
                        if let resultDict = json["result"] as? NSDictionary {
                            if resultDict.count > 0 {
                                if let object = resultDict["geometry"] as? NSDictionary {
                                    compilationHandler(true, object)
                                }
                                
                            }else {
                                compilationHandler(false, json["error_message"])
                            }
                        }
                        
                    }else{
                        
                        compilationHandler(false, json["error_message"])
                    }
                }
            }catch {
                compilationHandler(false, nil)
            }
        }.resume()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("GeoLocationManager ::: error in get location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let cordinate = manager.location?.coordinate {
            let locValue: CLLocationCoordinate2D = cordinate
            
            // println_debug("GeoLocationManager :: current lattitude \(locValue.latitude) , \(locValue.longitude)")
            
            currentLatitude = String(locValue.latitude)
            currentLongitude = String(locValue.longitude)
            
            //        mapView.centerCoordinate = CLLocationCoordinate2DMake(locValue.latitude, locValue.longitude)
            //
            //        var zoomRect: MKMapRect = MKMapRectNull
            //        for annotation in mapView.annotations {
            //            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            //            //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
            //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.0, 0.0)
            //
            //            zoomRect = MKMapRectUnion(zoomRect, pointRect)
            //
            //        }
            //        mapView.setVisibleMapRect(zoomRect, animated: true)
            //        locationManager.stopUpdatingLocation()
        }
    }
    
}


