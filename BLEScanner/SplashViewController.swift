//
//  SplashViewController.swift
//  BLEScanner
//
//  Created by iMac on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        let time : DispatchTime = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: time) {
            
            self.getInsideApp()
            
        }
        
        
    }
    
    
    func getInsideApp() {
        self.performSegue(withIdentifier: "InitialScreen", sender: self)
    }
    
}
