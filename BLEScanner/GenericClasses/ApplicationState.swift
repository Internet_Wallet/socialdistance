//
//  ApplicationState.swift
//  BLEScanner
//
//  Created by Tushar Lama on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

final class ApplicationState{
    
    // this object will keep bluetooth scanning alive inside application
    var blueToothMainClassObj: BluetoothMainClass!
    
    //this will have all the bluetooth unique data
    var blueToothModelObj = [BluetoothDataModel]()
    
    //this will just keep data for current user
    var currentUserCount = [BluetoothDataModel]()
    
    //this will just keep data for audio QR
    var currentAudioUserCount = [BluetoothDataModel]()
    
    //This will hold the data of all the bluetooth we scan duplicate also
    var copyBlueToothModelObj = [BluetoothDataModel]()
    
    //This will save geo fence and quarantine data inside application
    var geofenceModelObj = [GeofenceDataModel]()
    
   //This will save data for reports
    var previousReportsArray = [BluetoothDataModel]()
    
//    var mobileNumber: String = ""
//    var name: String = ""
//    var qrID: String = ""
//    var UserId: String = ""
    
    // the active user
    var currentUser : ActiveUser!
    
    // the application settings object which will help to save all the data
    fileprivate var appSettings : AppSettings = AppSettings();
    
    //helper to save custom object
    fileprivate var coreDataObject : CoreDataClass = CoreDataClass();
    
    //marking it singleton
    public static let sharedAppState : ApplicationState = {
        let instance = ApplicationState();
        return instance;
    }()
    
    
    private init() {
     // create a default guest user
      currentUser = ActiveUser();
      blueToothMainClassObj = BluetoothMainClass()
      // load in the app settings
      // takes place after default user creation in case we are keeping login from previous run
      loadCurrentUserData()
    }
    
    func logoutUser(){
        currentUser.logout()
    }
    
}


extension ApplicationState {
    
    internal func loadCurrentUserData(){
        appSettings.loadSettings()
        // update the active user with needed settings
        currentUser.loadFromSettings(appSettings: appSettings)
    }
    
    
    internal func refreshIgnoredUser(){
        //Made this code because when the user will login with A date so when the a new day comes i am refreshing all the ignored user

       // if ApplicationState.sharedAppState.blueToothModelObj.count>0{

            let hasDate = ApplicationState.sharedAppState.currentUser.refreshUserDate
            if hasDate.components(separatedBy: " ")[0] != Utility.getDateForCoreData().components(separatedBy: " ")[0]{
                 ApplicationState.sharedAppState.currentUser.counting = 1
                //saving the previous date data in dp
               // ApplicationState.sharedAppState.saveAppSettings()
                //removing all the data
                for i in 0..<ApplicationState.sharedAppState.blueToothModelObj.count{
                    ApplicationState.sharedAppState.previousReportsArray.append(ApplicationState.sharedAppState.blueToothModelObj[i])
                }
                                
                ApplicationState.sharedAppState.blueToothModelObj.removeAll()
                ApplicationState.sharedAppState.copyBlueToothModelObj.removeAll()
                ApplicationState.sharedAppState.currentUserCount.removeAll()
                ApplicationState.sharedAppState.currentAudioUserCount.removeAll()
                ApplicationState.sharedAppState.currentUser.bluetoothScantime =  Utility.getDateForCoreData()
                ApplicationState.sharedAppState.currentUser.FlatSoundPlay = "0"
                ApplicationState.sharedAppState.currentUser.refreshUserDate = Utility.getDateForCoreData()
               
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)
                
            }
        
    }
    
    func saveCurrentUserData(){
        // get the user settings
        appSettings = currentUser.saveToSettings(appSettings: appSettings)
        appSettings.saveSettings()
    }
        
    internal func loadAppSettings() {
        coreDataObject.retreiveData()
    }
    
    
    internal func saveAppSettings() {
        coreDataObject.saveDataToCoreData()
    }
    
    internal func deleteAppSettings(entity: String) {
        coreDataObject.deleteAllData(entity)
    }
    
}
