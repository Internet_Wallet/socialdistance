////
////  BluetoothViewController.swift
////  BLEScanner
////
////  Created by vamsi on 4/5/20.
////  Copyright © 2020 GG. All rights reserved.
////
//
//import UIKit
//import CoreBluetooth
//
//struct DisplayPeripheral: Hashable {
//    let peripheral: CBPeripheral
//    let lastRSSI: NSNumber
//    let isConnectable: Bool
//
//    var hashValue: Int { return peripheral.hashValue }
//
//    static func ==(lhs: DisplayPeripheral, rhs: DisplayPeripheral) -> Bool {
//        return lhs.peripheral == rhs.peripheral
//    }
//}
//
//
//class BluetoothViewController: UIViewController {
//    private var centralManager: CBCentralManager!
//    private var peripherals = Set<DisplayPeripheral>()
//    private var viewReloadTimer: Timer?
//    
//    private var selectedPeripheral: CBPeripheral?
//    override func viewDidLoad() {
//        super.viewDidLoad()
//  
//        // Do any additional setup after loading the view.
//    }
//
//    func startScanning() {
//        DispatchQueue.main.async {
//            self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
//        }
//        
//        peripherals = []
//        peripherals.removeAll()
//        self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
//            guard let strongSelf = self else { return }
//            if strongSelf.centralManager!.isScanning {
//                strongSelf.centralManager?.stopScan()
//                ApplicationState.sharedAppState.saveAppSettings()
//                //self?.startScanning()
//            }
//        }
//    }
//  
//}
//
//
//extension BluetoothViewController: CBCentralManagerDelegate{
//    func centralManagerDidUpdateState(_ central: CBCentralManager){
//        if (central.state == .poweredOn){
//            startScanning()
//        }else{
//        }
//    }
//    
//    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
//        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
//
//        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
//                   peripherals.insert(displayPeripheral)
//        var localBool = 0
//        
//        print(displayPeripheral)
//        
//        if Int(truncating: RSSI) > -75 {
//            localBool = 6
//        }else{
//            localBool = 7
//        }
//        
//        ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: localBool))
//        
//
////        if ApplicationState.sharedAppState.blueToothModelObj.count>0{
////            if !ApplicationState.sharedAppState.blueToothModelObj.contains(where: {($0.bluetoothId == peripheral.identifier.uuidString)}){
////                ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: localBool))
////            }
////        }else{
////            ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: localBool))
////        }
//    }
//}
//
//extension BluetoothViewController: CBPeripheralDelegate {
//    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
//        
//        print("Did fail to Connect")
//    }
//    
//    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
////        connectingViewController?.dismiss(animated: true, completion: {
////            print("Peripheral connected")
////            self.performSegue(withIdentifier: "PeripheralConnectedSegue", sender: self)
////            peripheral.discoverServices(nil)
////        })
//    }
//}
