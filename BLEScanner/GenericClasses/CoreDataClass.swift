//
//  CoreDataClass.swift
//  BLEScanner
//
//  Created by Tushar Lama on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class CoreDataClass: NSObject {
    let  persistentContainerQueue : OperationQueue = {
        let queue = OperationQueue.init();
        queue.maxConcurrentOperationCount = 1;
        return queue;
    }()
    
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    func saveDataToCoreData(){
        saveBluetoothModel()
        let GeofenceDataModelEntity = NSEntityDescription.entity(forEntityName: "GeofenceModel", in: ((appDelegate?.persistentContainer.viewContext)!))!
        let GeofenceDataModelObj = NSManagedObject(entity: GeofenceDataModelEntity, insertInto: appDelegate?.persistentContainer.viewContext)
        let GeofenceDataModelvalueNew = GeofenceDataModels(obj: ApplicationState.sharedAppState.geofenceModelObj)
        GeofenceDataModelObj.setValue(GeofenceDataModelvalueNew, forKey: "geofenceData")

        do{
            try appDelegate?.persistentContainer.viewContext.save()
        }catch let error as NSError{
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    //saving and updating bluetooth details here
    func saveBluetoothModel(){
        persistentContainerQueue.addOperation {
            let context = self.appDelegate?.persistentContainer.newBackgroundContext()
            context?.performAndWait({
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BlueToothModel")
                do{
                    let result = try context?.fetch(fetchRequest)
                    //for new user
                    if let value = result{
                        if value.count == 0 {
                            let previousDateRecord = ApplicationState.sharedAppState.previousReportsArray
                            let todayData = ApplicationState.sharedAppState.blueToothModelObj.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]})
                            let newValue = previousDateRecord + todayData
                            if let viewContext = context{
                                let blueToothEntity = NSEntityDescription.entity(forEntityName: "BlueToothModel", in: viewContext)
                                if let validEntity = blueToothEntity{
                                    let bluetoothObj = NSManagedObject(entity: validEntity, insertInto: viewContext)
                                    bluetoothObj.setValue(newValue, forKey: "bluetoothData")
                                    do{
                                        try viewContext.save()
                                    }catch let error as NSError{
                                        print("Could not save to core data \(error), \(error.userInfo)")
                                    }
                                }
                            }
                        }else{
                            if value.count>0{
                                for data in value as! [NSManagedObject]{
                                    do {
                                        let previousDateRecord = ApplicationState.sharedAppState.previousReportsArray
                                        let todayData = ApplicationState.sharedAppState.blueToothModelObj.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]})
                                        let newValue = previousDateRecord + todayData
                                        //this will add to core data
                                        
                                        data.setValue(newValue, forKey: "bluetoothData")
                                        try context?.save()
                                        print("update successfull")
                                        
                                    }catch let error as NSError {
                                        print("Could not Update. \(error), \(error.userInfo)")
                                    }
                                    break
                                }
                            }
                            
                        }

                    }
                } catch{
                    print("Failed")
                }
            })
        }
    }


func retreiveData(){
    persistentContainerQueue.addOperation {
        let context = self.appDelegate?.persistentContainer.newBackgroundContext()
        context?.performAndWait({
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BlueToothModel")
            let geofenceRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "GeofenceModel")
            do{
                let result = try context?.fetch(fetchRequest)
                let geofenceresult = try context?.fetch(geofenceRequest)
                
                if let results = result{
                    if results.count>0{
                        for data in results as! [NSManagedObject]{
                            let newarray = data.value(forKey: "bluetoothData") as? [BluetoothDataModel]
                            if let valueObj = newarray{
                                if valueObj.count>0{
                                    //this will fetch data for only current date
                                    ApplicationState.sharedAppState.blueToothModelObj.removeAll()
                                    ApplicationState.sharedAppState.blueToothModelObj = valueObj.filter({$0.bluetoothDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]})
                                    
                                    //this will fetch data for other dates for reports
                                    ApplicationState.sharedAppState.previousReportsArray.removeAll()
                                    ApplicationState.sharedAppState.previousReportsArray = valueObj.filter({$0.bluetoothDate.components(separatedBy: " ")[0] != Utility.getDateForCoreData().components(separatedBy: " ")[0]})
                                }
                            }
                            break
                        }
                    }
                }
                
                for data in geofenceresult as! [NSManagedObject]{
                    let geofencenewarray = data.value(forKey: "geofenceData") as! GeofenceDataModels
                    ApplicationState.sharedAppState.geofenceModelObj = geofencenewarray.obj
                }
            } catch{
                print("Failed")
            }
        })
    }
}


    func deleteAllData(_ entity:String){
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        // guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext: NSManagedObjectContext = (self.appDelegate?.persistentContainer.viewContext)!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.includesPropertyValues = false
        do {
            let results = (try managedContext.fetch(fetchRequest))
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
            try managedContext.save()
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    
}
