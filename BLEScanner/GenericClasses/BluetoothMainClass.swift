//
//  BluetoothMainClass.swift
//  BLEScanner
//
//  Created by Tushar Lama on 03/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import AVFoundation
import AudioToolbox.AudioServices
import AudioToolbox

struct DisplayPeripheral: Hashable {
    let peripheral: CBPeripheral
    let lastRSSI: NSNumber
    let isConnectable: Bool
    
    var hashValue: Int { return peripheral.hashValue }
    
    static func ==(lhs: DisplayPeripheral, rhs: DisplayPeripheral) -> Bool {
        return lhs.peripheral == rhs.peripheral
    }
}

class BluetoothMainClass: NSObject,AVAudioPlayerDelegate{
    var copyBlueToothModelObjNew = [BluetoothDataModel]()
    private var centralManager: CBCentralManager!
    private var peripherals = Set<DisplayPeripheral>()
    private var viewReloadTimer: Timer?
    lazy var downloadsInProgress: [IndexPath: Operation] = [:]
    
    //this variable will be used to hold sound and only play once the the previous sound has completed
    var isPlayingSound = false
    
    //this variable will be used to compare date to get time of each user and to update the latest time of the current peripal
    var currentDateToCompare = ""
    
    //this varaiblw will be used to compare bluetooth
    var currentBluetooth = ""
    
    //this vaiable will update on time comparission
    var feet = 0
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    var player: AVAudioPlayer = AVAudioPlayer()
    private var selectedPeripheral: CBPeripheral?
    
    //this will hold all rssi value rssi value is used to calculate meter
    var rssiID = ""
    
    //this will be used to manage the bluetooth scanning
    var isSartedCalled = false
    
    //this will be used to play notification
    var willPlayNotification = false
    
    public override init() {
        super.init()
        //starting blue tooth engine
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
        
    }
    
    func startScanning() {
        if !isSartedCalled {
            peripherals = []
            peripherals.removeAll()
            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
            //30
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 30) { [weak self] in
                guard let strongSelf = self else { return }
                if !strongSelf.centralManager!.isScanning {
                    //strongSelf.centralManager?.stopScan()
                    strongSelf.startScanning()
                    
                }
            }
            
            //20
            DispatchQueue.main.asyncAfter(deadline: .now() + 20) { [weak self] in
                guard let strongSelf = self else { return }
                if strongSelf.centralManager!.isScanning {
                    ApplicationState.sharedAppState.currentUserCount.removeAll()
                    strongSelf.willPlayNotification = false
                    strongSelf.centralManager?.stopScan()
                    //strongSelf.startScanning()
                }
            }
        }
    }
    
    
    func stop(){
        isSartedCalled = true
        centralManager.stopScan()
    }
    
    
    func playSound(tune: String) {
        do{
            let audioPath = Bundle.main.path(forResource: tune, ofType: "mp3")
            player = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioPath!))
            player.prepareToPlay()
            ApplicationState.sharedAppState.currentUser.counting = 1 + ApplicationState.sharedAppState.currentUser.counting
            player.volume = 7.0
            player.play()
            isPlayingSound = true
            player.delegate = self
        }
        catch{
            print(error)
        }
    }
    
    
    
    //play sound
    func playSoundWhenUSerNear(name: String,id: String,category: String,feet: Int){
        willPlayNotification = false
        if category == CustomNotification.DefaultValue.description  {
            if feet == 6{
                if Utility.isNotificationTimeActive(){
                    var showAlert = 0
                    let hasDate = ApplicationState.sharedAppState.currentUser.LoginDate
                    //logged in today for the first time only alert will come for 4 time
                    if hasDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]{
                        showAlert = 3
                    }else{
                        //once logged in then alert will come ie sound for 2 time after that notification
                        showAlert = 1
                    }
                    
                    //for everytwo hour one time sound will come
                    if Utility.getTimeDifferenc(timeOne: ApplicationState.sharedAppState.currentUser.bluetoothScantime, timeTwo: Utility.getDateForCoreData()) >= 120.0{
                        ApplicationState.sharedAppState.currentUser.bluetoothScantime = Utility.getDateForCoreData()
                        if !isPlayingSound{
                            //if at home no notification
                            
                            if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                                self.playSound(tune: "bluetooth_receiver")
                                self.appDelegate?.scheduleNotification(notificationType: NotificationType.Bluetooth.description,title:"BlueTooth Device is near to you",body: id, willVibrate: false)
                            }
                        }
                    } else{
                        if ApplicationState.sharedAppState.currentUser.counting <= showAlert{
                            if !isPlayingSound{
                                //if at home no notification
                                if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                                    self.playSound(tune: "bluetooth_receiver")
                                    self.appDelegate?.scheduleNotification(notificationType: NotificationType.Bluetooth.description,title:"BlueTooth Device is near to you",body: id, willVibrate: false)
                                }
                            }
                        }else{
                            if !isPlayingSound{
                                //  willPlayNotification = false
                                //if the active user at home no notification
                                if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                                    if #available(iOS 13.0, *) {
                                    }else{
                                        self.playSound(tune: "iphone_notification")
                                    }
                                    self.appDelegate?.scheduleNotification(notificationType: NotificationType.Bluetooth.description,title:"BlueTooth Device is near to you",body: id, willVibrate: true)
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    //managing the sound here
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        isPlayingSound = false
    }
    
    
    
}

extension BluetoothMainClass: CBCentralManagerDelegate{
    func centralManagerDidUpdateState(_ central: CBCentralManager){
        print(central.state)
        switch (central.state) {
        case CBManagerState.poweredOff:
            // centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
            print("CoreBluetooth BLE hardware is powered off")
            //  fireAlert(isPhoneBluetoothOff: true)
            //  fireAlert()
            break;
        case CBManagerState.unauthorized:
            //this will be called when phone bluetooth is on but application bluetooth is off
            centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
            fireAlert(isPhoneBluetoothOff: false)
            print("CoreBluetooth BLE state is unauthorized")
            break
            
        case CBManagerState.unknown:
            print("CoreBluetooth BLE state is unknown");
            break
            
        case CBManagerState.poweredOn:
            print("CoreBluetooth BLE hardware is powered on and ready")
            // bluetoothAvailable = true;
            break;
            
        case CBManagerState.resetting:
            print("CoreBluetooth BLE hardware is resetting")
            break;
        case CBManagerState.unsupported:
            print("CoreBluetooth BLE hardware is unsupported on this platform");
            break
        @unknown default:
            break
        }
    }
    
    func fireAlert(isPhoneBluetoothOff: Bool){
        let alert = UIAlertController(title: "BlueTooth Permission".localized, message: "Please Turn On Your Phone BlueTooth  to access the features of this app As this will help to scan people around you".localized, preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Yes".localized, style: .default, handler: { action in
            
            DispatchQueue.main.async {
                //move to app setting
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                
            }
        })
        
        alert.addAction(actionYes)
        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
        
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
        let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
        peripherals.insert(displayPeripheral)
        
        var localBool = 0
        let valueID = Int(truncating: RSSI)
        rssiID = String(valueID)
        
        //checking bluetooth less than 3 feet
        if Int(truncating: RSSI) > -65 {
            //assign 6 just for my reference to compare in below code for 3 feet check
            localBool = 6
        }else{
            //assign 7 just for my reference to compare in below code for 3 feet check
            localBool = 7
        }
        
        //adding data here to keep in record
        ApplicationState.sharedAppState.copyBlueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: localBool,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
        
        //getting the first date of the repeated content
        currentDateToCompare = ApplicationState.sharedAppState.copyBlueToothModelObj.first(where:{$0.bluetoothId == peripheral.identifier.uuidString})?.bluetoothDate ?? ""
        
        currentBluetooth = ApplicationState.sharedAppState.copyBlueToothModelObj.first(where:{$0.bluetoothId == peripheral.identifier.uuidString})?.bluetoothId ?? ""
        
        if currentDateToCompare.count>0{
            var locaBoolToCheck = false
                
                if ApplicationState.sharedAppState.blueToothModelObj.count>0{
                    let originalBluetoothObj = ApplicationState.sharedAppState.blueToothModelObj
                    let currentBluetoothObj = ApplicationState.sharedAppState.currentUserCount
                    let blueToothToCheck = currentBluetooth
                    let index = originalBluetoothObj.indices.filter({ originalBluetoothObj[$0].bluetoothId ==  blueToothToCheck})
                    let currentUserIndex = currentBluetoothObj.indices.filter({ currentBluetoothObj[$0].bluetoothId ==  blueToothToCheck})
                    
                    if index.count>0{
                        if originalBluetoothObj.indices.contains(index[0]){
                            locaBoolToCheck = true
                            let type = ApplicationState.sharedAppState.blueToothModelObj[index[0]].userCategory
                            let rssi = ApplicationState.sharedAppState.blueToothModelObj[index[0]].rssiID
                            let count = ApplicationState.sharedAppState.blueToothModelObj[index[0]].numberOfTimePeopleMeet
                            let number = ApplicationState.sharedAppState.blueToothModelObj[index[0]].mobileNumber
                            let name = ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothName
                            
                            if localBool == 6{
                                //adding current User
                                if  currentUserIndex.count == 0{
                                    ApplicationState.sharedAppState.currentUserCount.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)
                                }
                                
                                //checking if array contain index
                                if ApplicationState.sharedAppState.blueToothModelObj.indices.contains(index[0]){
                                    
                                    if Utility.getTimeDifferenc(timeOne: ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothDate, timeTwo:  Utility.getDateForCoreData()) >= 2.0{
                                        let peopleMeetMore = Utility.getTimeDifferenc(timeOne: currentDateToCompare, timeTwo:  Utility.getDateForCoreData())
                                        
                                        let newCount = (Int(count) ?? 0) + 1
                                        ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
                                        ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: name, id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 2,peopleMeet:Int(peopleMeetMore),userCategory: type,numberOfTimePeopleMeet: String(newCount),rssiID: rssi,mobileNumber: number))
                                        
                                        if ((newCount <= 5)  && type == CustomNotification.DefaultValue.description ){
                                            if !willPlayNotification{
                                                willPlayNotification = true
                                                playSoundWhenUSerNear(name: peripheral.name ?? "No Name",id: blueToothToCheck,category: type, feet: localBool)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        locaBoolToCheck = false
                    }
                    
                    if !locaBoolToCheck{
                        //only append record if less than 6 feet
                        if localBool == 6{
                                ApplicationState.sharedAppState.currentUserCount.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
                            
                                ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)
                                
                                if !willPlayNotification{
                                    willPlayNotification = true
                                    playSoundWhenUSerNear(name: peripheral.name ?? "No Name",id: peripheral.identifier.uuidString,category: CustomNotification.DefaultValue.description, feet: localBool)
                                }
                        }
                    }
                }else{
                    //this will work for the first time
                    
                    //only append record if less than 6 feet
                    if localBool == 6{
                            ApplicationState.sharedAppState.currentUserCount.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
                     //   }
                        
                        ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: peripheral.name ?? "No Name", id: peripheral.identifier.uuidString,bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "1",rssiID: rssiID,mobileNumber: ""))
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)

                        
                        if !willPlayNotification{
                            willPlayNotification = true
                            playSoundWhenUSerNear(name: peripheral.name ?? "No Name",id: peripheral.identifier.uuidString,category: CustomNotification.DefaultValue.description, feet: localBool)
                        }
                    }
            }
        }
    }
}

extension BluetoothMainClass: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        print("Did fail to Connect")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}



///ABOVE TODAY
