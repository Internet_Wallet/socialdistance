//
//  ApplicationSettings.swift
//  BLEScanner
//
//  Created by Tushar Lama on 12/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

protocol AppSettable
{
    func loadFromSettings(appSettings:AppSettings)
    func saveToSettings(appSettings:AppSettings) -> AppSettings
}

struct AppSettings
{
    let userDefaults : UserDefaults = UserDefaults.standard

    enum SettingItems : String
    {
        case
        isQuarintine = "isQuarintine",
        counting = "counting",
        timeStamp5Mint = "timeStamp5Mint",
        generateButtonShow = "generateButtonShow",
        qrID = "qrID",
        categoryID = "categoryID",
        notification = "notification",
        QR_CODE_GENERATED = "QR_CODE_GENERATED",
        QR_CODE_GENERATED_STR = "QR_CODE_GENERATED_STR",
        currentLanguage = "currentLanguage",
        currentFont = "appFont",
        MobileNumber = "MobileNumber",
        APNSToken = "APNSToken",
        name = "name",
        OTP = "OTP",
        UltraSoundId = "UltraSoundId",
        GoogleToken = "GoogleToken",
        Token = "Token",
        BluetoothID = "BluetoothID",
        LoginDate = "LoginDate",
        FlatSoundPlay = "FlatSoundPlay",
        userID = "userID",
        minRssi = "minRssi",
        maxRssi = "maxRssi",
        scanningTime =  "scanningTime",
        nextNotification = "nextNotification",
        ultraTimeAlertVaue = "ultraTimeAlertVaue",
        notificationAutoIgnore = "notificationAutoIgnore",
        notificationVoiceCount = "notificationVoiceCount",
        isAtHome = "isAtHome",
        isAtOffice = "isAtOffice",
        lastSynTime = "lastSynTime",
        refreshUserDate = "refreshUserDate",
        lastDateSynced = "lastDateSynced",
        deviceTimeAlert = "deviceTimeAlert",
        deviceVoiceAlertMax = "deviceVoiceAlertMax",
        syncDataTime = "syncDataTime",
        mobileNumberToDisplay = "mobileNumberToDisplay",
        bluetoothScantime = "bluetoothScantime"
        
        
        
        static func getKeys() -> [String] {
            return [isQuarintine.rawValue, counting.rawValue, timeStamp5Mint.rawValue, generateButtonShow.rawValue, qrID.rawValue, categoryID.rawValue, notification.rawValue,QR_CODE_GENERATED.rawValue,QR_CODE_GENERATED_STR.rawValue,currentFont.rawValue,MobileNumber.rawValue,APNSToken.rawValue,name.rawValue,OTP.rawValue,UltraSoundId.rawValue,GoogleToken.rawValue,BluetoothID.rawValue,LoginDate.rawValue,FlatSoundPlay.rawValue,userID.rawValue,minRssi.rawValue,scanningTime.rawValue,nextNotification.rawValue,ultraTimeAlertVaue.rawValue,notificationAutoIgnore.rawValue,notificationVoiceCount.rawValue,isAtHome.rawValue,isAtOffice.rawValue,lastSynTime.rawValue,categoryID.rawValue, notification.rawValue, QR_CODE_GENERATED.rawValue,QR_CODE_GENERATED_STR.rawValue,currentLanguage.rawValue,MobileNumber.rawValue,name.rawValue,BluetoothID.rawValue,LoginDate.rawValue,FlatSoundPlay.rawValue,userID.rawValue,minRssi.rawValue,scanningTime.rawValue,nextNotification.rawValue,ultraTimeAlertVaue.rawValue,notificationAutoIgnore.rawValue,notificationVoiceCount.rawValue,isAtHome.rawValue,lastSynTime.rawValue,refreshUserDate.rawValue,lastDateSynced.rawValue,deviceTimeAlert.rawValue,deviceVoiceAlertMax.rawValue,bluetoothScantime.rawValue,syncDataTime.rawValue,mobileNumberToDisplay.rawValue]
 
        }
    }
    
    
    
    
    fileprivate var cachedSettings : [String : Any] = [String : Any]()
    
    mutating func loadSettings()
    {
        let keys : [String] = SettingItems.getKeys()
        keys.forEach({
            if let value = userDefaults.object(forKey: $0) {
                cachedSettings[$0] = value
            }
        })
    }
    
    func saveSettings(){
        let keys : [String] = Array(cachedSettings.keys)
        keys.forEach({
            userDefaults.set(cachedSettings[$0], forKey: $0)
        })
    }
    
    func getSettingStringFor(key:SettingItems) -> String?
    {
        return cachedSettings[key.rawValue] as? String
    }
    
    func getSettingFor(key:SettingItems) -> Any?
    {
        return cachedSettings[key.rawValue]
    }
    
    mutating func updateSettingFor(key:SettingItems, value:Any)
    {
        cachedSettings[key.rawValue] = value
    }
}

