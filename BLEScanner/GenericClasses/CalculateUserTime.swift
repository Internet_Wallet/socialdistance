//
//  CalculateUserTime.swift
//  BLEScanner
//
//  Created by Tushar Lama on 04/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation


struct CalculateUserTime {
    
    static func calculateUser() -> ([Int],[Int],[Int],[Int]){
        
        let valueObj = ApplicationState.sharedAppState.blueToothModelObj
        var stringArray = [String]()
        var lessThanSixFeetMoreThanTwoMin = [Int]()
        var lessThanSixFeetLessThanTwoMin = [Int]()
        var moreThanSixFeetMoreThanTwoMin = [Int]()
        var moreThanSixFeetLessThanTwoMin = [Int]()
        
        for i in 0..<valueObj.count{
            stringArray.append("\(valueObj[i].bluetoothId)")
        }
        
        let adasdasda =  Array(Set(stringArray))
        stringArray.removeAll()
        stringArray = adasdasda
        var blue = [BluetoothDataModel]()
       
        for i in 0..<stringArray.count{
            blue.removeAll()
            for k in 0..<valueObj.count{
                if stringArray[i] == valueObj[k].bluetoothId{
                    blue.append(valueObj[k])
                }
            }
            
            //All under six Feet
            if blue[0].bluetoothDateLessThanSixFeet == 6{
                if Utility.getTimeDifferenc(timeOne: blue[blue.count - 1].bluetoothDate, timeTwo:  blue[0].bluetoothDate) < 1.9{
                    lessThanSixFeetLessThanTwoMin.append(0)
                }else{
                    lessThanSixFeetMoreThanTwoMin.append(0)
                }
            }else{
                //All above six feet
                if Utility.getTimeDifferenc(timeOne: blue[blue.count - 1].bluetoothDate, timeTwo:  blue[0].bluetoothDate) < 1.9{
                    moreThanSixFeetLessThanTwoMin.append(0)
                }else{
                    moreThanSixFeetMoreThanTwoMin.append(0)
                }
            }
        }
        
        return (lessThanSixFeetLessThanTwoMin,lessThanSixFeetMoreThanTwoMin,moreThanSixFeetLessThanTwoMin,moreThanSixFeetMoreThanTwoMin)
    }
}
