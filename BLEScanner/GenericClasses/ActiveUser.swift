//
//  ActiveUser.swift
//  BLEScanner
//
//  Created by Tushar Lama on 12/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class ActiveUser
{
    var isAtOffice: Bool = false
    var isAtHome: Bool = false
    var isQuarintine: Bool = false
    var counting: Int = 1
    var generateButtonShow: Bool = false
    var QR_CODE_GENERATED: Bool = false
    var timeStamp5Mint: String = ""
    var qrID: String = ""
    var categoryID: String = ""
    var notification: String = ""
    var QR_CODE_GENERATED_STR: String = ""
    var currentLanguage: String = "my"
    var currentFont: String = "Zawgyi-One"
    var MobileNumber: String = ""
    var APNSToken: String = ""
    var name: String = ""
    var BluetoothID: String = ""
    var LoginDate: String = ""
    var FlatSoundPlay: String = ""
    var userID: String = ""
    var OTP: String = ""
    var UltraSoundId: String = ""
    var GoogleToken: String = ""
    var ultraTimeAlertVaue: String = ""
    var notificationVoiceCount: String = ""
    var notificationAutoIgnore: String = ""
    var lastSynTime: String = ""
    var lastDateSynced: String = ""
    var deviceTimeAlert: String = ""
    var deviceVoiceAlertMax: String = ""
    //this will help to play sound after every two hour once for new device
    var bluetoothScantime: String = ""
    
    var dataSynFrom: String = ""
    var dataSynTo: String = ""
    var nightModeStart: String = ""
    var nightModeEnd: String = ""
    var refreshUserDate: String = ""
    var minRssi : String = ""
    var maxRssi : String = ""
    var scanningTime : String = ""
    var nextNotification: String = ""
    
    //this will be used to sync data after every 6 hour
    var syncDataTime = ""
    
    var mobileNumberToDisplay: String = ""
  
    
    
    // the application settings object
       fileprivate var appSettings : AppSettings = AppSettings();
    
    init() {
        
    }
    
}

extension ActiveUser{
    
    public func logout(){
        isQuarintine = false
        counting = 1
        generateButtonShow = false
        QR_CODE_GENERATED = false
        timeStamp5Mint = ""
        qrID = ""
        bluetoothScantime = ""
        categoryID = ""
        notification = "sdasdasda"
        QR_CODE_GENERATED_STR = ""
        //currentLanguage = ""
        MobileNumber = ""
        APNSToken = ""
        name = ""
        OTP = ""
        UltraSoundId = ""
        GoogleToken = ""
        BluetoothID = ""
        LoginDate = ""
        FlatSoundPlay = ""
        userID = ""
        ultraTimeAlertVaue = ""
        notificationVoiceCount = ""
        notificationAutoIgnore = ""
        lastDateSynced = ""
        isAtHome = false
        isAtOffice = false
        lastSynTime = ""
        refreshUserDate = ""
        deviceTimeAlert = ""
        deviceVoiceAlertMax = ""
        syncDataTime = ""
        mobileNumberToDisplay = ""
        ApplicationState.sharedAppState.currentUserCount.removeAll()
        ApplicationState.sharedAppState.currentAudioUserCount.removeAll()
        ApplicationState.sharedAppState.blueToothModelObj.removeAll()
        ApplicationState.sharedAppState.previousReportsArray.removeAll()
        ApplicationState.sharedAppState.geofenceModelObj.removeAll()
        ApplicationState.sharedAppState.copyBlueToothModelObj.removeAll()
        ApplicationState.sharedAppState.blueToothMainClassObj.stop()
        ApplicationState.sharedAppState.deleteAppSettings(entity: "BlueToothModel")
        ApplicationState.sharedAppState.deleteAppSettings(entity: "GeofenceModel")
        appDel.saveContext()
              
    }
}



//loading and fetching data of current logged in user
extension ActiveUser : AppSettable
{
    func loadFromSettings(appSettings:AppSettings)
    {
        
        //loading from setting only String
        timeStamp5Mint = appSettings.getSettingStringFor(key: .timeStamp5Mint) ?? ""
        qrID = appSettings.getSettingStringFor(key: .qrID) ?? ""
        categoryID = appSettings.getSettingStringFor(key: .categoryID) ?? ""
        notification = appSettings.getSettingStringFor(key: .notification) ?? ""
        QR_CODE_GENERATED_STR = appSettings.getSettingStringFor(key: .QR_CODE_GENERATED_STR) ?? ""
        currentLanguage = appSettings.getSettingStringFor(key: .currentLanguage) ?? "my"
        currentFont = appSettings.getSettingStringFor(key: .currentFont) ?? "Zawgyi-One"
        MobileNumber = appSettings.getSettingStringFor(key: .MobileNumber) ?? ""
        APNSToken = appSettings.getSettingStringFor(key: .APNSToken) ?? ""
        name = appSettings.getSettingStringFor(key: .name) ?? ""
         OTP = appSettings.getSettingStringFor(key: .OTP) ?? ""
         UltraSoundId = appSettings.getSettingStringFor(key: .UltraSoundId) ?? ""
         GoogleToken = appSettings.getSettingStringFor(key: .GoogleToken) ?? ""
        BluetoothID = appSettings.getSettingStringFor(key: .BluetoothID) ?? ""
        LoginDate = appSettings.getSettingStringFor(key: .LoginDate) ?? ""
        FlatSoundPlay = appSettings.getSettingStringFor(key: .FlatSoundPlay) ?? ""
        userID = appSettings.getSettingStringFor(key: .userID) ?? ""
        ultraTimeAlertVaue = appSettings.getSettingStringFor(key: .ultraTimeAlertVaue) ?? ""
        notificationAutoIgnore = appSettings.getSettingStringFor(key: .notificationAutoIgnore) ?? ""
        notificationVoiceCount = appSettings.getSettingStringFor(key: .notificationVoiceCount) ?? ""
        lastSynTime = appSettings.getSettingStringFor(key: .lastSynTime) ?? ""
        refreshUserDate = appSettings.getSettingStringFor(key: .refreshUserDate) ?? ""
        lastDateSynced = appSettings.getSettingStringFor(key: .lastDateSynced) ?? ""
        deviceVoiceAlertMax = appSettings.getSettingStringFor(key: .deviceVoiceAlertMax) ?? ""
        deviceTimeAlert = appSettings.getSettingStringFor(key: .deviceTimeAlert) ?? ""
        minRssi = appSettings.getSettingStringFor(key: .minRssi) ?? ""
        maxRssi = appSettings.getSettingStringFor(key: .maxRssi) ?? ""
        scanningTime = appSettings.getSettingStringFor(key: .scanningTime) ?? ""
        nextNotification = appSettings.getSettingStringFor(key: .nextNotification) ?? ""
        bluetoothScantime = appSettings.getSettingStringFor(key: .bluetoothScantime) ?? ""
        syncDataTime = appSettings.getSettingStringFor(key: .syncDataTime) ?? ""
        mobileNumberToDisplay = appSettings.getSettingStringFor(key: .mobileNumberToDisplay) ?? ""
         
        //loading from variable of different data type
        isQuarintine = appSettings.getSettingFor(key: .isQuarintine) as? Bool ?? false
        QR_CODE_GENERATED = appSettings.getSettingFor(key: .QR_CODE_GENERATED) as? Bool ?? false
        generateButtonShow = appSettings.getSettingFor(key: .generateButtonShow) as? Bool ?? false
        isAtHome = appSettings.getSettingFor(key: .isAtHome) as? Bool ?? false
        counting = appSettings.getSettingFor(key: .counting) as? Int ?? 1
        isAtOffice  = appSettings.getSettingFor(key: .isAtOffice) as? Bool ?? false
    }
    
    func saveToSettings(appSettings:AppSettings) -> AppSettings{
        var appSettings : AppSettings = appSettings
        
        // save the token
        appSettings.updateSettingFor(key: .mobileNumberToDisplay, value: mobileNumberToDisplay)
        appSettings.updateSettingFor(key: .isQuarintine, value: isQuarintine)
        appSettings.updateSettingFor(key: .counting, value: counting)
        appSettings.updateSettingFor(key: .generateButtonShow, value: generateButtonShow)
        appSettings.updateSettingFor(key: .QR_CODE_GENERATED, value: QR_CODE_GENERATED)
        appSettings.updateSettingFor(key: .timeStamp5Mint, value: timeStamp5Mint)
        appSettings.updateSettingFor(key: .qrID, value: qrID)
        appSettings.updateSettingFor(key: .categoryID, value: categoryID)
        appSettings.updateSettingFor(key: .notification, value: notification)
        appSettings.updateSettingFor(key: .QR_CODE_GENERATED_STR, value: QR_CODE_GENERATED_STR)
        appSettings.updateSettingFor(key: .MobileNumber, value: MobileNumber)
        appSettings.updateSettingFor(key: .APNSToken, value: APNSToken)
        appSettings.updateSettingFor(key: .name, value: name)
        appSettings.updateSettingFor(key: .BluetoothID, value: BluetoothID)
        appSettings.updateSettingFor(key: .currentLanguage, value: currentLanguage)
        appSettings.updateSettingFor(key: .LoginDate, value: LoginDate)
        appSettings.updateSettingFor(key: .FlatSoundPlay, value: FlatSoundPlay)
        appSettings.updateSettingFor(key: .userID, value: userID)
        appSettings.updateSettingFor(key: .ultraTimeAlertVaue, value: ultraTimeAlertVaue)
        appSettings.updateSettingFor(key: .notificationVoiceCount, value: notificationVoiceCount)
        appSettings.updateSettingFor(key: .notificationAutoIgnore, value: notificationAutoIgnore)
        appSettings.updateSettingFor(key: .isAtHome, value: isAtHome)
        appSettings.updateSettingFor(key: .isAtOffice, value: isAtOffice)
        appSettings.updateSettingFor(key: .lastSynTime, value: lastSynTime)
        appSettings.updateSettingFor(key: .refreshUserDate, value: refreshUserDate)
        appSettings.updateSettingFor(key: .OTP, value: OTP)
        appSettings.updateSettingFor(key: .lastDateSynced, value: lastDateSynced)
        appSettings.updateSettingFor(key: .deviceVoiceAlertMax, value: deviceVoiceAlertMax)
        appSettings.updateSettingFor(key: .deviceTimeAlert, value: deviceTimeAlert)
        appSettings.updateSettingFor(key: .minRssi, value: minRssi)
        appSettings.updateSettingFor(key: .maxRssi, value: maxRssi)
        appSettings.updateSettingFor(key: .scanningTime, value: scanningTime)
        appSettings.updateSettingFor(key: .nextNotification, value: nextNotification)
        appSettings.updateSettingFor(key: .bluetoothScantime, value: bluetoothScantime)
        appSettings.updateSettingFor(key: .syncDataTime, value: syncDataTime)
        
            
                
        
        return appSettings
    }
}

