//
//  AppDelegate.swift
//  BLEScanner
//
//  Created by Harry Goodwin on 21/01/2016.
//  Copyright © 2016 GG. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData
import IQKeyboardManagerSwift
import AVFoundation
import AudioToolbox.AudioServices
import Foundation
import engine

let appDel = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()
    var currentLanguage = "my"
    var currentFont = ""
    var localBundle : Bundle?
    var player: AVAudioPlayer = AVAudioPlayer()
    var isPlayingSound = false
    var session: AVAudioSession = AVAudioSession.sharedInstance()
    var scanUserList = [String]()
    let ccEngine = CUEEngine()
    var isAudioRecordingOn = false
    var userImage = UIImage()
    let apiReqObj = ApiRequestClass()
    var bd_ID = ""
    var bd_CAT = ""
    let validObj = PayToValidations()
    var contactName = ""
    var contactNumber = ""
    var dateTimeStr = ""
    let apiObj = ApiRequestClass()
    var apiCalledFor = ""
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
           // UIApplication.shared.isIdleTimerDisabled = true
        
         ApplicationState.sharedAppState.loadAppSettings()
        let textColor = [NSAttributedString.Key.foregroundColor:UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = textColor
        UIApplication.shared.registerForRemoteNotifications()
        ApplicationState.sharedAppState.currentUser.notification = "ewewewewewew"
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        setUPCEUENGINE()
        
        if ApplicationState.sharedAppState.currentUser.currentLanguage == ""{
            currentLanguage = "my"
            appLang = "Myanmar"
            appFont = "Zawgyi-One"
            ApplicationState.sharedAppState.currentUser.currentLanguage = "my"
            ApplicationState.sharedAppState.currentUser.currentFont = "Zawgyi-One"
            
            if let path = Bundle.main.path(forResource: "my", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else{
            let lang = ApplicationState.sharedAppState.currentUser.currentLanguage
            if let path =  Bundle.main.path(forResource: lang, ofType: "lproj") {
                currentLanguage = ApplicationState.sharedAppState.currentUser.currentLanguage
                localBundle = Bundle(path: path)
                if lang == "my" {
                    appLang = "Myanmar"
                    appFont = "Zawgyi-One"
                } else if lang == "en" {
                    appLang = "English"
                    appFont = "Zawgyi-One"
                } else if lang == "zh-Hans" {
                    appLang = "Chinese"
                    appFont = "Zawgyi-One"
                } else if lang == "th" {
                    appLang = "Thai"
                    appFont = "Zawgyi-One"
                } else if lang == "uni" {
                    appLang = "Unicode"
                    appFont = "Myanmar3"
                }
                ApplicationState.sharedAppState.currentUser.currentFont = appFont
            }
        }
        self.notificationCenter.delegate = self
        let options: UNAuthorizationOptions = [.alert]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        callNotificationAtEleven()
        
        var userlogin = ""
        
        userlogin = ApplicationState.sharedAppState.currentUser.userID
        
        if userlogin != "" && ApplicationState.sharedAppState.currentUser.OTP != ""
        {
            self.goDashBoard()
        }
        else
        {
            self.goToLoginPage()
        }
        _ = TownshipManager.shared
        return true
    }
    
    // MARK: Remote Notification

    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        ApplicationState.sharedAppState.currentUser.APNSToken = token

        //Messaging.messaging().apnsToken = deviceToken
        //UserDefaults.standard.set(token, forKey: "deviceToken")
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        /*
        let alert: UIAlertController
                  alert = UIAlertController(title: "ALERT!".localized, message: token, preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                      DispatchQueue.main.async {
                         
                      }
                  }))
                  DispatchQueue.main.async {
                      UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                  }
        */
    }
          
          func application(_ application: UIApplication,
                           didFailToRegisterForRemoteNotificationsWithError error: Error) {
              print("Failed to register: \(error)")
          }
    
    // MARK: Language Localization
    
    func setSeletedlocaLizationLanguage(language : String) {
        currentLanguage = language
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            localBundle = Bundle(path: path)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
        }
    }
    
    func goDashBoard() {
        
        var navigationController: UINavigationController? = nil
        if #available(iOS 13.0, *) {
            let storyboard = UIStoryboard(name: "QRCode", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
            if let homeViewController = homeViewController {
                navigationController = UINavigationController(rootViewController: homeViewController)
            }
        }
        else{
            //  var homeVC: HomeViewController?
            let  homeVC = UIStoryboard(name: "QRCode", bundle: nil)
            let dashbord = homeVC.instantiateViewController(withIdentifier: "BaseTabController") as? BaseTabController
            var objNav: UINavigationController? = nil
            if let dashbord = dashbord {
                objNav = UINavigationController(rootViewController: dashbord)
            }
            if objNav != nil {
                navigationController = UINavigationController(rootViewController: dashbord!)
            }
        }
        window = (UIApplication.shared.delegate?.window)!
        window?.rootViewController = navigationController
    }
    
    func goToLoginPage(){
        var navigationController: UINavigationController? = nil
        if #available(iOS 13.0, *) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            
            if let homeViewController = homeViewController {
                navigationController = UINavigationController(rootViewController: homeViewController)
            }
        }
        else{
            var homeVC: HomeViewController?
            
            homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            var objNav: UINavigationController? = nil
            if let homeVC = homeVC {
                objNav = UINavigationController(rootViewController: homeVC)
            }
            if objNav != nil {
                navigationController = UINavigationController(rootViewController: homeVC!)
            }
            
        }
        
        window = (UIApplication.shared.delegate?.window)!
        
        window?.rootViewController = navigationController
    }
    
    func getlocaLizationLanguage(key : String) -> String {
        if let bundl = localBundle {
            return NSLocalizedString(key, tableName: "Localizable", bundle: bundl, value: "", comment: "")
        } else {
            return key
        }
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        ApplicationState.sharedAppState.saveCurrentUserData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangFlashLightIcon"), object: nil, userInfo: nil)
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        //6 hours syn
//        if Utility.getTimeDifferenc(timeOne: ApplicationState.sharedAppState.currentUser.syncDataTime, timeTwo: Utility.getDateForCoreData()) >= 360.0{
//            showLoaderApp()
//            self.apiReqObj.customDelegate = self
//            self.apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getSyncApi())!, requestData: DataSynParam.makeParamForDataSync(), httpMethodName: NetWorkCode.serviceTypePost)
//        }
        
        ApplicationState.sharedAppState.refreshIgnoredUser()
        ApplicationState.sharedAppState.loadCurrentUserData()
        setUPCEUENGINE()
    }
    
    
    func callNotificationAtEleven(){
        
         // notificationCenter.delegate = self
        
        let options: UNAuthorizationOptions = [.alert]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        
        
        //Set notification to trigger 11:30AM everyday
        var dateComponents = DateComponents()
        dateComponents.hour = 23
        dateComponents.minute = 37
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        //Set your content
        let content = UNMutableNotificationContent()
        content.title = "Please Open Application To Sync Your Data"
        content.body = ""
        
        let request = UNNotificationRequest(
            identifier: NotificationType.DataSync.description, content: content, trigger: trigger
        )
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        ApplicationState.sharedAppState.saveCurrentUserData()
        ApplicationState.sharedAppState.currentUserCount.removeAll()
        ApplicationState.sharedAppState.currentAudioUserCount.removeAll()
        ApplicationState.sharedAppState.saveAppSettings()
        self.saveContext()
        
    }
    
    // MARK: - Ultra Sound Setup
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BlueToothModel")
        let geofencecontainer = NSPersistentContainer(name: "GeofenceModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func setUPCEUENGINE() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingOn = true
            self.setUpEngine()
            print("setUpEngine-recordPermission-AVAudioSessionRecordPermission")

//            self.startListening()
//            self.setupCUEEngineCallback()
            
            do {
                try self.session.setCategory(AVAudioSession.Category.playAndRecord)
                try self.session.setActive(true)
            }catch {
                print("Couldn't set Audio session category")
            }
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingOn = false
            
            let alert: UIAlertController
            alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Settings".localized, style: .default, handler: { (_) in
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                }
            }))
            DispatchQueue.main.async {
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                if allowed {
                    self.isAudioRecordingOn = true
                    let delayTime = DispatchTime.now() + 3.0
                    DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                        print("setUpEngine-AVAudioSessionRecordPermission")
                        self.setUpEngine()
//                        self.startListening()
//                        self.setupCUEEngineCallback()
                        
                    })
                    
                    do {
                        try self.session.setCategory(AVAudioSession.Category.playAndRecord)
                        try self.session.setActive(true)
                    }catch {
                        print("Couldn't set Audio session category")
                    }
                } else {
                    self.isAudioRecordingOn = false
                    let alert: UIAlertController
                    
                    alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Settings".localized, style: .default, handler: { (_) in
                        DispatchQueue.main.async {
                            
                            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                            
                        }
                    }))
                    
                    //   alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
                    // self.present(alert, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                    
                }
            })
            break
        default:
            break
        }
    }
    
    
    func setUpEngine() {
        if isAudioRecordingOn{
            ccEngine.setup(withAPIKey: API_KEY)//setup(withAPIKey: API_KEY)
            ccEngine.setDefaultGeneration(2)
        }
    }
    
    func startListening() {
        if isAudioRecordingOn{
            self.setUpEngine()
            ccEngine.startListening()
            self.setupCUEEngineCallback()
        }
    }
    
    func stopListening() {
        if isAudioRecordingOn{
            ccEngine.stopListening()
            ApplicationState.sharedAppState.currentAudioUserCount.removeAll()
        }
    }
    
    func setupCUEEngineCallback() {
        if isAudioRecordingOn{
            ccEngine.setReceiverCallback({(_ jsonString: String?) in
                print(jsonString ?? "")
                DispatchQueue.main.async {
                    let dict = self.convertToDictionary(text: jsonString ?? "{\"name\":\"James\"}")
                    self.scanUserList.append("\(dict?.safeValueForKey("trigger-as-number") as! Int)")
                    //"{\"Device Name\":\"Audio QR\","Id" : "1234567"}")
                    
                    let mob = ApplicationState.sharedAppState.currentUser.MobileNumber
                    if mob != "" {
                        if mob.contains("\(dict?.safeValueForKey("trigger-as-number") as! Int)"){
                            
                        } else {
                            let compareValue = dict?.safeValueForKey("trigger-as-number") as? Int
                            
                            if let valueAvailabel = compareValue{
                                let convertString = String(valueAvailabel)
                                let obj = ApplicationState.sharedAppState.blueToothModelObj
                                
                                let value = obj.filter({$0.bluetoothId == convertString})
                                
                                if value.count == 0{
                                    //add new value
                                    let rssiID = Utility.audioQRNoiseToRSSI(noise: dict?.safeValueForKey("noise") as? Int ?? -13)
                                    
                                    ApplicationState.sharedAppState.currentAudioUserCount.append(BluetoothDataModel(name: "Audio QR" , id: convertString, bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "0",rssiID: rssiID, mobileNumber: ""))
                                    
                                    ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: "Audio QR" , id: convertString, bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "0",rssiID: rssiID, mobileNumber: ""))
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)
                                    
                                    self.playSoundWhenUSerNear(name: "Audio QR",id: convertString,category: CustomNotification.DefaultValue.description,feet: 0)
                                    
                                }else{
                                    //if duplicate count available
                                    let items = ApplicationState.sharedAppState.blueToothModelObj
                                    let currentUser = ApplicationState.sharedAppState.currentAudioUserCount
                                    //getting the index of that element and we will get only one index because we are managing unique data
                                    let index = items.indices.filter({ items[$0].bluetoothId ==  convertString})
                                    let currentAudioCount = currentUser.indices.filter({ currentUser[$0].bluetoothId ==  convertString})
                                    //checking if index available
                                    if index.count>0{
                                        //checking if index availabe
                                        if items.indices.contains(index[0]){
                                            
                                            //if index available get data of particular from main array so that we can append
                                            let id = ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothId
                                            let type = ApplicationState.sharedAppState.blueToothModelObj[index[0]].userCategory
                                            let rssi = ApplicationState.sharedAppState.blueToothModelObj[index[0]].rssiID
                                            let soundTime = 0
                                            let count = ApplicationState.sharedAppState.blueToothModelObj[index[0]].numberOfTimePeopleMeet
                                            let number = ApplicationState.sharedAppState.blueToothModelObj[index[0]].mobileNumber
                                            
                                            if currentAudioCount.count == 0{
                                                ApplicationState.sharedAppState.currentAudioUserCount.append(BluetoothDataModel(name: "Audio QR" , id: convertString, bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 1,peopleMeet:0,userCategory: CustomNotification.DefaultValue.description,numberOfTimePeopleMeet: "0",rssiID: rssi, mobileNumber: ""))
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshUser"), object: nil, userInfo: nil)
                                            }
                                            
                                            if  Utility.getTimeDifferenc(timeOne: ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothDate, timeTwo:  Utility.getDateForCoreData()) >= 2.0{
                                                let newCount = (Int(count) ?? 0) + 1
                                                //removing that id to update
                                                ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
                                                ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: "Audio QR" , id: id, bluetoothDate: Utility.getDateForCoreData(),bluetoothDateLessThanSixFeet: 2,peopleMeet:soundTime,userCategory:type,numberOfTimePeopleMeet: count,rssiID: rssi,mobileNumber: number))
                                                //notification will come only for 6 time for particular user after that auto ignore
                                                if ((newCount <= 5) && (type == CustomNotification.DefaultValue.description)){
                                                    
                                                    self.playSoundWhenUSerNear(name: "Audio QR",id: "\(id)",category: type,feet: 0)
                                                    
                                                }
                                            }
                                            
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } else {
            print("isAudioRecordingOFF")
        }
    }
    
    //play sound
    func playSoundWhenUSerNear(name: String,id: String,category: String,feet: Int){
        
        if category == CustomNotification.DefaultValue.description  {
            if Utility.isNotificationTimeActive(){
                var showAlert = 0
                let hasDate = ApplicationState.sharedAppState.currentUser.LoginDate
                //logged in today for the first time only alert will come for 4 time
                if hasDate.components(separatedBy: " ")[0] == Utility.getDateForCoreData().components(separatedBy: " ")[0]{
                    showAlert = 3
                }else{
                    //once logged in then alert will come ie sound for 2 time after that notification
                    showAlert = 1
                }
                
                //for everytwo hour one time sound will come
                if Utility.getTimeDifferenc(timeOne: ApplicationState.sharedAppState.currentUser.bluetoothScantime, timeTwo: Utility.getDateForCoreData()) >= 120.0{
                    ApplicationState.sharedAppState.currentUser.bluetoothScantime = Utility.getDateForCoreData()
                    if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                        playSound(tune: "bluetooth_receiver")
                      self.scheduleNotification(notificationType: NotificationType.AudioQR.description,title:"Some one Near with you ->",body: id, willVibrate: false)
                    }
                } else{
                    if ApplicationState.sharedAppState.currentUser.counting <= showAlert{
                       // if !isPlayingSound{
                            //if at home no notification
                            if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                                playSound(tune: "bluetooth_receiver")
                              self.scheduleNotification(notificationType: NotificationType.AudioQR.description,title:"Some one Near with you ->",body: id, willVibrate: false)
                            }
                       // }
                    }else{
                       // if !isPlayingSound{
                            //  willPlayNotification = false
                            //if the active user at home no notification
                            if !ApplicationState.sharedAppState.currentUser.isAtHome || !ApplicationState.sharedAppState.currentUser.isAtOffice{
                                if #available(iOS 13.0, *) {
                                }else{
                                    playSound(tune: "iphone_notification")
                                }
                                
                                self.scheduleNotification(notificationType: NotificationType.AudioQR.description,title:"Some one Near with you ->",body: id, willVibrate: true)
                            }
                      //  }
                    }
                }
            }
        }
    }
    
    func playSound(tune: String) {
           do{
               let audioPath = Bundle.main.path(forResource: tune, ofType: "mp3")
               player = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioPath!))
               player.prepareToPlay()
               ApplicationState.sharedAppState.currentUser.counting = 1 + ApplicationState.sharedAppState.currentUser.counting
               player.volume = 7.0
               player.play()
               isPlayingSound = true
               player.delegate = self
                     }
           catch{
               print(error)
           }
       }

    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func invokeTimer() {
       // let _ = Timer.scheduledTimer(withTimeInterval: 30.0, repeats: false) { (timer) in
            //self.ccEngine.queueTrigger(asNumber: 1234)
            let mob = ApplicationState.sharedAppState.currentUser.MobileNumber
            if mob != "" {
                let last4 = mob.suffix(mob.count-4)
                //print(last4)
                //self.ccEngine.transmitMessage("\(UInt(last4) ?? 1234567)")
                self.ccEngine.queueTrigger(asNumber: (UInt(last4) ?? 1234567))
            }else {
                print("Mobil number not found")
                //self.stopListening()
            }
            // }
       // }
    }
    
}


extension AppDelegate: UNUserNotificationCenterDelegate,AVAudioPlayerDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == "Ignore" || response.actionIdentifier == "com.apple.UNNotificationDefaultActionIdentifier"{
            if response.notification.request.identifier == NotificationType.Bluetooth.description{
                ApplicationState.sharedAppState.currentUser.notification = NotificationType.Bluetooth.description
                ApplicationState.sharedAppState.currentUser.BluetoothID = response.notification.request.content.body
                userscountNew()
            }else if response.notification.request.identifier == NotificationType.AudioQR.description{
                ApplicationState.sharedAppState.currentUser.notification = NotificationType.AudioQR.description
                ApplicationState.sharedAppState.currentUser.BluetoothID = response.notification.request.content.body
                userscountNew()
            }else if response.notification.request.identifier == Constant.ENTERED_REGION_NOTIFICATION_ID{
                
            }
            else if response.notification.request.identifier == Constant.EXITED_REGION_NOTIFICATION_ID{
                
            }
            
            else {
                ApplicationState.sharedAppState.currentUser.notification = NotificationType.DataSync.description
                userscountNew()
            }
            
        }
        
        
        completionHandler()
    }
    
    
    
    func playSound() {
        do{
            let audioPath = Bundle.main.path(forResource: "you_are_going_out", ofType: "m4a")
            player = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: audioPath!))
            player.prepareToPlay()
            player.play()
            isPlayingSound = true
            player.delegate = self
        }
        catch{
            print(error)
        }
    }
    
    func scheduleNotification(notificationType: String,title: String,body: String,willVibrate: Bool) {
        
        //  self.PlayAudioSoundForFIle(fileName: "iphone_notification", type: "mp3", controller: self)
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = notificationType
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        let identifier = notificationType
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        self.notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        if willVibrate{
            let _ = try? AVAudioSession.sharedInstance().setCategory(.playAndRecord)
                   if #available(iOS 13.0, *) {
                       let _ = try? AVAudioSession.sharedInstance().setAllowHapticsAndSystemSoundsDuringRecording(true)
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                   }else{
                    
            }
        }
        
        //this notification will only work for audio QR and Bluetooth
        if notificationType != NotificationType.DataSync.description{
            let snoozeAction = UNNotificationAction(identifier: "Ignore", title: "Ignore", options: [])
            let deleteAction = UNNotificationAction(identifier: "Cancel", title: "Cancel", options: [.destructive])
            let category = UNNotificationCategory(identifier: notificationType,
                                                  actions: [snoozeAction, deleteAction],
                                                  intentIdentifiers: [],
                                                  options: [])
            
            notificationCenter.setNotificationCategories([category])
        }
        
        
    }
    
}

extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension AppDelegate{
    
    func userscountNew(){
        let valueNEW = ApplicationState.sharedAppState.currentUser.notification
        ApplicationState.sharedAppState.currentUser.notification = "ewewewewewew"
        if valueNEW == NotificationType.Bluetooth.description || valueNEW == NotificationType.AudioQR.description{
            showSAertToWindow(message: "Please confirm the category you want to ignore".localized, alertIcon: "", subTitle: "", Title: "", actionTitleFirst: "Home".localized, actionTitleSecond: "Office".localized, actionTitleThird: "Device".localized, actionTitleFourth: "Other".localized,actionTitleFifth: "Cancel".localized)
        }else{
            //this will work for data sync
            showLoaderApp()
            self.apiCalledFor = "dataSync"
            self.apiReqObj.customDelegate = self
            self.apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.getSyncApi())!, requestData: DataSynParam.makeParamForDataSync(), httpMethodName: NetWorkCode.serviceTypePost)
            
        }
    }
    
    func makeAsFav(id : String,categoryType: String) {
        self.bd_ID = id
        self.bd_CAT = categoryType
        let alert: UIAlertController
        alert = UIAlertController(title: "ALERT!".localized, message: "Do you want to save this device with Name and Contact Number?".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "NO".localized, style: .default, handler: { (_) in
            self.callBlockBluetoothDevice(c_name: "", c_number: "")
        }))
        alert.addAction(UIAlertAction(title: "YES".localized, style: .default, handler: { (_) in
            self.showContactPickerView()
        }))
        UIApplication.topViewController()?.present(alert, animated: false, completion: nil)
    }
    
    func showContactPickerView() {
        let nav = Utility.openContact(multiSelection: false, self)
        nav.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(nav, animated: false, completion: nil)
    }
    
    func callBlockBluetoothDevice(c_name: String, c_number : String) {
        var chennalName = "0"
        if self.bd_ID.count > 9 {
            chennalName = "1"
        }

     let index = ApplicationState.sharedAppState.blueToothModelObj.indices.filter({ ApplicationState.sharedAppState.blueToothModelObj[$0].bluetoothId ==  bd_ID})
     let value = ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothName
        self.dateTimeStr = Utility.makeDateFormatForAddFamily(date: ApplicationState.sharedAppState.blueToothModelObj[index[0]].bluetoothDate )
        self.callBlockBluetoothDevice(b_name: value, b_id: self.bd_ID, b_chennal: chennalName, block: "1", c_name: c_name, c_nubmer: c_number)
    }
    
    func callBlockBluetoothDevice(b_name: String,b_id : String, b_chennal:String, block: String, c_name: String, c_nubmer: String) {
            self.contactName = c_name
            self.contactNumber = c_nubmer
            let mobilNum = ApplicationState.sharedAppState.currentUser.MobileNumber
            var bluetooth = Dictionary<String,Any>()
            var ultrasound = Dictionary<String,Any>()
            var arrB = [Any]()
            var arrU = [Any]()
            if b_chennal == "0" {
                //UltraSound
                ultrasound = ["UltraSoundName":b_name,"UltraSoundid":b_id,"CategoryId": "54ad5797-189f-418c-9c7f-15a0e89c9e2c","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
                arrU.append(ultrasound)
            }else {
                // Bluetotth
                bluetooth = ["BluetoothName":b_name,"Bluetoothid":b_id,"CategoryId": "f1b171b8-b481-4b29-a5e1-d8ee55109613","Channel":b_chennal,"Self":0,"IsBlocked": block,"Name": c_name,"MobileNumber": c_nubmer,"EndDateTime" : dateTimeStr]
                arrB.append(bluetooth)
            }
            
            let dic = [ "UserDetails":
                ["MobileNumber": mobilNum,"OSType":"1","simid":Constant.uuid,"Msid":NetWorkCode.getMSIDForSimCard().0[1],"AppId":NetWorkCode.getTheAppID()
                ],
                        "DeviceDetails":[
                            "BlueTooth": arrB,
                            "UltraSound": arrU
                ]
                ] as [String : Any]
            self.showLoaderApp()
            print(dic)
            apiObj.customDelegate = self
            self.apiCalledFor = "block"
            apiObj.sendHttpRequest(requestUrl: URL(string:Constant.getBlutoothBlock())!, requestData: dic, httpMethodName: NetWorkCode.serviceTypePost)
    }
}


extension AppDelegate{
    
    func showSAertToWindow(message: String, alertIcon: String, subTitle : String , Title : String , actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, actionTitleFourth: String,actionTitleFifth: String) {
        var alertHeight = 0
        alert = UIAlertController.init(title: Title , message: "" , preferredStyle: .alert)
        imageView = UIImageView(frame: CGRect(x: 120, y: 30, width: 30, height: 30))
        alertHeight += 70
        messagelabel = UILabel(frame: CGRect(x: 10, y: alertHeight , width: 220, height: 20))
        messagelabel.text = message
        messagelabel.textAlignment = .center
        messagelabel.numberOfLines = 0
        messagelabel.lineBreakMode = .byTruncatingTail
        messagelabel.textColor = UIColor.black
        messagelabel.frame.size.height = messagelabel.optimalHeight + 10
        alertHeight = alertHeight + Int(messagelabel.frame.size.height) + 10
        
        namelabel = UILabel(frame: CGRect(x: 30, y: alertHeight , width: 200, height: 20))
        namelabel.text = subTitle
        namelabel.textAlignment = .center
        namelabel.numberOfLines = 0
        namelabel.lineBreakMode = .byTruncatingTail
        namelabel.textColor = UIColor.black
        namelabel.frame.size.height = namelabel.optimalHeight
        alertHeight = alertHeight + Int(namelabel.frame.size.height) + 10
        
        alert?.view.addSubview(messagelabel)
        alert?.view.addSubview(namelabel)
        
        if alertIcon.count != 0 {
            imageView.image = UIImage(named: alertIcon)
            alert?.view.addSubview(imageView)
        }
        else {
            imageView.image = UIImage(named: "AlertIcon")
            alert?.view.addSubview(imageView)
        }
        if actionTitleThird == "" {
            
            alertHeight += 50
        }
        else if (actionTitleFourth != "") {
            alertHeight += 200
        }
        else {
            alertHeight += 150
        }
        height = NSLayoutConstraint(item: alert!.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(alertHeight))
        width = NSLayoutConstraint(item: alert!.view as Any, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        alert!.view.addConstraint(height)
        alert!.view.addConstraint(width)
        
        if(!actionTitleFirst.isEmpty) {
            let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                self.makeAsFav(id: ApplicationState.sharedAppState.currentUser.BluetoothID, categoryType: CustomNotification.Home.description)
            }
            firstButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(firstButtonAction)
        }
        
        if(!actionTitleSecond.isEmpty) {
            let secondButtonAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                self.makeAsFav(id: ApplicationState.sharedAppState.currentUser.BluetoothID, categoryType: CustomNotification.Office.description)
            }
            secondButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(secondButtonAction)
        }
        
        if(!actionTitleThird.isEmpty) {
            let thirdButtonAction = UIAlertAction(title: actionTitleThird , style: .default) { (alert: UIAlertAction!) -> Void in
                self.makeAsFav(id: ApplicationState.sharedAppState.currentUser.BluetoothID, categoryType: CustomNotification.Devices.description)
            }
            thirdButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(thirdButtonAction)
        }
        if(!actionTitleFourth.isEmpty) {
            let fourthButtonAction = UIAlertAction(title: actionTitleFourth , style: .default) { (alert: UIAlertAction!) -> Void in
                self.makeAsFav(id: ApplicationState.sharedAppState.currentUser.BluetoothID, categoryType: CustomNotification.Devices.description)
            }
            fourthButtonAction.setValue(UIColor.systemBlue, forKey: "titleTextColor")
            alert!.addAction(fourthButtonAction)
        }
        if(!actionTitleFifth.isEmpty) {
            let fifthButtonAction = UIAlertAction(title: actionTitleFifth , style: .default) { (alert: UIAlertAction!) -> Void in
            //    self.makeAsFav(id: ApplicationState.sharedAppState.currentUser.BluetoothID, categoryType: CustomNotification.Devices.description)
            }
            fifthButtonAction.setValue(UIColor.systemRed, forKey: "titleTextColor")
            alert!.addAction(fifthButtonAction)
        }
        DispatchQueue.main.async {
            if let isAlertAvailabe = alert{
                 UIApplication.topViewController()?.present(isAlertAvailabe, animated: true, completion: nil)
            }
        }
    }
}


extension AppDelegate :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool) {
        //success Block
        self.hideLoaderApp()
        if !hasError {
            if self.apiCalledFor == "dataSync" {
                let dicValue = responseObj as? NSDictionary
                if let responseValue = dicValue{
                    if responseValue.count>0{
                        let code =  responseValue.value(forKey: "Code") as? Int
                        let message = responseValue.value(forKey: "Message") as? String
                        if let statusCode = code,let msg = message{
                            if statusCode == 200 || msg == "Success"{
                                ApplicationState.sharedAppState.currentUser.syncDataTime = Utility.getDateForCoreData()
                            }
                        }
                    }
                }
            }else {
                let dataResponse = responseObj as! NSDictionary
                if dataResponse.count > 0 {
                    DispatchQueue.main.async {
                        if let code = dataResponse["Code"] as? Int {
                            if code == 200 {
                                
                                let copyObj = ApplicationState.sharedAppState.blueToothModelObj
                                
                                let index = copyObj.indices.filter({ copyObj[$0].bluetoothId ==  self.bd_ID})
                                //taking the first index  
                                if ApplicationState.sharedAppState.blueToothModelObj.indices.contains(index[0]){
                                    ApplicationState.sharedAppState.blueToothModelObj.remove(at: index[0])
                                    if self.contactName == "" {
                                        ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: copyObj[index[0]].bluetoothName, id: copyObj[index[0]].bluetoothId, bluetoothDate: Utility.getDateForCoreData(), bluetoothDateLessThanSixFeet: copyObj[index[0]].bluetoothDateLessThanSixFeet, peopleMeet: copyObj[index[0]].peopleMeet, userCategory: self.bd_CAT,numberOfTimePeopleMeet: copyObj[index[0]].numberOfTimePeopleMeet,rssiID: copyObj[index[0]].rssiID,mobileNumber: self.contactNumber))
                                    }else {
                                        ApplicationState.sharedAppState.blueToothModelObj.append(BluetoothDataModel(name: self.contactName, id: copyObj[index[0]].bluetoothId, bluetoothDate: Utility.getDateForCoreData(), bluetoothDateLessThanSixFeet: copyObj[index[0]].bluetoothDateLessThanSixFeet, peopleMeet: copyObj[index[0]].peopleMeet, userCategory: self.bd_CAT,numberOfTimePeopleMeet: copyObj[index[0]].numberOfTimePeopleMeet,rssiID: copyObj[index[0]].rssiID,mobileNumber: self.contactNumber))
                                    }
                                }
                                let delayTime = DispatchTime.now() + 2.0
                                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                                    ApplicationState.sharedAppState.blueToothMainClassObj.isSartedCalled = false
                                    ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
                                })

                                self.bd_ID = ""
                                self.bd_CAT = ""
                                self.contactName = ""
                                self.contactNumber = ""
                                self.dateTimeStr = ""
                                // self.showMyAlert(title: "Notification is Ignored".localized)
                            }else {
                                self.showMyAlert(title: "API Failed".localized)
                            }
                        }else {
                            self.showMyAlert(title: "API Failed".localized)
                        }
                    }
                }
            }
        }else {
            self.showMyAlert(title: "API Failed".localized)
        }
    }
    
}

extension AppDelegate{
    
    func showLoaderApp() {
        container.frame = (UIApplication.shared.keyWindow?.rootViewController?.view.frame)!
        container.center = (UIApplication.shared.keyWindow?.rootViewController?.view.center)!
        container.backgroundColor = UIColor(white: 0, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        loadingView.center = (UIApplication.shared.keyWindow?.rootViewController?.view.center)!
        loadingView.backgroundColor = .clear
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingLabel.frame = CGRect(x: 0, y: 80, width: 120, height: 40)
        loadingLabel.text = "Loading...".localized
        loadingLabel.textAlignment = .center
        loadingLabel.textColor = .white
        //loadingLabel.font = .systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        loadingLabel.font =  UIFont(name: "Zawgyi-One", size: 15)
        
        // uiView.addSubview(container)
        
        container.addSubview(loadingView)
        loadingView.addSubview(activityIndicator)
        loadingView.addSubview(loadingLabel)
        UIApplication.shared.keyWindow?.addSubview(container)
        
        DispatchQueue.main.async {
            activityIndicator.startAnimating()
        }
    }
    
    func hideLoaderApp() {
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.removeFromSuperview()
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            loadingLabel.removeFromSuperview()
            loadingView.removeFromSuperview()
            container.removeFromSuperview()
        }
    }
    
}
