//
//  Constant.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit
import AdSupport
import Contacts
import CoreTelephony


enum CustomNotification: String{
    case Home = "Home"
    case Office = "Office"
    case Devices = "Devices"
    case Others = "Others"
    case DefaultValue = "DefaultValue"

    var description: String {
        return self.rawValue
    }
}

enum NotificationType: String{
    case Bluetooth = "Bluetooth"
    case AudioQR = "AudioQR"
    case DataSync = "DataSync"
   

    var description: String {
        return self.rawValue
    }
}


let baseURL = "http://6ftmyitta.com:8084/"
var preLoginDict = Dictionary<String,Any>()
var phNumValidationsApi: [PhNumValidationList] = []
var phNumValidationsFile: [PhNumValidationList] = []
//var API_KEY = "EH0GHbslb0pNWAxPf57qA6n23w4Zgu5U"//Dev Key
var API_KEY = "Jkb12r6vCOmvfjYFdotIsXtX8mxWtnAy"//Prod Key
var Google_API_KEY = "AIzaSyB4wTaf3qx48KN7wQrH7Pj9vafdMW-dIwE"
var locationArr = [addressModel]()
var appLang: String = "Myanmar"
var appName: String = "6ft Myitta"
let SEARCHCHARSETOffer = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_ -ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:()ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ျ ိ ့် ံ ြ ု ု ူ ူ  း  ါ် ှ ီ ွ ၤ ဲ ျှျွှ နျွှ ှုဥဧ ြြ၌ဋဏ္ဍ ဍ ္ဇ ္ထ ္ခ ္လ္ဘဉဎ္ဗိံ ႍ`ါ ္ဓြ္ဆ ္တ္န  ္မဤ္ပ ္က၍ ဿ္စဍ္ဍ၎င်းဩြဦ ္ဂရဪြ ဍ္ဎြ ္ဈ့၏ ာကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ြ ိ ျ့ ံ ွ ု ု ူ ူ  း  ါျ ှ ီ ှ ၤ ဲ ြြှှှ နြှှ ှုဥဧ ွ၌ဋဏ်ဍ ဍ ်ဇ ်ထ ်ခ ်လ်ဘဉဎ်ဗိံ ႍ`ါ ်ဓွ်ဆ ်တ်န  ်မဤ်ပ ်က၍ ူ်စဍ်ဍ၎င်းငျးဩွဦ ်ဂရဪွ ဍ်ဎွ ်ဈ့၏ ာ၁၂၃၄၅၆၇၈၉၀"

struct Constant{
     static let datePickerColor = UIColor(red: 21.0/255.0, green: 44.0/255.0, blue: 156.0/255.0, alpha: 1.0)
    static let kYellowColor = UIColor(red: 255.0 / 255.0, green: 197.0 / 255.0, blue: 2.0 / 255.0, alpha: 1)
    static let uuid =  ASIdentifierManager.shared().advertisingIdentifier.uuidString
    static let deviceModel = String(UIDevice.init().model)
    static let kOccupationCell = "occupationCell"
    static let isQuarintine = "isQuarintine"
    static let quarintine = "quarintineController"
    static let geofenceHome = "geofenceHomeController"
    static let geofenceWork = "geofenceWorkController"

    static let ENTERED_WORK_REGION_MESSAGE = "Welcome to work geofence If the waves are good, you can try surfing!"
    static let EXITED_Home_REGION_MESSAGE = "Bye! Hope you had a great day at the home grofence!"
    static let EXITED_WORK_REGION_MESSAGE = "Bye! Hope you had a great day at the work grofence!"
    static let ENTERED_HOME_REGION_MESSAGE = "Welcome to Home geofence If the waves are good, you can try surfing!"
    static let ENTERED_REGION_NOTIFICATION_ID = "EnteredRegionNotification"
    static let EXITED_REGION_NOTIFICATION_ID = "ExitedRegionNotification"
    
     
    static let typeHomeLocation = "homeLocation"
    static let typeWorkLocation = "worklocation"
    static let typeQuarintineLocation = "quarintinelocation"
    
    static func getRegisterApi() -> String{
        return baseURL + "UserRegistration/Post"
    }
    
    static func getLoginApi() -> String{
        return baseURL + "UserRegistration/Login"
    }
    
    static func UpdateDeviceDetailsApi() -> String{
        return baseURL + "Device/UpdateDeviceDetails"
    }
    
    static func getOccupationApi() -> String {
       return baseURL +  "Occupation/GetOccupation"
    }
    
    static func getSyncApi() -> String{
        return baseURL + "DataSync/Post"
    }
    
    static func getCategory() -> String {
        return baseURL + "Category/GetCategory"
    }
    
    static func getBluetoothFav() -> String {
           return baseURL + "BluetoothFav/POST"
       }
    
    static func updateQuarintine() -> String {
         return baseURL + "UserRegistration/AddQuarantineInfo"
    }
    
    static func getBlutoothBlock() -> String {
           return baseURL + "Device/BlockBlutoothDetails"
       }
    
    static func getBlockedBlutooth() -> String {
            return baseURL + "BluetoothFav/GetBlootoohtListByStatus"
        }
    static func qrPost() -> String {
          return baseURL + "QRInfo/Post"
    }
   
    static func qrScan() -> String {
            return baseURL + "QRInfo/Scan"
    }
    
    static func getAppSetting() -> String{
        return baseURL + "Category/GetAppSettings"
    }
    
    
}
