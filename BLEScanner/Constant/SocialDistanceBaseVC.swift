//
//  SocialDistanceBaseVC.swift
//  BLEScanner
//
//  Created by OK$ on 02/04/2020.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation
import UIKit
import CoreMotion
import AVFoundation
import CoreLocation

class SocialDistanceBaseVC: UIViewController {

    var motionManager: CMMotionManager!
    var player: AVAudioPlayer?
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    var boolFirstTime : Bool!
    
    //Location
    let locationManager = CLLocationManager()
    public var currentLocation: CLLocation?
    public var tenSecLocation: CLLocation?
    var distance: Int = 0

    //MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        distance = 0
        player?.delegate = self

        //Location
        //locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        //Accelerometer
        boolFirstTime = true
        
        updateAccelerometer()
        
    }

    //MARK: - Accelerometer
    func updateAccelerometer() {
        motionManager = CMMotionManager()
        motionManager.startAccelerometerUpdates()
        motionManager.accelerometerUpdateInterval = 10.0
        
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data{
                //                print("Acceleration")
                //                print(myData)
                if myData.acceleration.z < -0.9 && myData.acceleration.y >= -0.019 && self.distance == 0 {
                    DispatchQueue.main.async {
                                                
                        if self.boolFirstTime {
                            self.boolFirstTime = false
                        
                            let loginTime = Date()
                            ApplicationState.sharedAppState.currentUser.FlatSoundPlay = "0"
                            UserDefaults.standard.set(loginTime, forKey: "TimeStamp5Mint")
                            
                        } else {
                            if self.timeStampCalculation() {

                                let boolPlay = ApplicationState.sharedAppState.currentUser.FlatSoundPlay
                                if  (boolPlay == "0") {
                                    self.playSound()
                                    ApplicationState.sharedAppState.currentUser.FlatSoundPlay = "1"
                                    
                                }
                                let loginTime = Date()
                                UserDefaults.standard.set(loginTime, forKey: "TimeStamp5Mint")
                                
                                if Utility.isNotificationTimeActive(){//9PM to 6AM no alert
                                    
                                    if !ApplicationState.sharedAppState.currentUser.isAtHome && !ApplicationState.sharedAppState.currentUser.isAtOffice && !Utility.isDeviceCharging() && !Utility.isDeviceLocked() {
                                    let alert = UIAlertController(title: appName, message: "Put the mobile in pocket for safety!".localized, preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ignore".localized, style: .default, handler: { (_) in
                                        DispatchQueue.main.async {
                                            
                                        }
                                    }))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func timeStampCalculation() -> Bool{
        let loginTime = UserDefaults.standard.object(forKey: "TimeStamp5Mint") as? Date ?? Date()
        let loginInterval = loginTime.timeIntervalSinceReferenceDate
        let date = Date()
        let secondPress = date.timeIntervalSinceReferenceDate //the second button press
        let diffInSeconds = secondPress - loginInterval //total time difference in seconds
        let mint = Int(diffInSeconds/60) //hours=diff in seconds / 60 sec per min / 60 min per hour
        print(mint)
        let timeDiff = Int(ApplicationState.sharedAppState.currentUser.deviceTimeAlert) ?? 0
        if mint >= timeDiff {
             //print("less then 5 mint difference")
            return true
        } else {
             //print("greater/equal to 5 mint difference")
            return false
        }
    }
    
    //MARK: - Play Sound
    func playSound() {
        
        self.PlayAudioSoundForFIle(fileName: "please_put_mobile_in_your_poket", type: "m4a", controller: self)
        
//        guard let url = Bundle.main.url(forResource: "please_put_mobile_in_your_poket", withExtension: "m4a") else { return }
//
//        do {
//            if AVAudioSession.sharedInstance().isOtherAudioPlaying {
//
//                playSound()
//            }
//            else {
//
//                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
//                         try AVAudioSession.sharedInstance().setActive(true)
//
//                         player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
//
//                         guard let player = player else { return }
//
//                         player.play()
//            }
//
//        } catch let error {
//            print(error.localizedDescription)
//        }
    }
}

extension SocialDistanceBaseVC : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
         ApplicationState.sharedAppState.blueToothMainClassObj.isSartedCalled = false
         ApplicationState.sharedAppState.blueToothMainClassObj.startScanning()
    }
}

extension SocialDistanceBaseVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         
         currentLocation = locations.first
         DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
            self?.tenSecDifference(lat:locations.first?.coordinate.latitude,long:locations.first?.coordinate.longitude)
             }
         
         distance = durationFromLatLong(lat:currentLocation?.coordinate.latitude,long:currentLocation?.coordinate.longitude)
         print(distance)
     }
     
     func tenSecDifference (lat:Double?,long:Double?) {
         tenSecLocation = CLLocation(latitude: lat ?? 0.0, longitude: long ?? 0.0)
     }
     
     private func durationFromLatLong(lat:Double?,long:Double?) -> Int {
         let coordinate0 = CLLocation(latitude: tenSecLocation?.coordinate.latitude ?? 0.0, longitude: tenSecLocation?.coordinate.longitude ?? 0.0)
         let coordinate1 = CLLocation(latitude: Double(currentLocation?.coordinate.latitude ?? 0.0), longitude: Double(currentLocation?.coordinate.longitude ?? 0.0))
           let distance = coordinate0.distance(from: coordinate1) // result is in meters
        return Int(distance)
       }
     
     private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
         if status != CLAuthorizationStatus.denied{
            locationManager.startUpdatingLocation()
        }
      }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}
