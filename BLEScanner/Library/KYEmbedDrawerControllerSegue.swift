

import UIKit

public class KYEmbedDrawerControllerSegue: UIStoryboardSegue {
    
    final override public func perform() {
        if let sourceViewController = source as? KYDrawerController {
            sourceViewController.drawerViewController = destination
        } else {
            assertionFailure("SourceViewController must be KYDrawerController!")
        }
    }
    
}
