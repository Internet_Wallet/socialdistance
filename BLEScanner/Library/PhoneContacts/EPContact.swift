//
//  ContactPicker.swift
//  ContactPickers
//
//  Created by Prabaharan Elangovan on 13/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts

open class ContactPicker {
    
    open var firstName: String
    open var lastName: String
    open var company: String
    open var thumbnailProfileImage: UIImage?
    open var profileImage: UIImage?
    open var birthday: Date?
    open var birthdayString: String?
    open var contactId: String?
    
    open var phoneNumbers = [(phoneNumber: String, phoneLabel: String)]()
    open var emails = [(email: String, emailLabel: String )]()
    open var postalAddress = [(street : String , city : String , state : String, postalCode : String , country : String , AdressLable : String)]()
    open var identifier : String?
    open var storeContect: CNContact?
	
    public init (contact: CNContact) {
        
        identifier = contact.identifier
        firstName = contact.givenName
        lastName = contact.familyName
        company = contact.organizationName
        contactId = contact.identifier
        storeContect = contact
        
        if let thumbnailImageData = contact.thumbnailImageData {
            thumbnailProfileImage = UIImage(data:thumbnailImageData)
        }
        
        if let imageData = contact.imageData {
            profileImage = UIImage(data:imageData)
        }
        
        if let birthdayDate = contact.birthday {
            
            birthday = Calendar(identifier: Calendar.Identifier.gregorian).date(from: birthdayDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = ContactGlobalConstants.Strings.birdtdayDateFormat
            //Example Date Formats:  Oct 4, Sep 18, Mar 9
            birthdayString = dateFormatter.string(from: birthday!)
        }
        
        for phoneNumber in contact.phoneNumbers {
            var phoneLabel = "phone"
            if let label = phoneNumber.label {
                let lbl1 = label.replacingOccurrences(of: "_$!<", with: "")
                let lbl2 = lbl1.replacingOccurrences(of: ">!$_", with: "")
                phoneLabel = lbl2
            }
            let phone = phoneNumber.value.stringValue
            if let number = phone.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
                let phones  = number.replacingOccurrences(of: "%20", with: "")
                let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
                let newFormat = phones2.removingPercentEncoding!
                let serverFormat = newFormat.digitsPhone
                phoneNumbers.append((serverFormat.removingPercentEncoding!,phoneLabel))
            }
        }

        for emailAddress in contact.emailAddresses {
			guard let emailLabel = emailAddress.label else { continue }
			let email = emailAddress.value as String
			
			emails.append((email,emailLabel))
		}
        
        for address in contact.postalAddresses {
            if let postalData = address.value as? CNPostalAddress {
                //[(street : String , city : String , state : String, postalCode : String , country : String)]()
                let streetx = postalData.street
                let cityx   = postalData.city
                let statex = postalData.state
                let postalCodex = postalData.postalCode
                let countryx = postalData.country
                var addressLabel = "phone"
                if let label = address.label {
                    let lbl1 = label.replacingOccurrences(of: "_$!<", with: "")
                    let lbl2 = lbl1.replacingOccurrences(of: ">!$_", with: "")
                    addressLabel = lbl2
                }
                
                postalAddress.append((streetx,cityx,statex,postalCodex,countryx,addressLabel))
            }
        }
    }
    
    init(object: FavModel)  {
        phoneNumbers.append((object.phone, "Home"))
        firstName = object.name
        lastName = ""
        company = ""
        contactId = ""
    }
	
    open func displayName() -> String {
        var name = firstName + " " + lastName
        if name.replacingOccurrences(of: " ", with: "") == "" {
            name = "Unknown"
        }
        
        if name.count <= 0 {
            name = "Unknown"
        }
        
        return name
    }
    
    open func contactInitials() -> String {
        var initials = String()
		
		if let firstNameFirstChar = firstName.first {
			initials.append(firstNameFirstChar)
		}
		
		if let lastNameFirstChar = lastName.first {
			initials.append(lastNameFirstChar)
		}
		
        return initials
    }
    
}

fileprivate let allowedCharset = CharacterSet
.decimalDigits
.union(CharacterSet(charactersIn: "+"))


extension String {
var digitsPhone: String {
     let str = String(self.unicodeScalars.filter(allowedCharset.contains))
     return str
 }

}
