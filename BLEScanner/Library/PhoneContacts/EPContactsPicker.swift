//
//  ContactPickersPicker.swift
//  ContactPickers
//
//  Created by Prabaharan Elangovan on 12/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts



enum buttonAction : Int {
    case pay = 0
    case share = 1
    case contact = 2
    case call = 3
    case details = 4
}

class ContactNavigationController : UINavigationController {}

public protocol ContactPickerDelegate: class {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError)
    func contact(_: ContactPickersPicker, didCancel error: NSError)
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker)
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker])
}

public extension ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError) { }
    func contact(_: ContactPickersPicker, didCancel error: NSError) { }
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) { }
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) { }
}

typealias ContactsHandler = (_ contacts : [CNContact] , _ error : NSError?) -> Void
typealias okHandler = (_ contactsOk : [String] , _ error : NSError?) -> Void

public enum SubtitleCellValue{
    
    case phoneNumber
    case email
    case birthday
    case organization
    
}

open class ContactPickersPicker: UIViewController, UISearchBarDelegate , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var resultSearchController : UISearchBar!
    
    // MARK: - Properties
    open weak var contactDelegate: ContactPickerDelegate?
    var contactsStore: CNContactStore?
    //this variable will be true in only two case when coming from oversea recharge and other number just to push the user directly to pay screen
    //this variable will be used to change the text in contact edit view controller
    var isComingFromPayTo = false
    
    var orderedContacts = [String: [CNContact]]() //Contacts ordered in dicitonary alphabetically
    var sortedContactKeys = [String]()
    var collapsableContactArray = [String:Array<Bool>]()
    var sortedContactBool = [String]()
    var contactAll : [CNContact] = [CNContact]()
    var selectedContacts = [ContactPicker]()
    var filteredContacts = [CNContact]()
    var okContacts = [String]()
    var subtitleCellValue = SubtitleCellValue.phoneNumber
    var cellTouched = false
    var cellRow = 0
    var cellSection = 0
    var lastIndex : IndexPath?
    var searchString: String?
    // MARK: - Lifecycle Methods
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        registerContactCell()
        setUpNavigation()
        //inititlizeBarButtons()
        // initializeSearchBar()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70
        tableView.backgroundColor = UIColor.colorWithRedValue(redValue: 215.0, greenValue: 215.0, blueValue: 215.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.resultSearchController.showsCancelButton = false
        resultSearchController.delegate = self
        if let searchTextField = resultSearchController.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
//                searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
//                searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.resultSearchController.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.white
            }
        }
    }
    
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getContact()
    }
    
    
    fileprivate func getContact(){
        self.tableView.isUserInteractionEnabled = true
        self.contactAll.removeAll()
        self.orderedContacts.removeAll()
        reloadContacts()
    }
    
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = UIColor.getColorWithHexString("437BC8") //getColorWithHexString(437BC8)//UIColor.blue
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        //backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.setImage(UIImage(named: "back")?.withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(onTouchCancelButton), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 140, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Contacts".localized + " "//ContactGlobalConstants.Strings.contactsTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 25.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView

       // self.checkContactPermission()
    }
    
    private func checkContactPermission() {
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            break
        case .denied, .restricted, .notDetermined:
            showSettingsAlert(sms: "Please Enable Contact Permission".localized)
            break
        @unknown default:
            break
        }
    }
    
    private func showSettingsAlert(sms : String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Settings".localized, style: .default) { action in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    if UIApplication.shared.canOpenURL(url) {
                        _ =  UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            })
            self.present(alert, animated: true)
        }
    }
    
    
    func initializeSearchBar() {
        //        self.resultSearchController = ( {
        //
        //            let controller = UISearchController(searchResultsController: nil)
        //            controller.searchResultsUpdater = self
        //            controller.dimsBackgroundDuringPresentation = false
        //            controller.hidesNavigationBarDuringPresentation = false
        //            controller.searchBar.sizeToFit()
        //            controller.searchBar.delegate = self
        //            controller.searchBar.barTintColor = kYellowColor
        //            controller.searchBar.keyboardType = .default
        //            controller.searchBar.autocorrectionType = .no
        //            //self.tableView.tableHeaderView = controller.searchBar
        //            return controller
        //
        //        })()
        
    }
    
    func inititlizeBarButtons() {
        
        let buttonIcon      = UIImage(named: "back")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(onTouchCancelButton))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        
    }
    
    fileprivate func registerContactCell() {
        
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: ContactGlobalConstants.Strings.bundleIdentifier, withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: bundle)
                tableView.register(cellNib, forCellReuseIdentifier: "Cell")
            }
            else {
                assertionFailure("Could not load bundle")
            }
        }
        else {
            // Cell loafing changes according to cell. if any crashes come then put bundle as nil.
            let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: Bundle.main)
            tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        }
        
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
  
    // MARK: - Contact Operations
    open func reloadContacts() {
        contactsStore = nil
        getContacts( {(contacts, error) in
            if (error == nil) {
                self.contactAll = contacts
                DispatchQueue.main.async {
                   self.tableView.reloadData()
                }
            }
        })
    }
    
    func getContacts(_ completion:  @escaping ContactsHandler) {
        
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "ContactPickerPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
            
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {  action in
                completion([], error)
                self.showSettingsAlert(sms: "Please Enable Contact Permission".localized)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        case CNAuthorizationStatus.notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ) {
                   self.showSettingsAlert(sms: "Please Enable Contact Permission".localized)
                }else {
                    self.reloadContacts()
                }
            })
        case  CNAuthorizationStatus.authorized:
            //Authorization granted by user for this app.
            var contactsArray = [CNContact]()
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    
                    if contactsArray.count > 1 {
                        for phone in contact.phoneNumbers {
                            let contactMutable = contact.mutableCopy() as! CNMutableContact//CNMutableContact()
                            let stringPhone = phone.value.stringValue
                            if stringPhone.count > 3 {
                                let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                                contactMutable.givenName = (contact.givenName.count > 0) ? contact.givenName : "Unknown"
                                contactMutable.phoneNumbers = [homePhone]
                                contactsArray.append(contactMutable)
                                var key: String = "#"
                                //If ordering has to be happening via family name change it here.
                                if let firstLetter = contactMutable.givenName[0..<1] , firstLetter.containsAlphabets() {
                                    key = firstLetter.uppercased()
                                }
                                var contacts = [CNContact]()
                                if let segregatedContact = self.orderedContacts[key] {
                                    contacts = segregatedContact
                                }
                                
                                if contactMutable.phoneNumbers.count>0{
                                    contacts.append(contactMutable)
                                    self.orderedContacts[key] = contacts
                                    self.collapsableContactArray[key] = Array.init(repeating: false, count: contacts.count)
                                }
                            }
                        }
                    } else {
                        
                        if contact.phoneNumbers.count>0{
                            contactsArray.append(contact)
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            var contacts = [CNContact]()
                            if let segregatedContact = self.orderedContacts[key] {
                                contacts = segregatedContact
                            }
                            contacts.append(contact)
                            self.orderedContacts[key] = contacts
                            self.collapsableContactArray[key] = Array.init(repeating: false, count: contacts.count)
                        }
                    }
                })
                self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                self.sortedContactBool = Array(self.collapsableContactArray.keys).sorted(by: <)
                if self.sortedContactKeys.first == "#" {
                    self.sortedContactKeys.removeFirst()
                    self.sortedContactKeys.append("#")
                }
                if self.sortedContactBool.first == "#" {
                    self.sortedContactBool.removeFirst()
                    self.sortedContactBool.append("#")
                }
                completion(contactsArray, nil)
            }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
            catch let error as NSError {
                print(error.localizedDescription)
            }
        @unknown default:
            break
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactOrganizationNameKey as CNKeyDescriptor,
                CNContactBirthdayKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
                CNContactPostalAddressesKey as CNKeyDescriptor ,
        ]
    }
    
    @objc func performActionOnBUtton (sender : UIButton) {
        let buttonActionCase = sender.tag
        let cell = tableView.cellForRow(at: IndexPath(row: cellRow, section: cellSection)) as! ContactPickerCell
        let selectedContact = cell.contact!
        
        switch buttonActionCase {
        case buttonAction.pay.rawValue:
            do {
                    //Single selection code
                    // resultSearchController.isActive = false
                    self.dismiss(animated: true, completion: {
                        DispatchQueue.main.async {
                            self.contactDelegate?.contact(self, didSelectContact: selectedContact)
                        }
                    })
            }
        case buttonAction.contact.rawValue:
            print("contact button clicked")
        case buttonAction.call.rawValue:
            print("call button clicked")
            do {
                let callURL = URL(string: "telprompt://\(selectedContact.phoneNumbers.last?.phoneNumber ?? "")")
                UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        default:
            break;
        }
        
    }
    
    func restrictMultipleSpacesSearchBar(str : String , searchBar: UISearchBar , range: NSRange) -> Bool {
        if str == " " && searchBar.text?.last == " "{
            return false
        } else {
            if characterBeforeCursorInSearchBar(tf: searchBar, range: range) == " " && str == " " {
                return false
            } else {
                if characterAfterCursorInSearchBar(tf: searchBar, range: range) == " " && str == " " {
                    return false
                }
                else {
                    return true
                }
            }
        }
    }
    
    func characterBeforeCursorInSearchBar(tf : UISearchBar , range : NSRange) -> String? {
        if let cursorRange = range as? NSRange{
            // get the position one character before the cursor start position
            //        if let newPosition = tf.searchTextPositionAdjustment(from: cursorRange.start, offset: -1) {
            //            let range = tf.textRange(from: newPosition, to: cursorRange.start)
            //            return tf.text(in: range!)
            //        }
        }
        return nil
    }
    
    func characterAfterCursorInSearchBar(tf : UISearchBar, range : NSRange) -> String? {
        if let cursorRange = range as? NSRange{
            // get the position one character before the cursor start position
        }
        return nil
    }
    
    // MARK: -- UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            var headerFrame = self.resultSearchController.frame.origin.y
            headerFrame += abs(scrollView.contentOffset.y)
            self.resultSearchController.frame.origin.y = headerFrame
            print(self.resultSearchController.frame.origin.y)
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.resultSearchController.frame.height > 150 {
            animateHeader()
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if self.resultSearchController.frame.height > 150 {
            animateHeader()
        }
    }
    
    func animateHeader() {
        //self.resultSearchController.dHeight = 150
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Table View DataSource
    open func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableFooterView = UIView()
        if  let text = self.searchString , text.count > 0 {
            if filteredContacts.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Contact Found!!!".localized
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return 1
        }
        return sortedContactKeys.count
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  let text = self.searchString , text.count > 0
        { return filteredContacts.count }
        if let contactsForSection = orderedContacts[sortedContactKeys[section]] {
            return contactsForSection.count
        }
        return 0
    }
    
    // MARK: - Table View Delegates
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContactPickerCell
        cell.accessoryType = UITableViewCell.AccessoryType.none
        //Convert CNContact to ContactPicker
        var contact: ContactPicker
        
        if  let text = searchString , text.count > 0 && filteredContacts.count > 0 {
            contact = ContactPicker(contact: filteredContacts[(indexPath as NSIndexPath).row])
        }else {
            guard let contactsForSection = orderedContacts[sortedContactKeys[(indexPath as NSIndexPath).section]] else {
                assertionFailure()
                return UITableViewCell()
            }
            contact = ContactPicker(contact: contactsForSection[(indexPath as NSIndexPath).row])
        }
        
        if cellSection == indexPath.section && cellRow == indexPath.row {
            if self.cellTouched {
                cell.btnStack.isHidden = false
                cell.heightStack.constant = 55.0
            }else {
                cell.btnStack.isHidden = true
                cell.heightStack.constant = 0.0
            }
        }else {
            cell.btnStack.isHidden = true
            cell.heightStack.constant = 0.0
        }
        cell.updateContactsinUI(contact, indexPath: indexPath, subtitleType: subtitleCellValue)
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ContactPickerCell
        let selectedContact = cell.contact!
        
        //        let cell = tableView.cellForRow(at: indexPath) as! ContactPickerCell
        //        let selectedContact = cell.contact!

            if selectedContact.phoneNumbers.count > 0 {
                    print("genius ur code is called")
                    self.dismiss(animated: true, completion: {
                        DispatchQueue.main.async {
                            self.contactDelegate?.contact(self, didSelectContact: selectedContact)
                        }
                    })
            }
        
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if cellSection == indexPath.section && cellRow == indexPath.row {
            if self.cellTouched {
                return 120.0
            }else {
                return 61.0
            }
        }
        else {
            return 61.0
        }
    }
    
    open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if  let text = resultSearchController.text, text.count > 0 { return 0 }
        tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: UITableView.ScrollPosition.top , animated: false)        
        return sortedContactKeys.firstIndex(of: title)!
    }
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  let text = resultSearchController.text, text.count > 0 { return 0 }
        return 40
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 30, y: 0, width: tableView.frame.size.width, height: 35))
        view.backgroundColor = UIColor.colorWithRedValue(redValue: 215.0, greenValue: 215.0, blueValue: 215.0, alpha: 1.0)
        if let text = resultSearchController.text, text.count > 0{}
        else {
            let label = MarqueeLabel(frame: CGRect(x: 25, y: 0, width: 40 , height: 40))
            label.type = .continuous
            label.speed = .duration(4)
            label.text =  sortedContactKeys[section] + " "
            label.textColor = UIColor.black
            if let navFont = UIFont(name: ApplicationState.sharedAppState.currentUser.currentFont, size: 17.0) {
                label.font = navFont
            }
            label.textAlignment = .left
            view.addSubview(label)
        }
        return view
    }
    
    open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if let text = resultSearchController.text, text.count > 0 { return nil }
        return sortedContactKeys
    }
    
    //    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if resultSearchController.isActive , let text = resultSearchController.searchBar.text, text.count > 0{ return nil }
    //        return sortedContactKeys[section]
    //    }
    
    // MARK: - Button Actions
    @objc func onTouchCancelButton(){
        
        
        self.dismiss(animated: true, completion: nil)
        //        dismiss(animated: true, completion: {
        //            self.contactDelegate?.contact(self, didCancel: NSError(domain: "ContactPickerPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
        //        })
    }
    
    @objc func onTouchDoneButton(){
        dismiss(animated: true, completion: {
            //self.contactDelegate?.contact(self, didSelectMultipleContacts: self.selectedContacts)
        })
        
        //        dismiss(animated: true, completion: {
        //            self.contactDelegate?.contact(self, didSelectMultipleContacts: self.selectedContacts)
        //        })
    }
    
    // MARK: - Search Actions
    open func updateSearchResults( searchText : String ) {
        
        
        if !searchText.isEmpty {
            let predicate: NSPredicate
            if searchText.count > 0 {
                predicate = CNContact.predicateForContacts(matchingName: searchText)
            } else {
                predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore!.defaultContainerIdentifier())
            }
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            contactFetchRequest.predicate = predicate
            filteredContacts.removeAll()
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    for phone in contact.phoneNumbers {
                        
                        let contactMutable = contact.mutableCopy() as! CNMutableContact//CNMutableContact()
                        let stringPhone = phone.value.stringValue
                        if stringPhone.count > 3 {
                            
                            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                            contactMutable.givenName = (contact.givenName.count > 0) ? contact.givenName : "Unknown"
                            contactMutable.phoneNumbers = [homePhone]
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contactMutable.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            self.filteredContacts.append(contactMutable)
                        }
                    }
                })
                print("\(filteredContacts.count) count")
                self.tableView.reloadData()
            }
            catch{
                print("Error!")
            }
        } else {
            filteredContacts.removeAll()
            DispatchQueue.main.async {
                self.resultSearchController.text = self.searchString
                self.tableView.reloadData()
            }
        }
    }
    
    
    public func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let value = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        if range.location == 0 && text == " " {
            return false
        }
        if searchBar.text!.last == " " && text == " " {
            return false
        }
        if !(text == text.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
        self.searchString = value
        self.updateSearchResults(searchText: value)
        return restrictMultipleSpacesSearchBar(str: value, searchBar: searchBar, range: range)
    }
    
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.resultSearchController.showsCancelButton = false
        searchString = nil
        searchBar.endEditing(true)
        self.tableView.reloadData()
    }
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchString = nil
            self.tableView.delegate  = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
}
