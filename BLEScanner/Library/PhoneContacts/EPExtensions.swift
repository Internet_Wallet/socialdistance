//
//  EPExtensions.swift
//  ContactPickerPicker
//
//  Created by Prabaharan Elangovan on 14/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Foundation

extension String {
    subscript(r: Range<Int>) -> String? {
        get {
            let stringCount = self.count as Int
            if (stringCount < r.upperBound) || (stringCount < r.lowerBound) {
                return nil
            }
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound - r.lowerBound)
            return String(self[(startIndex ..< endIndex)])
        }
    }
    
    func containsAlphabets() -> Bool {
        //Checks if all the characters inside the string are alphabets
        let set = CharacterSet.letters
        return self.utf16.contains( where: {
            guard let unicode = UnicodeScalar($0) else { return false }
            return set.contains(unicode)
        })
    }
}



enum FavScreen {
    case first
    case second
}

struct FavModel {
    var id = ""
    var phone = ""
    var name = ""
    var createDate = ""
    var agentID = ""
    var type = ""
var favDeleteNum = ""
    
    init(_ id: String,  phone: String, name: String, createDate: String, agentID: String, type: String) {
        self.id         = id
        
        if phone.hasPrefix("959") {
            self.phone = "00" + phone
        }
        else if phone.hasPrefix("09"){
            self.phone = "0095" + phone.dropFirst()
        }
        
        else if phone.hasPrefix("00"){
           self.phone      =  phone
        }
        else {
            self.phone = "00" + phone
            
        }
        
        self.name       = name
        self.createDate = createDate
        self.agentID    = agentID
        self.type       = type
        self.favDeleteNum = phone
    }
    
    init(dictionary: Dictionary<String,Any>) {
        self.id = dictionary.safeValueForKey("ID").safelyWrappingString()
        self.phone = dictionary.safeValueForKey("PhoneNumber").safelyWrappingString()
        self.name = dictionary.safeValueForKey("Name").safelyWrappingString()
        self.createDate  =  dictionary.safeValueForKey("CreatedDate").safelyWrappingString()
        self.agentID = dictionary.safeValueForKey("AgentID").safelyWrappingString()
        self.type = dictionary.safeValueForKey("Type").safelyWrappingString()
        self.favDeleteNum = dictionary.safeValueForKey("PhoneNumber").safelyWrappingString()
    }
}


extension Optional {
    func safelyWrappingString() -> String {
        switch self {
        case .some(let value):
            return String(describing: value)
        case _:
            return ""
        }
    }
    
    func unwrap<T>(_ any: T) -> Any {
        let mirror = Mirror(reflecting: any)
        guard mirror.displayStyle == .optional, let first = mirror.children.first else {
            return any
        }
        return unwrap(first.value)
    }


    
}
