//
//  Utility.swift
//  BLEScanner
//
//  Created by Tushar Lama on 31/03/2020.
//  Copyright © 2020 GG. All rights reserved.
//


import UIKit

var appFont: String = "Zawgyi-One"


class AppUtility: NSObject{
    class func showToastlocal(message : String, view: UIView) {
        let toastView = UIView(frame: CGRect(x: 20, y: (view.frame.height/2)-40, width: view.frame.width-40, height: 80))
        toastView.layer.cornerRadius = 20
        toastView.layer.masksToBounds = true
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 10, y: 10, width: toastView.frame.size.width-20, height: 60)
        toastLabel.text = message.localized
        //        toastLabel.baselineAdjustment = .alignCenters
        toastLabel.lineBreakMode = .byClipping
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.textColor = .white
        toastView.backgroundColor = UIColor.black
        view.bringSubviewToFront(toastLabel)
        toastView.addSubview(toastLabel)
        view.addSubview(toastView)
        UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseOut, animations: {
            toastView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastView.removeFromSuperview()
        })
    }
}

class Utility: NSObject {
    //Present Alert controller with title, message and events
    static var alert = UIAlertController()
    static func alertContoller(title: String, message: String, actionTitleFirst: String, actionTitleSecond: String, actionTitleThird: String, firstActoin: Selector?,secondActoin: Selector?,thirdActoin: Selector?, controller: UIViewController) {
        alert = UIAlertController(title: title, message: message, preferredStyle: .alert) // 1
        if(!actionTitleFirst.isEmpty) {
            let firstButtonAction = UIAlertAction(title: actionTitleFirst, style: .default) { (alert: UIAlertAction!) -> Void in
                if(firstActoin != nil){
                    controller.perform(firstActoin)
                }
            }
            alert.addAction(firstButtonAction)
        }
        
        if(!actionTitleSecond.isEmpty) {
            let secondAction = UIAlertAction(title: actionTitleSecond, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
                if(secondActoin != nil){
                    controller.perform(secondActoin)
                }
            }
            alert.addAction(secondAction)
        }
        
        if(!actionTitleThird.isEmpty) {
            let thirdAction = UIAlertAction(title: actionTitleThird, style: .default) { (alert: UIAlertAction!) -> Void in
                //            NSLog("You pressed button two")
            }
            alert.addAction(thirdAction)
        }
        controller.present(alert, animated: true, completion:nil)
    }
    
    //Present Alert with title and message
    static func alert(message: String, title: String ,controller: UIViewController) {
        let alertController = UIAlertController(title: title.localized, message: message.localized, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK".localized, style: .default, handler: nil)
        alertController.addAction(OKAction)
        controller.present(alertController, animated: true, completion:nil)
    }
    
    static func isNetworkAvailable(controller: UIViewController) -> Bool{
        if !Reachability.isConnectedToNetwork(){
            Utility.alert(message: "Please check your network connection.", title: "", controller: controller)
            return false
        }else{
            return true
        }
    }
    
    static func setFrameForContactSuggestion(arrayCount: Int, indexpath: IndexPath,onTextField: UITextField,tableView: UITableView) -> CGRect {
        
        var height: CGFloat = CGFloat(Float(arrayCount) * 40.0)
        if height >= 200.0{
            height = 200.0
        }
        return CGRect(x: onTextField.frame.minX, y: tableView.convert(tableView.rectForRow(at: indexpath), to: tableView).minY - height + onTextField.frame.height + 20, width: onTextField.frame.width, height: height)
        
    }
    
    
    class func openContact(multiSelection : Bool,_ delegate: ContactPickerDelegate?) -> UINavigationController  {

        let story = UIStoryboard(name: "QRCode", bundle: nil)
         let contactPickerScene = story.instantiateViewController(withIdentifier: "ContactPickersPicker") as! ContactPickersPicker
        contactPickerScene.contactDelegate = delegate
        contactPickerScene.subtitleCellValue = SubtitleCellValue.phoneNumber
        let navigationController = story.instantiateViewController(withIdentifier: "ContactNavigationController") as! UINavigationController
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.setViewControllers([contactPickerScene], animated: true)
        return navigationController
    }

    static func audioQRNoiseToRSSI(noise:Int) -> String{
          var rssi = ""
          if noise > 0 && noise <= 16 {
              rssi = "-38"
          } else  if noise > 16 && noise <= 60 {
              rssi = "-50"
          } else {
              rssi = "-75"
          }
          return rssi
      }
    
    static func getDateForCoreData() -> String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        // again convert your date to string
        return formatter.string(from: yourDate!)
    }
    
    // for filter blocked user by current date
    static func getDateYYYYMMDD() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        return formatter.string(from: yourDate!)
    }
    
    static func makeDateFormatForSync(date: String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let newDate = formatter.date(from: date)
        formatter.dateFormat = "MM/dd/yyyy"
        if let newVaue = newDate{
          let newDateString = formatter.string(from: newVaue)
            return newDateString
        }
        return ""
    }
    
    static func makeDateFormatForSyncWithAMPM(date: String) -> String{
           let formatter = DateFormatter()
           formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
           let newDate = formatter.date(from: date)
           formatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
           if let newVaue = newDate{
             let newDateString = formatter.string(from: newVaue)
               return newDateString
           }
           return ""
       }
    
    static func makeDateFormatForAddFamily(date: String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let newDate = formatter.date(from: date)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let newVaue = newDate{
          let newDateString = formatter.string(from: newVaue)
            return newDateString
        }
        return ""
    }
    
    
    static func getDateForDataSyn() -> String{
           let formatter = DateFormatter()
           // initially set the format based on your datepicker date / server String
           formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
           let myString = formatter.string(from: Date()) // string purpose I add here
           // convert your string to date
           let yourDate = formatter.date(from: myString)
           //then again set the date format whhich type of output you need
           formatter.dateFormat = "MM/dd/yyyy"
           // again convert your date to string
           return formatter.string(from: yourDate!)
       }
    
    
    static func getTimeDifferenc(timeOne: String,timeTwo: String) ->Double{
        let dateFormatter = DateFormatter()
              dateFormatter.calendar = Calendar(identifier: .gregorian)
              dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
             
        if let newTimeOne = dateFormatter.date(from: timeOne){
            if let newTimeTwo = dateFormatter.date(from: timeTwo){
                return Double(newTimeTwo.timeIntervalSince(newTimeOne)/60)
            }
        }
            
        return 0.0
        
    }
    
    static func isDeviceLocked () -> Bool{
     
        if UIApplication.shared.isIdleTimerDisabled {
            print("Device is locked.")
            return true
        }
           return false
       }
    
    static func isDeviceCharging () -> Bool{
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        if (UIDevice.current.batteryState == .charging) {
            print("Device is charging.")
            return true
        }
        
        if (UIDevice.current.batteryState == .full) {
            print("Device is charging full.")
            return true
        }
        return false
    }
    
    static func isNotificationTimeActive () -> Bool{
        
        var showNotification = false
        if getDateForCoreData().components(separatedBy: " ").count>2{
            //12 hour format
            if getDateForCoreData().components(separatedBy: " ")[2] == "AM"{
                let valueTime = getDateForCoreData().components(separatedBy: " ")[1]
                let newtime = valueTime.components(separatedBy: ":")[0]
                let intTime = Int(newtime)
                if let _ = intTime{
                    
                    if ((intTime! < 6)) || (intTime! ==  12){
                        //dont show  notification here
                        showNotification = false
                    }else{
                        showNotification = true
                       //show notification
                    }
                }
            }else{
                let valueTime = getDateForCoreData().components(separatedBy: " ")[1]
                let newtime = valueTime.components(separatedBy: ":")[0]
                let intTime = Int(newtime)
                
                
                if let _ = intTime{
                    if ((intTime! < 9) || (intTime! == 12)) {
                        // show  notification here
                        //print("@#$@%$#$^%&*&(^&*%&$^#$%@$!@#@@$%#^$&%^*&(*&*%^&$%^#%@$!#~!$!@%#@^$#&%$*^%#^$@%#$!@#~!@$~@!%#@")
                         showNotification = true
                        
                    }else{
                        showNotification = false
                        //dont show notification
                        
                        
                    }
                    
                }
            }
        }else{
            //24 hour format
            let time = getDateForCoreData().components(separatedBy: " ")[1]
            let newtime = time.components(separatedBy: ":")[0]
            let intTime = Int(newtime)
            if let _ = intTime{
                if ((intTime! < 21) && (intTime! >  5)){
                    // show notification
                     showNotification = true
                    //print("@#$@%$#$^%&*&(^&*%&$^#$%@$!@#@@$%#^$&%^*&(*&*%^&$%^#%@$!#~!$!@%#@^$#&%$*^%#^$@%#$!@#~!@$~@!%#@")
                }else{
                    showNotification = false
                }
            }
        }
        
        return showNotification
    }
    
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

//MARK: - Date Extensions
extension Date {
    func stringValue(dateFormatIs: String = "EEE, dd-MMM-yyyy HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as! TimeZone
        dateFormatter.dateFormat = dateFormatIs
        return dateFormatter.string(from: self)
    }
}

//MARK:- Dateformatter Extension
extension DateFormatter {
    func setLocale() {
        let enUSPOSIXLocale:NSLocale = NSLocale(localeIdentifier: "en_US")
        self.locale = enUSPOSIXLocale as Locale
        self.timeZone = TimeZone(identifier: "UTC")
    }
}

//MARK: - String Extensions
extension String {
    var localized: String {
        return appDel.getlocaLizationLanguage(key: self)
    }
    
    func dateValue(dateFormatIs: String = "dd-MMM-yyyy HH:mm:ss") -> Date? {
         let dateFormatter = DateFormatter()
         dateFormatter.calendar = Calendar(identifier: .gregorian)
         dateFormatter.setLocale()
         dateFormatter.dateFormat = dateFormatIs
         return dateFormatter.date(from: self)
     }
}


enum SAlertButtonType {
    case defualt
    case custom
}

class SAlertController: UIViewController {
    
    var alrtType: SAlertButtonType = .defualt
    let bgView = UIView()
    let bgImage = UIImageView()
    let lblTitle = UILabel()
    let lblMessage = UILabel()
    let btnAction = UIButton()
    let stack = UIStackView()
    let fontWithSize =  UIFont(name: appFont, size: 16.0)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        bgView.frame = CGRect(x: 25, y: (UIScreen.main.bounds.height/2) - 150  , width: UIScreen.main.bounds.width - 50, height: 200)
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        bgView.backgroundColor = UIColor.white
        //Image Added-Gauri
        bgImage.frame = CGRect(x: (self.bgView.frame.width / 2 - 50) , y: -25 , width: 100 , height: 100 )
        bgImage.image = UIImage(named: "logo_blue")
        bgImage.contentMode = .scaleToFill
        bgView.addSubview(bgImage)
        
        lblTitle.frame = CGRect(x: 0, y: 0, width: bgView.frame.width, height: 45)
        lblTitle.textAlignment = .center
        lblTitle.font = fontWithSize
        bgView.addSubview(lblTitle)
        
        
         
        let Line = UIButton(frame: CGRect(x: 0, y: 45, width: bgView.frame.width, height: 1))
       
        Line.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
         
        bgView.addSubview(Line)
        
        let bgButon = UIButton()
        bgButon.frame = CGRect(x: 0, y: 150, width: bgView.frame.width, height: 50)
         
        bgButon.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 0, greenValue: 176, blueValue: 255, alpha: 1), UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)])
        bgButon.backgroundColor = UIColor.init(red: 36.0/255.0, green: 105.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        bgButon.isUserInteractionEnabled = false
        bgView.addSubview(bgButon)
        
        lblMessage.frame = CGRect(x: 0, y: 46, width: bgView.frame.width, height: 104)
        lblMessage.textAlignment = .center
        lblMessage.numberOfLines = 5
        lblMessage.lineBreakMode = .byWordWrapping
        lblMessage.font = fontWithSize
        bgView.addSubview(lblMessage)
        
        self.view.addSubview(bgView)
        
        self.view.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.6)
        
//        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
//               
//        self.view.addGestureRecognizer(TapGesture)
    }
    
    @objc func HandleTapGesture(_:UIGestureRecognizer) {
           
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
       }
    
    func addAction(action : [SAlertAction]) {
        
        guard action.count != 0 else {
            return
        }
        let w_width = bgView.frame.width / CGFloat(action.count)
        var x_Axis: CGFloat = 0
        
        for btn in action {
            btn.button.frame = CGRect(x: x_Axis, y: 150, width: w_width, height: 50)
            btn.button.setTitle(btn.button.titleLabel?.text, for: .normal)
            btn.button.titleLabel?.font = fontWithSize
            btn.button.backgroundColor = UIColor.clear
            btn.button.addTarget(self, action: #selector(okAction), for: .touchUpInside)
            bgView.addSubview(btn.button)
            x_Axis += w_width
        }
    }
    
    func ShowSAlert(title: String, withDescription message: String, onController controller: UIViewController) {
        controller.addChild(self)
        self.view.frame =  controller.view.bounds
        controller.view.addSubview(self.view)
        self.didMove(toParent: self)
        self.lblTitle.text = title
        self.lblMessage.text = message
    }
    
    @objc func okAction(sender: UIButton) {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
}


class SAlertAction: NSObject {
    
    var alertType: SAlertButtonType?
    var button = UIButton()
    var bgColor: UIColor?
    var txtColor: UIColor?
    
    func action(name: String, AlertType type: SAlertButtonType, withComplition completion: @escaping ()->Void)  {
        alertType = type
        if type == .defualt {
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor.blue
        }else {
            button.backgroundColor  = bgColor
            button.setTitleColor(txtColor, for: .normal)
        }
        button.setTitle(name, for: .normal)
        button.addTargetClosure(closure: { (button)in
            completion()
        })
        
    }
}



class ReturnAfterDone: NSObject {
    let closure: UIButtonClosure
    init(_ closure: @escaping UIButtonClosure) {
        self.closure = closure
    }
}

typealias UIButtonClosure = (UIButton) -> ()
extension UIButton {
    private struct AssociatedKeys {
        static var targetClosure = "targetClosure"
    }
    private var targetClosure: UIButtonClosure? {
        get {
            guard let closureWrapper = objc_getAssociatedObject(self, &AssociatedKeys.targetClosure) as? ReturnAfterDone else { return nil }
            return closureWrapper.closure
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            objc_setAssociatedObject(self, &AssociatedKeys.targetClosure, ReturnAfterDone(newValue), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    func addTargetClosure(closure: @escaping UIButtonClosure) {
        targetClosure = closure
        addTarget(self, action: #selector(UIButton.closureAction), for: .touchUpInside)
    }
    @objc func closureAction() {
        guard let targetClosure = targetClosure else { return }
        targetClosure(self)
    }
}
extension UINavigationBar {
    /// Applies a background gradient with the given colors
    func applyNavigationGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    func applyNavigationGradient1( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    func applyNavigationGradient2( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer {
            UIGraphicsEndImageContext()
        }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
     
}


@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

extension UIView {
    func setRadiusWithShadow(_ radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
    }
    
    
    func applyViewnGradient( colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height))
        imageView.image = UISearchBar.ViewGradient(size: frameAndStatusBar.size, colors: colors)
        self.addSubview(imageView)
        self.sendSubviewToBack(imageView)
    }
    
    /// Creates a gradient image with the given settings
    static func ViewGradient(size : CGSize, colors : [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        // From now on, the context gets ended if any return happens
        defer {
            UIGraphicsEndImageContext()
        }
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var VshadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = VshadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}
