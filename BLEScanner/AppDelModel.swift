//
//  AppDelModel.swift
//  BLEScanner
//
//  Created by vamsi on 4/22/20.
//  Copyright © 2020 GG. All rights reserved.
//

import Foundation

class AppDelModel: NSObject {
    let apiReqObj = ApiRequestClass()
    var delegate: ResponseDelegate? = nil
    
    func APNSAPI(CountryCode:String){
    
        apiReqObj.customDelegate = self
        apiReqObj.sendHttpRequest(requestUrl: URL(string:Constant.UpdateDeviceDetailsApi())!, requestData:AppDelFinalParam.getAppDelParam(CountryCode:CountryCode), httpMethodName: NetWorkCode.serviceTypePost)
    }
}
extension AppDelModel :ApiRequestProtocol {
    
    func httpResponse(responseObj:Any,hasError: Bool){
        //success Block
        if !hasError {
            let dataResponse = responseObj as! NSDictionary
            if dataResponse.count>0{
                let modelObj = APPDelResponseModel(dataDic: dataResponse)
                delegate?.getDataFromResponse(message: modelObj.Code ?? "", statusCode: "", title: modelObj.Message ?? "", OTP:"", mobileNumber: "")
            }
        }else{
            delegate?.getDataFromResponse(message: "Error ", statusCode: "", title:"", OTP: "", mobileNumber: "")
        }
        
    }
    
}
open class APPDelResponseModel {
    
    var Code: String?
    var Message: String?
    var statusCode: String?
    var mobilenumber:String?
    
    public init(dataDic: NSDictionary){
        if dataDic.count>0{
            self.Code = String(dataDic.value(forKey: "Code") as! Int)
            self.Message = dataDic["Message"] as? String
        }
    }
}
